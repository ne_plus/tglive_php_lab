<?php require_once("module/header.php"); ?>

	<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_footer_faq_returns?>
		</ol>
		<div class="container">
		<div class="row">
		<h3><?=$lang_footer_faq_returns?></h3>

	<p >『TGILive居生活』提供【七日鑑賞制度】與【單一退貨制度】簡化流程，好讓您更快取得需要的現貨商品。</p>

<p>在收到商品後的七天鑑賞期間內【包含假日】，可以直接線上申請部份退貨或整筆退貨，此退貨運費將由『TGILive居生活』負擔。</p>

 <li> 鑑賞期並非試用期，若超過七天鑑賞期間，恕無法受理退貨。 </li>
 <li> 在任何因素下若同筆訂單要申請第二次以上退貨，則需由顧客自行支付運費。</li>
 <li> 如欲退貨，所有退品商品、附件和贈品必須為全新完整，外包裝和產品開封處分未拆封開啟狀態，如有物件短缺或已拆封之使用狀態，恕無法辦理退貨程序。</li>
 <li> 若因人為使用不當而造成商品毀損者，恕不接受退換。請在收到物品時先行檢查，並於二日內來信告知，逾期無法辦理退貨，不便之處，敬請見諒。</li>
 <li> 非商品瑕疵與寄送錯誤之退貨由買方自行負擔來回郵資，退款必須再扣除轉帳手續費。 </li>

<p class="highlight">※客服時間：週一至週五（國定上班日） 10:00-18:00︱慎防詐騙！客服專員非必要，不會主動使用電話與您聯繫！</p>

<div class="subtitle">【退貨Q&A】</div>
<div class="subtitle">Q1：如果尺寸不合怎麼辦？</div>
<p>A：有任何更換尺寸或是更換款式需求，請參照退貨程序辦理即可。</p>

<div class="subtitle">Q2：選購商品可否加訂（更改或取消）商品？</div>
<p>A：為確保出貨流程順暢無誤，付款完成後的訂單將無法取消或更改，下訂前請務必留意。有任何更換或取消商品，請參照退貨程序辦理即可。</p>

<div class="subtitle">Q3：如何辦理退貨？</div>
<p>A：麻煩於7天鑑賞期內與客服聯繫，我們將會提供退貨明細單給您，請您依填妥退貨明細單後回傳，並保留商品的完整包裝，在確認您的退貨後，『TGI居生活』將派專員於五個工作天內到府收件，再以mail回覆後續處理進度。</p>

<div class="subtitle">【退貨申請說明】</div>                      
<p> <li> 退貨商品須退回完整商品包含配件、贈品、保證書、原廠包裝及所有附隨文件或資料的完整性，如有損壞恕不退費（瑕疵品除外）。 </li>
 <li> 『TGI居生活』收到退貨商品後會先行拍照和您再次確認商品的完整性，若商品有缺件部分，恕不退費。</li>
 <li> 退貨申請完成後，『TGI居生活』將與您聯繫退貨取件事宜。取件完成後，請保留宅配取貨單據，以方便日後查詢。</li>
 <li> 自申請退貨翌日起算7個工作天，若因無法聯繫、商品未寄回因而無法完成退貨作業，將自動取消退貨申請，且不可再次申請，請您見諒。</li>
 <li> 若購買商品以折價卷折抵消費金額，經折抵付費成功，退貨時需退還折價卷。</li></p>

<div class="subtitle">【提醒您】</div>
<p> <li> 商品外盒請勿直接黏貼物流單寄送，請另加外包裝袋，避免破壞商品外盒或原包裝，影響您退貨的權益。</li>
 <li> 請於退貨商品外包裝附上：姓名、電話、訂單編號、地址，以利退貨作業查詢。</li></p>

<div class="subtitle">Q4：收到瑕疵錯誤商品怎麼辦？</div>
<p>A：如遇「瑕疵/配送錯誤」之商品，請務必保持商品全新及外箱保持完整，並於7天鑑賞期內與客服聯繫，請先提供商品照片，我們將會提供退貨明細單給您，再請您依填妥退貨明細單後回傳，並保留商品的完整包裝，『TGI居生活』將派專員於三個工作天內到府收件，再以mail回覆後續處理進度。若經客服人員受理並判定非人為瑕疵，我們會將原商品寄回並由消費者自行負擔此運費。</p>

<div class="subtitle">Q5：什麼樣的狀況下無法退貨呢？</div>
<p>A：依「依消費者保護法第19條及行政院消費者保護處公告「通訊交易解除權合理例外情事適用準則」，已拆封之個人衛生用品，因有衛生安全上之考量，故無法授理退貨。</p>

 <p><li> 超過七天鑑賞期間，恕無法受理退貨</li>
 <li> 當您選擇7-11取貨，若無法於指定時間至超商取件，商品將會自動收回，無法申請退貨且該費用無法辦理退費，如需再寄送該件商品，需由消費者自行負擔運費。</li>
 <li> 運送過程外盒偶有碰撞擠壓，但並不損及內盒產品完整性，若只是外盒包裝的凹痕恕不提供退貨！ 如嚴格要求外盒完整者，購買前請三思。</li>
 <li> 美妝、彩妝品為私人消粍性產品，不可拆封或試用，一經拆封或使用，將會影響退貨權限。</li>
 <li> 因產品成份、產品內容物，以致使用後感到肌膚不適之情形。</li>
 <li> 因商品不當保存以致商品變質。</li>
 <li> 因使用不同的品牌螢幕以及解析度不同，造成圖片顏色呈現略有不同，請以實品顏色為準。</li>
 <li> 商品與盒裝外觀不得有刮傷、破損、受潮、塗寫文字。</li>
 <li> 商品如經拆封、使用、或拆解以致缺乏完整性及失去再販售價值時，將會影響退貨權限。</li>
 <li> 試穿時，衣物沾有粉妝或其他污漬等人為瑕疵</li>
 <li> 非正常試穿，己有使用過之髒污或味道（如香水、菸味…等）</li>
 <li> 貼身內著類衣物之安心縫線／釘線己拆除（如內褲、襪子…等）</li>
 <li> 商品己下水、配件不全或吊牌商標己剪（特殊包裝商品請務必退回所有包裝配件）</li>
 <li> 產品若因穿戴而對商品各部位所產生的外部損壞及刮傷，將會影響退貨權限。</li>
 <li> 屬個人衛生/清潔用品/消耗性產品，一經拆封使用，除產品本身瑕疵，將會影響退貨權限。</li>
 <li> 網頁如標示【無提供退貨服務】之特賣、福袋、貼身內著衣物（基於衛生原則）。</li></p>

<p>以上恕無法受理退換貨，若為商品本身或運送過程中所造成的污損瑕疵，則不在此限內。</p>

				
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>