<?php require_once("module/header.php"); 

if( !isset( $_REQUEST["pid"] ) ){
	header("Location: styles.php"); 
}

include("Frontframwork/ArticleDB.php");
$pid = isset( $_REQUEST["pid"] )? $_REQUEST["pid"] : null ; //文章ID
$page =  null ; //頁面
$tagId =  null ; //標籤id
$status = "article"; //文章ID搜尋文章 or 標籤搜尋所有文章
$recPerPage = null; //每一分頁要幾篇內容
$article = new Article( $status , $pid , $tagId , $page , $recPerPage ); 
$articles = $article -> articleQuery(); 
$page = $article-> pagenation();
$tagAll = $article->tagArticleRelateQueryAll() ;
$latest = $article->getArticleLatest(5); //可以填寫需要抓取的數量

if( isset( $_REQUEST["tag_no"] ) ){
	$tagInfo = $article->getTagInfo( $_REQUEST["tag_no"] ) ;
}


?>


	<section class="styleshow marginTop100">
					<ol class="breadcrumb">
						  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
						  <li class="breadcrumb-item">
						  	<a href="products.php"><?=$lang_menu_style?></a>
						  	<form action="styles.php" method="get"></form>
						  </li>
						  <li class="breadcrumb-item active"><?php echo $articles[0]["article_title"] ?></li>
					</ol>
			
		<div class="container mt-3">
			<div class="row">		
				<div class="col-12 col-lg-8">
					<div class="page">
						<?php  
							foreach ($articles as $key => $value) {
						?>
						<div class="style-box row">					
							<div class="title-detail text-center col-12">
								<?php echo $value["article_title"]; ?>
							</div>
							
							<!-- ====fb 分享==== -->
							<div class="fb-share-button text-right col-12 mb-1" data-href="<?php echo $url; ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a id="fbShare" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"></a></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.12&appId=432812180472781&autoLogAppEvents=1';
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));
							</script>
							<!-- ====fb 分享==== -->

							<div class="style-content col-12">
								<div class="subtitle subtitle-detail"><span><?php echo date("Y-m-d",$value["article_createtime"]); ?></span><span class="mx-1">|</span><span>words by <?php echo $value["article_owner"]; ?></span></div>
								<div class="d-flex justify-content-center">
									<?php  
										foreach ($value["tag_article_relate"] as $tagkey => $tagvalue) {
									?>
									<span class="article-tag mx-1"><a href="styles.php?tag_no=<?php echo $tagvalue['tag_no']; ?>">#<?php echo $tagvalue['tag_name']; ?></a></span>	
									<?php  
										} //end tag for loop
									?>
								</div>
								<div class="article my-2 p-2"><?php echo $value["article_content"]; ?></div>
							</div>
						</div>
						<?php } //end articles for loop?>
					</div>
					                
				</div>

				<div class="col-12 col-lg-4">
					<div class="article-history">
						<div class="history-title latest">
							<?=$lang_style_latest?>
						</div>
						<div class="history-pass row">
							<?php  
								foreach ($latest as $key => $value) {
								
							?>
							<div class="history-img col-4 col-lg-5 mb-3">
								<a href="styles-detail.php?pid=<?php echo  $value['article_no']; ?>">
									<div class="history-img-box">
										<img src="<?php echo $value["article_img"]; ?>" >
										<div class="style-hover-box">
											<div class="style-hover-box-inner">
												<div>Read more</div>
												<div class="lineUp"></div>
												<div class="lineRight"></div>
												<div class="lineLeft"></div>
												<div class="lineDown"></div>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="history-content col-8 col-lg-7">
								<div class="content-box">
									<a href="styles-detail.php?pid=<?php echo  $value['article_no']; ?>"><?php echo $value["article_title"]; ?></a>
									<div class="history-content-time"><?php echo date("Y-m-d",$value["article_createtime"]); ?>
										
									</div>
								</div>
							</div>
							<?php } ?>
						</div>

						<div class="history-title index">
							<?=$lang_style_index?>
						</div>	
						<div class="history-index row">
						<?php  
							foreach ($tagAll as $key => $value) {
						?>
							<div class="col-12"><a href="styles.php?tag_no=<?php echo $value['tag_no']; ?>"><span><?php echo $value["tag_name"]; ?></span><span>(<?php echo $value["count(*)"]; ?>)</span></a></div>
						<?php } ?>	
						</div>	

						<!-- <div class="history-title top">
							top hit
						</div>
						<div class="history-top row">
							<div class="top-content col-12">
								<div class="content-box"><a href="">三種戶外冒險必備的錦囊妙計</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">薑母鴨的冬天 VS 羊肉爐的四季</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">gogoro & GOGOGO Fire in the Hole</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">三種戶外冒險必備的錦囊妙計</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">薑母鴨的冬天 VS 羊肉爐的四季</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">gogoro & GOGOGO Fire in the Hole</a></div>
							</div>
						</div> -->
							
					</div>
				</div>
                

			</div>
		</div>
	</section>
	<script type="text/javascript">
		//影片 尺寸 RWD調整
		if( $(window).width() < 768 ){
			$(".article").find("iframe").attr("height","200");
			$(".article").find("iframe").attr("width","100%");
		}
		
	</script>
	
  
<?php require_once("module/footer.php"); ?>