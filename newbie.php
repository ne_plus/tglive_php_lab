<?php require_once("module/header.php"); ?>

	<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_footer_newbie?>
			
		</ol>
		<div class="container">
			
			
			<div class="row">
			<h3><?=$lang_footer_newbie?></h3>
				<p>親愛的顧客您好，感謝您支持「TGILive居生活」！購買前請先詳細閱讀以下資訊，確保您的權益。一旦您下單完畢，即表示已同意以下購物須知的所有內容，並且無法修改訂單內容，若是造成您的不便，懇請見諒！</p>
				<div class="subtitle">選購商品</div>
				
				<p>您可以在未登入狀態下透過網頁引導或是搜尋的方式，找到您需要的商品之後，按下 『加入購物車』的按鈕進行購買，亦或是按下『收藏』的按鈕，以利下次進行購買。</p>

				<div class="subtitle">首次購物即註冊成為會員</div>
<p>您可在尚未登入狀態下進行商品選購的動作，點選『填寫訂購資訊』即可進行訂購資料及加入會員資料填寫，完成此步驟您即成為TGILive居生活的會員。
上方步驟完畢後，按下『確認訂購』的按鈕，隨即進入第三方交易平台進行付款動作，完成付款即會顯示「購物成功」，之後我們將會為您發送訂單通知信件，您亦可至會員中心查詢訂單與購物紀錄。</p>

		<div class="subtitle">運費計算方式</div>
		<p>
		<li> 單筆結帳訂單金額低於NT$1,000元，無論地區一律酌收運費及處理費用NT$60。</li>
		<li>台灣本島單筆結帳訂單若高於NT$1,000，免收運費。</li>
		<li>金馬澎湖外島地區，無論金額一律加收運費NT$150。</li></p>


		<div class="subtitle">優惠碼使用</div>
<p>於『Step.1確認購物清單頁面』下方『折扣碼』欄位輸入優惠碼後即顯示優惠碼之折抵金額，若因其他因素導致優惠碼無使用，將會出現該優惠碼無法使用之提示，請您確認優惠碼使用效期，若有疑問歡迎來信洽詢客服人員。

<li> 每組優惠碼僅限用一次，且不得與其他優惠活動或優惠碼並用。</li>
 <li>單筆訂單最低抵用金額依TGILive活動公告為主。</li>
 <li>優惠券須於活動規定之使用期限內使用完畢。</li>
 <li>如遇不可抗力或其他因素，以致TGILive無法提供原折抵優惠，TGILive保有修改優惠的權利。</li></p>


<div class="subtitle">選擇付款方式</div>
<p>目前付款方式有：信用卡一次付清、信用卡3、6、12期0利率、現金匯款、超商付款</p>
<ul>
	<li>信用卡一次付清、分期
<ul>	<li>本站接受 VISA、Master、JCB、銀聯卡，輸入資料後等待銀行授權，填入信用卡背面簽名欄旁末三碼數字，以利訂單處理快速又安全。採用SSL 128 bits 最安全加密機制。</li>
<li>全館刷卡 3 期 0 利率；結帳金額滿 3,000 元以上享 6 期 0 利率；結帳金額滿 10,000 元以上享 12 期 0 利率。</li>
<li>合作銀行：中國信託、台新、玉山、富邦、遠東、永豐、國泰世華、華南、日盛、樂天、安泰、聯邦、兆豐、台中商銀、上海銀行、凱基、匯豐、星展、新光、合庫、彰銀、一銀、元大、陽信、台灣企銀、華泰、三信商銀</li>
<li>本站接受 VISA、Master、JCB、銀聯卡，輸入資料後等待銀行授權，填入信用卡背面簽名欄旁末三碼數字，以利訂單處理快速又安全。 採用SSL 128 bits 最安全加密機制。<span class="highlight">※請款日由依各家信用卡公司而定。</span></li></ul>

	</li>
	
	<li>超商付款<ul>
	<li>可選擇711、全家、OK、萊爾富付款，超商代碼繳費期限為7天，請務必於期限內進行付款，若您無法取得或遺失超商繳費代碼，請與居生活客服人員聯繫，或重新下單。</li>
<li>作流程可參閱<a href="https://www.ecpay.com.tw/service/pay_way_cvpay" target="_blank">ECPAY</a></li>
</ul></li>
<li>現金匯款<ul>
	 <li>訂單成立後會產生專屬的付款虛擬帳號，請於三日內匯款。若您無法取得或遺失虛擬帳號，請與居生活客服人員聯繫，或重新下單。</li>
				<li>操作流程可參閱<a href="https://www.ecpay.com.tw/Service/pay_qa_atm_acntr" target="_blank">ECPAY</a></li>
</ul></li>
	</ul>

<div class="subtitle">商品配送</div>
 <li>請務必確認收件人姓名、手機及地址欄位資料填寫正確，以確保您的收件權益。</li>
 <li>一筆訂單只接受一種配送方式，如果您需要將商品配送至兩個以上的地址、或希望以不同方式配送請分批訂購。</li>
 <li>宅配人員在配送前可能會與您聯絡，敬請保持手機暢通並留意來電。</li>
 <li>更多詳情請參照「運送問題」。</li>


<div class="subtitle">完成購物</div>
<p>網頁出現「訂購成功」，系統將以Email寄發訂單資訊，您也可以至「會員中心」>訂單查詢>檢視訂單記錄。</p>

				
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>