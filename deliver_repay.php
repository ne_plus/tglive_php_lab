<?php require_once("module/header.php"); 
	
	// -----購物車資訊----
	$orderTempList = [];
	if(isset($_SESSION["checkSessionKey-repay"])){ //有存在購物清單
		//刪除checkSessionKey
		unset($_SESSION["checkSessionKey-repay"]);
	}else{ //沒有購物清單
		header("location:memhistory.php");
	}
	//---訂單資訊----
	include("Frontframwork/memHistoryDataServer.php");	
	
?>

	<section class="purchase">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_cart_paymentinfo?>
			
		</ol>
		<div class="container">
			<div class="row cartMenu">
				<div class="col-6 step active">
					<li><?=$lang_cart_order_info?></li>
				</div>
				<div class="col-6 step">
					<li><?=$lang_cart_payment_done?></li>
				</div>
				
			</div><!-- .cartMenu.row -->

			<!-- ====商品訂購明細===== -->
			<?php foreach ($order as $key => $value) { ?>
			<h3><?=$lang_cart_order_info?></h3>
			<div id="cart" class="row">
				<div class="col-12 table-responsive">
					<table class="table table-striped">
						<thead id="thead-rwd-hide">
							<tr>
							  <th scope="col"><?=$lang_cart_product?></th>
							  <th scope="col"><?=$lang_cart_spec2?></th>
							  <th scope="col"><?=$lang_cart_brand?></th>
							  <th scope="col"><?=$lang_cart_qty?></th>
							  <th scope="col"><?=$lang_cart_sum?></th>						  
							</tr>
						</thead>
						<tbody>
						<?php
								foreach ($value["detail"] as $orderDeatilkey => $orderDeatilvalue) {						
									$prcie = (int)$orderDeatilvalue["order_detail_price"]*(int)$orderDeatilvalue["order_detail_quantity"]-(int)$orderDeatilvalue["order_detail_price_discount"]*(int)$orderDeatilvalue["order_detail_quantity"];

									$sqlProduct = "select product_name from product where product_no = ".$orderDeatilvalue['product_no'];
                                        $productResult = $db->DB_Query($sqlProduct);	
						?>	 
						<tr>
					      <td style="vertical-align: middle;">
					      	<?php echo $productResult[0]["product_name"]; ?>
					      	<!-- ===rwd==== -->
					      	<div class="order-rwd my-2"><span><?=$lang_cart_spec?> : </span><?php echo $orderDeatilvalue["product_spec"]; ?></div>
					      	<div class="order-rwd my-2"><span><?=$lang_cart_qty?> : </span><?php echo $orderDeatilvalue["order_detail_quantity"]; ?></div>
					      	<div class="order-rwd"><span><?=$lang_cart_sum?> : </span><?=$lang_product_currency?> <span ><?php echo $prcie; ?></div>
					      </td>
					      <td class="orderSpec"  style="vertical-align: middle;"><?php echo $orderDeatilvalue["product_spec"]; ?></td>
					      <td class="orderBrand" style="vertical-align: middle;"><?php echo $orderDeatilvalue["cate_name"]; ?></td>
					      <td class="orderQty Qty-rwd" style="vertical-align: middle;"><?php echo $orderDeatilvalue["order_detail_quantity"]; ?></td>					   
					      <td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span class="charge"><?php echo $prcie; ?></span></td>
					    </tr>  

						<?php 
							}//  end foreach $value["detail"]
						// =====全館免運======
						if($value["order_cargo"] != 0){ 
							$cargoPrice = $value["order_cargo"];
						}else{ //有運費
							$cargoPrice = "免運";
						}
					  ?>
					  	<tr id="cargoTR">
					  		<td colspan="4"></td>
					  		<td><span><?=$lang_cart_transfee?> : <?=$lang_product_currency?> </span><span id="cargoPrice"><?php echo $cargoPrice; ?></span></td>
					  	</tr>
					  	<tr id="totalPriceTR">
					  		<td colspan="4"></td>
					  		<td ><span><?=$lang_cart_total?> : <?=$lang_product_currency?> </span><span id="TotalPrice"><?php echo (int)$value["order_price"]+(int)$value["order_cargo"]; ?></span></td>
					  	</tr>
						</tbody>
					</table>  					
				</div>
			</div>

			<!-- ====end 商品訂購明細===== -->
	
			<h3><?=$lang_cart_order_info2?></h3>
			<div id="info" class="row" >	

				<div class="col-lg-7 col">
						<from>
						 <div class="form-group row">
						    <label for="memberEmail" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_cart_account?></label>
						    <div class="col-sm-9">
						      <input type="email" class="form-control form-control-sm" id="memberEmail" placeholder="<?=$lang_member_account_hint?>" value="<?php echo $value["order_email"]; ?>" disabled>
						    </div>
						 </div>
						  <div class="form-group row">
						    <label  class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_receiver_fullname?></label>
						    <div class="col-sm-9">
						      <input type="test" class="form-control form-control-sm" id="memberName" placeholder="<?=$lang_member_receiver_fullname?>" value="<?php echo $value["order_recipient"]; ?>" disabled>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="memberPhone" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_phone2?></label>
						    <div class="col-sm-9">
						      <input type="tel" class="form-control form-control-sm" id="memberPhone" placeholder="<?=$lang_member_phone2?>" value="<?php echo $value["order_tel"]; ?>" disabled>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="memberAddress" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_address2?></label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control form-control-sm" id="memberAddress" placeholder="<?=$lang_member_address2?>" value="<?php echo $value["order_address"]; ?>" disabled>
						    </div>
						  </div>
						  <div class="form-group row">
							  <div class="col-12">
							  <label for="purchaseMemo" class="col-form-label col-form-label-sm"><?=$lang_cart_memo?></label>
							  <textarea class="form-control form-control-sm" id="purchaseMemo" rows="3" disabled><?php echo $value["order_memo"]; ?></textarea>
							  </div>
							   
						  </div>
					 	 </from>
				
					
				</div><!-- .col-sm-7 -->
				
				
				<!-- <a class="btn btn-outline-secondary btn-sm submitBtn" href="payment.php" role="button">填寫付款資訊</a> -->
			</div>	<!-- .info-->

				
			<h4><?=$lang_cart_paymentinfo2?></h4>
			
			<div id="payment" class="row">
				<div class="col-12">
					<?=$lang_cart_choose_payby?>：
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway1" value="0" checked> <?=$lang_cart_choose_payby_cc?>
					  </label>
					</div>
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway2" value="2"> <?=$lang_cart_choose_payby_store?>
					  </label>
					</div>
				</div><!-- .col-12 -->
			</div>	<!-- .row -->
			<div class="row">
				<div class="col-12 ">
					<?=$lang_cart_choose_invoice?>：

					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="invoiceSelect" id="person_invoice" value="0" checked> <?=$lang_cart_invoice_einvoice?>
					  </label>
					</div>
					<div class="form-check form-check-inline"  style="display: none;">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="invoiceSelect" id="company_invoice" value="1"> <?=$lang_cart_invoice_cominvoice?>
					  </label>
					</div>
					
					<div class="col-lg-7 col invoiceInfo">
						<div class="form-group row companyInvoice" style="display: none;">
							<label for="orderUniNo" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_cominvoice_no?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderUniNo" placeholder="" value="">
						    </div>
						</div>
						<div class="form-group row companyInvoice" style="display: none;">
							<label for="orderInvoiceTitle" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_cominvoice_title?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderInvoiceTitle" placeholder="" value="">
						    </div>
						</div>
						<div class="form-group row invoice">
							<label for="orderInvoiceAddress" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_address?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderInvoiceAddress" placeholder="" value="<?php if($value["order_invoice_address"] != "同商品配送地址" ){ echo $value["order_invoice_address"];} ?>" <?php if($value["order_invoice_address"] == "同商品配送地址" ){echo 'disabled';} ?>>
						    </div>
						    <div class="col-12" style="padding-left: 35px;padding-top: 5px;">
						    	<input class="form-check-input" type="checkbox" id="invoiceCheck" <?php if($value["order_invoice_address"] == "同商品配送地址" ){echo 'checked';} ?>>
					       		<label class="form-check-label" for="invoiceCheck"><?=$lang_cart_invoice_address_same?></label>
						    </div>	
						</div>
					</div>


				</div><!-- .col-12 -->
				<div class="form-check col-12">
			        <input class="form-check-input" type="checkbox" id="termCheck">
			        <label class="form-check-label" for="termCheck"><?=$lang_cart_agree?><a target="_blank" href="terms.php"><?=$lang_footer_terms_2?></a><?=$lang_and?><a target="_blank" href="privacy.php"><?=$lang_footer_privacy?></a>
			        </label>
			    </div>
				<a id="RecheckoutBtn" class="btn btn-outline-secondary btn-sm submitBtn" href="deliver.php" role="button"><?=$lang_cart_gopay?></a>
				<form id="checkoutForm" action="payment-test.php" method="post" enctype="application/x-www-form-urlencoded">
					<input type="hidden" name="total_price" value="<?php echo (int)$value["order_price"]+(int)$value["order_cargo"]; ?>">
					<input type="hidden" name="order_no" value="<?php echo $_REQUEST["orderSearch"]; ?>">
					<input type="hidden" name="order_member_name" value="<?php echo $value["order_recipient"]; ?>">
					<input type="hidden" name="order_member_tel" value="<?php echo $value["order_tel"]; ?>">
					<input type="hidden" name="order_member_email" value="<?php echo $value["order_email"]; ?>">
				</form>
			
			</div><!-- .row-->
			
			</div><!-- 	.infoBox -->
			<?php } ?>
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>