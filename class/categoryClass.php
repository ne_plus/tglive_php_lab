<?php  
// include("../model/DB.php");
// // include("productClass.php");
// include("articleClass.php");
date_default_timezone_set("Asia/Taipei");

class category{
	public $cateNo ;
	public $cateName ;
	public $cateParent ;
	public $cateType = []; // 50 : article / 61 : tag / 62 : product / 63 :activity
	public $cateLevel ;
	public $cateLogoImg ;
	public $cateBannerImg;
	public $cateDescribe;
	public $cate360Status;
	public $cate360Main;
	public $cate360link;
	public $cateUpdateTime ;
	public $products = []; //產品內容
	public $articles = []; //文章內容

	//分頁
	public $pageSql ;

	public $record_count ;
	public $total_page ;
	public $page_no ;
	//分頁 

	public $categoryExistNone ;
	
	public function __construct($cid=null){
		if( $cid ){
			$db = new DB();
			$sql = "SELECT * FROM category where cate_no = :cate_no";
			$dic = array(":cate_no" => $cid );
			$result = $db -> DB_query($sql,$dic);
			if( count($result) != 0 ){
				foreach ($result as $key => $value) {
					$this->cateNo = $value["cate_no"];
					$this->cateName = $value["cate_name"];
					$this->cateParent = $value["cate_parents"];
					$this->cateLevel = $value["cate_level"];
					$this->cateLogoImg = $value["cate_logo_img"];
					$this->cateBannerImg = $value["cate_banner_img"];
					$this->cateDescribe = $value["cate_describe"];
					$this->cate360Status = $value["cate_360_status"];
					$this->cate360Main = $value["cate_360_main"];
					$this->cate360link = $value["cate_360_link"];
					if( $value["cate_level"] == 0 ){ //主要大分類的ID
						$this->cateType["cate_no"] = $value["cate_no"];
						$this->cateType["cate_name"] = $value["cate_name"];
					}else{ //搜尋此分類的大分類ID
						$this->cateType( $value["cate_parents"] );
					}
				}
			}else{
				$this->categoryExistNone = "1"; // 找不到分類
			}	
		}else{
			$this->categoryExistNone = "1"; // 找不到分類
			return false;//"沒有cid";
		}
	}

	public function getCateInfoByCateID(){
		if( !$this->categoryExistNone ){ 
			return array(
				"cate_no" => $this->cateNo,
				"cate_name" => $this->cateName,
			 	"cate_parents" => $this->cateParent,
			    "cate_level" => $this->cateLevel,
			    "cate_logo_img" => $this->cateLogoImg,
				"cate_banner_img" => $this->cateBannerImg,
				"cate_describe" => $this->cateDescribe,
				"cate_360_status" => $this->cate360Status,
				"cate_360_main" => $this->cate360Main,
				"cate_360_link" => $this->cate360link,
				"cate_type"=> $this->cateType,
			);
		}else{
			return false;//"找不到分類"
		}
		
	}

	public function cateType( $parentCid ){
		if( !$this->categoryExistNone ){ 
			$result = $this->cateFatherQuery( $parentCid );
			if( $result[0]["cate_parents"] == 0 && $result[0]["cate_level"] == 0 ){ //大分類層
				$this->cateType["cate_no"] =  $result[0]["cate_no"];
				$this->cateType["cate_name"] =  $result[0]["cate_name"];
			}else{ //父層不是大分類層
				$result2 = $this->cateFatherQuery( $result[0]["cate_parents"] );
				$this->cateType["cate_no"] =  $result2[0]["cate_no"];
				$this->cateType["cate_name"] =  $result2[0]["cate_name"];
			}
		}else{
			return false;//沒有分類項目
		}
		
	}


	public function cateFatherQuery($cid){ //找父層
		if(!$this->categoryExistNone){ // 有分類項目
			$db = new DB();
			$sql = "SELECT * from category where cate_no ='".$cid."'";
			$result = $db->DB_Query($sql) ;
			if(!count($result)){
				return false ;//echo "沒有父層"; 
			}else{
				return $result;
			}
		}else{//無分類項目
			return false ;//echo "無分類項目"
		}		
	} 

	public function getChildLevelbyCateID($parentCid , $without=null){
		if( !$this->categoryExistNone ){
			$withoutStr = "";
			if( isset($without) ){
				foreach ($without as $key => $value) {
					$withoutStr .= " and cate_no != '" .$value."' ";
				}
			}
			$db = new DB();
			$sql = "SELECT * from category where cate_parents = '".$parentCid."' ". $withoutStr;
			$result = $db->DB_Query($sql) ;
			if(!count($result)){
				return false ;//echo "沒有子層"; 
			}else{
				return $result;
			}
		}else{

		}
	}

	public function getProductsByCateId( $cid=null ,$without=null, $productStatus=null , $page=null , $isPagenation=null , $recPerPage=null ){
		if( !$this->categoryExistNone ){
			$product_status = isset($productStatus)? " and b.product_status = ".$productStatus : null;
			$db = new DB();
			$cateNo = $cid? $cid : $this->cateType["cate_no"] ;
			switch ( $cateNo ) {
				case '50': //article
					
					break;
				case '61': //tag
					
					break;	
				case '62': //product
					
					break;	
				case '63': //activity
					$child = $this->getChildLevelbyCateID($cateNo,$without); 
					foreach ($child as $key => $value) {
						$this->products[$key]["cate_no"] = $value["cate_no"];
						$this->products[$key]["cate_name"] = $value["cate_name"];
						$this->products[$key]["cate_logo_img"] = $value["cate_logo_img"];
						$this->products[$key]["cate_banner_img"] = $value["cate_banner_img"];
						$this->products[$key]["cate_describe"] = $value["cate_describe"];
						$sql ="SELECT a.product_no  from category_products_relate a  join product b on a.product_no = b.product_no  where a.cate_no = ".$value["cate_no"]." ".$product_status." order by b.product_no desc" ;
						$result = $db -> DB_query($sql);
						foreach ($result as $pkey => $pvalue) {
							$product = new productObj($pvalue["product_no"]);
							$this->products[$key]["products"][$pkey] = $product->brief();
						}
						
					}				

					return $this->products;

					break;
				default:
					if( isset($isPagenation) ){
						//pagenation
						$sqlCount ="SELECT count(*)  from category_products_relate a  join product b on a.product_no = b.product_no  where a.cate_no = ".$cateNo." ".$product_status." order by b.product_no desc" ;
						$resultCount = $db -> DB_query($sqlCount);
						$this->record_count = $resultCount[0]["count(*)"];

						$pageResult = $this->pagenation($page,$recPerPage);
						$this->products[0]["page"] = $pageResult;
					}

					$this->products[0]["cate_no"] = $this->cateNo;
					$this->products[0]["cate_name"] = $this->cateName;
					$this->products[0]["cate_logo_img"] = $this->cateLogoImg;
					$this->products[0]["cate_banner_img"] = $this->cateBannerImg;
					$this->products[0]["cate_describe"] = $this->cateDescribe;
					$sql ="SELECT a.product_no  from category_products_relate a  join product b on a.product_no = b.product_no  where a.cate_no = ".$cateNo." ".$product_status." order by b.product_no desc ".$this->pageSql ;
					$result = $db -> DB_query($sql);
					foreach ($result as $pkey => $pvalue) {
						$product = new productObj($pvalue["product_no"]);
						$this->products[0]["products"][$pkey] = $product->brief();
					}
					return $this->products;
					break;
			}
			
		}else{
			return false;//'沒有分類';	
		}
		
	}

	// 360 產品	
	public function get360ProductsByCateId($cid=null ,$without=null, $productStatus=null , $product360Status=null , $page=null , $isPagenation=null , $recPerPage=null ) {
		if( $this->cate360Status != 0){
			$product_status = isset($productStatus)? " and b.product_status = ".$productStatus : null;
			$product_360_status = isset($product360Status)? " and b.product_360_status = ".$product360Status : null;
			$db = new DB();
			$cateNo = $cid;
			$this->products[0]["cate_no"] = $this->cateNo;
			$this->products[0]["cate_name"] = $this->cateName;
			$this->products[0]["cate_logo_img"] = $this->cateLogoImg;
			$this->products[0]["cate_banner_img"] = $this->cateBannerImg;
			$this->products[0]["cate_describe"] = $this->cateDescribe;
			$this->products[0]["cate_360_status"] = $this->cate360Status;
			$this->products[0]["cate_360_main"] = $this->cate360Main;
			$this->products[0]["cate_360_link"] = $this->cate360link;
			$sql ="SELECT a.product_no  from category_products_relate a  join product b on a.product_no = b.product_no  where a.cate_no = ".$cateNo." ".$product_status." ".$product_360_status." order by b.product_no desc " ;
			$result = $db -> DB_query($sql);
			foreach ($result as $pkey => $pvalue) {
				$product = new productObj($pvalue["product_no"]);
				$this->products[0]["products"][$pkey] = $product->brief();
			}
			return $this->products;
			// return $sql;
		}else {
			return false;//'此ID沒有開啟360';	
		}
	}

	public function getArticles( $tag=null, $without=null, $articleStatus=null , $page=null , $isPagenation=null , $recPerPage=null ){
		if( !$this->categoryExistNone ){
			$article_status = isset($articleStatus)? " and b.article_status = ".$articleStatus : null;
			$db = new DB();
			$cateNo =  $this->cateType["cate_no"] ; //initial cate_no = 50 
			if( isset($tag) ){ //有標籤篩選
				$tagidSql = " and tag_no = ".$tag;
			}else{ //沒有標籤篩選
				$tagidSql = null;
			}	
			return $tagidSql;
		}else{
			return false;//'沒有分類';	
		}
	}

	public function pagenation($page,$recPerPage){
		//當前頁數
		$pageNo = isset($page)? $page : 1;
		$this->page_no = $pageNo;
		//總共query數量
		$totalRecord = $this->record_count;
		//每頁有幾筆
		$recPerPage = isset($recPerPage)? $recPerPage : 5;
		//共有幾頁
		$totalPage = ceil($totalRecord/$recPerPage);
		$this->total_page = $totalPage;

		$pageStart = ($pageNo-1) * $recPerPage;
		$pageSql = "limit $pageStart,$recPerPage";
	
		$this->pageSql = $pageSql;

		$result = array(
			"recordsTotal" => $this->record_count,
			"totalPage" =>  $this->total_page,
			"pageNo" => $this->page_no,
			);
		return $result;
	}

		
}
// echo "<pre>";
// $articleCate = new category(50);
// // print_r($articleCate->getCateInfoByCateID());
// print_r($articleCate->getArticles( $tag=19, $without=null, $articleStatus=null , $page=null , $isPagenation=null , $recPerPage=null ));

// echo "</pre>";
?>	