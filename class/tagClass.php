<?php  
// include("../model/DB.php");
// include("productClass.php");
// include("articleClass.php");
date_default_timezone_set("Asia/Taipei");

class tagObj{
	public $tagNo ;
	public $tagName ;
	public $tagSort ;
	public $tagUpdateTime ;
	public $products = []; //產品內容
	public $articles = []; //文章內容

	//分頁
	public $pageSql ;

	public $record_count ;
	public $total_page ;
	public $page_no ;
	//分頁 

	public $tagExistNone ;
	
	public function __construct($tid=null){
		if( $tid ){
			$db = new DB();
			$sql = "SELECT * FROM tag where tag_no = :tag_no";
			$dic = array(":tag_no" => $tid );
			$result = $db -> DB_query($sql,$dic);
			if( count($result) != 0 ){
				foreach ($result as $key => $value) {
					$this->tagNo = $value["tag_no"];
					$this->tagName = $value["tag_name"];
					$this->tagSort = $value["tag_sort"];
					$this->tagUpdateTime = $value["tag_updatetime"];					
				}
			}else{
				$this->tagExistNone = "1"; // 找不到標籤
			}	
		}else{
			$this->tagExistNone = "1"; // 找不到標籤
			return false;//"沒有cid";
		}
	}

	public function getTagInfoByTagID(){
		if( !$this->tagExistNone ){ 
			return array(
				"tag_no"=> $this->tagNo,
				"tag_name"=> $this->tagName,
				"tag_sort"=> $this->tagSort,
				"tag_updatetime"=> $this->tagUpdateTime
			);
		}else{
			return false;//"找不到標籤"
		}
		
	}

	public function getProductsByTagID( $tagArray=null, $without=null, $productStatus=null , $sort=null , $priceFilter=null , $page=null , $isPagenation=null , $recPerPage=null ){
		//產品狀態
			if( isset($tagArray) ){//有標籤
				$product_status = isset($productStatus)? "b.product_status = ".$productStatus : null;
			}else{
				$product_status = isset($productStatus)? "product_status = ".$productStatus : null;
			}
			
		//拼sort 字串			
			switch ($sort) {
				case "pricetop":
					if(isset($tagArray)){ //有標籤
						$sortSql = "order by b.product_price_sort desc";
					}else{
						$sortSql = "order by product_price_sort desc";
					}
				break;
				case "pricelow":
					if(isset($tagArray)){ //有標籤
						$sortSql = "order by b.product_price_sort asc";
					}else{
						$sortSql = "order by product_price_sort asc";
					}		
				break;
				case "time":
					if(isset($tagArray)){ //有標籤
						$sortSql = "order by b.product_createtime desc";
					}else{
						$sortSql = "order by product_createtime desc";
					}	
				break;
				default: //品牌上架時間
					// $sortSql = null;
					$sortSql = "order by product_createtime desc";
				break;
			}
		//價錢篩選 拼字串
			switch( $priceFilter ){
				case "500":
					$priceFilterSql ="and product_price_sort<=500 "; 
				break;
				case "500to1000":
					$priceFilterSql ="and product_price_sort between 501 and 1000 "; 
				break;
				case "1000to2000":
					$priceFilterSql ="and product_price_sort between 1001 and 2000 "; 
				break;
				case "2000to3000":
					$priceFilterSql ="and product_price_sort between 2001 and 3000 "; 
				break;
				case "3000":
					$priceFilterSql ="and product_price_sort>=3001 "; 
				break;
				default:
					$priceFilterSql =null; 
				break;	
			}	

		//1.計算count (標籤(複數 or null ) / Sort裝態(單一 or null) / 價錢篩選(單一 or null) )
			$db = new DB();
			if( !isset($tagArray) ){ //沒有標籤
			// a-1. count
				$sqlCount = sprintf("SELECT count(*) from product where %s %s order by product_price_sort desc",$product_status,$priceFilterSql);

				$resultCount = $db -> DB_query($sqlCount);
				$this->record_count = $resultCount[0]["count(*)"];

				if( isset($isPagenation) ){ //有分頁
					$pageResult = $this->pagenation($page,$recPerPage);
					$this->products["page"] = $pageResult;
				}
				

			// a-2. 排列sql 
				$sql = sprintf("SELECT product_no from product where %s %s %s %s",$product_status,$priceFilterSql,$sortSql,$this->pageSql);
				
			}else{  //有標籤
			// b-1. count
				$tagBind = [];
				foreach ($tagArray as $key => $value) {
					array_push($tagBind, "tag_no = ".$value);
				}
				$str = implode(" or ",$tagBind);
				

				$sqlCount = sprintf("SELECT count(DISTINCT a.product_no) from tag_products_relate a join product b on a.product_no = b.product_no where %s and (%s) %s %s",$product_status,$str,$priceFilterSql,$sortSql);

				$resultCount = $db -> DB_query($sqlCount);
				$this->record_count = $resultCount[0]["count(DISTINCT a.product_no)"];

				if( isset($isPagenation) ){ //有分頁
					$pageResult = $this->pagenation($page,$recPerPage);
					$this->products["page"] = $pageResult;
				}

				// b-2. 排列sql 
				$sql = sprintf("SELECT b.product_no from tag_products_relate a join product b on a.product_no = b.product_no where %s and (%s) %s group by a.product_no %s %s",$product_status,$str,$priceFilterSql,$sortSql,$this->pageSql);
			}	
			
			$result = $db -> DB_query($sql);
			foreach ($result as $pkey => $pvalue) {
				$product = new productObj($pvalue["product_no"]);
				$this->products["products"][$pkey] = $product->brief();
			}
		// //2.
	 		return $this->products;
	}


	public function getArticlesByTagID( $tag=null, $without=null, $articleStatus=null , $page=null , $isPagenation=null , $recPerPage=null ){
		if( !$this->categoryExistNone ){
			$article_status = isset($articleStatus)? " and b.article_status = ".$articleStatus : null;
			$db = new DB();
			$cateNo =  $this->cateType["cate_no"] ; //initial cate_no = 50 
			if( isset($tag) ){ //有標籤篩選
				$tagidSql = " and tag_no = ".$tag;
			}else{ //沒有標籤篩選
				$tagidSql = null;
			}	
			return $tagidSql;
		}else{
			return false;//'沒有標籤';	
		}
	}

	public function pagenation($page,$recPerPage){
		//當前頁數
		$pageNo = isset($page)? $page : 1;
		$this->page_no = $pageNo;
		//總共query數量
		$totalRecord = $this->record_count;
		//每頁有幾筆
		$recPerPage = isset($recPerPage)? $recPerPage : 5;
		//共有幾頁
		$totalPage = ceil($totalRecord/$recPerPage);
		$this->total_page = $totalPage;

		$pageStart = ($pageNo-1) * $recPerPage;
		$pageSql = "limit $pageStart,$recPerPage";
	
		$this->pageSql = $pageSql;

		$result = array(
			"recordsTotal" => $this->record_count,
			"totalPage" =>  $this->total_page,
			"pageNo" => $this->page_no,
			);
		return $result;
	}

		
}
// echo "<pre>";
// $tag = new tagObj();

// $tagArray = null;//
// $productStatus =1;
// $isPagenation =1 ;
// $priceFilter = null;
// $sort ="time";
// print_r($tag->getProductsByTagID( $tagArray, $without=null, $productStatus , $sort , $priceFilter , $page=null , $isPagenation , $recPerPage=40 ));


// echo "</pre>";
?>	