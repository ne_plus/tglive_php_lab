<?php  

// include("../model/DB.php");

date_default_timezone_set("Asia/Taipei");

class productObj{
	public $productNo;
	public $productModelId;
	public $productName ;
	public $productSubtitle ;
	public $productPromote ;
	public $productDescribe ;
	public $productContent ;
	public $productNote ;
	public $productStatus ;
	public $image =[] ;
	public $spec = [];
	public $productUpdateTime ;
	public $highPrice ;
	public $lowPrice ;

	public $productExistNone ;
	
	public function __construct($pid=null){
		if( $pid ){ 
			$db = new DB();
			$sql = "SELECT * FROM product where product_no = :product_no";
			$dic = array(":product_no"=>$pid);
			$result = $db -> DB_Query($sql,$dic);
			if( count($result) != 0 ){
				foreach ($result as $key => $value) {
					$this->productNo =  $value["product_no"];
					$this->productModelId =  $value["model_id"];
					$this->productName =  $value["product_name"];
					$this->productSubtitle =  $value["product_subtitle"];
					$this->productPromote =  $value["product_promote"];
					$this->productDescribe =  $value["product_describe"];
					$this->productContent =  $value["product_content"];
					$this->productNote =  $value["product_note"];
					$this->productStatus =  $value["product_status"];
					for ($i= 1; $i <= 5; $i++) { 
						if(trim($value["img".$i]) != null || trim($value["img".$i]) != ""){
							array_push( $this->image , $value["img".$i] );
						};
					};
					$this->specUseInfo($pid);
				}
			}else{
				$this->productExistNone = "1"; //找不到商品
			} 
			
			
		}else{		
			return false; // echo "沒有pid";
		}
		
	}

	public function brief(){
		if(!$this->productExistNone){
			return array(
				"product_no" => $this->productNo,
				"model_id" => $this->productModelId,
			 	"product_name" => $this->productName,
			    "product_subtitle" => $this->productSubtitle,
			    "product_promote" => $this->productPromote,
				"product_describe" => $this->productDescribe,
				"product_content" => $this->productContent,
				"product_note" => $this->productNote,
				"product_status" => $this->productStatus,
				"product_img" => $this->image,
				"spec" => $this->spec,
				"lowPrice" => $this->lowPrice,
				"highPrice" => $this->highPrice,
			);
		}else{
			return false;//"沒有商品";
		}
		
	}


	public function specUseInfo($pid){
		$db = new DB();
		$sql = "SELECT * FROM product_spec WHERE product_spec_status = 1 and product_no= ".$pid;
		$result = $db -> DB_Query($sql);
		foreach ($result as $key => $value) {
			array_push( $this->spec , $value );
		}	
		$this->getProductPrice($result);
	}

	public function getProductPrice($array){
		$price = [];
		foreach ($array as $key => $value){		
			array_push( $price , $value["product_spec_price1"] );
			if( $value["product_spec_price2"] != 0 ){
				array_push( $price , $value["product_spec_price2"] );
			}
			if( $value["product_spec_price3"] != 0 ){
				array_push( $price , $value["product_spec_price3"] );
			}
		}
		$this->highPrice = max($price);
		$this->lowPrice = min($price);
		if( max($price) == min($price) ){
			$this->highPrice = max($price);
			$this->lowPrice = null;
		}
	}



	//商品點擊次數 
	public function updateProductViews($pid,$date){ 
		$db = new DB();

		//check DB是否有瀏覽紀錄的資料
		$sql =sprintf("SELECT count(*),product_views_no FROM product_views WHERE product_no = %s and date_group = '%s' ",$pid,$date);
		$result = $db -> DB_Query($sql);
		if( $result[0]["count(*)"] == 0 ){ //沒有瀏覽紀錄
			//新增瀏覽紀錄次數
			$table = "product_views";
			$data = array(
				"product_no" => $pid,
				"times" => 1 ,
				"date_group" => $date
				);
			$checkColumn = array("product_views_no");
			$exec =  $db -> DB_Insert($table,$data);
		}else{ //有之前的瀏覽紀錄
			//更新瀏覽紀錄次數
			$table = "product_views";
			$add = "times + 1 " ;
			$data = array(
				"product_views_no" => $result[0]["product_views_no"],
				"times" => $add
				);
			$checkColumn = array("product_views_no");
			$updateSql = sprintf("UPDATE  product_views SET times = times + 1 WHERE product_views_no = %s ",$result[0]["product_views_no"]);
			$exec =  $db -> exec($updateSql);
		}
		//確認新增 更新結果	
		if( $exec == 1 ){
			return true ; //成功
		}else{
			return false ; //失敗
		}
	} 
}	


?>