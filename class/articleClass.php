<?php  

include("../model/DB.php");

date_default_timezone_set("Asia/Taipei");

class articleObj{
	public $articleNo;
	public $articleTitle;
	public $articleContent;
	public $articleDescribe;
	public $articleImg;
	public $articleOwner;
	public $articleStatus;
	public $articleCreatetime;
	public $articleUpdatetime;

	public $articleExistNone ;	
	
	public function __construct($aid=null){
		if( $aid ){ 
			$db = new DB();
			$sql = "SELECT * FROM article where article_no =  :article_no";
			$dic = array(":article_no"=>$aid);
			$result = $db -> DB_Query($sql,$dic);
			if( count($result) != 0 ){
				foreach ($result as $key => $value) {
					$this->articleNo =  $value["article_no"];
					$this->articleTitle =  $value["article_title"];
					$this->articleContent =  $value["article_content"];
					$this->articleDescribe =  $value["article_describe"];
					$this->articleImg =  $value["article_img"];
					$this->articleOwner =  $value["article_owner"];
					$this->articleStatus =  $value["article_status"];
					$this->articleCreatetime =  $value["article_createtime"];
					$this->articleUpdatetime =  $value["article_updatetime"];
				}
			}else{
				$this->articleExistNone = "1"; //無文章
			} 
		}else{
			$this->articleExistNone = "1"; //無文章
			return false; //echo "沒有aid";
		}
		
	}

	public function brief(){
		if(!$this->articleExistNone){
			return array(
				"article_no" => $this->articleNo,
				"article_title" => $this->articleTitle ,
				"article_content" => $this->articleContent ,
				"article_describe" => $this->articleDescribe,
				"article_img" => $this->articleImg,
				"article_owner" => $this->articleOwner,
				"article_status" => $this->articleStatus,
				"article_createtime" => $this->articleCreatetime,
				"article_updatetime" => $this->articleUpdatetime,
			);
		}else{
			return false;//無文章
		}
		
	}

}	

?>