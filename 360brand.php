<?php require_once("module/header.php"); 
include("class/categoryClass.php");
include("class/productClass.php");

if(isset($_REQUEST['cate_no'])){
    $cateID =  $_REQUEST['cate_no']; //指抓取 VR專區
}else{
	// header('Location: 360index.php');
	header('Location: vrLive.php');
}

// 360 所有品牌內容
$query360ALL = $db->DB_Query(sprintf("SELECT * from category where cate_360_status = 1 and cate_level = 2 and cate_no <> %s order by cate_updatetime",$cateID));

// 品牌內容
$query360 = $db->DB_Query(sprintf("SELECT * from category where cate_360_status = 1 and cate_level = 2 and cate_no = %s",$cateID));

if (count($query360) == 0){
	// header('Location: 360index.php');
	header('Location: vrLive.php');
}

$query360name = $query360[0]['cate_name'];
$query360link = $query360[0]['cate_360_link'];

$cate = new category($cateID);
$result = $cate->get360ProductsByCateId($cateID,null ,$productStatus=1 ,$product360Status=1, $page=null , $isPagenation=null , $recPerPage=null);

$productCount = isset($result[0]['products'])? count($result[0]["products"]) : 0 ;

?>


	<section class="vrlive marginTop100">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			<li class="breadcrumb-item active">
				<a href="vrLive.php"><?=$lang_menu_vrLive?></a>
				<form action="vrLive.php" method="get"></form>
			</li>
			<li class="breadcrumb-item active"><?=$query360name?></li>	 
        </ol>
        
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="vrlive-area">
						<iframe width="100%" height="100%" src="<?=$query360link?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>	
			
		<div class="container mt-3">
        <?php  
        if ($productCount != 0 ) {
            foreach ($result as $key => $value) {
        ?> 

			<div class="row">
        <?php  
                for ($i=0; $i < $productCount; $i++) { 
                    $priceTop = "NT";
                    $priceLow = "NT";	
                    if( trim($value["products"][$i]["lowPrice"]) == null ){
                        $priceLow .= $value["products"][$i]["highPrice"];
                        $priceTop = "";
                    }else{
                        $priceLow .= $value["products"][$i]["lowPrice"];
                        $priceTop .= $value["products"][$i]["highPrice"];
                    }		
        ?>	

				<div class="col-6 col-md-4 col-lg-2">
					<div class="row">
						<div class="product-items">
                              <form action="product-detail.php" method="get">
						  		<div class="content-hover">
							  		<img src="<?php echo $value['products'][$i]['product_img'][0]; ?>" class="img-fluid">
								  	<div class="block">
						  				<div class="info text-center d-flex align-items-center justify-content-center">
						  					<div class="product-name" style="font-size: 14px;"><?php echo $value["products"][$i]["product_name"]; ?></div>
						  				</div>
						  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
						  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
						  				</div>
						  			</div>
						  			<div class="price text-center">
										<!-- PRICE只抓第一筆資料 -->
										<div class="price-new mr-1" style="font-size: 16px; color: red;"><?php echo $priceLow; ?></div><div class="price-old" style="text-decoration: line-through; color: rgb(0, 0, 0);"><?php echo $priceTop; ?></div>		
					  				</div>
				  				</div>
					  			<input type="hidden" name="product_no" value="<?php echo $value["products"][$i]["product_no"]; ?>">
							  </form>
						  </div>	
					</div>	
				</div>

            <?php 
                    } //--end for
                } // --end foreach
            } // -- end if productCount  
            ?>

			</div>
		</div>

<!-- 相關店鋪 start-->
		<div class="container relateBrands mt-5">
			<h5 class="text-center"><?=$lang_360_relate_brands?></h5>
			<div class="brandsRelateBox">
			<?php 
				foreach ($query360ALL as $key => $value) {	
			?>
				<div class="recommend product-items">
					<form action="360brand.php" method="get">
						<div class="content-hover">
							<img src="<?=$value['cate_logo_img']?>" class="img-fluid">
							<div class="block" style="background-color: rgba(255,200,207,.7);">
								<div class="info text-center d-flex align-items-center justify-content-center">
									<div class="product-name" style="font-size: 14px;"><?=$value['cate_name']?></div>
								</div>
								<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
									<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
								</div>
							</div>
							<!-- <div class="price text-center">
								<div class="price-new mr-1" style="font-size: 16px; color: red;">NT80</div><div class="price-old" style="text-decoration: line-through; color: rgb(0, 0, 0);">NT100</div>		
							</div> -->
						</div>
						<input type="hidden" name="cate_no" value="<?=$value["cate_no"]?>">
						<input type="hidden" name="brand" value="<?=$value['cate_name']?>">
					</form>	
				</div>
			<?php
				}
			?>
			</div>
		</div>
		<style>
			.relateBrands h5{
				border-bottom: 1px solid #FDC5CD;
				padding: 10px 0;
				color: #FDC5CD;
				font-size: 16px;
				letter-spacing: 10px;
			}
			.relateBrands .brandsRelateBox .slick-prev{
				width: 40px;
				height: 40px;
				border-radius: 25px;
				background-color: #fff;
				box-shadow: 0 4px 4px rgba(0, 0, 0, 0.3), 0 0 4px rgba(0, 0, 0, 0.2);
				left: 0px;
				z-index: 1;
			}
			.relateBrands .brandsRelateBox .slick-next{
				width: 40px;
				height: 40px;
				border-radius: 25px;
				background-color: #fff;
				box-shadow: 0 4px 4px rgba(0, 0, 0, 0.3), 0 0 4px rgba(0, 0, 0, 0.2);
				right: 0px;
				z-index: 1;
			}
			.relateBrands .brandsRelateBox .slick-prev svg,
			.relateBrands .brandsRelateBox .slick-next svg{
				position: absolute;
				bottom: 0;
				fill-opacity: .3;
			}
		</style>
		<script>
			$(".brandsRelateBox").slick({
				slidesToShow: 6,
				slidesToScroll: 1,
				// autoplay: true,
				// autoplaySpeed: 3000,
				nextArrow: '<button type="button" class="slick-next" id="test">Next<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope yt-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g mirror-in-rtl="" class="style-scope yt-icon"><path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z" class="style-scope yt-icon"></path></g></svg></button>',
				prevArrow: '<button type="button" class="slick-prev">Previous<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope yt-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g mirror-in-rtl="" class="style-scope yt-icon"><path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z" class="style-scope yt-icon"></path></g></svg></button>',
				responsive: [
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 4
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 2
						}
					}
				]	
			})
		</script>	
<!-- 相關店鋪 end-->
	</section>
	
  
<?php require_once("module/footer.php"); ?>