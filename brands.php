<?php require_once("module/header.php"); ?>
<?php 

// ---優惠活動---
$activitySql="select * from category where cate_parents = 63 and cate_level = 1 order by cate_sort asc";
$activityResult = $db->DB_Query($activitySql);

// ---供應商品牌---
if(isset($_GET["cate_no"])){	
	$cateNo = $_GET["cate_no"] ;
	$cateSelectSql = "select * from category where cate_no= $cateNo ";
	$cateSelectResult = $db -> DB_Query($cateSelectSql);


// =====供應商商品====
	//計算總數量
	$sqlCount ="select count(*) from category_products_relate a join product b where a.product_no=b.product_no and cate_no=$cateNo and product_status =1"; // 
	$statement = $db->pdo->query($sqlCount);
	$row = $statement->fetch(PDO::FETCH_NUM);
	$totalRecord = $row[0];
	//每頁有幾筆
	$recPerPage = 9;
	//共有幾頁
	$totalPage = ceil($totalRecord/$recPerPage);

	if(isset($_REQUEST["pageNo"])==false){
		$pageNo=1;
	}else{ 
		$pageNo=$_REQUEST["pageNo"];
	}
	$pageStart = ($pageNo-1) * $recPerPage;

	$productSql = "select * from category_products_relate a join product b where a.product_no=b.product_no and cate_no=$cateNo and product_status =1 order by product_createtime desc limit $pageStart,$recPerPage";
	$productResult = $db -> DB_Query($productSql);
	if($productResult){
			$products = [];
			foreach ($productResult as $key => $value) {
				$products[$key]["product_no"] = $value["product_no"];
				$products[$key]["product_name"] = $value["product_name"];
				$products[$key]["product_subtitle"] = $value["product_subtitle"];
				$products[$key]["img1"] = $value["img1"];
				$products[$key]["product_status"] = $value["product_status"];
				$sql = "select * from product where product_no=:product_no" ;
				$dic=array(":product_no"=>$value["product_no"]);
				$product = new Product($sql,$dic); //product DB initial
				// -------------標籤搜尋
				$tagRelate = $product->productRelateTag();
				if($tagRelate){ //有標籤存在
					foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
						$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
						$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
					}
					 
				}else{ //沒有標籤
					$products[$key]["tag_name"] = null ;
					$products[$key]["tag_no"] =null ;
				}

				// -------------分類搜尋
				
				$resultRelate = $product->productRelateCate();
				if($resultRelate){ //有商品分類
					foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
						$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
						$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
						$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
						$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
						if($valuecateRelate["cate_parents"] != 0 ){//有父層
								$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
								$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
								$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
						}else{ //沒有父層
							$products[$key]["cate_father_name"] = null ;
							$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
						}
					}

				}else{ // 沒有商品分類
					$products[$key]["cate"] = null;
				}

				// ---------產品規格-----
				$resultSpec = $product->productSpecUseInfo();
				if($resultSpec){ //使用中的規格
					foreach ($resultSpec as $keySpec => $valueSpec){
						$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
						$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
						if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
							$products[$key]["product_spec_price_discount"][$keySpec]= null ;
						}else{ //有折扣
							$priceDiscount = $valueSpec["product_spec_price2"];
							if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
								$priceDiscount = $valueSpec["product_spec_price3"];
								$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
							}elseif($valueSpec["product_spec_price3"] == 0){
								$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
							}else{
								$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
							}
						}
						
					}
					
				}else{ //沒有規格
					$products[$key]["product_stock"][$keySpec] = null;
				}

			}
			$array = array("recordsTotal"=>$totalRecord);
			$array["totalPage"] = $totalPage;
			$array["pageNo"] = $pageNo;
			$array["data"] = $products;
			// echo "<pre>";
			// print_r($array);
			// echo "</pre>";
		}else{ //沒有商品
			$products = [];
			$array = array("recordsTotal"=>$totalRecord);
			$array["totalPage"] = $totalPage;
			$array["pageNo"] = $pageNo;
			$array["data"] = $products;
			// echo json_encode($array);
		}

?>

	<section class="productshow brands">
			<?php
				$img_url = trim($cateSelectResult[0]['cate_banner_img']);
				if (strlen($img_url) > 0 ){
					 ?>
				<div class="brand-banner" style="background: #ffffff url('<?=trim($cateSelectResult[0]['cate_banner_img']);?>') no-repeat center top;">
<!--
				<div class="row brand-info">
								<img src="<?php echo $cateSelectResult[0]['cate_logo_img']; ?>" />
								<h4><?php echo $cateSelectResult[0]["cate_name"]; ?></h4>
								<small><?php echo $cateSelectResult[0]["cate_describe"]; ?></small>
							</div>
-->

			</div><!-- brand-banner -->
				<?php } ?>
		
			
		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <?php
			  	if(isset($_GET["cate_no"])){
			  ?>
			  <li class="breadcrumb-item active"><?php echo $cateSelectResult[0]["cate_name"]; ?></li>
			  <?php		
				}else{
					
				} 
			  ?>
		</ol>
		<div id="brandsBtn-rwd" ><?=$lang_brands_other?></div>
		<div class="container mt-5">
			<div class="row">	
				<div class="col-12 col-md-3 ">
			         <div class="sidebar">
			         <button id="cancelBrandsButton-rwd"><i class="fa fa-times" aria-hidden="true"></i></button>
			            <!-- <div class="sidebar_block catagory mb-30">
			                <span class="sidebar_title">優惠活動</span>
			                <div class="sidebar_desc form-check">
			                	<?php foreach ($activityResult as $activitykey => $activityvalue){ ?>	
				                <div class="promotBox">
				                	 <a href=""><?php echo $activityvalue['cate_name']; ?></a>
				                </div>  
								<?php } ?>
			                	</div>
			            </div> -->
						<div class="sidebar_block cateSearch mb-30">
			                <span class="sidebar_title"><?=$lang_brands_brand?></span>
			                <div class="sidebar_desc form-check">	
				              	<?php foreach ($cateBrandUse as $key => $value){?>
								<div class="promotBox">
				                	 <a href="?cate_no=<?php echo $value['cate_no'];?>&brand=<?php echo preg_replace('/\s(?=)/', '', $value['cate_name']); ?>"><?php echo $value['cate_name']; ?></a>
				                </div> 
			                	<?php }?>
			                </div>
			            </div>

			        </div>
			    </div> 

				<div class="col-12 col-md-9 main">
					<?php
						// if (strlen($img_url) > 0 ){
							?>
							<div class="row brand-info">
								<div class="brand-wrap"><img src="<?php echo $cateSelectResult[0]['cate_logo_img']; ?>" /></div>
								<h4 class="font15"><?php echo $cateSelectResult[0]["cate_name"]; ?></h4>
								<small class="font14"><?php echo $cateSelectResult[0]["cate_describe"]; ?></small>
							<?php if ($cateSelectResult[0]['cate_360_link'] && $cateSelectResult[0]['cate_360_status'] == 1) {?>
								<div class="w-100 d-flex justify-content-center mt-3">
									<a class="py-1 px-2" target="_blank" href="360brand.php?cate_no=<?=$cateSelectResult[0]['cate_no']?>&brand=<?=$cateSelectResult[0]['cate_name']?>" style="border-radius:5px;color: #fff;background-color: rgba(255,200,207,1);"><?=$lang_360_link?></a>
								</div>
							<?php } ?>
							</div>
							<?php
						// }
						
					?>
                    <div class="shop_grid_product_area">
                        <div class="row">
							 
							<?php  
								foreach ($array["data"] as $productkey => $productvalue) {
							?>
							<div class="product-items">
								<form action="product-detail.php" method="get">
								  		<div class="content-hover">
									  		<img src="<?php echo $productvalue['img1']; ?>" class="img-fluid">
										  	<div class="block">
								  				<div class="info text-center d-flex align-items-center justify-content-center">
								  					<div class="product-name" style="font-size: 14px;"><?php echo $productvalue['product_name'];?></div>
								  				</div>
								  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
								  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
								  				</div>
								  			</div>
								  			<div class="price text-center">
												<!-- PRICE只抓第一筆資料 -->
												<?php  
													if($productvalue['product_spec_price_discount'][0] == null ){
														// echo "沒有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$productvalue['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
												<?php		
													}else{
														// echo "有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$productvalue['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$productvalue['product_spec_price_old'][0]; ?></div>
												<?php		
													}
												?>	
							  				</div>
						  				</div>
						  				<input type="hidden" name="product_no" value="<?php echo $productvalue['product_no'] ;?>">
								  </form>
							</div>
							<?php
								}
							?>
                        </div>
                    </div>

                    <div class="shop_pagination_area">
                       <?php 
							if(strpos($_SERVER['QUERY_STRING'],'pageNo='.$array["pageNo"])){
								// echo "有分頁";
								$path = str_replace('&pageNo='.$array["pageNo"],'',$_SERVER['QUERY_STRING']);
							}else{
								// echo "沒有"; 
								$path = $_SERVER['QUERY_STRING'];
							} 
						?>
                        <nav aria-label="Page navigation example">
						  <ul class="pagination justify-content-center">
						    <li class="page-item <?php if($array['pageNo'] == 1 ){echo 'disabled';} ?>">
						      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]-1); ?>" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						        <span class="sr-only">Previous</span>
						      </a>
						    </li>
						    <?php 
							    	for($p=1 ; $p<=$array["totalPage"] ; $p++){
										if( $p == $array["pageNo"]){ //當頁
							?>
									<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php				
										}else{
							?>
									<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php			
										}
									} 
						    ?>
						    <!-- <li class="page-item active"><a class="page-link" href="#!">1</a></li>
						    <li class="page-item"><a class="page-link" href="#!">2</a></li>
						    <li class="page-item"><a class="page-link" href="#!">3</a></li> -->
						    <li class="page-item <?php if($array['pageNo'] == $array['totalPage']){echo 'disabled';} ?>">
						      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]+1); ?>" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						        <span class="sr-only">Next</span>
						      </a>
						    </li>
						  </ul>
						</nav>
                    </div>

                </div>


			</div>
		</div>
	</section>



<?php	
}else{
	$sqlSort ="select * from category where cate_level=1 and cate_parents=62 order by cate_sort asc";
	$cateSortResult = $db -> DB_Query($sqlSort);

	$cateBrandUse =[];
	foreach ($cateSortResult as $key => $value) {
		$cateParent = $value["cate_no"];
		$cateUseSql = "select * from category_products_relate a join category b where a.cate_no = b.cate_no and cate_level = 2 and cate_parents = $cateParent group by b.cate_no" ;
		$cateUseResult = $db -> DB_Query($cateUseSql);
		if($cateUseResult){
			foreach ($cateUseResult as $key => $value) {
				array_push($cateBrandUse,$value);
			}
		}
	}
?>
	<section class="productshow">
		<ol class="breadcrumb">
						  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
						  <li class="breadcrumb-item active"><?=$lang_menu_findBrand?></li>
		</ol>
		<div class="container-fluid">
			<div class="col-12">
				<div class="container">
					<div class="row">
					<?php foreach ($cateBrandUse as $key => $value){ ?>	
						<div class="brand-list-noselect col-12 col-lg-6 mb-30">
							<div class="paddingTopBox">
								
							</div>
							  <form action="brands.php" method="get">
							  	<div class="brand-banner mb-30">
									<img src="<?php echo $value['cate_banner_img']; ?>" class="img-fluid">
									<div class="banner-logo">
										<img src="<?php echo $value['cate_logo_img']; ?>" class="img-fluid">
									</div>
									<div class="brand-describe">
										<div class="brand-describe-content"><span class="brand-describe-title"><?php echo $value['cate_name'] ;?></span></div>
									</div>
								</div>
								<input type="hidden" name="cate_no" value="2">
							  </form>

		                </div>
					<?php } ?>
		            </div>   
				</div>
				 
			</div>
		</div>
	</section>			
<?php	
}

?>

	
	
  
<?php require_once("module/footer.php"); ?>