<?php require_once("module/header.php"); 
		
		
		$orderNo = urldecode($_REQUEST["Td"]);
		$paycode = urldecode($_REQUEST["paycode"]);
        $price = urldecode($_REQUEST["MN"]);

?>

	<section class="purchase">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_cart_cart?>
			
		</ol>
		<div class="container">
			<div class="row cartMenu">
				<div class="col-4 step">
					<li><span>1.</span> <?=$lang_cart_cart_list?></li>
				</div>
				<div class="col-4 step">
					<li><span>2.</span> <?=$lang_cart_fill_form?></li>
				</div>
				<div class="col-4 step active">
					<li><span>3.</span> <?=$lang_cart_order_create?></li>
				</div>
				
			</div><!-- .cartMenu.row -->
			<?php if($paycode){ ?>
			<h3><?=$lang_cart_order_create?></h3>
			<div style="margin-bottom: 30px"><?=$lang_cart_order_create_thank_1?><b><?php echo $orderNo; ?></b><?=$lang_cart_order_create_thank_2?><b><?php echo $price; ?></b><?=$lang_cart_order_create_thank_3?></div>
            <div class="parcode-step">A. <?=$lang_cart_order_create_paycode_get?></div>
            <div style="margin-bottom: 30px"><?=$lang_cart_order_create_paycode_hint?> : <strong class="paycodeShow"><?php echo $paycode ;?></strong></div>
            <div class="parcode-step">B. <?=$lang_cart_order_create_paycode_work?></div>
            <ul class="nav nav-tabs"  role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="seven-tab" href="#seven" data-toggle="tab" role="tab" aria-controls="seven" aria-selected="true"><?=$lang_cart_choose_payby_store_711?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="family-tab" href="#family" data-toggle="tab" role="tab" aria-controls="family" aria-selected="false"><?=$lang_cart_choose_payby_store_family?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="hilife-tab" href="#hilife" data-toggle="tab" role="tab" aria-controls="hilife" aria-selected="false"><?=$lang_cart_choose_payby_store_hilife?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="ok-tab" href="#ok" data-toggle="tab" role="tab" aria-controls="ok" aria-selected="false"><?=$lang_cart_choose_payby_store_OK?></a>
                </li>
            </ul>  
            <div class="tab-content" id="myTabContent" style="margin-bottom: 30px">
                <div class="card tab-pane fade show active" style="border-top: none;" id="seven" role="tabpanel" aria-labelledby="seven-tab">
                    <div class="card-body">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block" src="image/frontend/paycode/7-11/ibon_001.jpg"  alt="First slide">
                                    <div class="text-center step-info my-2">1.<?=$lang_cart_order_create_paycode_work_step_711_1?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/7-11/ibon_002.jpg" alt="Second slide">
                                    <div class="text-center step-info my-2">2.<?=$lang_cart_order_create_paycode_work_step_711_2?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/7-11/ibon_003.jpg" alt="Three slide">
                                    <div class="text-center step-info my-2">3.<?=$lang_cart_order_create_paycode_work_step_711_3?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/7-11/ibon_004.jpg" alt="Forth slide">
                                    <div class="text-center step-info my-2">4.<?=$lang_cart_order_create_paycode_work_step_711_4?></div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>   
                <div class="card tab-pane fade" style="border-top: none;" id="family" role="tabpanel" aria-labelledby="family-tab">
                    <div class="card-body">
                        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block" src="image/frontend/paycode/family/famiport_001.jpg"  alt="First slide">
                                    <div class="text-center step-info my-2">1.<?=$lang_cart_order_create_paycode_work_step_family_1?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/family/famiport_002.jpg" alt="Second slide">
                                    <div class="text-center step-info my-2">2.<?=$lang_cart_order_create_paycode_work_step_family_2?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/family/famiport_003.jpg" alt="Three slide">
                                    <div class="text-center step-info my-2">3.<?=$lang_cart_order_create_paycode_work_step_family_3?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/family/famiport_004.jpg" alt="Forth slide">
                                    <div class="text-center step-info my-2">4.<?=$lang_cart_order_create_paycode_work_step_family_4?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/family/famiport_005.jpg" alt="Fifth slide">
                                    <div class="text-center step-info my-2">5.<?=$lang_cart_order_create_paycode_work_step_family_5?></div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>  
                <div class="card tab-pane fade" style="border-top: none;" id="hilife" role="tabpanel" aria-labelledby="hilife-tab">
                    <div class="card-body">
                        <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_001.jpg"  alt="First slide">
                                    <div class="text-center step-info my-2">1.<?=$lang_cart_order_create_paycode_work_step_hilife_1?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_002.jpg" alt="Second slide">
                                    <div class="text-center step-info my-2">2.<?=$lang_cart_order_create_paycode_work_step_hilife_2?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_003.jpg" alt="Three slide">
                                    <div class="text-center step-info my-2">3.<?=$lang_cart_order_create_paycode_work_step_hilife_3?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_004.jpg" alt="Forth slide">
                                    <div class="text-center step-info my-2">4.<?=$lang_cart_order_create_paycode_work_step_hilife_4?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_005.jpg" alt="Fifth slide">
                                    <div class="text-center step-info my-2">5.<?=$lang_cart_order_create_paycode_work_step_hilife_5?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/hi-life/Lift-ET_006.jpg" alt="Fifth slide">
                                    <div class="text-center step-info my-2">6.<?=$lang_cart_order_create_paycode_work_step_hilife_6?></div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>  
                <div class="card tab-pane fade" style="border-top: none;" id="ok" role="tabpanel" aria-labelledby="ok-tab">
                    <div class="card-body">
                        <div id="carouselExampleIndicators4" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_001.jpg"  alt="First slide">
                                    <div class="text-center step-info my-2">1.<?=$lang_cart_order_create_paycode_work_step_OK_1?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_002.jpg" alt="Second slide">
                                    <div class="text-center step-info my-2">2.<?=$lang_cart_order_create_paycode_work_step_OK_2?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_003.jpg" alt="Three slide">
                                    <div class="text-center step-info my-2">3.<?=$lang_cart_order_create_paycode_work_step_OK_3?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_004.jpg" alt="Forth slide">
                                    <div class="text-center step-info my-2">4.<?=$lang_cart_order_create_paycode_work_step_OK_4?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_005.jpg" alt="Fifth slide">
                                    <div class="text-center step-info my-2">5.<?=$lang_cart_order_create_paycode_work_step_OK_5?></div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block" src="image/frontend/paycode/ok/OKGO_006.jpg" alt="Fifth slide">
                                    <div class="text-center step-info my-2">6.<?=$lang_cart_order_create_paycode_work_step_OK_6?></div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators4" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators4" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>        
            </div>
			<div class="row">
				<div class="col-md-6 col-sm-12 text-center">
						<div class="purchase_area" style="padding-left: 0px;">
		                        <a id="keepShop" href="index.php" role="button" class="btn btn-sm"><?=$lang_member_goshop2?></a>
		                        <a id="checkOrderList" href="memhistory.php" role="button" class="btn btn-sm"><?=$lang_cart_check_order?></a>
		               </div>
				</div><!-- .col-sm-12 -->
			</div>	<!-- .row -->
			<?php }else{ ?>
				<h3><?=$lang_cart_order_fail?></h3>	
			<?php } ?>
			
		</div><!-- .container -->
	</section>			
<style>
    .paycodeShow{
        font-size : 18px;
        padding-left : 15px;
        color: #f00;
    }
    .purchase .parcode-step{
        background-color: rgba(255,200,207,.3);
        display: inline-block;
        padding: 10px;
        border-radius: 5px;
        color: #f67f90;
        margin-bottom: 10px;
    }
    .purchase .step-info{
        font-size:16px;
    }
    .purchase .carousel img{
        margin:auto;
        width:100%;
    }
    @media screen and ( min-width : 992px ){
        .purchase .carousel img{
            width:60%;
        }
    } 
    .purchase .carousel-control-prev-icon,
    .purchase .carousel-control-next-icon {
        height: 100px;
        width: 100px;
        outline: black;
        background-size: 100%, 100%;
        border-radius: 50%;
        background-image: none;
    }

    .purchase .carousel-control-next-icon:after
    {
        content: '>';
        font-size: 55px;
        color: #f67f90;
    }

    .purchase .carousel-control-prev-icon:after {
        content: '<';
        font-size: 55px;
        color: #f67f90;
    }
</style>