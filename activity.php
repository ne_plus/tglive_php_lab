<?php require_once("module/header.php"); 
include("class/categoryClass.php");
include("class/productClass.php");


$cateID = isset($_REQUEST["cate_no"])? $_REQUEST["cate_no"] : 63;
$cate = new category($cateID);
$without = array(90,94,49,183); //不抓取id90 以及 id 94內容 49內容 183 內容
$result = $cate->getProductsByCateId($cateID,$without ,$productStatus=1 , $page=null , $isPagenation=null , $recPerPage=null);
$productCount = isset($_REQUEST["cate_no"])? count($result[0]["products"]) : 8;//

?>


	<section class="activityshow marginTop100">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
		<?php if( !isset($_REQUEST["cate_no"]) ){
		?>
			<li class="breadcrumb-item active"><?=$lang_menu_activity?></li>	 
		<?php 
			}else{
		?>
			<li class="breadcrumb-item">
				<a href="products.php"><?=$lang_menu_activity?></a>
				<form action="activity.php" method="get"></form>
			</li>	
			<li class="breadcrumb-item active"><?php echo $result[0]["cate_name"]; ?></li>
		<?php		
			} 
		?>	
		</ol>
			
		<div class="container activitySection mt-3">
		<?php  
		foreach ($result as $key => $value) {
			if( isset($value["products"]) && count($value["products"]) >= 8 ){				
		?>
			<div class="row activityBannerSection">
				<div class="col-12">
					<div class="row">
						<div class="activity-banner" style="background-image: url(<?php echo $value["cate_banner_img"]; ?>);">
							<!--<div class="activity-banner-title">
								 <span><?php //echo $value["cate_name"] ?></span> 
								 <div class="activity-banner-title-background"></div> 
							</div>		-->
						</div>
					</div>
					
				</div>
			</div>

			<div class="row activityProductSection">
		<?php  
			for ($i=0; $i < $productCount; $i++) { 
				$priceTop = "NT";
				$priceLow = "NT";	
				if( trim($value["products"][$i]["lowPrice"]) == null ){
					$priceLow .= $value["products"][$i]["highPrice"];
					$priceTop = "";
				}else{
					$priceLow .= $value["products"][$i]["lowPrice"];
					$priceTop .= $value["products"][$i]["highPrice"];
				}		
		?>	
				<div class="col-6 col-md-4">
					<div class="row activityProductItem">
						<div class="product-items">
							<form action="product-detail.php" method="get">
						  		<div class="content-hover">
							  		<img src="<?php echo $value['products'][$i]['product_img'][0]; ?>" class="img-fluid">
								  	<div class="block">
						  				<div class="info text-center d-flex align-items-center justify-content-center">
						  					<div class="product-name" style="font-size: 14px;"><?php echo $value["products"][$i]["product_name"]; ?></div>
						  				</div>
						  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
						  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
						  				</div>
						  			</div>
						  			<div class="price text-center">
										<!-- PRICE只抓第一筆資料 -->
										<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo $priceLow; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo $priceTop; ?></div>		
					  				</div>
				  				</div>
					  			<input type="hidden" name="product_no" value="<?php echo $value["products"][$i]["product_no"]; ?>">
							  </form>
						  </div>	
					</div>	
				</div>
		<?php  
			}//end for

				if( !isset($_REQUEST["cate_no"]) ){ 
		?>		
				<!-- ====== read more ============ -->
				<div class="col-12 col-md-4">
					<div class="row activityProductItem">	
						<div class="readMore">
							<a class="anchor" href="#">
								<div class="readMore-box">
									<div>Read More</div>
									<div class="lineUp"></div>
									<div class="lineRight"></div>
									<div class="lineLeft"></div>
									<div class="lineDown"></div>
								</div>
								<form action="activity.php" method="get">
									<input type="hidden" name="cate_no" value="<?php echo $value["cate_no"]; ?>">
								</form>
							</a>	
						</div>
					</div>	
				</div>
			</div>
		<?php  
				}// end if (read more) 分頁隱藏
			} //end if 
		} //end foreach $result
		?>
		</div>
	</section>
	
  
<?php require_once("module/footer.php"); ?>