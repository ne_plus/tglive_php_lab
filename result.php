<?php  
require_once("module/header.php"); 

// =====1. 搜尋字串傳入====
$searchStr = $_REQUEST["productSearch"];

// =====ex 如搜尋字串包含'使用跳脫字元=======
$strRe  = str_replace("'","\'",$searchStr);

// =====2.去除空白字元多筆搜尋字串===
$searchArr = explode(" ",$strRe);

// =====3.重組搜尋條件====
$searchArrFinal=[];
foreach ($searchArr as $key => $value) {
	if($value != "" || $value != null){
		array_push($searchArrFinal,$value);
	}
}

include("Frontframwork/searchResult_form.php");
?>


<div class="container marginBottom60 marginTop100" id="searchResult">
	<div class="row">
		<div class="col-12 mt-3 text-center">
			<h5 ><?=$lang_search_title?><span class="mx-2">-</span><span id="search-title" style="font-size: 14px; vertical-align: middle;"><?php echo $searchStr; ?></span></h5> 
		</div>
		<div class="col-12 mt-4">
			<h4 class="text-center"><?=$lang_search_match?></h4>
		</div>
		
		<?php if($array['recordsTotal'] != 0){ ?>
		<div id="searchResultBox" class="col-12">
			<div class="row">
		<?php 
			foreach ($array['data'] as $key => $value) {
		?>	
				<div class="product-items">
			  	<form action="product-detail.php" method="get">
			  		<div class="content-hover">							  		
				  		<img src="<?php echo $value['img1']; ?>" class="img-fluid">
				  		<div class="block">
			  				<div class="info text-center d-flex align-items-center justify-content-center">
			  					<div class="product-name" style="font-size: 14px;"><?php echo $value['product_name']; ?></div>			
			  				</div>
			  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
			  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
			  				</div>
			  			</div>	
			  			<div class="price text-center">
							<!-- PRICE只抓第一筆資料 -->
							<?php  
								if($value['product_spec_price_discount'][0] == null ){
									// echo "沒有折扣";
							?>
							<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
							<?php		
								}else{
									// echo "有折扣";
							?>
							<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div>
							<?php		
								}
							?>	
		  				</div>
				  	</div>
				  	<input type="hidden" name="product_no" value="<?php echo $value['product_no']; ?>">
			  	</form>	
		  	</div>
		<?php } //----end foreach ?>

		  </div>
		</div>  

		

		<div class="col-12">
            
            <div class="shop_pagination_area mt-3">
            	<?php 
					if(strpos($_SERVER['QUERY_STRING'],'pageNo='.$array["pageNo"])){
						// echo "有分頁";
						$path = str_replace('&pageNo='.$array["pageNo"],'',$_SERVER['QUERY_STRING']);
					}else{
						// echo "沒有"; 
						$path = $_SERVER['QUERY_STRING'];
					} 
				?>
                <nav aria-label="Page navigation example">
				  <ul class="pagination justify-content-center">
				    <li class="page-item <?php if($array['pageNo'] == 1 ){echo 'disabled';} ?>">
				      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]-1); ?>" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				        <span class="sr-only">Previous</span>
				      </a>
				    </li>
				    <?php 
				    		if($array["totalPage"] > 7){ //頁數大於 7 
				    			if($array["pageNo"]<5){ 
				    				for($p=1 ; $p<=5 ; $p++){
				    					if( $p == $array["pageNo"]){ //當頁
					?>	
								<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
					<?php
										}else{
					?>
								<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
					<?php					
										}	    					
				    				}
				    ?>
				    			<li class="page-item disabled"><a class="page-link" >...</a></li>
				    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $array["totalPage"]; ?>"><?php echo $array["totalPage"]; ?></a></li>
				    <?php				
				    			}else if(($array["totalPage"]-3) <= $array["pageNo"]){
				    ?>
				    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
				    			<li class="page-item disabled"><a class="page-link" >...</a></li>
				    <?php
				    				for($p=($array["totalPage"]-4) ; $p<=$array["totalPage"] ; $p++){
				    					if( $p == $array["pageNo"]){ //當頁
				    ?>
				    			<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
				    <?php						
				    					}else{
				    ?>
								<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
				    <?php						
				    					}							    					
				    				}
				    			}else if($array["pageNo"]>=5){
				    ?>
				    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
				    			<li class="page-item disabled"><a class="page-link" >...</a></li>
				    <?php				
				    				for($p=($array["pageNo"]-1) ; $p<=($array["pageNo"]+1) ; $p++){
				    					if( $p == $array["pageNo"]){ //當頁
				    ?>
								<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
				    <?php						
				    					}else{
				    ?>
								<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
				    <?php								
				    					}	
				    				}
				    ?>
				    			<li class="page-item disabled"><a class="page-link" >...</a></li>
				    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $array["totalPage"]; ?>"><?php echo $array["totalPage"]; ?></a></li>
				    <?php				
				    			}
				  
				    		}else{
					    		for($p=1 ; $p<=$array["totalPage"] ; $p++){
								if( $p == $array["pageNo"]){ //當頁
					?>
							<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
					<?php				
									}else{
					?>
							<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
					<?php			
									} 
								} //----end for 
							} //----end if	
				    ?>
				    <li class="page-item <?php if($array['pageNo'] == $array['totalPage']){echo 'disabled';} ?>">
				      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]+1); ?>" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				        <span class="sr-only">Next</span>
				      </a>
				    </li>
				  </ul>
				</nav>
            </div>
        </div>

	<?php }else{  //搜尋不到商品
		
		// ----特色商品---
		$popularCateNo = 94; //cateNo
		$popularProducts = get_activityId_queryProducts($popularCateNo,null);
	?>	  	
	<div id="searchResultBox-none" class="col-12 mt-2 mb-5">
		<div class="alert alert-danger text-center" role="alert">
		  <div class="cross">
		  	<div class="left"></div>
		  	<div class="right"></div>
		  </div>
		  <div class="alert-title"><?=$lang_search_notfound?></div>
		</div>
	</div>
	<section>
		<div class="container feature">
			<div class="row">
			<div class="col-12 mt-4">
				<h5 class="text-center"><?=$lang_product_recommand?></h5>
			</div>
<?php  foreach ($popularProducts as $key => $value) { ?>
			<div class="product-items">
			  	<form action="product-detail.php" method="get">
			  		<div class="content-hover">							  		
				  		<img src="<?php echo $value['img1']; ?>" class="img-fluid">
				  		<div class="block">
			  				<div class="info text-center d-flex align-items-center justify-content-center">
			  					<div class="product-name" style="font-size: 14px;"><?php echo $value["product_name"]; ?></div>			
			  				</div>
			  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
			  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
			  				</div>
			  			</div>	
			  			<div class="price text-center">
		  					<!-- PRICE只抓第一筆資料 -->
								<?php  
									if($value['product_spec_price_discount'][0] == null ){
										// echo "沒有折扣";
								?>
								<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
								<?php		
									}else{
										// echo "有折扣";
								?>
								<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div>
								<?php		
									}
								?>	
		  				</div>
				  	</div>
				  	<input type="hidden" name="product_no" value="<?php echo $value["product_no"]; ?>">
			  	</form>	
			  </div>
		<?php
				} 
			// }
		?>
				
		</div><!-- row -->
	  	</div><!-- container -->
	  </section>
	<?php } ?>	  
			
		</div>
	</div>	
</div>





  <?php require_once("module/footer.php"); ?>