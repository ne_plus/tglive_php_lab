<?php  
include("DB.php");

class Category extends DB{
	public $cateNo ;
	public $cateName ;
	public $cateParents ;
	public $cateLevel ;
	public $cateSort ;
	public $cateLogoImg ;
	public $cateBannerImg;
	public $cateCate360link;
	public $cateCate360status;
	public $cateDescribe;
	public $cateUpdateTime ;


	public $cateExist ;


	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
				if($value["cate_parents"]==0){ //第一層
					$this->cateNo = $value["cate_no"];
					$this->cateParents = $value["cate_parents"];
					$this->cateName = $value["cate_name"];
					$this->cateLevel = $value["cate_level"];
					$this->cateSort = $value["cate_sort"];
					$this->cateLogoImg = $value["cate_logo_img"];
					$this->cateBannerImg = $value["cate_banner_img"];
					$this->cateCate360link = $value["cate_360_link"];
					$this->cateCate360status = $value["cate_360_status"];
					$this->cateDescribe = $value["cate_describe"];
					$this->cateUpdateTime = $value["cate_updatetime"];
				}else{ //子層
					$this->cateNo = $value["cate_no"];
					$this->cateName = $value["cate_name"];
					$this->cateParents = $value["cate_parents"];
					$this->cateLevel = $value["cate_level"];
					$this->cateSort = $value["cate_sort"];
					$this->cateLogoImg = $value["cate_logo_img"];
					$this->cateBannerImg = $value["cate_banner_img"];
					$this->cateCate360link = $value["cate_360_link"];
					$this->cateCate360status = $value["cate_360_status"];
					$this->cateDescribe = $value["cate_describe"];
					$this->cateUpdateTime = $value["cate_updatetime"];
				}
			}
		}else{
			$this->cateExist = "1"; //找不到分類
		}
	}
	public function cateSelectAll($sql){
		return $this->DB_Query($sql) ;
	}

	public function cateInfo(){
		if(!$this->cateExist){// 有分類項目
			$cate = array(
				"cate_no" => $this->cateNo,
				"cate_name" => $this->cateName,
				"cate_parents" => $this->cateParents,
				"cate_logo_img" => $this->cateLogoImg,
				"cate_banner_img" => $this->cateBannerImg,
				"cate_360_link" => $this->cateCate360link,
				"cate_360_status" => $this->cateCate360status,
				"cate_describe" => $this->cateDescribe
				);
			return $cate;
		}else{
			return false ; //沒有分類項目存在
		}
		
	}

	public function cateChildQuery(){ 
		if(!$this->cateExist){ // 有分類項目
			if($this->cateParents == 62){ //父層找子層
				$sql = "select * from category where cate_parents ='".$this->cateNo."'";
				$result = $this->DB_Query($sql) ;
				if(!count($result)){
					return false ;//echo "沒有子層"; 
				}else{
					return $result;
				}
			}else{ //子層找同層兄弟
				$sql = "select * from category where cate_parents ='".$this->cateParents."'";
				return  $this->DB_Query($sql);		
			}
		}else{//無分類項目
			echo "無分類項目";
		}		
	}

	public function cateFatherQuery(){
		if(!$this->cateExist){// 有分類項目
			if($this->cateParents == 62){ //父層
				$result = array(
					"cate_no"=> $this->cateNo,
					"cate_parents"=> $this->cateParents,
					"cate_Name"=> $this->cateName,
					"cate_level"=> $this->cateLevel,
					"cate_sort"=> $this->cateSort,
					"cate_updatetime"=> $this->cateUpdateTime
					);
				return $result;
			}else{ //子層找父層
				$sql = "select * from category where cate_no ='".$this->cateParents."'";
				$result = $this->DB_Query($sql);
				if(count($result)){ //有父親並顯示
					return $result;
				}
			}
		}else{//無分類項目
			echo "無分類項目";
		}
	}

// -----會先偵測Cate_name 是否有相同的存在-------
	public function cateCreate($table,$data){
		if($this->cateExist){ //沒有分類項目
			return $this->DB_Insert($table,$data);
		}else{
			return false ;// "有存在帳號";
		}
	}
// -----直接新增-------
	public function cateCreate2($table,$data){
		return $this->DB_Insert($table,$data);
	}



	public function cateUpdate($table,$data,$checkColumn){
		if(!$this->cateExist){ //存在分類項目
			return $this->DB_UpdateOnly($table,$data,$checkColumn);
		}else{
			return false ;// "找不到分類";
		}
	}
	public function delete($sql){
		if(!$this->cateExist){ //存在分類項目
			$this->exec($sql);
			return true ;
		}else{
			return false ;//"找不到分類";
		}
	}



	// -------ID找尋分類兒子-----
	public function cateAllChildQuery($cateParent){ 
		if(!$this->cateExist){ // 有分類項目
			$sql = "select * from category where cate_parents ='".$cateParent."'";
			$result = $this->DB_Query($sql) ;
			if(!count($result)){
				return false ;//echo "沒有子層"; 
			}else{
				return $result;
			}
		}else{//無分類項目
			return false ;//echo "無分類項目"
		}		
	} 
	// -------ID找尋分類父親-----
	public function cateAllFatherQuery($cateNo){ 
		if(!$this->cateExist){ // 有分類項目
			$sql = "select * from category where cate_no ='".$cateNo."'";
			$result = $this->DB_Query($sql) ;
			if(!count($result)){
				return false ;//echo "沒有子層"; 
			}else{
				return $result;
			}
		}else{//無分類項目
			return false ;//echo "無分類項目"
		}		
	} 


}

?>