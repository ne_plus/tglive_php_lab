<?php  
include("DB.php");

class Coupon extends DB{
	public $couponNo=[];
	public $couponName=[];
	public $couponCode=[];
	public $couponDiscount=[];
	public $couponStart=[];
	public $couponEnd=[];
	public $couponUsed=[];
	public $couponTarget=[];
	public $couponStatus=[];
	public $couponTimes=[];

	public $couponExist ;


	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
				$this->couponNo = $value["coupon_no"];
				$this->couponName = $value["coupon_name"];
				$this->couponCode = $value["coupon_code"];
				$this->couponDiscount = $value["coupon_discount"];
				$this->couponStart = $value["coupon_start"];
				$this->couponEnd = $value["coupon_end"];
				$this->couponUsed = $value["coupon_used"];
				$this->couponTarget = $value["coupon_target"];
				$this->couponStatus = $value["coupon_status"];
				$this->couponTimes = $value["coupon_times"];
			}
		}else{
			$this->couponExist = "1"; //找不到優惠劵
		}
		

	}
	public function couponQuery(){ 
		if($this->couponExist){
			return false ;//"找不到優惠劵";
		}else{
			$result = array(
				"coupon_no"=> $this->couponNo,
				"coupon_name"=> $this->couponName,
				"coupon_code"=>$this->couponCode,
				"coupon_discount"=>$this->couponDiscount,
				"coupon_start"=>$this->couponStart,
				"coupon_end"=>$this->couponEnd,
				"coupon_used"=>$this->couponUsed,
				"coupon_target"=>$this->couponTarget,
				"coupon_status"=>$this->couponStatus,
				"coupon_times"=>$this->couponTimes
			);
			return $result;
		};		
	}
	public function couponCreate($table,$column,$checkColumn){
		if($this->couponExist){ 
			return $this->DB_Update($table,$column,$checkColumn);
		}else{
			return false ; //"已經存在優惠劵";
		};	
	}
	public function couponUpdate($table,$column,$checkColumn){
		if($this->couponExist){	
			return false ; //"找不到優惠劵";
		}else{
			return $this->DB_Update($table,$column,$checkColumn);
		};
	}
	public function delete($sql){
		if($this->couponExist){
			return false ;//"找不到優惠劵";
		}else{		
			$this->exec($sql);
			return true ;
		}
	}

}

?>