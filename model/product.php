<?php  
include("DB.php");

class Product extends DB{
	public $productNo ;
	public $productModelId;
	public $productName ;
	public $productSubtitle ;
	public $productPromote ;
	public $productDescribe ;
	public $productContent ;
	public $productNote ;
	public $productStatus ;
	public $img1 ;
	public $img2 ;
	public $img3 ;
	public $img4 ;
	public $img5 ;
	public $productUpdateTime ;
	

	public $productPrice1 ;
	public $productPrice2 ;
	public $productPrice3 ;

	// // 與商品分類的關聯
	// public $productRelateCate_no;
	// public $productRelateCate_name;
	// public $productRelateCate_level;

	public $productExist ;



	// =====initial 找出該商品的 資訊==========
	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
				$this->productNo = $value["product_no"];
				$this->productName = $value["product_name"];
				$this->productStatus = $value["product_status"];
				$this->productModelId = $value["model_id"];
				$this->productSubtitle = $value["product_subtitle"];
				$this->productPromote = $value["product_promote"];
				$this->productDescribe = $value["product_describe"];
				$this->productContent = $value["product_content"];
				$this->productNote = $value["product_note"];
				$this->img1 = $value["img1"];
				$this->img2 = $value["img2"];
				$this->img3 = $value["img3"];
				$this->img4 = $value["img4"];
				$this->img5 = $value["img5"];
				$this->productUpdateTime = $value["product_updatetime"];
			}
		}else{
			$this->productExist = "1"; //找不到商品
		}
	}

	public function productInfo(){
		if(!$this->productExist){ //有產品
			$item = array(
				"product_no" => $this->productNo ,
				"product_name" => $this->productName ,
				"product_status" => $this->productStatus ,
				"model_id" => $this->productModelId ,
				"product_subtitle" => $this->productSubtitle ,
				"product_promote" => $this->productPromote ,
				"product_describe" => $this->productDescribe ,
				"product_content" => $this->productContent ,
				"product_note" => $this->productNote ,
				"img1" => $this->img1 ,
				"img2" => $this->img2 ,
				"img3" => $this->img3 ,
				"img4" => $this->img4 ,
				"img5" => $this->img5 ,
				"product_updatetime" => $this->productUpdateTime
			); 
			return $item ;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}

	public function productSpecInfo(){
		if(!$this->productExist){ //有產品
			$sql = "select * from product_spec where product_no='".$this->productNo."'";
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}

	public function productSpecUseInfo(){
		if(!$this->productExist){ //有產品
			$sql = "select * from product_spec where product_spec_status = 1 and product_no='".$this->productNo."' order by product_spec_price1 asc";
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}
 
	public function productRelateCate(){  //搜尋商品的供應商和品牌
		if(!$this->productExist){ //有產品
			$sql = "select * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$this->productNo."' and b.cate_parents <> 63 and b.cate_parents <> 61";
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}
	public function productRelateCateMonth(){ //找本月推薦分類
		if(!$this->productExist){ //有產品
			$sql = "select * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$this->productNo."' and b.cate_no = 90 ";
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}
	public function productRelateCateActivity(){ //找尋所有行銷活動類的分類
		if(!$this->productExist){ //有產品
			$sql = "select * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$this->productNo."' and cate_parents = 63  "; //and b.cate_no <> 90
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}
	public function productCombo() { //找加購價
		if(!$this->productExist){ //有加購價
			$sql = "SELECT c.*, p.model_id, p.product_name, p.product_status, s.product_spec_info, s.product_stock, s.product_spec_price1, s.product_spec_price2, s.product_spec_price3, s.product_spec_status FROM product_combo c INNER JOIN product p ON c.product_no = p.product_no INNER JOIN product_spec s ON c.product_spec_no = s.product_spec_no WHERE rel_product_no='".$this->productNo."'" ;
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有加購價
			return false ;//"找不到加購價";
		}	
	}

	public function cateParents($cateParents) {
		$sql = "select * from category where cate_no='".$cateParents."'";
		$result = $this->DB_Query($sql);
		return $result;
	}


	public function productRelateTag(){
		if(!$this->productExist){ //有產品
			$sql = "select * from tag_products_relate a JOIN tag b on a.tag_no = b.tag_no where a.product_no ='".$this->productNo."'";
			$result = $this->DB_Query($sql);
			return $result;
		}else{ //沒有商品
			return false ;//"找不到商品";
		}	
	}


	public function editProduct($table,$data,$checkColumn){
		if(!$this->productExist){ //產品資訊更新
			return $result = $this->DB_UpdateOnly($table,$data,$checkColumn);
		}else{ //無產品
			return false;
		}
	}

	public function productUpdateTag($table,$data,$checkColumn,$tagNo){  //加入標籤或是新增
		if(!$this->productExist){ //產品新增標籤
			// ======判斷tag_product_relate 是否存在====
			$sql = "select * from tag_products_relate a JOIN tag b on a.tag_no = b.tag_no where a.product_no ='".$this->productNo."' and a.tag_no = '".$tagNo."'";
			if ($this->DB_Query($sql)){
				return "have" ;//echo '有存在標籤與產品的關係';
			}else{   //沒有存在標籤與產品的關係  新增至資料庫
				$this->DB_Update($table,$data,$checkColumn); //echo '沒有';
				// =====儲存後回傳值=====
				$sql =  "select * from tag_products_relate a JOIN tag b on a.tag_no = b.tag_no where a.product_no ='".$this->productNo."' and a.tag_no = '".$tagNo."'";
				return $this->DB_Query($sql);
			}
		}else{ //無產品
			return false;
		}
	}

	public function productUpdateCate($table,$data,$checkColumn,$cateNo){  //加入分類或是新增
		if(!$this->productExist){ //產品新增標籤
			// ======判斷cate_product_relate 是否存在====
			$sql = "select * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$this->productNo."' and a.cate_no = '".$cateNo."'";
			if($this->DB_Query($sql)){ 
				return "have" ; //echo "有存在關係";
			}else{   //沒有存在分類與產品的關係  新增至資料庫
				$this->DB_Update($table,$data,$checkColumn); //echo "沒有";
				// =====儲存後回傳值=====
				$sql =  "select * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$this->productNo."' and a.cate_no = '".$cateNo."'";
				return $this->DB_Query($sql);
			}
		}else{ //無產品
			return false;
		}
	}

	// ========product_spec =======
	public function productUpdateSpec($table,$data,$checkColumn){
		if(!$this->productExist){
			return $this->DB_Update($table,$data,$checkColumn);
		}else{//無產品
			return false ;
		}
	}
	public function productUpdateSpecQuery(){
		if(!$this->productExist){
			$sql = "select * from product_spec where product_spec_no='".$this->lastId."'";
			return $this->DB_Query($sql);
		}else{//無產品
			return false ;
		}
	}
	// ========product_spec =======


	public function delete($sql){
		if(!$this->productExist){ //產品資訊刪除
			$this->exec($sql);
			return true ;
		}else{ //無產品
			return false;
		}
	}


}

?>