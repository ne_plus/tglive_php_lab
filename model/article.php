<?php  
include("DB.php");

class Article extends DB{
	public $articleNo;
	public $articleTitle;
	public $articleContent;
	public $articleDescribe;
	public $articleImg;
	public $articleOwner;
	public $articleStatus;
	public $articleCreatetime;

	public $articleExist ;


	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
				$this->articleNo = $value["article_no"];
				$this->articleTitle = $value["article_title"];
				$this->articleContent = $value["article_content"];
				$this->articleDescribe = $value["article_describe"];
				$this->articleImg = $value["article_img"];
				$this->articleOwner = $value["article_owner"];
				$this->articleStatus = $value["article_status"];
				$this->articleCreatetime = $value["article_createtime"];
			}
		}else{
			$this->articleExist = "1"; //找不到文章
		}
		

	}
	public function articleQuery(){  //單筆文章內容
		if($this->articleExist){
			return false ;//"找不到文章";
		}else{
			$result = array(
				"article_no"=> $this->articleNo,
				"article_title"=> $this->articleTitle,
				"article_content"=> $this->articleContent,
				"article_describe"=> $this->articleDescribe,
				"article_img"=> $this->articleImg,
				"article_owner"=> $this->articleOwner,
				"article_status"=> $this->articleStatus,
				"article_createtime"=> $this->articleCreatetime,
			);
			return $result;
		};		
	}
	public function articleCreate($table,$column){ //新增文章(不偵測是否有重複的標題)
		if($this->DB_Insert($table,$column)){ //TRUE
			return $this->lastId;
		}else{
			return false;
		}
	}
	public function articleUpdate($table,$column,$checkColumn){ //文章編輯
		if($this->articleExist){	
			return false ; //"找不到文章";
		}else{
			return $this->DB_Update($table,$column,$checkColumn);
		};
	}
	public function delete($sql){ //文章刪除
		if($this->articleExist){ 
			return false ;//"找不到文章";
		}else{		
			$this->exec($sql);
			return true ;
		}
	}

}

?>