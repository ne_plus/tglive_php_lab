<?php  
include('DB.php');

date_default_timezone_set("Asia/Taipei");

class USER extends DB{  //預設僅有單一會員 增刪改查
	public $userMail ;
	public $userId;
	public $userVertifyCode ;
	public $userTel ;
	public $userFirstname;
	public $userLastname;
	public $userUpdateTime;
	public $userCreateTime;
	public $userStatus;

	public $admAuthLevel;
	public $admName ;

	public $notMember; //確認是否已經有存在帳號

	private $userPsw;

	public function __construct($sql,$dic=null,$table) {
		// =====連接 DB========
		parent::__construct();
		$result = parent::DB_Query($sql,$dic);
		if(count($result)!=0){
			if($table=="admin"){ //<====管理員
				foreach ($result as $key => $value) {
					$this->userMail = $value["adm_email"];
					$this->userId = $value["adm_no"] ;
					$this->admName = $value["adm_name"];
					$this->userStatus = $value["adm_status"];
					$this->admAuthLevel = $value["adm_authLevel"];
					$this->userPsw = $value["adm_password"];
					$this->userCreateTime = $value["adm_createtime"];
				}
			}elseif($table=="member"){ //<====會員
				foreach ($result as $key => $value) {
					$this->userMail = $value["mem_mail"];
					$this->userId = $value["mem_no"];
					$this->userFirstname = $value["mem_firstname"];
					$this->userLastname = $value["mem_lastname"];
					$this->userVertifyCode = $value["mem_vertifycode"];
					$this->userTel = $value["mem_tel"];
					$this->userStatus = $value["mem_status"];
					$this->userPsw = $value["mem_password"];
					$this->userVertifyCode = md5($value["mem_no"].$value["mem_mail"]);
				}
			}		
		}else{
			$this->notMember ="1";  //1不是會員 
		}
	}
	
// =======>isMember(email/vertifyCode) 
	public function isMember(){
		if($this->notMember){
			return false;
		}else{
			if($this->admAuthLevel){ //<===管理者
				$admin = array(
					"adm_no" => $this->userId,
					"adm_email" => $this->userMail,
					"adm_name" => $this->admName,
					"adm_authLevel" => $this->admAuthLevel,
					"adm_status" => $this->userStatus,
					"adm_createtime" => date("Y-m-d H:i:s",$this->userCreateTime)
					);
				return $admin;
			}else{ //<===會員
				$member = array(
					"mem_no" => $this->userId,
					"mem_mail" => $this->userMail,
					"mem_firstname" => $this->userFirstname,
					"mem_lastname" => $this->userLastname,
					"mem_tel" => $this->userTel,
					"mem_status" =>$this->userStatus,
					"mem_vertifycode" => $this->userVertifyCode
					);
				return $member ;
			}	
		}	
	}
// =======>新會員get資訊
	public function getNewMemberLastInsertId(){
		if($this->notMember){
			// ====新註冊會員建立vertifyCode======			
			return $this->lastId;
		}else{
			return false ;
		}
	}
// 

// =======>getMemberVertityCode(mem_no/email)
	public function getMemberVertityCode($mail){
		if($this->notMember){
			// ====新註冊會員建立vertifyCode======			
			return md5($this->lastId.$mail);
		}else{
			return $this->userVertifyCode ;
		}
	}
// =======>adduser() 
	public function adduser($table,$column,$checkColumn){
		if($this->notMember){
			if($table == "admin"){
				return parent::DB_Update($table,$column,$checkColumn);
			}elseif($table == "member"){
				return parent::DB_Update($table,$column,$checkColumn);
			} 	
		}else{ //<==========已經是會員
			return false;
		}
		
	}
// =======>edituser() 
	public function edituser($table,$column,$checkColumn){  
		if($this->notMember){ //新註冊會員
			$result = parent::DB_Update($table,$column,$checkColumn);
		}else{  //admin/member共用
			// ====更新資訊======
			$result = parent::DB_Update($table,$column,$checkColumn);
		}
		
		// ====reset AutoIncrease======
		if($table == "admin"){
			$Id = $this->DB_Query("select adm_no from ".$table." order by adm_no desc");	
		}elseif($table == "member"){
			$Id = $this->DB_Query("select mem_no from ".$table." order by mem_no desc");	
		}
		$AINew = implode($Id[0])+1 ;
		$this->exec("ALTER TABLE ".$table." AUTO_INCREMENT = ".$AINew);
		return $result;  			
		
	}
// =======>deleteuser()
	public function deleteuser(){}

} //<==================== class USER end ====================>

?>