<?php  
include("DB.php");

class TAG extends DB{
	public $tagNo=[];
	public $tagName=[];
	public $tagSort=[];
	public $tagUpdateTime=[];

	public $tagExist ;


	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
			array_push($this->tagNo, $value["tag_no"]);
			array_push($this->tagName, $value["tag_name"]);
			array_push($this->tagSort, $value["tag_sort"]);
			array_push($this->tagUpdateTime, $value["tag_updatetime"]);
			}
		}else{
			$this->tagExist = "1"; //找不到標籤
		}
		

	}
	public function tagQuery(){ 
		if($this->tagExist){
			return false ;//"找不到標籤";
		}else{
			$result = array(
				"tag_no"=> $this->tagNo,
				"tag_name"=> $this->tagName,
				"tag_sort"=> $this->tagSort,
				"tag_updatetime"=> $this->tagUpdateTime
			);
			return $result;
		};		
	}
	public function tagCreate($table,$column,$checkColumn){
		if($this->tagExist){
			return $this->DB_Update($table,$column,$checkColumn);
		}else{
			return false ; //"已經存在標籤";
		};	
	}
	public function tagUpdate($table,$column,$checkColumn){
		if($this->tagExist){	
			return false ; //"找不到標籤";
		}else{
			return $this->DB_Update($table,$column,$checkColumn);
		};
	}
	public function delete($sql){
		if($this->tagExist){
			return false ;//"找不到標籤";
		}else{		
			$this->exec($sql);
			return true ;
		}
	}

}

?>