<?php  
include("DB.php");

class Order extends DB{
	// ----order_item----
	public $order_no;
	public $mem_no;
	public $cate_no;
	public $cate_name;
	public $order_group;
	public $order_email;
	public $order_recipient;
	public $order_address;
	public $order_tel;
	public $order_price;
	public $order_cargo;
	// public $coupon_no;
	// public $coupon_code;
	// public $order_discount;
	public $order_pay;
	public $order_pay_status;
	public $order_status;
	public $order_delivery;
	public $order_createtime;
	public $order_updatetime;

	public $order_memo;
	public $admin_memo;
	public $order_vat;
	public $order_uni_no;


	public $orderExist ;


	public function __construct($sql,$dic=null){
		parent::__construct();
		$result = $this->DB_Query($sql,$dic);
		if(count($result)!=0){
			foreach ($result as $key => $value) {
				$this->order_no = $value["order_no"];
				$this->mem_no = $value["mem_no"];
				$this->cate_no = $value["cate_no"];
				$this->cate_name = $value["cate_name"];
				$this->order_group = $value["order_group"];
				$this->order_email = $value["order_email"];
				$this->order_recipient = $value["order_recipient"];
				$this->order_address = $value["order_address"];
				$this->order_tel = $value["order_tel"];
				$this->order_price = $value["order_price"];
				$this->order_cargo = $value["order_cargo"];
				// $this->coupon_no = $value["coupon_no"];
				// $this->coupon_code = $value["coupon_code"];
				// $this->order_discount = $value["order_discount"];
				$this->order_pay = $value["order_pay"];
				$this->order_pay_status = $value["order_pay_status"];
				$this->order_status = $value["order_status"];
				$this->order_delivery = $value["order_delivery"];
				$this->order_createtime = $value["order_createtime"];
				$this->order_updatetime = $value["order_updatetime"];

				$this->order_memo = $value["order_memo"];
				$this->admin_memo = $value["admin_memo"];
				$this->order_vat = $value["order_vat"];
				$this->order_uni_no = $value["order_uni_no"];
			}
		}else{
			$this->orderExist = "1"; //找不到訂單
		}
		

	}
	public function orderQuery(){ 
		if($this->orderExist){
			return false ;//"找不到訂單";
		}else{
			$result = array(
				"order_no" => $this->order_no,
				"mem_no" => $this->mem_no,
				"cate_no" => $this->cate_no,
				"cate_name" => $this->cate_name,
				"order_group" => $this->order_group,
				"order_email" => $this->order_email,
				"order_recipient" => $this->order_recipient,
				"order_address" => $this->order_address, 
				"order_tel" => $this->order_tel,
				"order_price" => $this->order_price,
				"order_cargo" => $this->order_cargo,
				// "coupon_no" => $this->coupon_no,
				// "coupon_code" => $this->coupon_code,
				// "order_discount" => $this->order_discount,
				"order_pay" => $this->order_pay,
				"order_pay_status" => $this->order_pay_status,
				"order_status" => $this->order_status,
				"order_delivery" => $this->order_delivery,
				"order_createtime" => $this->order_createtime,
				"order_updatetime" => $this->order_updatetime,

				"order_memo" => $this->order_memo,
				"admin_memo" => $this->admin_memo,
				"order_vat" => $this->order_vat,
				"order_uni_no" => $this->order_uni_no
			);
			return $result;
		};		
	}

	public function orderDetailQuery($orderNo){
		if($this->orderExist){ //沒有存在訂單
			return false;
		}else{
			$sql = "select * from order_detail where order_no ='".$orderNo."'";
			$result = $this->DB_Query($sql);
			return $result ;
		}
	}




	public function orderCreate($item,$cateValue){
		try{
			//啟動交易系統
			$this->pdo->beginTransaction(); 
			//寫入主要檔案
			$sql = "insert into order_item (".implode(',',array_keys($item)).") values(:".implode(',:',array_keys($item)).")";
			$order = $this->pdo->prepare($sql);	
			foreach ($item as $key => $value) {
				$order ->bindValue(":".$key,$value);
			}
			$result = $order -> execute();
			$this->lastId = $this->pdo->lastInsertId();
			//寫入明細 //從session寫入
			
			$sqlDetail = "insert into order_detail (order_detail_no,order_no,product_no,cate_no,cate_name,product_spec,product_spec_no,order_detail_quantity,order_detail_price,coupon_no,coupon_name,coupon_code,coupon_discount,order_detail_price_discount,order_detail_additional) values(null,:order_no,:product_no,:cate_no,:cate_name,:product_spec,:product_spec_no,:order_detail_quantity,:order_detail_price,:coupon_no,:coupon_name,:coupon_code,:coupon_discount,:order_detail_price_discount,:order_detail_additional)" ;

			$prdocutCount=[];
			$orderDetail = $this->pdo->prepare($sqlDetail);
			foreach ($_SESSION["orders"] as $productKey => $productValue) {
				foreach ($productValue as $key => $value) {
					$couponNo = $value["coupon_no"] ==""? null : $value["coupon_no"];
					$couponName = $value["coupon_name"] ==""? null : $value["coupon_name"];
					$couponCode = $value["coupon_code"] =="" ? null : $value["coupon_code"];
					$discount = $value["coupon_discount"]=="" ? null : (100-(int)$value["coupon_discount"])/100;
					$orderPriceDis = $value["order_price_discount"] ==""? null : $value["order_price_discount"];
					if( $value["vendor_no"] == $cateValue){
						$orderDetail ->bindValue(":order_no", $this->lastId);
						$orderDetail ->bindValue(":product_no", $value['product_no']);
						$orderDetail ->bindValue(":cate_no", $value['cate_no']);
						$orderDetail ->bindValue(":cate_name", $value['cate_name']);
						$orderDetail ->bindValue(":product_spec", $value["product_spec_info"]);
						$orderDetail ->bindValue(":product_spec_no", $value["product_spec_no"]);
						$orderDetail ->bindValue(":order_detail_quantity", $value["quanty"]);
						$orderDetail ->bindValue(":order_detail_price", $value["price"]);
						$orderDetail ->bindValue(":coupon_no", $couponNo);
						$orderDetail ->bindValue(":coupon_name", $couponName);
						$orderDetail ->bindValue(":coupon_code", $couponCode);
						$orderDetail ->bindValue(":coupon_discount", $discount);
						$orderDetail ->bindValue(":order_detail_price_discount", $orderPriceDis);
						$orderDetail ->bindValue(":order_detail_additional", null);
						$orderDetail->execute();
						array_push($prdocutCount,$key);
						
						// 加價購
						// $orderComboList = [];
						// if(isset($_COOKIE["comboorders"])){ //有存在加價購清單
						// 	$comboorders = json_decode($_COOKIE["comboorders"], true);
						// 	foreach ($comboorders as $orderskey => $ordersvalue) {
						// 		if (!in_array($orderskey, $orderComboList) && $ordersvalue["quanty"] >= 1)
						// 			array_push($orderComboList, $orderskey);
						// 	}
						// }
						
						// $sql = sprintf("SELECT c.*, p.model_id, p.product_name, p.img1, s.product_spec_info FROM product_combo c INNER JOIN product p ON c.product_no = p.product_no INNER JOIN product_spec s ON c.product_spec_no = s.product_spec_no WHERE c.rel_product_no= %d AND NOW() BETWEEN c.starttime AND c.endtime AND c.product_combo_status = 1 AND c.isdeleted = 0 AND p.product_status = 1 AND s.product_stock >= 1 AND s.product_spec_status = 1", $value['product_no']);
						// $comboProducts = $this->DB_Query($sql);
						// foreach ($comboProducts as $key => $value) {
						// 	// 子類別
						// 	$sql = sprintf("SELECT c1.cate_name, c1.cate_no, c2.cate_name parent_name, c2.cate_no vendor_no FROM category c1 LEFT OUTER JOIN category c2 ON c1.cate_parents = c2.cate_no where c1.cate_no in (SELECT cate_no FROM category_products_relate where product_no = %d) AND c1.cate_level = 2", $value['product_no']);
						// 	$result2 = $this->DB_Query($sql);
						// 	if ($result2 && count($result2) >= 1) {
						// 		$comboProducts[$key]["cate_no"] = $result2[0]["cate_no"];
						// 		$comboProducts[$key]["cate_name"] = $result2[0]["cate_name"];
						// 		$comboProducts[$key]["vendor_no"] = $result2[0]["vendor_no"];
						// 		$comboProducts[$key]["parent_name"] = $result2[0]["parent_name"];
						// 	}
						// }
						// if ($comboProducts && count($comboProducts) >= 1) {
						// 	foreach ($comboProducts as $key2 => $value2) {
						// 		if (in_array($value2["id"], $orderComboList)) {
						// 			//if( $value2["vendor_no"] == $cateValue){
						// 				$orderDetail ->bindValue(":order_no", $this->lastId);
						// 				$orderDetail ->bindValue(":product_no", $value2['product_no']);
						// 				$orderDetail ->bindValue(":cate_no", $value2['cate_no']);
						// 				$orderDetail ->bindValue(":cate_name", $value2['cate_name']);
						// 				$orderDetail ->bindValue(":product_spec", $value2["product_spec_info"]);
						// 				$orderDetail ->bindValue(":product_spec_no", $value2["product_spec_no"]);
						// 				$orderDetail ->bindValue(":order_detail_quantity", $comboorders[$value2["id"]]['quanty']);
						// 				$orderDetail ->bindValue(":order_detail_price", (int)$value2["product_spec_price"]);
						// 				$orderDetail ->bindValue(":coupon_no", null);
						// 				$orderDetail ->bindValue(":coupon_name", null);
						// 				$orderDetail ->bindValue(":coupon_code", null);
						// 				$orderDetail ->bindValue(":coupon_discount", null);
						// 				$orderDetail ->bindValue(":order_detail_price_discount", null);
						// 				$orderDetail ->bindValue(":order_detail_additional", 1);
						// 				$orderDetail->execute();
						// 				array_push($prdocutCount,$key);
						// 			//}
						// 		}
						// 	}
						// }
					}
				}
			}

			// 加購商品
			if(isset($_SESSION["orderCombos"])){
				foreach ($_SESSION["orderCombos"] as $comboKey => $comboValue){
					if($comboKey == $cateValue) {
						foreach($comboValue as $comboKey2 => $comboValue2){
							$orderDetail ->bindValue(":order_no", $this->lastId);
							$orderDetail ->bindValue(":product_no", $comboValue2['product_no']);
							$orderDetail ->bindValue(":cate_no", $comboValue2["cate_no"]);
							$orderDetail ->bindValue(":cate_name", $comboValue2["cate_name"]);
							$orderDetail ->bindValue(":product_spec", $comboValue2["spec_info"]);
							$orderDetail ->bindValue(":product_spec_no", $comboValue2["spec_no"]);
							$orderDetail ->bindValue(":order_detail_quantity", $comboValue2['quanty']);
							$orderDetail ->bindValue(":order_detail_price", $comboValue2["price"]);
							$orderDetail ->bindValue(":coupon_no", null);
							$orderDetail ->bindValue(":coupon_name", null);
							$orderDetail ->bindValue(":coupon_code", null);
							$orderDetail ->bindValue(":coupon_discount", null);
							$orderDetail ->bindValue(":order_detail_price_discount", null);
							$orderDetail ->bindValue(":order_detail_additional", 1);
							$orderDetail->execute();
							array_push($prdocutCount,$key);
						}	
					}
				}
			}
			$this->pdo->commit();

			return true;

		}catch(PDOException $e){
			$this->pdo->rollBack();
	      	echo "錯誤行號 : ", $e->getLine(), "<br>";
	      	echo "錯誤訊息 : ", $e->getMessage(), "<br>";
	      	// return false;
		}			
	}
	public function orderUpdate($table,$column,$checkColumn){
		if($this->orderExist){	
			return false ; //"找不到訂單";
		}else{
			return $this->DB_UpdateOnly($table,$column,$checkColumn);
		};
	}
	public function delete($sql){
		if($this->orderExist){
			return false ;//"找不到優惠劵";
		}else{		
			$this->exec($sql);
			return true ;
		}
	}

}

?>