<?php require_once("module/header.php"); ?>

	<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_footer_contacts?>
		</ol>
		<div class="container">
		<div class="row">
		<h3><?=$lang_footer_contacts?></h3>
		<p>如需與TGiLive居生活團隊聯繫或有其他網站上沒有列出的問題，歡迎您利用以下方式與我們聯繫！</p>


<div class="subtitle">會員問題/客戶服務</div>
<a href="mailto:info@tgilive.com" target="_blank">info@tgilive.com</a>

<div class="subtitle">TGiLive居生活粉絲團私訊</div>
<a href="https://www.facebook.com/TGiLiveTW/" target="_blank">https://www.facebook.com/TGiLiveTW/</a>

<div class="subtitle">合作提案</div>
<p>感謝您對的TGiLive居生活支持與讚賞，也歡迎您加入我們合作廠商或媒體合作的行列。<br/>
請將您的行銷合作提案或業務合作之商品資訊介紹、圖片與貴公司簡介和聯繫方式，寄到<a href="mailto:info@tgilive.com" target="_blank">info@tgilive.com</a>   信件主旨：業務合作（或行銷合作）<br/>
業務與行銷單位收到您的資訊後，約1-2周內進行評估並進行回覆與聯繫，謝謝。</p>


		

				
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>