<?php require_once("module/header.php"); ?>
<script src="js/productsServer.js"></script>
<?php 

// PHP FORM 程式(排序、標籤篩選)
// include('Frontframwork/products_form.php');
include("class/tagClass.php");
include("class/productClass.php");

$tag = new tagObj();

if(isset($_REQUEST['tag_no']) == false){ //沒值
	$tagArray = null;
}else{
	$tagArray =[];
	foreach ($_REQUEST['tag_no'] as $key => $value) {
		array_push($tagArray,$value);
	}
}
$productStatus =1;
$isPagenation = 1 ;
$priceFilter = isset($_REQUEST['pricefilter'])? $_REQUEST['pricefilter'] : null ;
$sort = isset($_REQUEST['sort'])? $_REQUEST['sort'] : null ;
$page = isset($_REQUEST['pageNo'])? $_REQUEST['pageNo'] : null;
$getProducts = $tag->getProductsByTagID( $tagArray, $without=null, $productStatus , $sort , $priceFilter , $page , $isPagenation , $recPerPage=40 );

//// PHP FORM END


//標籤
// 分類搜尋
$tagCateAllSql = "SELECT * FROM `category` WHERE cate_parents = 61 and ( cate_name != '優惠劵' and cate_no != 189 )" ;
$tagCateAllResult = $db->DB_Query($tagCateAllSql);

$tagGroup = [];
foreach ($tagCateAllResult as $key => $value) {
	$tagSqltest ="select * from tag_products_relate a join tag b on a.tag_no = b.tag_no left join category_tag_relate c on b.tag_no = c.tag_no where c.cate_no = ".$value["cate_no"]." group by a.tag_no"; //找分類為找心情的標籤
	$tagResulttest = $db->DB_Query($tagSqltest);
	$tagGroup[$value["cate_no"]]["cate_name"] = $value["cate_name"];
	$tagGroup[$value["cate_no"]]["tag_info"] = $tagResulttest;
}

//行銷活動
$activitySql="select * from category where cate_parents = 63 and cate_level = 1 order by cate_sort asc";
$activityResult = $db->DB_Query($activitySql);


// 人氣商品 
$popularCateNo = 183; //cateNo
$popularCateProducts = get_activityId_queryProducts($popularCateNo,5);
?>

	<section class="productshow">
					<ol class="breadcrumb">
						  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
						  <li class="breadcrumb-item active"><?=$lang_menu_findProducts?></li>
					</ol>
		<div class="container mt-65">
			<div class="row">
				<div class="col-12 col-md-3">
			        <div class="sidebar">
						<?php if(count($popularCateProducts)>=1){ ?>
			            <div class="sidebar_block catagory mb-30">
			                <span class="sidebar_title"><?=$lang_product_hot?></span>
			                <div class="sidebar_desc form-check">
			                	<?php foreach ($popularCateProducts as $popularkey => $popularvalue){
			                	?>	
				                <div class="promotBox">
									<a class="d-flex align-items-center" href="product-detail.php?product_no=<?=$popularvalue['product_no'];?>" >
										<img src="<?php echo $popularvalue['img1']; ?>" class="img-fluid mr-1" style="width:30px;">
										<span class="ml-1 text-left"><?php echo $popularvalue['product_name']; ?></span>
									</a>
				                </div>  
								<?php } ?>
			                		
			                </div>
						</div>
						<?php } ?>
						
						<!-- <div class="sidebar_block priceSearch mb-30">
			                <span class="sidebar_title"><?=$lang_product_hot?></span>
							<div class="sidebar_desc form-check">
								<?php
									$maxHot = 5;
									$currentHot = 0;
									$sql = sprintf("SELECT product_no, SUM(times) counts FROM product_views GROUP BY product_no ORDER BY counts DESC");
									$result = $db -> DB_Query($sql);
									if( count($result) != 0 ){
										foreach ($result as $key => $value) {
											$sql = "SELECT * FROM product where product_no = :product_no";
											$dic = array(":product_no"=>$value["product_no"]);
											$result2 = $db -> DB_Query($sql,$dic);
											if( count($result2) != 0 ){
												echo '<label class="form-check-label d-block">';
												echo '<a href="product-detail.php?product_no=' . $value["product_no"] . '">';
												echo $result2[0]["product_name"];
												echo '</a>';
												echo '</label>';
												$currentHot++;
											}
											if ($currentHot >= $maxHot) break;
										}
									}
								?>
			                </div>
			            </div> -->
						
						<?php  foreach ($tagGroup as $tagGroupkey => $tagGroupvalue) {
						?>
						<div class="sidebar_block tagSearch mb-30">
			                <span class="sidebar_title"><?php echo $tagGroupvalue["cate_name"]; ?></span>
			                <div class="sidebar_desc form-check">	
				                <?php
			                		foreach ($tagGroupvalue["tag_info"] as $tagKey => $tagValue) {
			                	?>
			                	 <label class="form-check-label d-block">
			                	 	<!-- ====PHP FORM=== -->
			                	 	 <input type="checkbox" class="form-check-input tagSelector" value="" <?php if(isset($_REQUEST["tag_no"]) === true){foreach ($_REQUEST["tag_no"] as $key => $value) {if($value == $tagValue['tag_no']){echo "checked" ;}}} ?>><?php echo $tagValue["tag_name"]; ?><input type="hidden" name="tag_no[]" value="<?php echo $tagValue['tag_no']; ?>"> 
				                 </label>

			                	<?php
			                		}
			                	?>
			                	
			                </div>
			            </div>
						<?php }//----end foreach ?>
						
						<div class="sidebar_block priceSearch mb-30">
			                <span class="sidebar_title"><?=$lang_product_filter_price?></span>
			                <div class="sidebar_desc form-check">
			                	<label class="form-check-label d-block">
				                  <input type="checkbox" class="form-check-input" value="500" <?php if(isset($_GET['pricefilter'])===true){if($_GET['pricefilter']=='500'){echo 'checked';}} ?>><?=$lang_product_filter_price_1?></label>
			                    <label class="form-check-label d-block">
				                  <input type="checkbox" class="form-check-input" value="500to1000" <?php if(isset($_GET['pricefilter'])===true){if($_GET['pricefilter']=='500to1000'){echo 'checked';}} ?>><?=$lang_product_filter_price_2?></label>
				                 <label class="form-check-label d-block">
				                  <input type="checkbox" class="form-check-input" value="1000to2000" <?php if(isset($_GET['pricefilter'])===true){if($_GET['pricefilter']=='1000to2000'){echo 'checked';}} ?>><?=$lang_product_filter_price_3?></label>
				                <label class="form-check-label d-block">
				                  <input type="checkbox" class="form-check-input" value="2000to3000" <?php if(isset($_GET['pricefilter'])===true){if($_GET['pricefilter']=='2000to3000'){echo 'checked';}} ?>><?=$lang_product_filter_price_4?></label>
				                <label class="form-check-label d-block">
				                  <input type="checkbox" class="form-check-input" value="3000" <?php if(isset($_GET['pricefilter'])===true){if($_GET['pricefilter']=='3000'){echo 'checked';}} ?>><?=$lang_product_filter_price_5?></label>

				                  <form id="priceFilter" action="products.php" method=get>
				                  <?php  
				                  	if(isset($_REQUEST['pricefilter']) === true ){
				                  ?>
				                    <input type="hidden" name="pricefilter" value="<?php echo $_REQUEST['pricefilter']; ?>">
				                  <?php 		
				                  	}
				                  ?>
				                  </form>
			                </div>
			            </div>

						<form id="tagFilter" action="products.php" method=get>
	                	<?php 
	                	if(isset($_REQUEST["tag_no"]) === true){
	                		foreach ($_REQUEST["tag_no"] as $key => $value) {
	                	?>
	                		<input type="hidden" name="tag_no[]" value="<?php echo $value; ?>">
	                	<?php		
	                		} 
	                	}
	                	
	                	?>
						</form>

						<form id="tagMixPriceFilter" action="products.php" method=get></form>

			        </div>
			    </div>

				<div class="col-12 col-md-9">
                    <div class="shop_grid_product_area">
                        <div class="shop_top_sidebar_area mb-30">
                            <div class="search_by_terms text-right">
                                <select id="priceSortSelect" class="custom-select widget-title form-control-sm">
                                  <option value="time" <?php if(isset($_GET["sort"]) === true){if($_GET["sort"] == "time"){echo "selected";}}?>><?=$lang_product_sort_time?></option>
                                  <option value="pricetop" <?php if(isset($_GET["sort"]) === true){if($_GET["sort"] == "pricetop"){echo "selected";}}?>><?=$lang_product_sort_pricetop?></option>
                                  <option value="pricelow" <?php if(isset($_GET["sort"]) === true){if($_GET["sort"] == "pricelow"){echo "selected";}}?>><?=$lang_product_sort_pricelow?></option>
                                </select>
                                <form id="priceSort" action="products.php" method=get>
				                  	<input type="hidden" name="sort" value="pricetop">
				                </form>
                            </div>
                        </div>
                        <div class="row">
							<?php 
							if(!isset($getProducts["products"])){
							?>
							<div class="product-list col-12 mb-30">
								<div class="alert alert-danger text-center" role="alert">
								  <?=$$lang_product_notfound?>
								</div>
							</div>
							<?php	
							}else{ 
								foreach ($getProducts["products"] as $productkey => $productvalue) {
									$priceTop = "NT";
									$priceLow = "NT";	
									if( trim($productvalue["lowPrice"]) == null ){
										$priceLow .= $productvalue["highPrice"];
										$priceTop = "";
									}else{
										$priceLow .= $productvalue["lowPrice"];
										$priceTop .= $productvalue["highPrice"];
									}	
							?>
							
								<div class="product-items">
									<form action="product-detail.php" method="get">
									  		<div class="content-hover">
										  		<img src="<?php echo $productvalue['product_img'][0]; ?>" class="img-fluid">
											  	<div class="block">
									  				<div class="info text-center d-flex align-items-center justify-content-center">
									  					<div class="product-name" style="font-size: 14px;"><?php echo $productvalue['product_name'];?></div>
									  				</div>
									  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
									  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
									  				</div>
									  			</div>
									  			<div class="price text-center">
													<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo $priceLow; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo $priceTop; ?></div>
								  				</div>
							  				</div>
							  				<input type="hidden" name="product_no" value="<?php echo $productvalue['product_no'] ;?>">
									  </form>
								  </div>
							
							<?php
								} //--- end for loop
							} //-----end if( count($data) )	
							?>

								  

                            
                        </div>
                    </div>

                    <div class="shop_pagination_area">
                    	<?php 
							if(strpos($_SERVER['QUERY_STRING'],'pageNo='.$getProducts["page"]["pageNo"])){
								// echo "有分頁";
								$path = str_replace('&pageNo='.$getProducts["page"]["pageNo"],'',$_SERVER['QUERY_STRING']);
							}else{
								// echo "沒有"; 
								$path = $_SERVER['QUERY_STRING'];
							} 
						?>
                        <nav aria-label="Page navigation example">
						  <ul class="pagination justify-content-center">
						    <li class="page-item <?php if($getProducts["page"]["pageNo"] == 1 ){echo 'disabled';} ?>">
						      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($getProducts["page"]["pageNo"]-1); ?>" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						        <span class="sr-only">Previous</span>
						      </a>
						    </li>
						    <?php 
						    		if($getProducts["page"]["totalPage"] > 7){ //頁數大於 7 
						    			if($getProducts["page"]["pageNo"]<5){ 
						    				for($p=1 ; $p<=5 ; $p++){
						    					if( $p == $getProducts["page"]["pageNo"]){ //當頁
							?>	
										<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php
												}else{
							?>
										<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php					
												}	    					
						    				}
						    ?>
						    			<li class="page-item disabled"><a class="page-link" >...</a></li>
						    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $getProducts["page"]["totalPage"]; ?>"><?php echo $getProducts["page"]["totalPage"]; ?></a></li>
						    <?php				
						    			}else if(($getProducts["page"]["totalPage"]-3) <= $getProducts["page"]["pageNo"]){
						    ?>
						    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
						    			<li class="page-item disabled"><a class="page-link" >...</a></li>
						    <?php
						    				for($p=($getProducts["page"]["totalPage"]-4) ; $p<=$getProducts["page"]["totalPage"] ; $p++){
						    					if( $p == $getProducts["page"]["pageNo"]){ //當頁
						    ?>
						    			<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
						    <?php						
						    					}else{
						    ?>
										<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
						    <?php						
						    					}							    					
						    				}
						    			}else if($getProducts["page"]["pageNo"]>=5){
						    ?>
						    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
						    			<li class="page-item disabled"><a class="page-link" >...</a></li>
						    <?php				
						    				for($p=($getProducts["page"]["pageNo"]-1) ; $p<=($getProducts["page"]["pageNo"]+1) ; $p++){
						    					if( $p == $getProducts["page"]["pageNo"]){ //當頁
						    ?>
										<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
						    <?php						
						    					}else{
						    ?>
										<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
						    <?php								
						    					}	
						    				}
						    ?>
						    			<li class="page-item disabled"><a class="page-link" >...</a></li>
						    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $getProducts["page"]["totalPage"]; ?>"><?php echo $getProducts["page"]["totalPage"]; ?></a></li>
						    <?php				
						    			}
						  
						    		}else{
							    		for($p=1 ; $p<=$getProducts["page"]["totalPage"] ; $p++){
										if( $p == $getProducts["page"]["pageNo"]){ //當頁
							?>
									<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php				
											}else{
							?>
									<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
							<?php			
											} 
										} //----end for 
									} //----end if	
						    ?>
						    <li class="page-item <?php if($getProducts["page"]["pageNo"] == $getProducts["page"]["totalPage"]){echo 'disabled';} ?>">
						      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($getProducts["page"]["pageNo"]+1); ?>" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						        <span class="sr-only">Next</span>
						      </a>
						    </li>
						  </ul>
						</nav>
                    </div>
                </div>

			</div>
		</div>
	</section>


<?php require_once("module/footer.php"); ?>