<?php require_once("module/header.php"); ?>

	<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_footer_faq_member?>
			
		</ol>
		<div class="container">
		<div class="row">
		<h3><?=$lang_footer_faq_member?></h3>
			<div class="subtitle">Q1：忘記帳號怎麼辦？</div>
<p>A：為保護消費者個資，『TGILive居生活』無法提供信箱帳號身份比對服務，請您重新註冊新帳號。</p>

<p class="highlight">※貼心提醒：單一信箱僅供註冊單一帳號，若此信箱註冊過，系統會告訴您無法重覆註冊，或依此可找回您原先的註冊帳號。</p>

	<div class="subtitle">Q2：忘記密碼無法登入怎麼辦？</div>
<p>A：請於網站右上方點選「登入」，選擇「忘記密碼」，填入您所註冊的電子信箱，系統將會寄出一封重設密碼的信件至您註冊的電子信箱用中，請利用此信件內提供的臨時密碼，回到居生活網站上即可登入。</p>

<p class="highlight">※建議您此次登入後至>會員專區 重新設定一組新密碼，以方便您下次購物登入使用。</p>
	<div class="subtitle">Q3：如何修改個人資料？</div>
<p>A：您可登入後點選頁面右上方「會員中心」，依您的需求進行資料編輯。</p>

	
				
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>