<?php require_once("module/header.php"); 
include("../controller/productControl.php");
$db = new DB();
$brandSelect ="SELECT * from brand_select";
$cateNoResult =  $db->DB_Query($brandSelect);

//品牌篩選
$cateNo = $cateNoResult[0]['cate_no'] ;	
$brandIdProducts = get_brandId_queryProducts($cateNo);
$brandInfoSql = "select * from category where cate_no = ".$cateNo;
$brandInfoResult = $db -> DB_Query($brandInfoSql);

//商品數量高於8個商品的品牌
$cateBrandUse =[];
$brandsSql = "SELECT * from category_products_relate a join category b where a.cate_no = b.cate_no and cate_level = 2 group by b.cate_name asc";
$cateUseResult = $db -> DB_Query($brandsSql);
if($cateUseResult){
	foreach ($cateUseResult as $key => $value) {
		array_push($cateBrandUse,$value);
	}
}

//免運金額
$cartSql = "SELECT * from cart_set where cs_no = 1";
$cartSetResult = $db -> DB_Query($cartSql);
?>
      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">首頁設定</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">首頁設定</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
              <ul class="nav nav-tabs border-0" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link indexgroup <?php if( !isset($_REQUEST["in"]) || $_REQUEST["in"] == 'banner'){echo 'active' ;} ?>" href="?in=banner">首頁Banner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link indexgroup <?php if( $_REQUEST["in"] == 'brand'){echo 'active' ;} ?>" href="?in=brand">首頁精選品牌</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link indexgroup <?php if( $_REQUEST["in"] == 'cart'){echo 'active' ;} ?>" href="?in=cart">運費設定</a>
                    </li>
             	</ul>
            	<div class="card">
            <?php if( !isset($_REQUEST["in"]) || $_REQUEST["in"] == 'banner'){?>    
             <!-- =====dataTable====== -->
                <div class="demo">
                  <table id="dataTable-banner" class="display" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th style="width:25px;">編號</th>
                            <th style="width:300px;">圖示</th>
                            <th style="width:200px;">連結</th>
                            <th>跳窗</th>
                            <th>狀態</th>
                            <th>更新時間</th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                  </table>
                </div>	
             <!-- =====/dataTable====== -->
            <?php }elseif( $_REQUEST["in"] == 'brand' ){ ?>
                <div class="demo">
                    <div class="col-12 mb-2">
                        <div class="row d-flex justify-content-end">
                            <div class="col-3">
                                <div class="row">
                                    <select name="brandSel" class="form-control" id="bs">
                                    <?php foreach ($cateBrandUse as $key => $value) { ?>
                                      <option value="<?php echo $value['cate_no']; ?>" <?php if($value['cate_no'] == $cateNo){echo "selected";}?> ><?php echo $value['cate_name'];?></option>
                                    <?php } ?>   
                                    </select>                   
                                </div>
                            </div>
                            <div class="selectBrandBtn">
                                <button id="selectBrandConfirm" type="button" class="btn-sm btn-outline-success">修改設定</button>
                            </div>                        
                        </div>
                    </div>
                    <div class="card indexBrand">
                        <div class="card-header d-flex align-items-center">                            
                            <h2 class="h5 display display"><?php echo $brandInfoResult[0]["cate_name"]; ?></h2>
                        </div>
                        <div class="card-block row">
                            <div class="col-8">
                                <p class="mb-5"><?php echo $brandInfoResult[0]["cate_describe"] ;?></p>
                                <h5>產品項目</h5>
                                <div class="text-left py-2">
                                <?php foreach ($brandIdProducts as $key => $value) { ?>
                                  <span class="brandProduct"><?php echo $value['product_name'];?></span>
                                <?php }?>  
													    	</div>
                            </div>
                            <div class="col-4 text-center">
                              <img src="<?php echo '../'.$brandInfoResult[0]["cate_logo_img"] ;?>" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>  
            <?php }elseif( $_REQUEST["in"] == 'cart' ){ ?>
              <div class="demo">
                  <div class="col-12">
                      <div class="form-group text-right">
                            <button id="cartEditConfirm" type="button" class="btn-sm btn-primary ml-4 buttonDisabled" disabled>儲存設定</button>
                      </div>
                      <div class="line"></div>
                      <div class="form-inline mt-3">
                          <label class="col-form-label mr-4" for="cartPrice">免運金額</label>
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control text-right" id="cartPrice" value="<?php echo $cartSetResult[0]['cs_price'];?>"><span class="input-group-addon">.00</span>
                          </div>
                      </div>
                      <div class="form-inline mt-3">
                          <label class="col-form-label mr-4" for="cartPrice">運費金額</label>
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control text-right" id="cartCargo" value="<?php echo $cartSetResult[0]['cs_cargo'];?>"><span class="input-group-addon">.00</span>
                          </div>
                      </div>
                  </div>
                </div>	
            <?php } ?>
             	</div>
            </div> 
          </div>
        </div>
      </section>

      <!-- Modal -->
			<div class="modal fade" id="bannerEditModel" tabindex="-1" role="dialog" aria-labelledby="bannerNewModel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Banner編輯</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
	                    <div class="line"></div>
	                    <div class="form-group row">
			        		<label class="col-3 col-form-label">Banner</label>
							<div class="col-9 text-center">
							  <button id="bannerUpload" class="btn-sm btn-outline-secondary">檔案上傳</button>	
							  <input type="file" name="banner" style="display:none;" class="form-control">
							  <img src="" id="temporaryBanner" class="img-fluid" style="margin: 10px 0;">
							  <img id="Banner" src=""  class="imgShow img-fluid" style="display:none;">
							   <button id="bannerDelete" style="display:none;" class="btn-sm btn-outline-danger">刪除</button>
							</div>
							<div class="col-9 offset-3 imgInfo">
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸需求*</div>
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">1920x640<!-- <sub>px</sub> --></div>
                            </div>
	                    </div>
	                    <div class="line"></div>
	                    <div class="form-group row">
			        		<label for="bannerLink" class="col-3 col-form-label">連結</label>
							<div class="col-9">
							  <input id="bannerLink" type="text" name="banner_link" class="form-control" placeholder="請輸入連結網址">
							</div>
	                    </div>
                        <div class="form-group row">
                            <label class="col-3 form-control-label" for="blankStatus">跳窗</label>
                            <div class="col-9">
                                <div class="row">
                                    <div class="col-12 col-form-label switchButtonColor"><input id="blankStatus" type="checkbox" name="my-checkbox"></div>
                                </div>          
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 form-control-label" for="bannerStatus">狀態</label>
                            <div class="col-9">
                                <div class="row">
                                    <div class="col-12 col-form-label switchButtonColor"><input id="bannerStatus" type="checkbox" name="my-checkbox"></div>
                                </div>          
                            </div>
                        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
			        <button type="button" class="btn-sm btn-primary" id="editBannerConfirm">儲存設定</button>
                    <input type="hidden" name="bannerNo" id="banner_no">
			      </div>
			    </div>
			  </div>
			</div>
			<!-- Modal -->  


      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>