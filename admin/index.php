<?php require_once("module/header.php"); 
date_default_timezone_set("Asia/Taipei");
$now = date("Y-m");
?>

      <!-- Counts Section -->
      <section class="dashboard-counts section-padding index1">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6">
              <div class="card" style="height: auto;">
                <div class="card-header d-flex align-items-center">
                  <h2 class="h5 display">待處理訂單</h2>
                </div>
                <div class="card-block">
                  <table id="orderServerTable-index" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>訂單編號</th>
                        <th>下單時間</th>
                        <th>訂單狀態</th>
                        <th>立即檢閱</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card" style="height: auto;">
                <div class="card-header d-flex align-items-center">
                  <h2 class="h5 display">新會員加入</h2>
                </div>
                <div class="card-block">
                  <table id="memberServerTable-index" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>編號</th>
                        <th>姓名</th>
                        <th>加入時間</th>
                        <th>最後登入時間</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Header Section-->
      <section class="dashboard-header section-padding">
        <div class="container-fluid">
          <div class="row d-flex align-items-md-stretch">
            <!-- Line Chart -->
            <div class="col-12 flex-lg-last flex-md-first align-self-baseline">
              <div class="wrapper sales-report">
                <div class="col">
                  <h2 class="display h4">銷售總覽(NT)</h2>
                </div>
                <!-- <div class="line-chart">
                  <canvas id="lineCahrt"></canvas>
                </div> -->  
                <div class="col text-right">
                  <label for="from">從</label>
                  <input type="text" id="from" name="from">
                  <label for="to">到</label>
                  <input type="text" id="to" name="to">        
                  <button id="timeZoneConfirm" class="btn-sm btn-outline-success">查詢</button>
                </div> 

                <!-- ===銷售總覽datatable export table==== -->
                <div class="col">
                  <table id="exportTable-sale" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>日期</th>
                                <th>訂單編號(群組)</th>
                                <th>訂單編號</th>
                                <th>品牌</th>
                                <th>產品編號</th>
                                <th>商品名稱</th>
                                <th>銷售數量</th>
                                <th>售價</th>
                                <th>優惠卷</th>
                                <th>折扣金額</th>
                                <th>運費</th>
                                <th>銷售總額</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                  </table> 
                </div>
                <!-- ===銷售總覽datatable export table==== -->

                <div class="col">
                  <div id="chartdiv"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Updates Section -->
      <section class="dashboard-counts section-padding index1">
        <div class="container-fluid">
          <div class="row">
            <div class="col-6">
              <div class="card" style="height: auto;">
                <div class="card-header d-flex align-items-center">
                  <h2 class="h5 display">商品瀏覽次數(<?php echo $now; ?>)</h2>
                </div>
                <div class="card-block">
                  <table id="viewsServerTable-index" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>品牌</th>
                        <th>商品名稱</th>
                        <th>瀏覽次數</th>
                        <th>瀏覽次數佔比(%)</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card" style="height: auto;">
                <div class="card-header d-flex align-items-center">
                  <h2 class="h5 display">商品低庫存通知</h2>
                </div>
                <div class="card-block">
                  <table id="stockServerTable-index" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>商品編號</th>
                        <th>商品名稱</th>
                        <th>規格名稱</th>
                        <th>庫存數量</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            

          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>

<!-- =====index圖表 charts===== -->
   <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="../plugIn/bootstrap/js/charts-home.js"></script> -->
<!-- ========amChart========== -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>    
<!-- ======amChart user===== -->
<script src="js/amChart.js" ></script>    
<!-- datePick -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<script type="text/javascript">
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
</script>
<!-- datePick -->

<?php require_once("module/footer.php"); ?>