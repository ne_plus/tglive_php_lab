<?php require_once("module/header.php"); ?>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>TGI Live </span><strong class="text-primary"> 管理者 </strong></div>
            <p>請輸入管理者帳號密碼</p>
            <form id="login-form" method="post">
              <div class="form-group">
                <label for="login-username" class="label-custom">User Name</label>
                <input id="login-username" type="text" name="loginUsername" required="">
              </div>
              <div class="form-group">
                <label for="login-password" class="label-custom">Password</label>
                <input id="login-password" type="password" name="loginPassword" required="">
              </div><a id="login" href="index.php" class="btn btn-primary">登入</a>
              <!-- This should be submit button but I replaced it with <a> for demo purposes-->
            </form><!-- <a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a> -->
          </div>
          <div class="copyrights text-center">
            <p>TGI Live <span>居生活數位整合有限公司</span></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
  </body>
<script src="js/login.js"></script>
<?php require_once("module/footer.php"); ?>