<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">訂單管理</li>
          </ul>
        </div>
      </div>
      <section class="charts order">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">訂單管理</h1>
          </header>
          <div class="row">

            <div class="col-lg-12">
            <!-- ========定單分頁======== -->
               <ul class="nav nav-tabs" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link active orderProcess orderClass" href="#!">待處理</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link orderHistory orderClass" href="#!">歷史訂單</a>
                    </li>
                </ul>

            	<div class="card product">
               
             <!-- =====dataTable====== -->
			             <div class="demo" id="orderProcess">
                   <div class="row" style="margin-bottom:15px;">
                     <div class="col-6 text-left">
                        <label for="from">從</label>
                        <input type="text" id="from" name="from">
                        <label for="to">到</label>
                        <input type="text" id="to" name="to">        
                        <button id="timeZoneConfirm-order" class="btn-sm btn-outline-success">查詢</button>
                      </div> 
                     <div class="col-6 text-right">
                        <span>狀態篩選</span>
                        <select id="orderStatusSelects" class="form-control form-control-sm">
                          <option value="all" selected>所有商品</option>
                          <option value="0">處理中</option>
                          <option value="1">收款確認</option>
                          <option value="2">出貨</option>
                        </select>
                    </div>
                   </div>
			             			             	
      							<table id="orderServerTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
      								<thead>
      									<tr>
      										<th>訂單編號</th>
                          <th>訂單關聯</th>
      										<th>狀態</th>
      		                <th>買家</th>
      		                <th>下單時間</th>
      		                <th>供應商</th>
      		                <th>金額</th>
      		                <th>運費</th>	
      		                <th>付款方式</th>
                          <th>配送地址</th>
                          <th></th>					               
      									</tr>
      								</thead>
      								
      						    	<tbody>
      						    	</tbody>
      							</table>
      							
      						</div>	
                   <!-- =====/dataTable====== -->
                  
                   <!-- =====dataTable====== -->
                   <div class="demo" id="orderHistory" style="display: none;">
                    <div class="row" style="margin-bottom:15px;">
                      <div class="col-6 text-left">
                        <label for="from-history">從</label>
                        <input type="text" id="from-history" name="from">
                        <label for="to-history">到</label>
                        <input type="text" id="to-history" name="to">        
                        <button id="timeZoneConfirm-order-history" class="btn-sm btn-outline-success">查詢</button>
                      </div> 
                      <div class="col-6 text-right">
                        <span>狀態篩選</span>
                        <select id="orderStatusHistorySelects" class="form-control form-control-sm">
                          <option value="all" selected>所有商品</option>
                          <option value="3">退貨</option>
                          <option value="4">取消</option>
                          <option value="5">完成</option>
                        </select>
                      </div>
                    </div>  

                    <table id="orderHistoryServerTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>訂單編號</th>
                          <th>訂單關聯</th>
                          <th>狀態</th>
                          <th>買家</th>
                          <th>下單時間</th>
                          <th>供應商</th>
                          <th>金額</th>
                          <th>運費</th> 
                          <th>付款方式</th>
                          <th>配送地址</th>
                          <th></th>                        
                        </tr>
                      </thead>
                      
                        <tbody>
                        </tbody>
                    </table>
                    
                  </div>  
                   <!-- =====/dataTable====== -->

               	</div>
              </div> 
            </div>
          </div>
	






      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

<!-- datePick -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<script type="text/javascript">
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    $( "#from-history" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to-history" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to-history" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from-history" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
</script>
<!-- datePick -->

<?php require_once("module/footer.php"); ?>