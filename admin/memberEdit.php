<?php 
require_once("module/header.php"); 
require_once('../model/DB.php');
date_default_timezone_set("Asia/Taipei");
$db = new DB();
$sql = "select * from member where mem_no ='".$_REQUEST['ID']."'" ;
$result = $db->DB_Query($sql);
?>
      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="member.php">會員</a></li>
            <li class="breadcrumb-item active">會員資料編輯</li>
          </ul>
        </div>
      </div>
      <section class="charts memberEdit">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">會員資料編輯</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<!-- =====card===== -->
            	<div class="card">
	                <div class="card-block">
	                	<div class="form-group row">
		                      <label class="col-6 col-sm-2 col-form-label">會員編號：</label>
		                      <div class="col-6 col-sm-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo $_REQUEST["ID"]; ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label class="col-2 col-form-label">加入日期：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo date("Y-m-d H:i:s",$result[0]["mem_createtime"]); ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label class="col-2 col-form-label">最後登入時間：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo $result[0]["mem_updatetime"]; ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                	<div class="form-group row">
		                      <label for="lastnameEdit" class="col-2 col-form-label"><span class="text-danger">*</span>會員姓名：</label>
		                      <div class="col-4">
		                      		<div class="row">
				                      	<label  class="col-12 col-form-label">
				                      		<div class="row">
												<div class="col-6">
													<input class="form-control" type="text" id="lastnameEdit" value='<?php echo $result[0]["mem_lastname"]; ?>' placeholder="姓氏">
												</div>
												<div class="col-6">
													<input class="form-control" type="text" id="firstnameEdit" value='<?php echo $result[0]["mem_firstname"]; ?>' placeholder="名">
												</div>
				                      		</div>
				                      	</label>
			                      	</div>
		                      </div>
		                      
	                    </div>
	                  	<div class="form-group row">
		                      <label  class="col-2 col-form-label"><span class="text-danger">*</span>帳號(E-Mail)：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label class="col-12 col-form-label"><input  id="memMail" class="form-control" type="text" value="<?php echo $result[0]["mem_mail"]; ?>" disabled></label>
		                      	 	</div>
		                      </div>
		                     
	                    </div>
	                    <div class="form-group row">
		                      <label for="telEdit" class="col-2 col-form-label"><span class="text-danger">*</span>聯絡電話：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      			<label class="col-12 col-form-label"><input class="form-control" type="tel" id="telEdit" value=<?php echo $result[0]["mem_tel"]; ?>></label>
		                      		</div>
		                          
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label for="addrEdit" class="col-2 col-form-label">地址：</label>
		                      <div class="col-6">
		                      		<div class="row">
		                      			<label class="col-12 col-form-label"><input class="form-control" type="text" id="addrEdit" value=<?php echo $result[0]["mem_address"]; ?>></label>
		                      		</div>
		                          
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label for="noteEdit" class="col-2 col-form-label">備註：</label>
		                      <div class="col-6">
		                      		<div class="row">
		                      			<label class="col-12 col-form-label"><textarea class="form-control" id="noteEdit" rows="3"><?php echo $result[0]["mem_note"] ?></textarea></label>
		                      		</div>
              					 
		                      </div>
	                    </div>
	                    <div class="form-group row">
	                         <label for="memStatus" class="col-2 col-form-label">狀態</label>
	                         <div class="col-3">
	                         		<div class="row">
	                         			<label class="col-12 col-form-label switchButtonColor"><input id="memStatus" type="checkbox" name="my-checkbox" 
								  <?php if($result[0]['mem_status']){echo "checked" ;}else{} ?> ></label>
	                         		</div>
	                          	  
	                         </div>
	                    </div>
	                    <div class="modal-footer">
		                    <button id="memCreateCancel" type="button" class="btn-sm btn-secondary">取消</button>
		                    <button id="memUpdateConfirm" type="button" class="btn-sm btn-success">儲存編輯</button>
	                  	</div>
	                </div>
              </div>
            	<!-- =====card===== -->
            </div> 
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

<?php require_once("module/footer.php"); ?>