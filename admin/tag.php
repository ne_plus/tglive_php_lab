<?php require_once("module/header.php"); ?>
<?php  
	date_default_timezone_set("Asia/Taipei");
	require_once('../model/DB.php');
	$db = new DB();
?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">標籤管理</li>
          </ul>
        </div>
      </div>
      <section class="charts category">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">標籤管理</h1>
          </header>
          <div class="row">        
				<!-- tag==== -->
			<div class="col-12">
				<ul class="nav nav-tabs" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link taggroup <?php if(isset($_REQUEST["tag"]) === false || $_REQUEST["tag"] == 'set'){echo 'active';} ?>" href="?tag=set">標籤設定</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link taggroup <?php if($_REQUEST["tag"] == 'group'){echo 'active';} ?>" href="?tag=group">標籤群組設定</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link taggroup <?php if($_REQUEST["tag"] == 'article'){echo 'active';} ?>" href="?tag=article">文章分類設定</a>
                    </li>
             	</ul>
             	<?php  
             		if(isset($_REQUEST["tag"]) === false || $_REQUEST["tag"] == 'set'){
             	?>
				<div class="card" id="tagSet">
					<div class="container">
						<div class="cresateBar">
							<div class="row">
								<div class="col-8">
										<input type="text" class="form-control form-control-sm" placeholder="新增標籤">	
								</div>
								<div class="col-4 text-right">
										<button type="button" class="btn-sm btn-outline-success" id="tagCreate">新增標籤</button>
								</div>
							</div>
						</div>
						
						<table class="table table-striped ">
		                    <thead>
		                      <tr>
		                        <th>標籤名稱</th>	 
		                        <th></th>                       
		                      </tr>
		                    </thead>
		                    <tbody>
		                    	<?php  
							    	$query = $db->DB_Query("select * from tag order by tag_no desc");
							    	if(!count($query)){
							    ?>
							    <tr><td class="text-center" colspan="2">找無資料</td></tr>
							    <?php		
							    	}else{
							    		foreach ($query as $key => $value) {
							    ?>
							    <tr>
									<td class="tagName"><?php echo $value["tag_name"]; ?>
										<input type="hidden" name="tag_no" value="<?php echo $value["tag_no"]; ?>">
									</td>
									<td class="text-right"><span class="edit"><button class="tagEdit" data-toggle="modal" data-target="#tagEditModel">編輯</button></span><span class="edit"><button class="tagDelet">刪除</button></span></td>
								</tr>
							    <?php
							    		}
							    	}
							    	
		                    	?>
		                    </tbody>
	                  </table>  
                  </div>
				</div>

				<?php  						
             		}elseif( $_REQUEST["tag"] == 'group'){
				?>

				<div class="card" id="tagGroup">
					<div class="container">
						<div class="cresateBar">
							<div class="row">
								<div class="col-8">
										<input type="text" class="form-control form-control-sm" placeholder="新增標籤分類">	
								</div>
								<div class="col-4 text-right">
										<button type="button" class="btn-sm btn-outline-success" id="cateCreate">新增標籤分類</button>
								</div>
							</div>
						</div>
						<table class="table table-striped tagGroupTitle">
		                    <thead>
		                      <tr>
		                        <th>標籤分類</th>	 
		                        <th></th>                       
		                      </tr>
		                    </thead>
		                </table>  
		                <div id="accordion" role="tablist">
		                <?php 
					    	$queryGroup = $db->DB_Query("select * from category where cate_parents = 61 and cate_level =1 order by cate_sort asc");
					    	if(!count($queryGroup)){
					    ?>
					    	
							  	<div class="tag_group_box">
								    <div class="card-header text-center" role="tab" id="headingOne">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								          沒有群組分類項目
								        </a>
								      </h5>
								    </div>						  						  
								</div>
						
					    <?php
					    	}else{
								// echo "<pre>";
						  //   	print_r($queryGroup);
						  //   	echo "</pre>";
						    	for($i=0;$i<count($queryGroup);$i++){
						?>
							
							  <div class="tag_group_box">
							    <div class="card-header" role="tab" id="<?php echo 'heading'.($i+1); ?>">
							    	<div class="row">
							    		<div class="col-6">
									    	<h5 class="mb-0">
										        <a data-toggle="collapse" href="<?php echo'#collapse'.($i+1); ?>" aria-expanded="true" aria-controls="<?php echo'collapse'.($i+1); ?>">
										          <i class="fa fa-angle-down"></i>
										          <?php echo $queryGroup[$i]['cate_name']; ?>
										          <input type="hidden" name="cate_no" value="<?php echo $queryGroup[$i]['cate_no'];?>">
										        </a>
										      </h5>
									    </div>
									    <div class="col-6 text-right">
								    		<span class="edit"><button class="cateTagEdit" data-toggle="modal" data-target="#cateTagEditModel">加入標籤設定</button></span><span class="edit"><button class="cateTagRemoveEdit" data-toggle="modal" data-target="#cateTagEditModel">移除標籤設定</button></span><span class="edit"><button class="cateTagDelete">刪除</button></span>
								    	</div>
							    	</div>
								    
							    </div>

							    <div id="<?php echo'collapse'.($i+1); ?>" class="collapse" role="tabpanel" aria-labelledby="<?php echo 'heading'.($i+1); ?>">
							      <div class="card-body">
							      		<div class="text-left py-2">
						<?php  
							$queryCateTagRelate = $db->DB_Query("select * from category_tag_relate a join tag b on a.tag_no = b.tag_no  where cate_no = ' ".$queryGroup[$i]['cate_no'] ."'");
							if(!count($queryCateTagRelate)){
						?>
								<div class="text-center">沒有標籤</div>
								
									<!-- <span class="tagSelect">#好心情</span><span class="tagSelect">#憂鬱</span><span class="tagSelect">#陽光</span> -->
								
						<?php
							}else{
								foreach ($queryCateTagRelate as $key => $value) {
						?>
									<span class="tagSelect">#<?php echo $value['tag_name'] ?><input type="hidden" name="tag_no" value="<?php echo $value['tag_no']; ?>"><input type="hidden" name="cate_tag_no" value="<?php echo $value['cate_tag_no']; ?>"></span>
						<?php
								}
								// echo "<pre>";
						  //   	print_r($queryCateTagRelate);
						  //   	echo "</pre>";
							}
							
						?>
							    		</div>  
							    	</div> <!-- END card-body -->
							    </div>
							  </div>						
						<?php		
						    	}
					    	}
					    	
						?>
						
						
						</div>
					</div>
				</div>
				<?php  
					}else{
				?>
					<div class="card" id="tagArticle">
						<div class="container">
							<div class="card-header" role="tab" id="heading1">
						    	<div class="row">
						    		<div class="col-6">
								    	<h5 class="mb-0">
									        文章分類(標籤)
									      </h5>
								    </div>
								    <div class="col-6 text-right">
							    		<span class="edit"><button id="addArticleTag" data-toggle="modal" data-target="#cateTagEditModel">加入標籤設定</button></span><span class="edit"><button  id="removeArticleTag" data-toggle="modal" data-target="#cateTagEditModel">移除標籤設定</button></span>
							    	</div>
						    	</div>	    
							</div>
							<div class="card-body">
							    <div class="text-left py-2">
							<?php  
								$queryCateTagRelate = $db->DB_Query("select * from category_tag_relate a join tag b on a.tag_no = b.tag_no  where cate_no = 50");
								if(!count($queryCateTagRelate)){
							?>
								<div class="text-center">沒有標籤</div>
							<?php
								}else{
									foreach ($queryCateTagRelate as $key => $value) {
							?>
							<span class="tagSelect">#<?php echo $value['tag_name'] ?><input type="hidden" name="tag_no" value="<?php echo $value['tag_no']; ?>"><input type="hidden" name="cate_tag_no" value="<?php echo $value['cate_tag_no']; ?>"></span>		
							<?php	
									}	
								}
							?>		
								</div>
							</div>
						</div>
					</div>
				<?php			
					}
				?>

             </div> 

			<!-- Modal -->
			<div class="modal fade" id="tagEditModel" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModal3Label">修改標籤名稱</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
				        <div class="form-group row">
	                      <div class="col-12">
	                          <input class="form-control" type="text" id="tagNewName" placeholder="輸入新的標簽名">
	                      </div>
	                    </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
			        <button type="button" class="btn-sm btn-primary" id="editTagNameConfirm">儲存設定</button>
			      </div>
			    </div>
			  </div>
			</div>

			
			<!-- =====標籤群組分類======= -->

			<div class="modal fade" id="cateTagEditModel" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModal3Label">編輯標籤分類</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
				        <div class="row">
		                    <div class="col-12 text-left py-2 tagArea">
									<span class="tagSelect">#好心情</span><span class="tagSelect">#憂鬱</span><span class="tagSelect">#陽光</span>
							</div>
	                    </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
			        <button type="button" class="btn-sm btn-danger" id="removeTagCateConfirm">移除標籤設定</button>
			        <button type="button" class="btn-sm btn-primary" id="editTagCateConfirm">加入標籤設定</button>
			      </div>
			    </div>
			  </div>
			</div>

            	<!-- tag==== -->

				
			       
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

<?php require_once("module/footer.php"); ?>