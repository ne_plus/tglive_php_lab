<?php  
ob_start();
session_start();

if(!strpos(strtolower($_SERVER['PHP_SELF']),"/login.php")){ //不是Login.php
    if(!$_SESSION["adm_email"]){ //沒有登入狀態
      header("location:login.php");
    }else{ //有登入 
      if($_SESSION["adm_status"] != 1){
        session_unset();
        header("location:login.php");
      }
    }
}else{
    if(isset($_SESSION["adm_email"])){ 
      header("location:index.php");
    }else{}
}


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TGI Live</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../plugIn/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../plugIn/bootstrap/css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../plugIn/bootstrap/css/style.default.css" id="theme-stylesheet">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="../plugIn/bootstrap/css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom stylesheet - for your changes-->
    <!-- <link rel="stylesheet" href="css/custom.css"> -->
    <!-- Favicon-->
    <!-- <link rel="shortcut icon" href="img/favicon.ico"> -->
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <script defer src="../js/fontawesome/fontawesome-all.js"></script>
    <!-- Font Icons CSS-->
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    
    <!-- ====dataTable======== -->
    <script src="js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
    <!-- datatableButton(export) -->
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/buttons.colVis.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css">


    <script src="js/dataTableAdmin.js"></script>


     <!-- user style -->

    <link rel="stylesheet" href="css/admin.css"> 
    <script src="js/admin.js"></script>
    <script src="js/summerNote.js"></script>
    
    <!-- ========bootstrap-switch========= -->
    <link rel="stylesheet" href="../plugIn/bootstrap/css/bootsrap-switch/bootstrap-switch.min.css">
    <script src="../plugIn/bootstrap/js/bootsrap-switch/bootstrap-switch.min.js"></script>

    <!-- =========summernote=========== -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
  </head>
  <body>

  <?php if(!strpos(strtolower($_SERVER['PHP_SELF']),"/login.php")){
  ?>
    <!-- Side Navbar -->
    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="../image/frontend/LogoC.png" alt="img" class="img-fluid" style="width:150px;height:auto;">
            <h2 class="h5 text-uppercase"><?php echo $_SESSION["adm_name"]; ?></h2><span class="text-uppercase"><?php 
            switch ($_SESSION["adm_authLevel"]){
              case "1":
                echo "(產品 / 訂單)管理";
                break;
              case "2":
                echo "(報表 / 使用者 / 匯入匯出)管理";
                break;
              case "3":
                echo "最高權限";
                break;    
            } 
            ?></span>
          </div>
          <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>TGI</strong><strong class="text-primary">L</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li class="active"><a href="index.php" class="indexSelect"> <i class="icon-home"></i><span>Home</span></a></li>
            <li><a href="member.php" class="memberSelect"><i class="icon-user"></i><span>會員</span></a></li>
            <li><a href="administrator.php" class="adminSelect"><i class="icon-user"></i><span>管理員</span></a></li>
            <li><a href="#pages-nav-list" data-toggle="collapse" aria-expanded="false"><i class="icon-interface-windows"></i><span>分類</span>
                <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
              <ul id="pages-nav-list" class="collapse list-unstyled">
                <li> <a href="tag.php">標籤管理</a></li>
                <li> <a href="category.php">品牌管理</a></li>
                <li> <a href="categoryActivity.php">行銷分類</a></li>
              </ul>
            </li>
            <li><a href="banner.php" class="bannerSelect"><i class="icon-form"></i><span>首頁設定</span></a></li>
            <li><a href="product.php" class="productSelect"><i class="icon-presentation"></i><span>商品管理</span></a></li>            
            <li><a href="coupon.php" class="couponSelect"><i class="icon-form"></i><span>優惠劵管理</span></a></li>
            <li><a href="order.php" class="orderSelect"><i class="icon-form"></i><span>訂單管理</span></a></li>
            <li><a href="style.php" class="styleSelect"><i class="icon-form"></i><span>居生活文章</span></a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="page home-page">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.php" class="navbar-brand">
                  <div class="brand-text hidden-sm-down"><span>TGI-Live </span><strong class="text-primary">ADMIN</strong></div></a></div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <li class="nav-item"><a id="logout" href="framwork/adminLogOut.php" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
  <?php
  }else{} 
 ?>
    