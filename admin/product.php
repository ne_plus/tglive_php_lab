<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">商品管理</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">商品管理</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<div class="card product">
				
				<?php require_once("../model/product.php") ;
					$db = new DB();

          //搜尋供應商/品牌分類
					$sql ="select * from category where cate_parents=62 and cate_level= 1 order by cate_sort asc";
					$cate = $db->DB_Query($sql);

          //搜尋行銷活動分類(所有)
          $activitySql = "select * from category where cate_parents=63 and cate_level= 1 order by cate_sort asc"; // and cate_no <> 90 
          $activity = $db->DB_Query($activitySql);
 
				?>

             <!-- =====dataTable====== -->
			             <div class="demo">
			             	<div class="row" style="margin-bottom:30px;">
			             		<div class="col-6 productSelectBox">
			             			<button id="productSelectAll" class="btn-sm btn-outline-info">全選</button>
				             		<button id="productSelectNone" class="btn-sm btn-outline-info">全不選</button>
				             		<select id="productSelects" class="form-control form-control-sm">
				             			<option value disabled selected>批次處理動作</option>
				             			<option value="1" >上架</option>
				             			<option value="2" >下架</option>
                        	<option value="3" >加入品牌設定</option>
                        	<option value="4" >移除品牌設定</option>
                        	<option value="5" >加入標籤設定</option>
                        	<option value="6" >移除標籤設定</option>
                          <!-- <option value="7" >加入本月推薦</option>
                          <option value="8" >移除本月推薦</option> -->
                          <option value="9" >加入行銷活動</option>
                          <option value="10" >移除行銷活動</option>
                          <option value="11" >開啟360商品</option>
                          <option value="12" >關閉360商品</option>
				             		</select>
				             		<button id="productSelectConfirm" class="btn-sm btn-outline-success">確認</button>
								
				             	</div>
				             	<div class="col-6 text-right">
				             		<span>品牌篩選</span>
				             		<select id="cateSelects" class="form-control form-control-sm mr-2">
				             			<option value="0" selected>所有商品</option>
				             			<?php	
											foreach ($cate as $key => $value) {
										?>
				             			<option value="<?php echo $value['cate_name']; ?>" ><?php echo $value['cate_name']; ?></option>
                          <?php $sqlChild = "select * from category where cate_parents=".$value['cate_no']; 
                            $cateChild = $db->DB_Query($sqlChild);
                              foreach ($cateChild as $key => $ChildValue) {
                          ?>
                          <option value="<?php echo $ChildValue['cate_name']; ?>" >&nbsp&nbsp&nbsp&nbsp<?php echo $ChildValue['cate_name']; ?></option>
                          <?php      
                              }
                          ?>

		                              	<?php }	?>
				             		</select>
                        <span>商品狀態</span>
                        <select id="productStatus" class="form-control form-control-sm mr-2">
                          <option value="3" selected>所有商品</option>
                          <option value="1">銷售中</option>
                          <option value="2">下架</option>
                          <option value="0">未啟用</option>
                        </select>  
				             		<button id="productCreatebutton" class="btn-sm btn-outline-success" data-toggle="modal" data-target="#producrCreateNew">新增商品</button>
				             	</div>
			             	</div>
			             	

			             			             	
							<table id="dataTableServer" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>編號</th>
										<th>產品編號</th>
		                <th>產品名稱</th>
		                <th>品牌</th>
		                <th  style="width:100px;">標籤</th>
                    <!-- <th  style="width: 60px;">本月推薦</th> -->
                    <th  style="width: 100px;">行銷活動</th>
                    <!-- <th  style="width: 60px;">優惠劵</th> -->
		                <th  style="width: 50px;">商品狀態</th>
                    <th  style="width: 50px;">360狀態</th>
		                <th  style="width: 60px;">更新時間</th>	
		                <th  style="width: 90px;"></th>					               
									</tr>
								</thead>
								
						    	<tbody>
						    	</tbody>
							</table>
							
						</div>	
             <!-- =====/dataTable====== -->
             	</div>
            </div> 
          </div>
        </div>
		

	<!-- Modal 分類新增(品牌/供應商)-->
        <div class="modal fade" id="cateCreateModal" tabindex="-1" role="dialog" aria-labelledby="cateCreateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="cateCreateModalLabel">加入品牌設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <ul class="nav nav-tabs" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link active cateHave cateclass" href="#!">加入現有品牌</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link cateNew cateclass" href="#!">新增品牌</a>
                    </li>
              </ul>
    

              <div id="cateHave">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                         	一共選擇了<span class="cateCreateModalCount"></span>筆商品,選擇你要加入的品牌或是新增品牌
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="cateSelect" class="col-2 col-form-label">分類</label>
                        <div class="col-10">
                          <select class="form-control" id="cateSelect" multiple>
                              <option value="" disabled selected>選擇分類</option>
                              <?php foreach ($cate as $key => $value) {
                              ?>
    						  <option value="<?php echo $value["cate_no"]; ?>" disabled><?php echo $value["cate_name"]; ?></option>
                            <?php $sqlChild = "select * from category where cate_parents=".$value['cate_no']; 
                              $cateChild = $db->DB_Query($sqlChild);
                                foreach ($cateChild as $key => $ChildValue) {
                            ?>
                            <option value="<?php echo $ChildValue['cate_no']; ?>" >&nbsp&nbsp&nbsp&nbsp<?php echo $ChildValue['cate_name']; ?></option>
                            <?php      
                                }
                            ?>

                              <?php
                              // echo $value;
                              } ?>
                              
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="cateCreateConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>
              </div>


              <div id="cateNew" style="display:none;">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                          一共選擇了<span class="cateCreateModalCount"></span>筆商品,選擇你要加入的品牌或是新增品牌
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="cateCreateNew" class="col-2 col-form-label">新增</label>
                        <div class="col-5">
                          <input class="form-control" type="text" id="cateCreateNew" placeholder="想要新增的品牌">
                        </div>
                        <div class="col-5">
                          <select class="form-control" id="newCateFatherSelect">
                            <option value disabled selected>選擇上層分類</option>
                            <!-- <option value="62" >第一層分類</option> -->
                          </select>
                        </div>
                    </div>
                        
                  </div>  <!-- end modal-body -->
    		          <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="cateCreateNewConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>
              </div>


            </div>   <!-- end modal-content -->
          </div>
        </div>

        <!-- Modal 分類新增(品牌/供應商)end-->

<!-- Modal 分類新增(行銷活動)-->
        <div class="modal fade" id="activityCreateModal" tabindex="-1" role="dialog" aria-labelledby="activityCreateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="activityCreateModalLabel">加入設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <ul class="nav nav-tabs" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link active activityHave activityclass" href="#!">加入現有行銷活動清單</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link activityNew activityclass" href="#!">新增活動</a>
                    </li>
              </ul>
    

              <div id="activityHave">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                          一共選擇了<span class="activityCreateModalCount"></span>筆商品,選擇你要加入或是新增行銷活動
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="activitySelect" class="col-2 col-form-label">分類</label>
                        <div class="col-10">
                          <select class="form-control" id="activitySelect" multiple>
                              <option value="" disabled selected>選擇分類</option>
                              <?php foreach ($activity as $key => $value) {
                              ?>
                              <option value="<?php echo $value["cate_no"]; ?>"><?php echo $value["cate_name"]; ?></option>
                              <?php                        
                              } ?>
                              
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="activityCreateConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>
              </div>


              <div id="activityNew" style="display:none;">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                          一共選擇了<span class="activityCreateModalCount"></span>筆商品,選擇你要加入的品牌或是新增品牌
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="activityCreateNew" class="col-2 col-form-label">新增</label>
                        <div class="col-10">
                          <input class="form-control" type="text" id="activityCreateNew" placeholder="想要新增的分類">
                        </div>
                    </div>
                        
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="activityCreateNewConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>
              </div>


            </div>   <!-- end modal-content -->
          </div>
        </div>

        <!-- Modal 分類新增(行銷活動)end-->


	 

		

		<?php  
			$sql ="select * from tag ";
			$tag = $db->DB_Query($sql);
		?>

	<!-- Modal 標籤新增-->
        <div class="modal fade" id="tagCreateModal" tabindex="-1" role="dialog" aria-labelledby="tagCreateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="tagCreateModalLabel">加入標籤設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <ul class="nav nav-tabs" style="margin-top:10px;">
                    <li class="nav-item">
                      <a class="nav-link active tagHave tagclass" href="#!">加入現有標籤</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link tagNew tagclass" href="#!">新增標籤</a>
                    </li>
              </ul>


              <div  id="tagHave">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                         	一共選擇了<span class="tagCreateModalCount"></span>筆商品,選擇你要加入的標籤或是新增標籤
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="tagSelect" class="col-2 col-form-label">標籤</label>
                        <div class="col-10">
                          <select class="form-control" id="tagSelect" multiple>
                              <option id="iniTag" value="" disabled selected>選擇標籤</option>
                              <?php foreach ($tag as $key => $value) {
                              ?>
                              <option value="<?php echo $value['tag_no']; ?>"><?php echo $value['tag_name'] ?></option>
                              <?php	
                              } ?>
                              
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="tagCreateConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>   <!-- end button -->
              </div>


              <div  id="tagNew" style="display:none;">
                  <div class="modal-body">
                    <div class="form-group row">
                      <div class="col-12">
                          一共選擇了<span class="tagCreateModalCount"></span>筆商品,選擇你要加入的標籤或是新增標籤
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="tagCreateNew" class="col-2 col-form-label">新增</label>
                        <div class="col-10">
                          <input class="form-control" type="text" id="tagCreateNew" placeholder="想要新增的標籤">
                        </div>
                    </div>
                        
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="tagCreateNewConfirm" type="button" class="btn-sm btn-primary">確認加入</button>
                  </div>   <!-- end button -->
              </div>



            </div>   <!-- end modal-content -->
          </div>
        </div>

        <!-- Modal 標籤新增-->


    <!-- Modal 新增 -->
            <div class="modal fade" id="producrCreateNew" tabindex="-1" role="dialog" aria-labelledby="productNew" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="productNew">新增商品</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="modelIdNew" class="col-sm-3 col-form-label">產品編號</label>
                      <div class="col-sm-9">
                          <input class="form-control" type="text" id="modelIdNew" name="product_no" placeholder="輸入產品編號">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="productNameNew" class="col-sm-3 col-form-label">產品名稱</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" id="productNameNew" name="product_name" placeholder="輸入產品名稱">
                        </div>
                    </div>
                    
                    
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="productCreateConfirm" type="button" class="btn-sm btn-primary">確認新增</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>









      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>