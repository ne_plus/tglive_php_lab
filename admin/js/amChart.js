$(document).ready(function(){
function timeSelect(a=null,b=null){
	var start_time = a;//"2018-02-26"
	var end_time = b;// "2018-01-31"

	var formData = new FormData();
	if( start_time ){
		formData.append("startTime",start_time);
	}
	if( end_time ){
		formData.append("endTime",end_time);
	}

	$.ajax({
		url:"framwork/orderCheckServer.php",
		data : formData,
		type : "POST",
	    cache : false,
	    contentType : false,
        processData: false,
	    success : function(result){
	    	amChart( (JSON.parse(result))["amChart"] );
	    	exportData( (JSON.parse(result))["export"] );
	    },
	    error : function(error){
	    	alert("傳送失敗");
	    },
	    complete : function(result){
	    	
	    } 
	});
}
//initial
timeSelect();

//select TimeZone
$("#timeZoneConfirm").click(function(){
	var startZone = $("input#to").val();
	var endZone = $("input#from").val();	
	if( !startZone || !endZone ){
		alert("請輸入區間");
		if( !endZone ){	
			$("#from").focus();	
		}else{
			$("#to").focus();
		}	
	}else{
		var startZoneArr =  startZone.split("/");
		var endZoneArr =  endZone.split("/");
		var startZoneReStr = startZoneArr[2]+"-"+startZoneArr[0]+"-"+startZoneArr[1];
		var endZoneReStr = endZoneArr[2]+"-"+endZoneArr[0]+"-"+endZoneArr[1];
		timeSelect(startZoneReStr,endZoneReStr);
	}
});

// =======amChart=======
function amChart(a){	
	var chartData = [];
	for (var d = a.length-1 ; d >=0 ; d--) {
		var chartObj = {
		   "date": a[d]["date"],
	       // "cars":  a[d]["price"]+10,
	       // "motorcycles": a[d]["price"]-10 ,
	       "price" : a[d]["price"]
		}; 
		chartData.push(chartObj);
	}

	var chart =  AmCharts.makeChart("chartdiv", {
	    "type": "serial",
	    "fontFamily": "Lato",
	    "autoMargins": true,
	    "addClassNames": true,
	    "zoomOutText": "",
	    "defs": {
	        "filter": [
	            {
	                "x": "-50%",
	                "y": "-50%",
	                "width": "200%",
	                "height": "200%",
	                "id": "blur",
	                "feGaussianBlur": {
	                    "in": "SourceGraphic",
	                    "stdDeviation": "50"
	                }
	            },
	            {
	                "id": "shadow",
	                "width": "150%",
	                "height": "150%",
	                "feOffset": {
	                    "result": "offOut",
	                    "in": "SourceAlpha",
	                    "dx": "2",
	                    "dy": "2"
	                },
	                "feGaussianBlur": {
	                    "result": "blurOut",
	                    "in": "offOut",
	                    "stdDeviation": "10"
	                },
	                "feColorMatrix": {
	                    "result": "blurOut",
	                    "type": "matrix",
	                    "values": "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .2 0"
	                },
	                "feBlend": {
	                    "in": "SourceGraphic",
	                    "in2": "blurOut",
	                    "mode": "normal"
	                }
	            }
	        ]
	    },
	    "fontSize": 15,
	    "dataProvider": chartData,
	    "dataDateFormat": "YYYY-MM-DD",
	    "marginTop": 0,
	    "marginRight": 1,
	    "marginLeft": 0,
	    "autoMarginOffset": 5,
	    "categoryField": "date",
	    "categoryAxis": {
	        "gridAlpha": 0.07,
	        "axisColor": "#DADADA",
	        "startOnAxis": true,
	        "tickLength": 0,
	        "parseDates": true,
	        "minPeriod": "DD"
	    },
	    "valueAxes": [
	        {
	            "ignoreAxisWidth":true,
	            "stackType": "regular",
	            "gridAlpha": 0.07,
	            "axisAlpha": 0,
	            "inside": true
	        }
	    ],
	    "graphs": [
	        // {
	        //     "id": "g1",
	        //     "type": "line",
	        //     "title": "Cars",
	        //     "valueField": "cars",
	        //     "fillColors": [
	        //         "#0066e3",
	        //         "#802ea9"
	        //     ],
	        //     "lineAlpha": 0,
	        //     "fillAlphas": 0.8,
	        //     "showBalloon": false
	        // },
	        // {
	        //     "id": "g2",
	        //     "type": "line",
	        //     "title": "Motorcycles",
	        //     "valueField": "motorcycles",
	        //     "lineAlpha": 0,
	        //     "fillAlphas": 0.8,
	        //     "lineColor": "#5bb5ea",
	        //     "showBalloon": false
	        // },
	        {
	            "id": "g3",
	            "title": "Price",
	            "valueField": "price",
	            "lineAlpha": 0.5,
	            "lineColor": "#FFFFFF",
	            "bullet": "round",
	            "dashLength": 2,
	            "bulletBorderAlpha": 1,
	            "bulletAlpha": 1,
	            "bulletSize": 15,
	            "stackable": false,
	            "bulletColor": "#5d7ad9",
	            "bulletBorderColor": "#FFFFFF",
	            "bulletBorderThickness": 3,
	            "balloonText": "<div style='margin-bottom:30px;text-shadow: 2px 2px rgba(0, 0, 0, 0.1); font-weight:200;font-size:30px; color:#ffffff'>[[value]]</div>"
	        }
	    ],
	    "chartCursor": {
	        "cursorAlpha": 1,
	        "zoomable": false,
	        "cursorColor": "rgba(255,255,255,.3)",//"#FFFFFF"
	        "categoryBalloonColor": "#8d83c8",
	        "fullWidth": true,
	        "categoryBalloonDateFormat": "YYYY-MM-DD",
	        "balloonPointerOrientation": "vertical"
	    },
	    "balloon": {
	        "borderAlpha": 0,
	        "fillAlpha": 0,
	        "shadowAlpha": 0,
	        "offsetX": 40,
	        "offsetY": -50
	    }
	});

	// we zoom chart in order to have better blur (form side to side)
	chart.addListener("dataUpdated", zoomChart);

	function zoomChart(){
	    chart.zoomToIndexes(0, chartData.length );
	}
	// =======amChart=======end
}

// ===export data===
function exportData(data){
	//清除舊的datatable 資料
	if ($.fn.DataTable.isDataTable("#exportTable-sale")) {
	  $('#exportTable-sale').DataTable().clear().destroy();
	}
	 

	if( data != undefined ){	
		var content = '';	
		var datatableData = [];
		$.each( data ,function( index , value ){
			$.each( value , function( contentIndex , contentValue ){
				$.each( contentValue ,function( detailIndex, detailValue){
					var discount = 0;
					if( detailValue["order_detail_price_discount"] != null ){
						discount = detailValue["order_detail_price_discount"];
					}

					dataArr = [ 				
						detailValue["date"],
						detailValue["order_group"],
						detailValue["order_no"],
						detailValue["product_brand"],
						detailValue["model_id"],
						detailValue["product_name"],
						detailValue["product_quantity"],
						detailValue["product_price"],
						detailValue["coupon"],
						'-'+discount,
						detailValue["cargo"],
						detailValue["total_price"]
					];
					datatableData.push(dataArr);
				});
				
			});
		});
		//重新建立datatable 資料
		$('#exportTable-sale').DataTable({
			order: [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                // 'pdfHtml5'
            ],
			data:datatableData
		});	
	}else{
		// initail datatable
		$('#exportTable-sale').DataTable({
			order: [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                // 'pdfHtml5'
            ],
		});	
	}
}

});