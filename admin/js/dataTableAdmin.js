$(document).ready(function(){

	

    // ------member.php-------
	$("#dataTable-member").DataTable({
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		"responsive": true,
		"pagingType": "full_numbers",
        "dom": 'lBfrtip',
        "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ':visible' ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": ':visible'
                }
            },
            // {
            //     "extend": 'pdfHtml5',
            //    " exportOptions": {
            //         "columns": [ 0, 1, 2, 5 ]
            //     }
            // },
            'colvis'
        ]   , 
        "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableMemberAjaxServer.php",
        "columns": [
            { "data": "mem_no" },
            { "data": function(source, type, val){
                return source.mem_lastname + source.mem_firstname;
            }},
            { "data": function(source, type, val){
                return source.mem_mail;
            }},
            { "data": function(source, type, val){
                return source.mem_tel;
            }},
            { "data": function(source, type, val){
                var createTime = source.mem_createtime;
                var date = new Date(parseInt(createTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": function(source, type, val){
                var loginInTime = source.mem_lastLoginTime;
                var date = new Date(parseInt(loginInTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": function(source, type, val){
                if( source.mem_status == "1"){
                    return "啟用";
                }else{
                    return "關閉";
                } 
            }},
            { "data": function(source, type, val){
                return '<span class="edit"><form action="memberEdit.php" method="get"><input type="hidden" name="ID" value='+ source.mem_no +'><button>編輯</button></form></span>';
            }},
           
        ],
        "fnDrawCallback" : function(oSettings){
            
        }  
	});


    //administrator.php
    $("#dataTable-admin").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        "pagingType": "full_numbers",
        "dom": 'lBfrtip',
        "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ':visible' ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": ':visible'
                }
            },
            // {
            //     "extend": 'pdfHtml5',
            //    " exportOptions": {
            //         "columns": [ 0, 1, 2, 5 ]
            //     }
            // },
            'colvis'
        ]   , 
        "order": [[ 0, "asc" ]],
        "processing": true,
        "serverSide": true,
        "ajax": "framwork/datatableAdminAjaxServer.php",
        "columns": [
            { "data": "adm_no" },
            { "data": "adm_email"},
            { "data": "adm_authLevel"},
            { "data": "adm_name"},
            { "data": function(source, type, val){
                var createTime = source.adm_createtime;
                var date = new Date(parseInt(createTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": function(source, type, val){
                var loginInTime = source.adm_lastLoginTime;
                if( loginInTime !=0 ){
                    var date = new Date(parseInt(loginInTime)*1000);
                    var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                    return time;
                }else{
                    return "null";
                }                 
            }},
            { "data": function(source, type, val){
                if( source.adm_status == "1"){
                    return "啟用";
                }else{
                    return "關閉";
                } 
            }},
            { "data": function(source, type, val){
                return '<span class="edit"><form action="administratorEdit.php" method="get"><input type="hidden" name="ID" value='+ source.adm_no +'><button>編輯</button></form></span>';
            }},
           
        ],
        "fnDrawCallback" : function(oSettings){
            
        }  
    });


	// ------product.php-------
	productTableServer = $("#dataTableServer").DataTable({
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		"responsive": true,
		// "pagingType": "full",
		"order": [[ 0, "desc" ]],
        "dom": 'lBfrtip',
        "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ':visible' ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": ':visible'
                }
            },
            // {
            //     "extend": 'pdfHtml5',
            //    " exportOptions": {
            //         "columns": [ 0, 1, 2, 5 ]
            //     }
            // },
            'colvis'
        ]   ,   
		"processing": true,
        "serverSide": true,
        "ajax": "framwork/datatableAjaxServer.php",
        "columns": [
            { "data": "product_no" },
            { "data": "model_id" },
            { "data": function(source, type, val){
                return '<a target="_blank" href="../product-detail.php?product_no='+source.product_no+'">'+source.product_name+'</a>';
            }},
            // { "data": "cate" },
            { "data": function(source, type, val){
            	if(source.cate){ //存在分類
					var cateArr = "";
	            	for(var i=0 ; i<source.cate.length ; i++){
	            		var cateProductNo = '<input type="hidden" name="cateRelateNo" value="'+source.cate_product_no[i]+'">';
                        var cateNo = '<input type="hidden" name="cate_no" value="'+source.cate_no[i]+'">';
	            		cateArr += '<a target="_blank" href="../brands.php?cate_no='+source.cate_no[i]+'">'+source.cate[i]+'</a>'+cateNo+cateProductNo+'<br>'; 
	            	}
	            	return cateArr;
            	}else{ //不存在分類
            		return "無";
            	}
            	
            }},
            // { "data": "tag_name" },
            { "data": function(source, type, val){
            	if(source.tag_name){ //存在
            		var tagArr = "";
	            	for(var i=0 ; i<source.tag_name.length ; i++){
	            		var tagProductNo = '<input type="hidden" name="tag_product_no" value="'+source.tag_product_no[i]+'">';
                        if(source.cate_tag_groupName[i]){
                            tagGroupName = '('+source.cate_tag_groupName[i]+')';
                        }else{
                            tagGroupName = '' ;
                        }
	            		tagArr += '<a target="_blank" href="../products.php?tag_no[]='+source.tag_no[i]+'">'+'#'+source.tag_name[i]+tagGroupName+'</a>'+tagProductNo+'<br>'; 
	            	}
	            	return tagArr;

            	}else{ //不存在
            		return "無";
            	} 
            }},
            // { "data": "product_status" },
            // { "data": function(source, type, val){
            //     if(source.month == 1){
            //         var cateProductNo = '<input type="hidden" name="cateRelateNo" value="'+source.monthRelateNo+'">';
            //         var monthArr = '本月推薦商品'+cateProductNo;
            //         return monthArr;
            //     }else{
            //         return"無";
            //     }     
            // }},
            { "data": function(source, type, val){
                if(source.activity){
                    var activityArr = "";
                    for(var i=0 ; i<source.activity.length ; i++){
                        var activityProductNo = '<input type="hidden" name="cateRelateNo" value="'+source.activityRelateNo[i]+'">';
                        activityArr += source.activity[i]+activityProductNo+'<br>'; 
                    }
                    return activityArr;
                }else{
                    return "無";
                }
                return"無";
            }},
            // { "data": function(source, type, val){return"無";}},
            { "data": function(source, type, val){
            	var status = "<input type='hidden' name='product_status' value='"+source.product_status+"'>"
            	if(source.product_status == 2){
            		var b = "已下架";
            	}else if(source.product_status == 1){
            		var b = "銷售中";
            	}else{
            		var b = "未啟用";
            	}
            	return b+status;
            }},
            { "data": function(source, type, val){
            	if(source.product_360_status == 0){
            		var status = "關閉" ;
            		return '<span class="badge badge-pill badge-danger">'+status+'</span>';
            	}else{
                    var status = "開啟" ;
    				return '<span class="badge badge-pill badge-primary">'+status+'</span>';
                }          
            }},
            { "data": "product_updatetime" },
            { "data": function(source, type, val){
            	var span = '';
            	var button ='';
            	if(source.product_status == 1){
            		var status = "下架" ;
            		return '<span class="edit"><form action="productEdit.php" method="get"><input type="hidden" name="product_no" value="'+source.product_no+'"><button class="productEditButton">編輯</button></form></span><span class="edit"><button class="productStatusButton">'+status+'</button></span>';
            	}else if(source.product_status == 2){
        			var status = "上架" ;
        			return '<span class="edit"><form action="productEdit.php" method="get"><input type="hidden" name="product_no" value="'+source.product_no+'"><button class="productEditButton">編輯</button></form></span><span class="edit"><button class="productStatusButton">'+status+'</button></span>';
    			}else{
    				return '<span class="edit"><form action="productEdit.php" method="get"><input type="hidden" name="product_no" value="'+source.product_no+'"><button class="productEditButton">啟用</button></form>';
    			} 
            }}

        ],
        "fnDrawCallback" : function(oSettings){
        	$("#dataTableServer").find("tbody tr").addClass("product");
        	callback();
        }
	});


	$("#dataTableServer tbody").on("click","tr",function(){
		$(this).toggleClass('selected');
	});
	// ===全選====
	$("#productSelectAll").click(function(){
		// console.log($(this).parents(".demo").find("tbody tr"));
		$(this).parents(".demo").find("tbody tr").addClass("selected");
	});
	// ====全不選=====
	$("#productSelectNone").click(function(){
		// console.log($(this).parents(".demo").find("tbody tr"));
		$(this).parents(".demo").find("tbody tr").removeClass("selected");
	});


    // ======serverside 生成後建立callback 註冊======
	function callback(){
		$(".productEditButton").click(function(e){
			e.stopPropagation(); // =====取消====bubble or capture
			
			// location.href="productEdit.php";
		});

		$(".productStatusButton").click(function(e){
		e.stopPropagation(); // =====取消====bubble or capture
		$(".productStatusButton").removeClass("currentRow");
		$(this).addClass("currentRow");

		var tableWrite = "product";
		var statusWrite = "修改";
		var productId = $(this).parents("tr.product").find("td").eq(0).text();
		if($(this).text() == "上架"){
			var productStatus = "1" ;
		}else{
			var productStatus = "2" ;
		}
		$.ajax({
				url:"framwork/productAjax.php",
				data : {table : tableWrite,status : statusWrite,product_no : productId,product_status : productStatus},
	 			type : "POST",
	 			dataType : "json",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 

				    		// =====dom 寫入=======
				    		// if(result["product_status"] == 1){
				    		// 	var status = "下架";
				    		// 	var statusForColumn = "銷售中";
				    		// }else{
				    		// 	var status = "上架";
				    		// 	var statusForColumn = "已下架";
				    		// }
				    		// $(".currentRow").text(status);
				    		// $(".currentRow").parents("tr.product").find("td").eq(8).text(statusForColumn);
				    		// $(".currentRow").parents("tr.product").find("td").eq(9).text(result["product_updatetime"]);

				    		// ======server reload======
							// productTableServer.draw();
							productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁

				    	}else{ //false
				    		// alert("找無分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});
	});

	} //-----end callback

	$("#cateSelects").change(function(){
		var cateSelector = $("#cateSelects").val();
		// console.log(cateSelector);
		if(cateSelector == 0){
			productTableServer.column(3).search('').draw();  //回到預設資料初始值 --> 原始所有資料
		}else{
			productTableServer.column(3).search(cateSelector).draw(); //下拉式搜尋
		}
		
	});

    $("#productStatus").change(function(){
        var productStatus = $("#productStatus").val();
        // console.log(productStatus);
        if(productStatus == 3){ //全部商品
            productTableServer.column(6).search('').draw();
        }else{
            productTableServer.column(6).search(productStatus).draw();
        }
    });

	

	// ------coupon.php-------
	couponTableServer = $("#couponServerTable").DataTable({
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		"responsive": true,
		// "pagingType": "full",
		"processing": true,
        "serverSide": true,
        "ajax": "framwork/datatableCouponAjaxServer.php",
        "columns": [
            { "data": "coupon_name" },
            { "data": "coupon_code" },
            { "data": "coupon_used" },
            { "data": function(source, type, val){
            	return source.coupon_discount+"%";
            }},
            { "data": function(source, type, val){
            	var start = source.coupon_start;
            	var date = new Date(parseInt(start)*1000);
            	var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);	            	
            	return time;
            }},
            { "data": function(source, type, val){
            	var end = source.coupon_end;
            	var date = new Date(parseInt(end)*1000);
            	var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);	            	
            	return time;
            }},
            { "data": function(source, type, val){
            	if(source.coupon_status == 0){
            		return "停用";
            	}else{
            		return "啟用";
            	}
            }},
            { "data": function(source, type, val){
        		   if(source.coupon_no){ //存在
        		   		return '<span class="edit"><form action="couponEdit.php" method="post"><input type="hidden" name="coupon_no" value="'+source.coupon_no+'"><button class="couponEditButton">編輯</button></form></span><span class="edit"><button class="couponDeleteButton">刪除</button></span>';
 	     		   }else{ //不存在

 	     		   }
            }} 
        ],
        "fnDrawCallback" : function(oSettings){
        	couponDeleteButton();
        }
	});


	function couponDeleteButton(){
		// -----優惠劵 刪除------
		$(".couponDeleteButton").click(function(e){
			e.preventDefault();
			var couponNo = $(this).parents("td").find("input[name='coupon_no']").val();
			var tableWrite = "coupon";
			var statusWrite ="刪除";
			// console.log($(this).parents("td").find("input[name='coupon_no']").val());
			$.ajax({
					url:"framwork/couponAjax.php",
					data : {table : tableWrite,status : statusWrite,coupon_no:couponNo},
		 			type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		couponTableServer.ajax.reload( null, false );
				    	}else{ //false
				    		alert("刪除失敗");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});

		});
	}



	// ------order.php-------
	orderServerTable = $("#orderServerTable").DataTable({
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		"responsive": true,
		// "pagingType": "full",
		"processing": true,
		"order": [[ 0, "desc" ]],
        "serverSide": true,
        "dom": 'lBfrtip',
        "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ':visible' ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": ':visible'
                }
            },
            // {
            //     "extend": 'pdfHtml5',
            //    " exportOptions": {
            //         "columns": [ 0, 1, 2, 5 ]
            //     }
            // },
            'colvis'
        ]   , 
        "ajax": "framwork/datatableOrderAjaxServer.php",
        "columns": [
            { "data": "order_no" },
            { "data": "order_group" },
            { "data": function(source, type, val){
            	switch(source.order_status){
            		case "0":
            		 	return "處理中";
            		 	break;
            		case "1":
            		 	return "收款確認";
            		 	break; 
            		case "2":
            		 	return "出貨";
            		 	break;  
            		case "3":
            		 	return "退貨";
            		 	break;
            		case "4":
            		 	return "取消";
            		 	break;
            		case "5":
            		 	return "完成";
            		 	break; 	 	  			
            	}
            }},
            { "data": function(source, type, val){
            	if(source.mem_no){ //是會員
            		return source.order_recipient+"(會員)";
            	}else{
            		return source.order_recipient;
            	}
            }},
            { "data": function(source, type, val){
            	var orderTime = source.order_createtime;
            	var date = new Date(parseInt(orderTime)*1000);
            	var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
            	return time;
            }},
            { "data": "cate_name"},
            { "data": function(source, type, val){
    		   return "$"+source.order_price;
            }},
            { "data": function(source, type, val){
    		   return "$"+source.order_cargo;
            }},
            { "data": function(source, type, val){
            	switch(source.order_pay){
            		case "0":
            		 	return "信用卡";
            		 	break;
            		case "1":
            		 	return "匯款";
            		 	break; 
            		case "2":
            		 	return "超商付款";
            		 	break;
            		case "3":
            		 	return "信用卡分期付款(" + source.order_pay_install + "期)";
            		 	break;
            		case "4":
            		 	 return "現金匯款";
            		 	 break 
            		 default:
            		 	 return "未知的付款方式";
            		 	 break
            	}
            }},
            { "data": "order_address"},
            { "data": function(source, type, val){
    		   return '<span class="edit"><form action="orderEdit.php" method="get"><input type="hidden" name="order_no" value="'+source.order_no+'"><button class="orderEditButton">編輯訂單</button></form></span><span class="edit"><form action="orderGroupEdit.php" method="get"><input type="hidden" name="order_group" value="'+source.order_group+'"><button class="orderCheckButton">查看群組詳細訂單</button></span>';
            }} 
        ],
        "fnDrawCallback" : function(oSettings){
        	
        }
	});

    $("#orderStatusSelects").change(function(){
        var orderStatusSelect = $(this).val();
        // console.log(orderStatusSelect);
        if(orderStatusSelect == 'all'){
            orderServerTable.column(2).search('').draw();  //回到預設資料初始值 --> 原始所有資料
        }else{
            orderServerTable.column(2).search(orderStatusSelect).draw(); //下拉式搜尋
        }
        
    });

    $("#timeZoneConfirm-order").click(function(){
        var startZone = $(this).siblings("#to").val();
        var endZone = $(this).siblings("#from").val();
        if( !startZone || !endZone ){
            orderServerTable.column(3).search("").draw();  //回到預設資料初始值 --> 原始所有資料
            orderServerTable.column(4).search("").draw();  //回到預設資料初始值 --> 原始所有資料
            alert("請輸入區間");
            if( !endZone ){ 
                $("#orderProcess #from").focus(); 
            }else{
                $("#orderProcess #to").focus();
            }   
        }else{
            var startZoneArr =  startZone.split("/");
            var endZoneArr =  endZone.split("/");
            var startZoneReStr = startZoneArr[2]+"-"+startZoneArr[0]+"-"+startZoneArr[1];
            var endZoneReStr = endZoneArr[2]+"-"+endZoneArr[0]+"-"+endZoneArr[1];
            orderServerTable.column(3).search(endZoneReStr).draw();  //開始時間
            orderServerTable.column(4).search(startZoneReStr).draw();  //結束時間
        }
    });



	orderHistoryServerTable = $("#orderHistoryServerTable").DataTable({
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
		"responsive": true,
		// "pagingType": "full",
		"processing": true,
		"order": [[ 0, "desc" ]],
        "dom": 'lBfrtip',
        "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ':visible' ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": ':visible'
                }
            },
            // {
            //     "extend": 'pdfHtml5',
            //    " exportOptions": {
            //         "columns": [ 0, 1, 2, 5 ]
            //     }
            // },
            'colvis'
        ]   , 
        "serverSide": true,
        "ajax": "framwork/datatableOrderHistoryAjaxServer.php",
        "columns": [
            { "data": "order_no" },
            { "data": "order_group" },
            { "data": function(source, type, val){
            	switch(source.order_status){
            		case "0":
            		 	return "處理中";
            		 	break;
            		case "1":
            		 	return "收款確認";
            		 	break; 
            		case "2":
            		 	return "出貨";
            		 	break;  
            		case "3":
            		 	return "退貨";
            		 	break;
            		case "4":
            		 	return "取消";
            		 	break;
            		case "5":
            		 	return "完成";
            		 	break; 	 	  			
            	}
            }},
            { "data": function(source, type, val){
            	if(source.mem_no){ //是會員
            		return source.order_recipient+"(會員)";
            	}else{
            		return source.order_recipient;
            	}
            }},
            { "data": function(source, type, val){
            	var orderTime = source.order_createtime;
            	var date = new Date(parseInt(orderTime)*1000);
            	var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
            	return time;
            }},
            { "data": "cate_name"},
            { "data": function(source, type, val){
    		   return "$"+source.order_price;
            }},
            { "data": function(source, type, val){
    		   return "$"+source.order_cargo;
            }},
            { "data": function(source, type, val){
            	switch(source.order_pay){
            		case "0":
            		 	return "信用卡";
            		 	break;
            		 case "3":
            		 	return "信用卡分期";
            		case "1":
            		 	return "匯款";
            		 	break; 
            		case "2":
            		 	return "超商付款";
            		 	break; 
            		 default:
            		 	return "source.order_pay";
            		 	break; 
            	}
            }},
            { "data": "order_address"},
            { "data": function(source, type, val){
    		   return '<span class="edit"><form action="orderEdit.php" method="get"><input type="hidden" name="order_no" value="'+source.order_no+'"><button class="orderEditButton">查看訂單</button></form></span><span class="edit"><form action="orderGroupEdit.php" method="get"><input type="hidden" name="order_group" value="'+source.order_group+'"><button class="orderCheckButton">查看群組訂單</button></span>';
            }} 
        ],
        "fnDrawCallback" : function(oSettings){
        	
        }
	});
    $("#orderStatusHistorySelects").change(function(){
        var orderStatusHistorySelects = $(this).val();
        // console.log(orderStatusHistorySelects);
        if(orderStatusHistorySelects == 'all'){
            orderHistoryServerTable.column(2).search('').draw();  //回到預設資料初始值 --> 原始所有資料
        }else{
            orderHistoryServerTable.column(2).search(orderStatusHistorySelects).draw(); //下拉式搜尋
        }
        
    });
    $("#timeZoneConfirm-order-history").click(function(){
        var startZone = $(this).siblings("#to-history").val();
        var endZone = $(this).siblings("#from-history").val();
        if( !startZone || !endZone ){
            orderHistoryServerTable.column(3).search("").draw();  //回到預設資料初始值 --> 原始所有資料
            orderHistoryServerTable.column(4).search("").draw();  //回到預設資料初始值 --> 原始所有資料
            alert("請輸入區間");
            if( !endZone ){ 
                $("#orderProcess #from").focus(); 
            }else{
                $("#orderProcess #to").focus();
            }   
        }else{
            var startZoneArr =  startZone.split("/");
            var endZoneArr =  endZone.split("/");
            var startZoneReStr = startZoneArr[2]+"-"+startZoneArr[0]+"-"+startZoneArr[1];
            var endZoneReStr = endZoneArr[2]+"-"+endZoneArr[0]+"-"+endZoneArr[1];       
            orderHistoryServerTable.column(3).search(endZoneReStr).draw();  //開始時間
            orderHistoryServerTable.column(4).search(startZoneReStr).draw();  //結束時間
        }
    });



    // ------style.php-------
    articleServerTable = $("#articleServerTable").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableArticleAjaxServer.php",
        "columns": [
            { "data": "article_no" },
            { "data": function(source, type, val){
                var titleArr = '<a target="_blank" href="../styles-detail.php?pid='+source.article_no+'">'+source.article_title+'</a>';
                return titleArr;
            }},
            { "data": function(source, type, val){
               if(source.article_tag){
                    var tagArr ="";
                    for(var i=0 ; i<source.article_tag.length ; i++){
                        var tagArticleNo = '<input type="hidden" name="tag_article_no" value="'+source.article_tag[i]['tag_article_no']+'">'; 
                        tagArr += '<a target="_blank" href="../styles.php?tag_no='+source.article_tag[i]['tag_no']+'">'+'#'+source.article_tag[i]['tag_name']+'</a>'+tagArticleNo+'<br>';                         
                    } 
                    return tagArr;
               }else{
                    return "無";
               }
            }},
            { "data": function(source, type, val){
                var imgSrc = source.article_img;
                return "<img src='../"+imgSrc+"' class='img-fluid'>" ;   
            }},
            { "data": function(source, type, val){
                var createTime = source.article_createtime;
                var date = new Date(parseInt(createTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": "article_owner" },
            { "data": function(source, type, val){
                switch(source.article_status){
                    case "0":
                        return "關閉";
                        break;
                    case "1":
                        return "開啟";
                        break; 
                }
            }},
            { "data": function(source, type, val){
               return '<span class="edit"><form action="styleEdit.php" method="get"><input type="hidden" name="article_no" value="'+source.article_no+'"><button class="articleEditButton">編輯文章</button></form></span><span class="edit"><button class="articleDeleteButton">刪除</button></span>';
            }} 
        ],
        "fnDrawCallback" : function(oSettings){
            //重新綁定
            articleDeleteButton();
        }
    });


    // --------style.php-----文章
    // ====刪除====
    function articleDeleteButton(){
        $(".articleDeleteButton").click(function(e){
            e.preventDefault();
            var articleNo = $(this).parents("td").find("input[name='article_no']").val();
            // alert(articleNo);
            var status = "delete";
            $.ajax({
                data: {status , status,article_no : articleNo},
                type: "POST",
                url: "framwork/articleAjax.php", 
                cache: false,
                success: function(resp) {
                  if(resp){
                    articleServerTable.ajax.reload( null, false );
                  }else{
                    alert("刪除失敗");
                  }
                }
            });
        });
    };



    // ------order狀態(index)-------
    orderServerTableIndex = $("#orderServerTable-index").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableOrderAjaxServer.php",
        "columns": [
            { "data": function(source, type, val){
                if(source.mem_no){ //是會員
                    return source.order_recipient+"(會員)";
                }else{
                    return source.order_recipient;
                }
            }},
            { "data": "order_group" },
            { "data": function(source, type, val){
                var orderTime = source.order_createtime;
                var date = new Date(parseInt(orderTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": function(source, type, val){
                switch(source.order_status){
                    case "0":
                        return "處理中";
                        break;
                    case "1":
                        return "收款確認";
                        break; 
                    case "2":
                        return "出貨";
                        break;  
                    case "3":
                        return "退貨";
                        break;
                    case "4":
                        return "取消";
                        break;
                    case "5":
                        return "完成";
                        break;                  
                }
            }},
            { "data": function(source, type, val){
               return '<span class="edit"><form action="orderGroupEdit.php" method="get"><input type="hidden" name="order_group" value="'+source.order_group+'"><button class="orderCheckButton">查看訂單</button></span>';
            }} 
        ],
        "fnDrawCallback" : function(oSettings){
            
        }
    });
     // ------mem狀態(index)-------
    memServerTableIndex = $("#memberServerTable-index").DataTable({
        // "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableMemberAjaxServer.php",
        "columns": [
            { "data": "mem_no" },
            { "data": function(source, type, val){
                return source.mem_lastname + source.mem_firstname;
            }},
            { "data": function(source, type, val){
                var createTime = source.mem_createtime;
                var date = new Date(parseInt(createTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }},
            { "data": function(source, type, val){
                var loginInTime = source.mem_lastLoginTime;
                var date = new Date(parseInt(loginInTime)*1000);
                var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
                return time;
            }}
           
        ],
        "fnDrawCallback" : function(oSettings){
            
        }
    });

     // ------views狀態(index)-------
    viewsServerTableIndex = $("#viewsServerTable-index").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        // "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableViewsAjaxServer.php",
        "columns": [
            { "data": "brand" },
            { "data": "product" },
            { "data": "times" },
            { "data": function(source, type, val){
                var p = source.percentage;
                var num = new Number(p*100);
                return num.toFixed(2)+"%";
            } }
        ],
        "fnDrawCallback" : function(oSettings){
            
        }
    });

      // ------stock狀態(index)-------
    stockServerTableIndex = $("#stockServerTable-index").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        // "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableStockAjaxServer.php",
        "columns": [
            { "data": "product_no" },
            { "data": function(source, type, val){
                var product = "<a href='productEdit.php?product_no="+source.product_no+"'>"+source.product+"</a>";
                return product;
            }},
            { "data": "product_spec_info" },
            { "data": function(source, type, val){
                var stock = "<span style='color:#db363d;'>"+source.product_stock+"</span>"
                return stock;
            }}
        ],
        "fnDrawCallback" : function(oSettings){
            
        }
    });

    //banner.php
    bannerServerTable = $("#dataTable-banner").DataTable({
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
        "responsive": true,
        // "pagingType": "full",
        "processing": true,
        // "order": [[ 0, "desc" ]],
        "serverSide": true,
        "ajax": "framwork/datatableBannerAjaxServer.php",
        "columns": [
            { "data": "banner_no" },
            { "data": function(source, type, val){
                var imgBox ='';
                if(source.img){
                    imgBox = "<img class=\"img-fluid\" src='../"+source.img+"'>" ;
                }else{
                    imgBox = "<img class=\"img-fluid\" src='../image/frontend/banner/default.png'>" ;
                }
                return imgBox;
            }},
            { "data": function(source, type, val){
                var aBox = '';
                if(source.link){
                    aBox = "<a class='bannerLink' target='_blank' href='"+source.link+"' >"+source.link+"</a>"
                }
                return aBox;
            }},
            { "data": function(source, type, val){
                var blank =''
                if(source.blank == 1 ){
                    blank = '跳頁連結';
                }else{
                    blank = '不跳頁連結';
                }
                return blank;
            }},
            { "data" : function(source, type, val){
                var status = '';    
                if( source.banner_status == 1){
                    status = '啟用中';
                }else{
                    status = '停用中';
                }
                return status;
            }},
            { "data" : "banner_update" },
            { "data" : function(){
                return '<span class="edit"><button data-toggle="modal" data-target="#bannerEditModel">編輯</button></span>';
            }}
        ],
        "fnDrawCallback" : function(oSettings){
            bannerEdit();
            // bannerDeleteButton();
        }
    });

    function bannerEdit(){
        //跳出調整視窗
        $(".edit").find("button[data-target='#bannerEditModel']").click(function(){
            //intial model
            $("#temporaryBanner").attr("src","") ;
            $("#cateBanner").attr("src","");
            $("#bannerDelete").hide();
            $("#bannerLink").val("");
            $("input#banner_no").val("");

            var bannerNoCurrent = $(this).parents("tr").find("td").eq(0).text();
            var imgSrc = $(this).parents("tr").find("img").attr("src");
            var linkHref = $(this).parents("tr").find("a.bannerLink").attr("href");
            var bannerStatus = $(this).parents("tr").find("td").eq(4).text();
            if(bannerStatus.trim() == '啟用中'){
                bannerStatus = true;
            }else{
                bannerStatus = false;
            }
            var blankStatus = $(this).parents("tr").find("td").eq(3).text();
            if( blankStatus.trim() =='跳頁連結' ){
                blankStatus = true;
            }else{
                blankStatus = false;
            }

            // banner圖
            if(imgSrc.trim() == "../image/frontend/banner/default.png"){ //沒有儲存圖片路徑
                $("#temporaryBanner").attr("src","") ;
                $("#Banner").attr("src","");
                $("#bannerDelete").hide();
            }else{
                $("#temporaryBanner").attr("src",imgSrc) ;
                $("#Banner").attr("src",imgSrc) ; 
                $("#bannerDelete").show();
            }
           //連結
           $("#bannerLink").val(linkHref); 
           //banner狀態
           $("input#bannerStatus").bootstrapSwitch('state',bannerStatus);
           //blank狀態
           $("#blankStatus").bootstrapSwitch('state',blankStatus);
           //記錄點擊的banner id 
           $("input#banner_no").val(bannerNoCurrent.trim());
        });
    }

});







