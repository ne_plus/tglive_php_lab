$(document).ready(function(){


	// ------file upload 圖片-------
	// document.getElementById('fileImg1').onchange = file1Change;
	// function file1Change(){
	// 	var file = document.getElementById('fileImg1').files[0];
	// 	var content = '';

	// 	content += 'File Name: '+file.name+'\n';
	// 	content += 'File Size: '+file.size+' byte(s)\n';
	// 	content += 'File Type: '+file.type+'\n';
	// 	content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
	// 	console.log(content);
	// 	console.log($(this));
	// 	var readFile = new FileReader();
	// 	readFile.readAsDataURL(file);
	// 	readFile.addEventListener('load',function(){
	// 		var image = document.getElementById('img1');
	// 		image.src = readFile.result;
	// 	},false);
	// }

	$(".imgUpload").click(function(e){
		e.preventDefault();
		$(this).parent().siblings("input[type='file']").click();
	});

	$("input[type='file']").change(function(){	
		var file = $(this)[0].files[0] ;
		var content = '';

		content += 'File Name: '+file.name+'\n';
		content += 'File Size: '+file.size+' byte(s)\n';
		content += 'File Type: '+file.type+'\n';
		content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
		console.log(content);
		

		var img = $(this).siblings("img.imgShow");
		var readFile = new FileReader();
		readFile.readAsDataURL(file);
		readFile.addEventListener('load',function(){
			// var image = document.getElementById('img1');
			// image.src = readFile.result;
			img.attr("src",readFile.result);
			// console.log($(this));
		},false);
		$(this).siblings(".imgdeletebutton").find("button.imgdelete").show();
		// $(this).parent().submit();
		var productNo = $("input[name='product_no']").val();
		var modelId = $("input[name='model_id']").val();
		var imgNo = img.attr("id");
		var status = "product";
		var formData = new FormData();
		formData.append('file', file);
		formData.append('product_no', productNo);
		formData.append('model_id', modelId);
		formData.append('img_no', imgNo);
		formData.append('status', status);
		$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           // alert(data);
		       }
		});
	});


	$(".imgdelete").click(function(e){
		e.preventDefault();
		var productNo = $("input[name='product_no']").val();
		var modelId = $("input[name='model_id']").val();
		var imgCurrent = $(this).parent("div.imgdeletebutton").siblings("img.imgShow");
		var imgNo = $(this).parent("div.imgdeletebutton").siblings("img.imgShow").attr("id");
		var deleteButton = $(this);
		var statusWrite = "product";

		$.ajax({
		       url : 'framwork/imgDelete.php',
		       type : 'POST',
		       data : {product_no : productNo,model_id : modelId,img_no : imgNo,status : statusWrite},
		       cache : false,
		       success : function(result) {
		           if(result){ //true;
		           		imgCurrent.attr("src",""); 
		           		deleteButton.hide();
		           };
		           // alert(data);
		       }
		});
	});


});