$(document).ready(function(){

// ======= memberEdit.php======
	$("#memUpdateConfirm").click(function(){
		var tableWrite = "member";
 		var statusWrite = "修改";
		var mem_lastnameWrite = $("input#lastnameEdit").val();
		var mem_firstnameWrite = $("input#firstnameEdit").val();
		var mem_mailWrite = $("input#memMail").val();
		var mem_telWrite = $("input#telEdit").val();
		var mem_addressWrite = $("input#addrEdit").val();
		var mem_noteWrite = $("textarea#noteEdit").val();
		var mem_statusWrite = $("input#memStatus").prop("checked");
		$.ajax({
 			url:"framwork/adminAjax.php",
 			data : {table : tableWrite,status : statusWrite,mem_mail : mem_mailWrite,mem_lastname : mem_lastnameWrite,mem_firstname : mem_firstnameWrite,mem_tel : mem_telWrite,mem_address : mem_addressWrite,mem_note : mem_noteWrite,mem_ststus : mem_statusWrite},
 			type : "POST",
		    cache : false,
		    success : function(result){
		    	if(result){ //true 
		    		alert("更新成功");
		    		location.href="member.php";
		    	}else{ //false
		    		alert("更新失敗");
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
 		});
	});
	$("#memCreateCancel").click(function(){
		location.href="member.php";
	});


// ======= administrator.php======
	// ======bootsrap 4 lightbox=====

	// ======新增管理員========
 	$('#exampleModal').on('show.bs.modal', function (event) {	
  	})

 	$("#admCreateConfirm").click(function(){
 		// =====RegExp====
 		var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/;  //email 
 		var pswReg = /^\w{6,}$/; //password
		// =====end RegExp====
 		var tableWrite = "admin";
 		var statusWrite = "新增";
 		var adm_emailWrite = $("#admCreateMail").val();
 		var adm_passwordWrite = $("#admCreatePsw").val();
 		var adm_nameWrite = $("#admCreateName").val();
 		var adm_authLevelWrite = $("#admAuthLevel").val();
 		var adm_statusWrite = $("#admStatus").prop("checked");
 		if(adm_emailWrite.trim().length< 1 || adm_emailWrite.trim() == null){
 			alert("需填寫帳號");
 			$("#admCreateMail").focus();
 		}else{
 			if(emailReg.test(adm_emailWrite) == false ){  //mail regExp 判斷
 				alert("帳號填寫有誤");
 				$("#admCreateMail").focus();
 			}else{
 				if(adm_passwordWrite.trim().length< 6 || adm_passwordWrite.trim() == null){
	 				alert("需填寫6位數英數密碼");
	 				$("#admCreatePsw").select();

	 			}else{
	 				if(pswReg.test(adm_passwordWrite) == false ){  //password regExp 判斷
	 					alert("不可填英數以外的文字");
 						$("#admCreatePsw").select();
	 				}else{
	 					if(adm_nameWrite.trim().length< 1 || adm_nameWrite.trim() == null){
		 					alert("需填寫管理者名稱");
		 					$("#admCreateName").select();
		 				}else{
		 					if(adm_authLevelWrite == null){
		 						alert("需選擇管理者權限");
		 						$("#admAuthLevel").focus();
		 					}else{
		 						$.ajax({
						 			url:"framwork/adminAjax.php",
						 			data : {table : tableWrite,status : statusWrite,adm_email : adm_emailWrite,adm_password : adm_passwordWrite,adm_name : adm_nameWrite,adm_authLevel : adm_authLevelWrite,adm_status : adm_statusWrite},
						 			type : "POST",
								    cache : false,
								    success : function(result){
								    	if(result){ //true 
								    		alert("帳號新增成功");
								    		$('#exampleModal').modal('hide'); //====關閉光箱
								    		location.reload();
								    	}else{ //false
								    		alert("帳號已存在");
								    	}
								    },
								    error : function(error){
								    	alert("傳送失敗");
								    } 
						 		});		
		 					} // --- end 判斷管理者權限
		 				} // --- end 判斷管理者名稱
	 				}  // ---------end password regExp 判斷	
	 			} // --- end 判斷密碼
 			} 	//----end mail regExp 判斷 			
 		} //---- end 判斷帳號
 		
	});



	// ======編輯管理員======
	$("#admCancel").click(function(){
		location.href="administrator.php";
	});

	$("#admUpdate").click(function(){
		var tableWrite = "admin";
		var statusWrite = "修改";
		var adm_emailWrite = $("#adm_email").val();
		var adm_nameWrite = $("#nameEdit").val();
		var adm_authLevelWrite = $("#admAuthLevelEdit").val();
		var adm_statusWrite = $("#admStatus").prop("checked");
		if(adm_nameWrite.trim().length<1 || adm_nameWrite.trim() == null){
			alert("名稱不能空白");
 			$("#nameEdit").focus();
		}else{
			$.ajax({
	 			url:"framwork/adminAjax.php",
	 			data : {table : tableWrite,status : statusWrite,adm_email : adm_emailWrite,adm_name : adm_nameWrite,adm_authLevel : adm_authLevelWrite,adm_status : adm_statusWrite},
	 			type : "POST",
			    cache : false,
			    success : function(result){
			    	if(result){ //true 
			    		alert("更新成功");
			    		location.href = "administrator.php";
			    	}else{ //false
			    		alert("更新失敗");
			    	}
			    },
			    error : function(error){
			    	alert("傳送失敗");
			    } 
	 		});	
		} // 名稱欄位判斷
		
	});

// ======= category.php====== 
	// 主打360 品牌設定
	$("#primary360").change(function(){
		var currentID = $("#currentPrimary360ID").val();
		var selectID = $(this).val();

		if( currentID == selectID ){
			$("#selectPrimary360Confirm").attr("disabled" , true);
			$("#selectPrimary360Confirm").addClass("buttonDisabled text-white");
		}else{
			$("#selectPrimary360Confirm").attr("disabled" , false);
			$("#selectPrimary360Confirm").removeClass("buttonDisabled text-white");
		}
	})
	$("#selectPrimary360Confirm").unbind('click').click(function(){
		var tableWrite = "category";
		var statusWrite = "update360";
		var currentID = $("#currentPrimary360ID").val();
		var selectID = $("#primary360").val();
		// a. 刪除原本設定的主打
		if ( currentID != 0) {
			$.ajax({
				url:"framwork/categoryAjax.php",
				 data : {table : tableWrite,status : statusWrite,cate_no : currentID, cate_360_main : 0},
				 type : "POST",
				cache : false,
				success : function(result){
						if(result){ //true 
							console.log(result)
						}else{ //false
							alert("ID有問題");
						}
					},
				error : function(error){
						alert("傳送失敗");
					} 
			});
		}
		
		// b. 更新新選擇的主打
		$.ajax({
			url:"framwork/categoryAjax.php",
			 data : {table : tableWrite,status : statusWrite,cate_no : selectID, cate_360_main : 1},
			 type : "POST",
			cache : false,
			success : function(result){
					if(result){ //true 
						alert("更新主打品牌成功");	
						location.reload();
					}else{ //false
						alert("ID有問題");
					}
				},
			error : function(error){
					alert("傳送失敗");
				} 
		});
	})



	// ======下拉式箭頭=====
	$(".category a[data-toggle='collapse']").click(function(){
		var classStr = $(this).find("i").attr("class");
		if(classStr.indexOf("angleRight")!= -1){
			$(this).find("i").removeClass("angleRight");
		}else{
			$(this).find("i").addClass("angleRight");
		}
	});



	// =======新增分類=======
	$("#createCate").click(function(){
		var str = $(this).parent().parent().find("input[id='createCateName']").val();
		// console.log(str.trim().length);
		if(str.trim().length< 1 || str.trim() == null){
			$("input[id='createCateName']").focus();
			alert("請輸入新增分類的名稱");
		}else{
			if($("#cate_level").val() != null){
				var tableWrite = "category";
				var statusWrite = "新增";
				var cateName = str.trim();
				var cateParents = $("#cate_level").val();
				$.ajax({
					url:"framwork/categoryAjax.php",
		 			data : {table : tableWrite,status : statusWrite,cate_name : cateName,cate_parents : cateParents},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		alert("新增分類成功");	
					    		location.reload();
					    	}else{ //false
					    		alert("有存在相同分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});
			}else{
				alert("需選擇分類層級");
				$("#cate_level").focus();
			}
		};
	});


	// ========編輯分類======= 父層
	$(".cateFatherEdit").click(function(){
		// 360 區域
		$("#cate360area").hide();
		// 360 區域 end

		//select 歸0
		$("#cateEditFatherSelect").val(62); //因為是父層所以都停留在第一層
		$("#cateEditFatherSelect option").each(function(){
			if($(this).val() != null){
				if($(this).val() != 62){
					$(this).remove();
				}
			}
		});

		var oldCateName = $(this).parents(".cateFather").find(".cateFatherInfo").text();
		var cateNo = $(this).parents(".cateFather").find("input[name='cate_no']").val();

		// ------抓取所有父層------
		var statusWrite ="查詢";
		$.ajax({
			url:"framwork/categoryAjax.php",
			data : {status : statusWrite},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
			    	 //有父層分類 
		    		for(var i=0 ;i<result.length ;i++){
		    			if(result[i]["cate_no"] != cateNo){
		    				var text = result[i]["cate_name"];
			    			var value = result[i]["cate_no"];
			    			option = new Option(text,value) ;
			    			$("#cateEditFatherSelect").append(option);
		    			}		    	
		    		}
			    	
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});
		// =====圖片暫存刪除=====

		// ======DB圖片搜尋 有的話放入img=====
		var infoStatus = "分類資訊";
		$.ajax({
			url:"framwork/categoryAjax.php",
			data : {status : infoStatus,cate_no : cateNo},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
			    	// console.log(result);

			    	// log圖
			    	if(result["cate_logo_img"].trim() == "" || result["cate_logo_img"].trim() == null){ //沒有儲存圖片路徑
			    		$("#temporaryLogo").attr("src","") ;
			    		$("#cateLogo").attr("src","");
			    		$("#logoDelete").hide();
			    	}else{
			    		// console.log("有");
			    		$("#temporaryLogo").attr("src","../"+result["cate_logo_img"]) ;
			    		$("#cateLogo").attr("src","../"+result["cate_logo_img"]) ; 
			    		$("#logoDelete").show();
			    	}
			    	// banner圖
			    	if(result["cate_banner_img"].trim() == "" || result["cate_banner_img"].trim() == null){ //沒有儲存圖片路徑
			    		$("#temporaryBanner").attr("src","") ;
			    		$("#cateBanner").attr("src","");
			    		$("#bannerDelete").hide();
			    	}else{
			    		// console.log("有");
			    		$("#temporaryBanner").attr("src","../"+result["cate_banner_img"]) ;
			    		$("#cateBanner").attr("src","../"+result["cate_banner_img"]) ; 
			    		$("#bannerDelete").show();
			    	}
			    	$("#cateDescribe").val(result["cate_describe"]);	    					    	
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});

		// ======Logo圖片上傳==========
		$("#logoUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).siblings("input[type='file']").click();
		});
		$("input[name='cate_logo']").unbind('change').change(function(){
			var file = $(this)[0].files[0] ;
			var content = '';
			
			content += 'File Name: '+file.name+'\n';
			content += 'File Size: '+file.size+' byte(s)\n';
			content += 'File Type: '+file.type+'\n';
			content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
			console.log(content);

			$(this).siblings("#logoDelete").show();
			var img = $(this).siblings("img.imgShow");
			var imgLogoTemperary = $(this).siblings("img#temporaryLogo");
			// //====js 讀圖檔位置尚未儲存檔案=====
			var readFile = new FileReader();
			readFile.readAsDataURL(file);
			readFile.addEventListener('load',function(){
				imgLogoTemperary.attr("src",readFile.result);
			},false);

			var status = "categoryLogo";
			var formData = new FormData();
			formData.append('file', file); 
			formData.append('cate_no', cateNo); 
			formData.append('status', status);
			$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           $("#cateLogo").attr("src","../image/category/"+data);
		       }
			});
		});
		// =====baneer圖片上傳==========
		$("#bannerUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).siblings("input[type='file']").click();
		});
		$("input[name='cate_banner']").unbind('change').change(function(){
			var file = $(this)[0].files[0] ;
			var content = '';
			
			content += 'File Name: '+file.name+'\n';
			content += 'File Size: '+file.size+' byte(s)\n';
			content += 'File Type: '+file.type+'\n';
			content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
			console.log(content);

			$(this).siblings("#bannerDelete").show();
			var img = $(this).siblings("img.imgShow");
			var imgBannerTemperary = $(this).siblings("img#temporaryBanner");
			// //====js 讀圖檔位置尚未儲存檔案=====
			var readFile = new FileReader();
			readFile.readAsDataURL(file);
			readFile.addEventListener('load',function(){
				imgBannerTemperary.attr("src",readFile.result);
			},false);

			var status = "categoryBanner";
			var formData = new FormData();
			formData.append('file', file); 
			formData.append('cate_no', cateNo); 
			formData.append('status', status);
			$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           $("#cateBanner").attr("src","../image/category/"+data);
		       }
			});
		});
		// ====編輯圖片刪除=====
		// ---logo-----
		var imglogoDelete ;
		$("#logoDelete").unbind('click').click(function(){
			// -----儲存圖檔路徑 在確認後執行刪除----
			imglogoDelete = $(this).siblings("img#cateLogo").attr("src").slice(3).trim() ;
			// -----將網頁圖片拉掉----
			$(this).siblings("img#temporaryLogo").attr("src","");
			$(this).siblings("img#cateLogo").attr("src","");
			$(this).hide();
		});
		// ---banner-----
		var imgBannerDelete ;
		$("#bannerDelete").unbind('click').click(function(){
			// -----儲存圖檔路徑 在確認後執行刪除----
			imgBannerDelete = $(this).siblings("img#cateBanner").attr("src").slice(3).trim() ;
			// -----將網頁圖片拉掉----
			$(this).siblings("img#temporaryBanner").attr("src","");
			$(this).siblings("img#cateBanner").attr("src","");
			$(this).hide();
		});

		$("input[id='cateNewName']").val(oldCateName);
		$("#editCateNameConfirm").unbind('click').click(function(){
			var newCateName = $("input[id='cateNewName']").val();
			var cateParents = $("#cateEditFatherSelect").val();
			var cateNewlogoImg = $("#cateLogo").attr("src").slice(3).trim();
			var cateNewbannerImg = $("#cateBanner").attr("src").slice(3).trim();
			var cateNewDescribe = $("#cateDescribe").val().trim();
			// console.log(cateNewlogoImg);
			if(newCateName.trim().length<1 || newCateName.trim()==null){
				alert("請輸入新的分類");
				$("input[id='cateNewName']").focus();
			}else{
				var tableWrite = "category";
				var statusWrite = "父層修改";
				var cateName = newCateName.trim();
				$.ajax({
					url:"framwork/categoryAjax.php",
					data : {table : tableWrite,status : statusWrite,cate_no : cateNo,cate_name : cateName,cate_parents : cateParents,cate_logo_img : cateNewlogoImg,cate_banner_img : cateNewbannerImg,cate_describe : cateNewDescribe},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		if(result == "相同分類"){
					    			alert("重複分類名稱");
					    		}else{
					    			 alert("修改分類成功");	
					    			location.reload();
					    			// console.log(result);
					    		}	
					    	}else{ //false
					    		alert("找無分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});

				// ===logo====
				if(imglogoDelete == undefined){ //圖片沒有刪除
					console.log("沒有喔");
				}else if( imglogoDelete != undefined && $("img#cateLogo").attr("src") != ""){ //圖片有刪除 但有新增圖檔
					if(imglogoDelete == $("img#cateLogo").attr("src").slice(3).trim()){ //不用刪除舊的檔案,會被新的覆蓋
						// console.log("相同的副圖檔");
						// console.log(imglogoDelete);
						// console.log($("img#cateLogo").attr("src").slice(3).trim());
					}else{ //刪除舊的檔案
						// console.log("不同的副檔名");
						// console.log(imglogoDelete);
						// console.log($("img#cateLogo").attr("src").slice(3).trim());
						deleteImg(imglogoDelete);
					}
				}else{ //有刪除但沒更新 ,休要刪除檔案
					// console.log("跑錯邊");
					deleteImg(imglogoDelete);
				}

				// ===banner====
				if(imgBannerDelete == undefined){ //圖片沒有刪除
					console.log("沒有喔");
				}else if( imgBannerDelete != undefined && $("img#cateBanner").attr("src") != ""){ //圖片有刪除 但有新增圖檔
					if(imgBannerDelete == $("img#cateBanner").attr("src").slice(3).trim()){ //不用刪除舊的檔案,會被新的覆蓋
						// console.log("相同的副圖檔");
						// console.log(imgBannerDelete);
						// console.log($("img#cateBanner").attr("src").slice(3).trim());
					}else{ //刪除舊的檔案
						// console.log("不同的副檔名");
						// console.log(imgBannerDelete);
						// console.log($("img#cateBanner").attr("src").slice(3).trim());
						deleteImg(imgBannerDelete);
					}
				}else{ //有刪除但沒更新 ,休要刪除檔案
					// console.log("跑錯邊");
					deleteImg(imgBannerDelete);
				}
				
			}


			function deleteImg(imgDelete){
				var imgSrc = imgDelete;
				var statusWrite = "category";
					$.ajax({
				       url : 'framwork/imgDelete.php',
				       type : 'POST',
				       data : {status : statusWrite,img_src : imgSrc},
				       cache : false,
				       success : function(result) {
				           if(result){ //true
				           		$("#cateLogo").attr("src","");
				           		$("#logoDelete").hide();
				           }else{ //刪除失敗

				           }  
				       }
					});
			}

		});

		
	});
	// ========刪除分類======= 父層
	$(".cateFatherDelete").click(function(){
		var cateNo = $(this).parents(".cateFather").find("input[name='cate_no']").val();
		var cateName = $(this).parents(".cateFather").find(".cateFatherInfo").text();
		var tableWrite = "category";
		var statusWrite = "刪除";
		var cateLevel = "1";
		if(confirm('確定要刪除 "'+cateName+'" 嗎？')){ //true yes

			// ------移除分類-----

			$.ajax({
				url:"framwork/categoryAjax.php",
				data : {table : tableWrite,status : statusWrite,cate_no : cateNo,cate_level : cateLevel},
	 			type : "POST",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 	
				    		location.reload();
				    		// console.log(result);
				    	}else{ //false
				    		alert("找無分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});

			
		}else{ //false no

		}
		

	});




	// ========編輯分類======= 子層
	$(".cateChildEdit").click(function(){
		// 360 區域
		$("#cate360area").show();
		$("#cate360url").val(null);
		$("#cate360Status").bootstrapSwitch('state',false)
		// 360 區域 end

		$("#cateEditFatherSelect").val(null);
		$("#cateEditFatherSelect option").each(function(){
			if($(this).val() != null){
				if($(this).val() != 62){
					$(this).remove();
				}
			}
		});

		var oldCateName = $(this).parents(".cate-level").find(".card-body").text();
		var cateNo = $(this).parents(".cate-level").find("input[name='cate_no']").val();
		// ------查詢父親是誰------
		var fatherCateno = $(this).parents(".catebox").find(".cateFatherInfo").find("input[name='cate_no']").val();
		// console.log(fatherCateno);

		// ------抓取所有父層------
		var statusWrite ="查詢";
		$.ajax({
			url:"framwork/categoryAjax.php",
			data : {status : statusWrite},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
			    	 //有父層分類 
		    		for(var i=0 ;i<result.length ;i++){
	    				var text = result[i]["cate_name"];
		    			var value = result[i]["cate_no"];
		    			option = new Option(text,value) ;
		    			$("#cateEditFatherSelect").append(option);			    	
		    		}
		    		// ----舊有父親選取------
			    	$("#cateEditFatherSelect").val(fatherCateno);
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});

		// =====圖片暫存刪除=====

		// ======DB圖片搜尋 有的話放入img=====
		var infoStatus = "分類資訊";
		$.ajax({
			url:"framwork/categoryAjax.php",
			data : {status : infoStatus,cate_no : cateNo},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
			    	// console.log(result);
			    	//logo圖
			    	if(result["cate_logo_img"].trim() == "" || result["cate_logo_img"].trim() == null){ //沒有儲存圖片路徑
			    		$("#temporaryLogo").attr("src","") ;
			    		$("#cateLogo").attr("src","");
			    		$("#logoDelete").hide();
			    	}else{
			    		// console.log("有");
			    		$("#temporaryLogo").attr("src","../"+result["cate_logo_img"]) ;
			    		$("#cateLogo").attr("src","../"+result["cate_logo_img"]) ; 
			    		$("#logoDelete").show();
			    	}
			    	// banner圖
			    	if(result["cate_banner_img"].trim() == "" || result["cate_banner_img"].trim() == null){ //沒有儲存圖片路徑
			    		$("#temporaryBanner").attr("src","") ;
			    		$("#cateBanner").attr("src","");
			    		$("#bannerDelete").hide();
			    	}else{
			    		// console.log("有");
			    		$("#temporaryBanner").attr("src","../"+result["cate_banner_img"]) ;
			    		$("#cateBanner").attr("src","../"+result["cate_banner_img"]) ; 
			    		$("#bannerDelete").show();
			    	}
					$("#cateDescribe").val(result["cate_describe"]);
					$("#cate360url").val(result["cate_360_link"]);
					if (result["cate_360_status"] == 1) {
						$("#cate360Status").bootstrapSwitch('state',true)
					} 					    	
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});

		// ======logo圖片上傳==========
		$("#logoUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).siblings("input[type='file']").click();
		});
		$("input[name='cate_logo']").unbind('change').change(function(){
			var file = $(this)[0].files[0] ;
			var content = '';
			
			content += 'File Name: '+file.name+'\n';
			content += 'File Size: '+file.size+' byte(s)\n';
			content += 'File Type: '+file.type+'\n';
			content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
			console.log(content);

			$(this).siblings("#logoDelete").show();
			var img = $(this).siblings("img.imgShow");
			var imgLogoTemperary = $(this).siblings("img#temporaryLogo");
			// //====js 讀圖檔位置尚未儲存檔案=====
			var readFile = new FileReader();
			readFile.readAsDataURL(file);
			readFile.addEventListener('load',function(){
				imgLogoTemperary.attr("src",readFile.result);
			},false);

			var status = "categoryLogo";
			var formData = new FormData();
			formData.append('file', file); 
			formData.append('cate_no', cateNo); 
			formData.append('status', status);
			$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           $("#cateLogo").attr("src","../image/category/"+data);
		       }
			});
		});
		// ========banner 圖片上傳=======
		$("#bannerUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).siblings("input[type='file']").click();
		});
		$("input[name='cate_banner']").unbind('change').change(function(){
			var file = $(this)[0].files[0] ;
			var content = '';
			
			content += 'File Name: '+file.name+'\n';
			content += 'File Size: '+file.size+' byte(s)\n';
			content += 'File Type: '+file.type+'\n';
			content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
			console.log(content);

			$(this).siblings("#bannerDelete").show();
			var img = $(this).siblings("img.imgShow");
			var imgBannerTemperary = $(this).siblings("img#temporaryBanner");
			// //====js 讀圖檔位置尚未儲存檔案=====
			var readFile = new FileReader();
			readFile.readAsDataURL(file);
			readFile.addEventListener('load',function(){
				imgBannerTemperary.attr("src",readFile.result);
			},false);

			var status = "categoryBanner";
			var formData = new FormData();
			formData.append('file', file); 
			formData.append('cate_no', cateNo); 
			formData.append('status', status);
			$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           $("#cateBanner").attr("src","../image/category/"+data);
		       }
			});
			// console.log(cateNo);
		});

		// ====編輯圖片刪除=====
		// ---logo-----
		var imglogoDelete ;
		$("#logoDelete").unbind('click').click(function(){
			// -----儲存圖檔路徑 在確認後執行刪除----
			imglogoDelete = $(this).siblings("img#cateLogo").attr("src").slice(3).trim() ;
			// -----將網頁圖片拉掉----
			$(this).siblings("img#temporaryLogo").attr("src","");
			$(this).siblings("img#cateLogo").attr("src","");
			$(this).hide();
		});
		// ---banner-----
		var imgBannerDelete ;
		$("#bannerDelete").unbind('click').click(function(){
			// -----儲存圖檔路徑 在確認後執行刪除----
			imgBannerDelete = $(this).siblings("img#cateBanner").attr("src").slice(3).trim() ;
			// -----將網頁圖片拉掉----
			$(this).siblings("img#temporaryBanner").attr("src","");
			$(this).siblings("img#cateBanner").attr("src","");
			$(this).hide();
		});

		$("input[id='cateNewName']").val(oldCateName);
		$("#editCateNameConfirm").unbind('click').click(function(){
			var fatherNo = $("#cateEditFatherSelect").val();
			var newCateName = $("input[id='cateNewName']").val();
			var cateNewlogoImg = $("#cateLogo").attr("src").slice(3).trim();
			var cateNewbannerImg = $("#cateBanner").attr("src").slice(3).trim();
			var cateNewDescribe = $("#cateDescribe").val().trim();
			var cate360status = $("#cate360Status").bootstrapSwitch('state') == true ? 1 : 0 ;
			var cate360url = $("#cate360url").val().trim();
			if(newCateName.trim().length<1 || newCateName.trim()==null){
				alert("請輸入新的分類");
				$("input[id='cateNewName']").focus();
			}else if( cate360status == 1 && cate360url == '' ){
				alert("請輸入360連結");
				$("#cate360url").focus();
			}else{
				var tableWrite = "category";
				var statusWrite = "修改";
				var cateName = newCateName.trim();
				$.ajax({
					url:"framwork/categoryAjax.php",
					data : {table : tableWrite,status : statusWrite,cate_no : cateNo,cate_name : cateName,cate_parents : fatherNo,cate_logo_img : cateNewlogoImg,cate_banner_img : cateNewbannerImg,cate_describe : cateNewDescribe ,cate_360_status : cate360status,cate_360_url :  cate360url},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		if(result == "相同分類"){
					    			alert("重複分類名稱");
					    		}else{
					    			 alert("修改分類成功");	
					    			location.reload();
					    			// console.log(result);
					    		}	
					    	}else{ //false
					    		alert("找無分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});

				// ===logo====
				if(imglogoDelete == undefined){ //圖片沒有刪除
					console.log("沒有喔");
				}else if( imglogoDelete != undefined && $("img#cateLogo").attr("src") != ""){ //圖片有刪除 但有新增圖檔
					if(imglogoDelete == $("img#cateLogo").attr("src").slice(3).trim()){ //不用刪除舊的檔案,會被新的覆蓋
						// console.log("相同的副圖檔");
						// console.log(imglogoDelete);
						// console.log($("img#cateLogo").attr("src").slice(3).trim());
					}else{ //刪除舊的檔案
						// console.log("不同的副檔名");
						// console.log(imglogoDelete);
						// console.log($("img#cateLogo").attr("src").slice(3).trim());
						deleteImg(imglogoDelete);
					}
				}else{ //有刪除但沒更新 ,休要刪除檔案
					// console.log("跑錯邊");
					deleteImg(imglogoDelete);
				}

				// ===banner====
				if(imgBannerDelete == undefined){ //圖片沒有刪除
					console.log("沒有喔");
				}else if( imgBannerDelete != undefined && $("img#cateBanner").attr("src") != ""){ //圖片有刪除 但有新增圖檔
					if(imgBannerDelete == $("img#cateBanner").attr("src").slice(3).trim()){ //不用刪除舊的檔案,會被新的覆蓋
						// console.log("相同的副圖檔");
						// console.log(imgBannerDelete);
						// console.log($("img#cateBanner").attr("src").slice(3).trim());
					}else{ //刪除舊的檔案
						// console.log("不同的副檔名");
						// console.log(imgBannerDelete);
						// console.log($("img#cateBanner").attr("src").slice(3).trim());
						deleteImg(imgBannerDelete);
					}
				}else{ //有刪除但沒更新 ,休要刪除檔案
					// console.log("跑錯邊");
					deleteImg(imgBannerDelete);
				}
			}

			function deleteImg(imgDelete){
				var imgSrc = imgDelete;
				var statusWrite = "category";
					$.ajax({
				       url : 'framwork/imgDelete.php',
				       type : 'POST',
				       data : {status : statusWrite,img_src : imgSrc},
				       cache : false,
				       success : function(result) {
				           if(result){ //true
				           		$("#cateLogo").attr("src","");
				           		$("#logoDelete").hide();
				           }else{ //刪除失敗

				           }  
				       }
					});
			}

		});
	});

	// ========刪除分類======= 子層
	$(".cateDelete").click(function(){
		var cateNo = $(this).parents(".cate-level").find("input[name='cate_no']").val();
		var oldCateName = $(this).parents(".cate-level").find(".card-body").text();
		var tableWrite = "category";
		var statusWrite = "刪除";
		var cateLevel = "2";
		if(confirm("確認要刪除 '" + oldCateName + "' 嗎?")){ //true yes
			
			// ------移除分類-----
								
			$.ajax({
				url:"framwork/categoryAjax.php",
				data : {table : tableWrite,status : statusWrite,cate_no : cateNo,cate_level : cateLevel},
	 			type : "POST",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 	
				    		location.reload();
				    	}else{ //false
				    		alert("找無分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});


		}else{   //false no
			// console.log("no");
		}
	});

// ======= categoryActivity.php====== 
	// 新增行銷分類
	$("#createActivityCate").click(function(){
		var str = $(this).parent().parent().find("input#createCateActivityName").val();
		if(str.trim().length< 1 || str.trim() == null){
			$(this).parent().parent().find("input#createCateActivityName").focus();
			alert("請輸入新增行銷分類");
		}else{
			var cateNewName = str.trim();
			var tableWrite = "category";
			var statusWrite = "新增";
			var cateParents = 63 ;
			$.ajax({
					url:"framwork/categoryAjax.php",
		 			data : {table : tableWrite,status : statusWrite,cate_name : cateNewName,cate_parents : cateParents},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		alert("新增活動成功");	
					    		location.reload();
					    		// console.log(result);
					    	}else{ //false
					    		alert("有存在相同分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});
		}	
	});

		// 編輯行銷分類
	$(".cateEdit").click(function(){
		var oldCateName =$(this).parents("tr").find(".cateName").text().trim();
		var cateNo = $(this).parents("tr").find("input[name='cate_no']").val();
		var cateDescribe = $(this).parents("tr").find("input[name='cate_describe']").val().trim();
		$("input#cateActivityNewName").val(oldCateName);
		$("textarea#cateDescribe").val(cateDescribe);

		// ======DB圖片搜尋 有的話放入img=====
		var infoStatus = "分類資訊";
		$.ajax({
			url:"framwork/categoryAjax.php",
			data : {status : infoStatus,cate_no : cateNo},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
			    	// console.log(result);
			    	// banner圖
			    	if(result["cate_banner_img"].trim() == "" || result["cate_banner_img"].trim() == null){ //沒有儲存圖片路徑
			    		$("#temporaryBanner").attr("src","") ;
			    		$("#cateBanner").attr("src","");
			    		$("#bannerDelete").hide();
			    	}else{
			    		// console.log("有");
			    		$("#temporaryBanner").attr("src","../"+result["cate_banner_img"]) ;
			    		$("#cateBanner").attr("src","../"+result["cate_banner_img"]) ; 
			    		$("#bannerDelete").show();
			    	}
			    	$("#cateDescribe").val(result["cate_describe"]);	    					    	
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});


		// =====baneer圖片上傳==========
		$("#bannerUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).siblings("input[type='file']").click();
		});
		$("input[name='cate_banner']").unbind('change').change(function(){
			var file = $(this)[0].files[0] ;
			var content = '';
			
			content += 'File Name: '+file.name+'\n';
			content += 'File Size: '+file.size+' byte(s)\n';
			content += 'File Type: '+file.type+'\n';
			content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
			console.log(content);

			$(this).siblings("#bannerDelete").show();
			var img = $(this).siblings("img.imgShow");
			var imgBannerTemperary = $(this).siblings("img#temporaryBanner");
			// //====js 讀圖檔位置尚未儲存檔案=====
			var readFile = new FileReader();
			readFile.readAsDataURL(file);
			readFile.addEventListener('load',function(){
				imgBannerTemperary.attr("src",readFile.result);
			},false);

			var status = "categoryBanner";
			var formData = new FormData();
			formData.append('file', file); 
			formData.append('cate_no', cateNo); 
			formData.append('status', status);
			$.ajax({
		       url : 'framwork/imgUpload.php',
		       type : 'POST',
		       data : formData,
		       cache : false,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		           console.log(data);
		           $("#cateBanner").attr("src","../image/category/"+data);
		       }
			});
		});

		// ====編輯圖片刪除=====
		// ---banner-----
		var imgBannerDelete ;
		$("#bannerDelete").unbind('click').click(function(){
			// -----儲存圖檔路徑 在確認後執行刪除----
			imgBannerDelete = $(this).siblings("img#cateBanner").attr("src").slice(3).trim() ;
			// -----將網頁圖片拉掉----
			$(this).siblings("img#temporaryBanner").attr("src","");
			$(this).siblings("img#cateBanner").attr("src","");
			$(this).hide();
		});
		

		//儲存設定
		$("button#editCateConfirm").unbind('click').click(function(){
			var newCateName =$("input#cateActivityNewName").val().trim();
			var cateCateDescribe =$("textarea#cateDescribe").val().trim();
			if(newCateName.trim().length<1 || newCateName.trim()==null){
				alert("請輸入活動的名稱");
				$("input#cateActivityNewName").focus();
			}else{
				var tableWrite = "category";
				var statusWrite = "修改";
				var fatherNo = "63"; //活動類別
				var cateNewlogoImg = null ;
				var cateNewbannerImg = $("#cateBanner").attr("src").slice(3).trim();
				$.ajax({
					url:"framwork/categoryAjax.php",
					data : {table : tableWrite,status : statusWrite,cate_no : cateNo,cate_name : newCateName,cate_parents : fatherNo,cate_logo_img : cateNewlogoImg,cate_banner_img : cateNewbannerImg,cate_describe : cateCateDescribe},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		if(result == "相同分類"){
					    			alert("重複分類名稱");
					    		}else{
					    			 alert("修改分類成功");	
					    			location.reload();
					    			// console.log(result);
					    		}	
					    	}else{ //false
					    		alert("找無分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});

				// ===banner====
				if(imgBannerDelete == undefined){ //圖片沒有刪除
					console.log("沒有喔");
				}else if( imgBannerDelete != undefined && $("img#cateBanner").attr("src") != ""){ //圖片有刪除 但有新增圖檔
					if(imgBannerDelete == $("img#cateBanner").attr("src").slice(3).trim()){ //不用刪除舊的檔案,會被新的覆蓋
						// console.log("相同的副圖檔");
					}else{ //刪除舊的檔案
						// console.log("不同的副檔名");
						deleteImg(imgBannerDelete);
					}
				}else{ //有刪除但沒更新 ,休要刪除檔案
					// console.log("跑錯邊");
					deleteImg(imgBannerDelete);
				}
			}
			function deleteImg(imgDelete){
				var imgSrc = imgDelete;
				var statusWrite = "category";
				$.ajax({
			       url : 'framwork/imgDelete.php',
			       type : 'POST',
			       data : {status : statusWrite,img_src : imgSrc},
			       cache : false,
			       success : function(result) {
			           if(result){ //true
			           		$("#cateLogo").attr("src","");
			           		$("#logoDelete").hide();
			           }else{ //刪除失敗

			           }  
			       }
				});
			}
		});

		
	});	



		// 刪除行銷分類
	$(".cateDelet").click(function(){
		var cateNo = $(this).parents("tr").find("input[name='cate_no']").val();
		var oldCateName = $(this).parents("tr").find(".cateName").text().trim();
		console.log(cateNo);
		var tableWrite = "category";
		var statusWrite = "刪除";
		
		if(confirm("確認要刪除 '" + oldCateName + "' 嗎?")){ //true yes
			
			// ------移除分類-----
								
			$.ajax({
				url:"framwork/categoryAjax.php",
				data : {table : tableWrite,status : statusWrite,cate_no : cateNo},
	 			type : "POST",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 	
				    		location.reload();
				    	}else{ //false
				    		alert("找無分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});


		}else{   //false no
			// console.log("no");
		}
	});





// =======tag.php=======
	// =====標籤編輯新增=====
	$("#tagCreate").click(function(){
		var str = $(this).parent().parent().find("input[type='text']").val();
		if(str.trim().length< 1 || str.trim() == null){
			$(this).parent().parent().find("input[type='text']").focus();
			alert("請輸入新增標籤的名稱");
		}else{
			var tagNameWrite = str.trim();
			var tableWrite = "tag";
	 		var statusWrite = "新增";
			$.ajax({
				url:"framwork/tagAjax.php",
				data : {table : tableWrite,status : statusWrite,tag_name:tagNameWrite},
	 			type : "POST",
			    cache : false,
			    success : function(result){
			    	if(result){ //true 
			    		alert("標籤新增成功");
			    		location.reload();
			    	}else{ //false
			    		alert("標籤已存在");
			    	}
			    },
			    error : function(error){
			    	alert("傳送失敗");
			    } 
			});
		}
		

	});



	// =======標籤編輯======	
	$(".tagEdit").click(function(){
		var oldTagName = $(this).parents("tr").find("td.tagName").text().trim();
		var tagNo = $(this).parents("tr").find("td.tagName input[name='tag_no']").val() ;
		$("input[id='tagNewName']").val(oldTagName);
		$("#editTagNameConfirm").unbind('click').click(function(){
			var tableWrite = "tag";
 			var statusWrite = "修改";
			var tagNewName = $("#tagNewName").val().trim();
			if(tagNewName.trim().length<1 || tagNewName.trim() ==null){
				alert("請輸入新的標籤");
				$("#tagNewName").focus();
			}else{
				$.ajax({
					url:"framwork/tagAjax.php",
					data : {table : tableWrite,status : statusWrite,tag_no : tagNo,tag_name:tagNewName},
		 			type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		if(result =="有相同標籤"){
				    			alert("有重複標籤");
				    		}else{
				    			alert("標籤已更新");
				    			location.reload();
				    		}
				    	}else{ //false
				    		alert("找不到相同標籤");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}
			
		});

	});	
	// =====標籤刪除=====	
	$(".tagDelet").click(function(){
		var tableWrite = "tag";
 		var statusWrite = "刪除";
		var tagNoWrite = $(this).parent().parent().parent().find("td.tagName input[name='tag_no']").val();
		$.ajax({
			url:"framwork/tagAjax.php",
			data : {table : tableWrite,status : statusWrite,tag_no:tagNoWrite},
 			type : "POST",
		    cache : false,
		    success : function(result){
		    	if(result){ //true 
		    		alert("標籤已刪除");
		    		location.reload();
		    	}else{ //false
		    		alert("找不到相同標籤");
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		});
	});


	// 標籤群組新增
	$("#cateCreate").click(function(){
		var cateName = $(this).parent().parent().find("input[type='text']").val().trim();
		if(cateName.length<1 || cateName == null){
			alert("請輸入標籤分類名稱");
			$(this).parent().parent().find("input[type='text']").focus();
		}else{
			var cateNewName = cateName;
			var tableWrite = "category";
			var statusWrite = "新增";
			var cateParents = 61 ;
			$.ajax({
				url:"framwork/categoryAjax.php",
	 			data : {table : tableWrite,status : statusWrite,cate_name : cateNewName,cate_parents : cateParents},
	 			type : "POST",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 
				    		alert("新增標籤分類成功");	
				    		location.reload();
				    		// console.log(result);
				    	}else{ //false
				    		alert("有存在相同分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});
		}
	});
	// 編輯標籤群組內的標籤(加入標籤)
	$(".cateTagEdit").click(function(){
		var cateName = $(this).parent().parent().parent().find("a").text();
		var cateNo = $(this).parent().parent().parent().find("input[name='cate_no']").val();
		$("#cateTagEditModel .modal-title").html(cateName.trim()+"<small class='ml-2' style='color:#aaa;'>(加入標籤)</small><input id='cateNoGroup' type='hidden' name='cate_no' value='"+cateNo+"'>");
		$("#editTagCateConfirm").show();
		$("#removeTagCateConfirm").hide();

		$(".tagArea").children().remove();
		// console.log(cateNo);
		var tableWrite = "tag";
		var statusWrite = "searchOutCate";
		$.ajax({
			url:"framwork/tagAjax.php",
 			data : {table : tableWrite,status : statusWrite,cateNo : cateNo},
 			type : "POST",
		    cache : false,
		    success : function(result){
			    	if(result){ //true 
			    		var tag =JSON.parse(result);
			    		// console.log(tag);
			    		// console.log(tag.length);
			    		tagCateFeedback(tag,"此分類已包含所有標籤");
			    		
			    	}else{ //false
			    		alert("沒有值");
			    	}
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});
	});
	// 編輯標籤群組內的標籤(移除標籤)
	$(".cateTagRemoveEdit").click(function(){
		var cateName = $(this).parent().parent().parent().find("a").text();
		var cateNo = $(this).parent().parent().parent().find("input[name='cate_no']").val();
		$("#cateTagEditModel .modal-title").html(cateName.trim()+"<small class='ml-2' style='color:#aaa;'>(移除標籤)</small>");
		$("#editTagCateConfirm").hide();
		$("#removeTagCateConfirm").show();

		$(".tagArea").children().remove();
		// console.log(cateNo);
		var tableWrite = "tag";
		var statusWrite = "searchIncate";
		$.ajax({
			url:"framwork/tagAjax.php",
 			data : {table : tableWrite,status : statusWrite,cateNo : cateNo},
 			type : "POST",
		    cache : false,
		    success : function(result){
			    	if(result){ //true 
			    		var tag =JSON.parse(result);
			    		// console.log(tag);
			    		// console.log(tag.length);
			    		tagCateFeedback(tag,"此分類裡沒有標籤");
			    		
			    	}else{ //false
			    		alert("有存在相同分類");
			    	}
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});
	});

	// 文章加入標籤
	$("#addArticleTag").click(function(){
		var cateNo = 50;
		$("#cateTagEditModel .modal-title").html("文章分類<small class='ml-2' style='color:#aaa;'>(加入標籤)</small><input id='cateNoGroup' type='hidden' name='cate_no' value='"+cateNo+"'>");
		$("#editTagCateConfirm").show();
		$("#removeTagCateConfirm").hide();

		$(".tagArea").children().remove();
		var tableWrite = "tag";
		var statusWrite = "searchOutCate";
		$.ajax({
			url:"framwork/tagAjax.php",
 			data : {table : tableWrite,status : statusWrite,cateNo : cateNo},
 			type : "POST",
		    cache : false,
		    success : function(result){
			    	if(result){ //true 
			    		var tag =JSON.parse(result);
			    		// console.log(tag);
			    		// console.log(tag.length);
			    		tagCateFeedback(tag,"此分類已包含所有標籤");
			    		
			    	}else{ //false
			    		alert("沒有值");
			    	}
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});
	});
	// 文章刪除標籤
	$("#removeArticleTag").click(function(){
		var cateNo = 50;
		$("#cateTagEditModel .modal-title").html("文章分類<small class='ml-2' style='color:#aaa;'>(移除標籤)</small>");
		$("#editTagCateConfirm").hide();
		$("#removeTagCateConfirm").show();
		$(".tagArea").children().remove();
		var tableWrite = "tag";
		var statusWrite = "searchIncate";
		$.ajax({
			url:"framwork/tagAjax.php",
 			data : {table : tableWrite,status : statusWrite,cateNo : cateNo},
 			type : "POST",
		    cache : false,
		    success : function(result){
			    	if(result){ //true 
			    		var tag =JSON.parse(result);
			    		// console.log(tag);
			    		// console.log(tag.length);
			    		tagCateFeedback(tag,"此分類裡沒有標籤");
			    		
			    	}else{ //false
			    		alert("有存在相同分類");
			    	}
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		});
	});

	$("#cateTagEditModel .tagSelect").click(function(){
		// $(this).addClass("active");
		var tagclass = $(this).attr("class");
		if(tagclass.search("active") != -1){
			$(this).removeClass("active");
		}else{			
			$(this).addClass("active");
			// console.log(tagclass.search("active"));
		}
	});

	function tagCateFeedback(tag,state){
		if(tag.length != 0){
			tag.forEach(function(e){
				$(".tagArea").append("<span class='tagSelect'>#"+e['tag_name']+"<input type='hidden' name='tag_no' value='"+e['tag_no']+"'><input type='hidden' name='cate_tag_no' value='"+e['cate_tag_no']+"'></span>");
			});
		}else{
			$(".tagArea").append("<div class='text-center'>"+state+"</span>");
		}
		// ===重新註冊標籤按鈕事件===
		$("#cateTagEditModel .tagSelect").click(function(){
			// $(this).addClass("active");
			var tagclass = $(this).attr("class");
			if(tagclass.search("active") != -1){
				$(this).removeClass("active");
			}else{			
				$(this).addClass("active");
				// console.log(tagclass.search("active"));
			}
		});
	}

	//選擇加入的標籤並儲存
	$("#editTagCateConfirm").click(function(){
		var cateNo = $("#cateNoGroup").val();
		var tagPlus = $(".tagArea").find('.tagSelect.active');
		console.log(cateNo);
		if(tagPlus.length == 0 ){
			alert("請點選想要加入的標籤")
		}else{
			for(var i=0 ;i<tagPlus.length;i++){
				var tagNo = tagPlus.eq(i).find("input[name='tag_no']").val();
				var tableWrite = "category_tag_relate";
				var statusWrite = "addTagInCate";
				$.ajax({
					url:"framwork/tagAjax.php",
		 			data : {table : tableWrite,status : statusWrite,cateNo : cateNo,tagNo : tagNo},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		// alert("標籤分類已儲存");
					    		location.reload();
					    	}else{ //false
					    		alert("有存在相同分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});
			}
			
		}
	});

	//選擇要移除的標籤並儲存
	$("#removeTagCateConfirm").click(function(){
		var tagPlus = $(".tagArea").find('.tagSelect.active');
		if(tagPlus.length == 0 ){
			alert("請點選想要移除的標籤")
		}else{
			for(var i=0 ;i<tagPlus.length;i++){
				var cateTagNo = tagPlus.eq(i).find("input[name='cate_tag_no']").val();
				var tableWrite = "category_tag_relate";
				var statusWrite = "removeTagInCate";
				$.ajax({
					url:"framwork/tagAjax.php",
		 			data : {table : tableWrite,status : statusWrite,cateTagNo : cateTagNo},
		 			type : "POST",
				    cache : false,
				    success : function(result){
					    	if(result){ //true 
					    		// // alert("標籤分類已儲存");
					    		location.reload();
					    	}else{ //false
					    		alert("有存在相同分類");
					    	}
			    		},
				    error : function(error){
					    	alert("傳送失敗");
					    } 
				});
			}
			
		}
	});


	// 刪除標籤分類
	$(".cateTagDelete").click(function(){
		var cateNo = $(this).parent().parent().parent().find("input[name='cate_no']").val();
		var CateName = $(this).parent().parent().parent().find("a").text().trim();
		// console.log(cateNo);
		var tableWrite = "category";
		var statusWrite = "刪除";
		
		if(confirm("確認要刪除 '" + CateName + "' 嗎?")){ //true yes
			
			// ------移除分類-----
								
			$.ajax({
				url:"framwork/categoryAjax.php",
				data : {table : tableWrite,status : statusWrite,cate_no : cateNo},
	 			type : "POST",
			    cache : false,
			    success : function(result){
				    	if(result){ //true 	
				    		location.reload();
				    	}else{ //false
				    		alert("找無分類");
				    	}
		    		},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});


		}else{   //false no
			// console.log("no");
		}
	});


		
	


	// ======product.php======

	// -----新增商品-----
	$("#productCreateConfirm").click(function(){
		var modelIdNew = $("input[id='modelIdNew']").val();
		var productNameNewNew = $("input[id='productNameNew']").val();
		// console.log(modelIdNew,productNameNewNew);
		var modelIdReg = /^\w{1,}$/;
		if(modelIdReg.test(modelIdNew) == false){
			alert("請輸入英數碼");
			$("input[id='modelIdNew']").focus();
		}else{
			if(productNameNewNew.trim().length< 1 || productNameNewNew.trim() == null){
				alert("請輸入產品名稱");
				$("input[id='productNameNew']").focus();
			}else{
				var table2Write = "product";
		 		var statusWrite = "新增";
		 		$.ajax({
						url:"framwork/productAjax.php",
						data : {table2 : table2Write,status : statusWrite,model_id : modelIdNew,product_name : productNameNewNew},
			 			type : "POST",
			 			dataType : "json",
					    cache : false,
					    success : function(result){
					    	if(result){ //	新增成功回傳新增的lastId 
					    		// location.href="product.php";
					    		$('#producrCreateNew').modal('hide');
					    		productTableServer.draw(); // 更新後reload網頁
					    		var newForm = "<form id='newForm' action='productEdit.php' method='post'><input type='hidden' name='product_no' value='"+result+"'><form>";
					    		$("section.charts").append(newForm);
					    		$("#newForm").submit();
					    	}else{ //false
					    		// alert("找不到相同標籤");
					    	}
					    },
					    error : function(error){
					    	alert("傳送失敗");
					    } 
					});
			}
		}
	});
	



	$("#productSelectConfirm").click(function(){
		// ======刪除 model 設定=====
		$(this).attr("data-toggle","");
		$(this).attr("data-target","");

		var trProduct = $(this).parents(".demo").find("tr.selected");
		var productSelects = $(this).siblings("#productSelects").val();
		// =====判斷使用者是否有選取要修改的商品======
		if((trProduct.size() > 0) ? true : false ){  
			// ======判斷是否有選取要執行的動作=======
			switch (productSelects) {
				case "1" : // 上架
					// ------計算未啟用欄位
					var productDisableCount=0;
					for(var j = 0 ; j< trProduct.length ;j++){
						if(trProduct.eq(j).find("input[name='product_status']").val() == 0){
							productDisableCount++;
						}
					}
					if(productDisableCount != 0){ //有未啟用的產品	
						alert("請先啟用商品");
					}else{
						for(var i=0 ; i<trProduct.length ;i++ ){
							var tableWrite = "product";
							var statusWrite = "修改";
							var productStatus = "1" ;
							var trProductCurrent = trProduct.eq(i);
							var productId = trProduct.eq(i).find("td").eq(0).text();
							var startno = parseInt($("a.current").text())*parseInt($("select[name='dataTableServer_length']").val())-parseInt($("select[name='dataTableServer_length']").val());
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table : tableWrite,status : statusWrite,product_no : productId,product_status : productStatus},
					 			type : "POST",
					 			dataType : "json",
							    cache : false,
							    async: false, //同步
							    success : function(result){
								    	if(result){ //true 
								    		// if(result["product_status"] == 1){
								    		// 	var status = "下架";
								    		// 	var statusForColumn = "銷售中";
								    		// }else{
								    		// 	var status = "上架";
								    		// 	var statusForColumn = "已下架";
								    		// }	
								    		// =====dom 寫入=======
								    		// trProductCurrent.find("td").eq(8).text(statusForColumn);
								    		// trProductCurrent.find("button.productStatusButton").text(status);
								    		// trProductCurrent.find("td").eq(9).text(result["product_updatetime"]);
								    		// ======server reload======
											// productTableServer.draw();
											productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    } 
							});
						}
					}
					
					break;
				case "2" : //下架
					// ------計算未啟用欄位
					var productDisableCount=0;
					for(var j = 0 ; j< trProduct.length ;j++){
						if(trProduct.eq(j).find("input[name='product_status']").val() == 0){
							productDisableCount++;
						}
					}
					if(productDisableCount != 0){ //有未啟用的產品
						alert("請先啟用商品");
					}else{
						for(var i=0 ; i<trProduct.length ;i++ ){
							var tableWrite = "product";
							var statusWrite = "修改";
							var productStatus = "2" ;
							var trProductCurrent = trProduct.eq(i);
							var productId = trProduct.eq(i).find("td").eq(0).text();
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table : tableWrite,status : statusWrite,product_no : productId,product_status : productStatus},
					 			type : "POST",
					 			dataType : "json",
							    cache : false,
							    async: false, //同步
							    success : function(result){
								    	if(result){ //true 
								    		// if(result["product_status"] == 1){
								    		// 	var status = "下架";
								    		// 	var statusForColumn = "銷售中";
								    		// }else{
								    		// 	var status = "上架";
								    		// 	var statusForColumn = "已下架";
								    		// }
								    		// =====dom 寫入=======
								    		// trProductCurrent.find("td").eq(8).text(statusForColumn);
								    		// trProductCurrent.find("button.productStatusButton").text(status);
								    		// trProductCurrent.find("td").eq(9).text(result["product_updatetime"]);
								    		// ======server reload======
											// productTableServer.draw();
											productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    } 
							});
						};
					
					}
					break;
				case "3" : //加入分類
					// alert("加入分類");
					$(this).attr("data-toggle","modal");
					$(this).attr("data-target","#cateCreateModal");

					// ---歸0 之前得設定----
					$("#cateSelect").val(null);
					$("#cateCreateNew").val(null);
					$("#newCateFatherSelect").val(null);
					$("#newCateFatherSelect option").each(function(){
						if($(this).val() != null){
							if($(this).val() != 62){
								$(this).remove();
							}
						}
					});
					

					//計算欄位數量
					$(".cateCreateModalCount").text(trProduct.length);

					// -----建立現有分類----
					$("#cateCreateConfirm").unbind('click').click(function(){   
						var cateNo = $("#cateSelect").val();
						if(cateNo.length == 0 ){ 
							alert("請選擇分類項目");
							$("#cateSelect").focus();
						}else if(cateNo.length != 0){
							for(var i=0;i<trProduct.length ;i++ ){
								var productId = trProduct.eq(i).find("td").eq(0).text();
								var productName = trProduct.eq(i).find("td").eq(2).text();
								var productCateColumn = trProduct.eq(i).find("td").eq(3);
								
								for(var j=0 ;j<cateNo.length;j++){
									var table1Write = "product";
									var table2Write = "category_products_relate";
									var statusWrite = "修改";
									var cateNoWrite = cateNo[j];
									// ===== 刪除'無'的textnode 加入新增的分類欄位=======
									if(trProduct.eq(i).find("td").eq(3).text() == "無"){
										trProduct.eq(i).find("td").eq(3).empty();
									}
									$.ajax({
										url:"framwork/productAjax.php",
										data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_no : cateNoWrite},
							 			type : "POST",
							 			dataType : "json",
									    cache : false,
									    async: false, //同步
									    success : function(result){
										    	if(result == "have"){ //  //有分類重複
										    		alert(productName+"已存在相同分類");	
										    	}else if(result){
										    		// console.log(result);
										    		// =====dom 寫入=======
														// productCateColumn.append(result["cate"]+"<br>");
											   //  		productCateColumn.append("<input type='hidden' name='cateRelateNo' value='"+result["cate_product_no"]+"'>");
											   		// ======server reload======
										    		// productTableServer.draw();
										    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
										    	}else{ //false
										    		// alert("找無分類");
										    	}
								    		},
									    error : function(error){
										    	alert("傳送失敗");
										    	// console.log(error);
										    } 
									});
								}
								
							}
							// // =====關閉光箱=====
					    	$('#cateCreateModal').modal('hide');
						}
					});

					// ------抓取所有父層------
					var statusWrite ="查詢";
					$.ajax({
						url:"framwork/categoryAjax.php",
						data : {status : statusWrite},
			 			type : "POST",
			 			dataType : "json",
					    cache : false,
					    success : function(result){
						    	 //有父層分類 
					    		for(var i=0 ;i<result.length ;i++){
					    			var text = result[i]["cate_name"];
					    			var value = result[i]["cate_no"];
					    			option = new Option(text,value) ;
					    			$("#newCateFatherSelect").append(option);
					    		}
						    	
				    		},
					    error : function(error){
						    	alert("傳送失敗");
						    } 
					});

					// -----建立新增分類----
					$("#cateCreateNewConfirm").unbind('click').click(function(){
						var cateNewName = $("#cateCreateNew").val().trim();
						var cateLevelNew =  $("#newCateFatherSelect").val();
						if(cateNewName.length == 0 ){
							alert("請建立新的分類");
							$("#cateCreateNew").focus();
						}else if(!cateLevelNew){
							alert("請選擇分類的層級");
							$("#newCateFatherSelect").focus();
						}else{
							// -----寫入資料庫-----
								var statusWrite = "新增";
								var tableWrite = "category";			
								$.ajax({
									url:"framwork/categoryAjax.php",
									data : {table : tableWrite,status : statusWrite,cate_parents : cateLevelNew,cate_name : cateNewName},
						 			type : "POST",
						 			dataType : "json",
								    cache : false,
								    success : function(result){
									    	
									    	productCateNew(result);
							    		},
								    error : function(error){
									    	alert("傳送失敗");
									    } 
								});
							
						}
					});

					function productCateNew(result){
						var cateNoWrite =result;
						for(var i=0;i<trProduct.length ;i++ ){
							var productId = trProduct.eq(i).find("td").eq(0).text();
							var productName = trProduct.eq(i).find("td").eq(2).text();
							var table1Write = "product";
							var table2Write = "category_products_relate";
							var statusWrite = "修改";
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_no : cateNoWrite},
					 			type : "POST",
					 			dataType : "json",
							    cache : false,
							    async: false, //同步
							    success : function(result){
								    	if(result == "have"){ //  //有分類重複
								    		alert(productName+"已存在相同分類");	
								    	}else if(result){
								    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    	// console.log(error);
								    } 
							});	
						}
						// // =====關閉光箱=====
				    	$('#cateCreateModal').modal('hide');
					}

					break;	
				case "4" : //刪除分類
					alert("刪除分類");
					for( var i=0 ; i<trProduct.length ;i++ ){
						var productId = trProduct.eq(i).find("td").eq(0).text();
						var cateProductNo = trProduct.eq(i).find("td").eq(3).find("input[name='cateRelateNo']");
						for(var j=0 ; j<cateProductNo.length ; j++){
							var table1Write = "product";
							var table2Write = "category_products_relate";
							var statusWrite = "刪除";
							var cateProductNoWrite = cateProductNo.eq(j).val();
							// console.log(productId,cateProductNoWrite);
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table1 : table1Write,table2 : table2Write,product_no : productId,status : statusWrite,cate_product_no : cateProductNoWrite},
					 			type : "POST",
							    cache : false,
							    success : function(result){
								    	if(result){ //true 
								    		// alert("分類已刪除");	
								    		// location.reload();
								    		// ======server reload======
										    // productTableServer.draw();
										    productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    } 
							});
						}
						
					}
					
					break;
				case "5" : //加入標籤
					// alert("加入標籤");
					// 開啟model tag 
					$(this).attr("data-toggle","modal");
					$(this).attr("data-target","#tagCreateModal");
					//計算欄位數量
					$(".tagCreateModalCount").text(trProduct.length);

					// ---歸0 之前得設定----
					$("#tagSelect").val(null);
					$("#tagCreateNew").val(null);


					// -----建立現有標籤----
					$("#tagCreateConfirm").unbind('click').click(function(){
						var tagNo = $("#tagSelect").val();
						// var tagCreate = $("#tagCreateNew").val();
						if(tagNo.length == 0 ){ //&& tagCreate.trim().length == 0
							alert("請選擇標籤");
							$("#tagSelect").focus();
						}else if( tagNo.length != 0){  // && tagCreate.trim().length == 0
							for(var i=0;i<trProduct.length ;i++ ){
								var productId = trProduct.eq(i).find("td").eq(0).text();
								var productName = trProduct.eq(i).find("td").eq(2).text();
								var productTagColumn = trProduct.eq(i).find("td").eq(4);
								
									for(var j=0 ;j<tagNo.length;j++){
										var table1Write = "product";
										var table2Write = "tag_products_relate";
										var statusWrite = "修改";
										var tagNoWrite = tagNo[j];
										// ===== 刪除'無'的textnode 加入新增的標籤欄位=======
										if(trProduct.eq(i).find("td").eq(4).text() == "無"){
											trProduct.eq(i).find("td").eq(4).empty();
										}
										$.ajax({
											url:"framwork/productAjax.php",
											data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,tag_no : tagNoWrite},
								 			type : "POST",
								 			dataType : "json",
										    cache : false,
										    async: false, //同步
										    success : function(result){ 
											    	if(result == "have"){   //有標籤重複
											    		alert(productName+"已存在相同標籤");
											    	}else if(result){    //沒有標籤重複
														// ===dom 寫入欄位======
											    		// productTagColumn.append(result[0]["tag_name"]+"<br>");
											    		// productTagColumn.append("<input type='hidden' name='tag_product_no' value='"+result[0]["tag_product_no"]+"'>");
											    		// ======server reload======
											    		// productTableServer.draw();
											    		// console.log(result);
											    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
											    	}else{ //false
											    		// alert("找無分類");	
											    	}
											    	
									    		},
										    error : function(error){
											    	alert("傳送失敗");
											    } 
										});
									}
								
							}
						// // =====關閉光箱=====
				    	$('#tagCreateModal').modal('hide');
						}	
					});

					// -----建立新增標籤----
					$("#tagCreateNewConfirm").unbind('click').click(function(){
						var tagCreate = $("#tagCreateNew").val().trim();
						if(tagCreate.length == 0){
							alert("請建立新的標籤");
							$("#tagCreateNew").focus();
						}else if( tagCreate.length != 0){
							// ---建立新的tag---
							var tableWrite = "tag" ;
							var statusWrite = "新增加入" ;
							$.ajax({
								url:"framwork/tagAjax.php",
								data : {table : tableWrite,status : statusWrite,tag_name : tagCreate},
					 			type : "POST",
							    cache : false,
							    async: false, //同步
							    success : function(result){ 
								    	if(result){   //更新成功 回傳更新的tag_no
								    		// // alert("更新成功");
								    		// console.log(result);
								    		productTag(result);
								    	}else{ //false
								    		alert("已存在相同標籤"+"'"+tagCreate+"'");
								    	}
						    		},
							    error : function(error){
								    	// alert("傳送失敗");
								    	console.log(error);
								    } 
							});							

							function productTag(result){
								var tagNewNo = result ;
								for(var i=0;i<trProduct.length ;i++ ){
									var productId = trProduct.eq(i).find("td").eq(0).text();
									var table1Write = "product";
									var table2Write = "tag_products_relate";
									var statusWrite = "修改";
									$.ajax({
										url:"framwork/productAjax.php",
										data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,tag_no : tagNewNo},
							 			type : "POST",
							 			dataType : "json",
									    cache : false,
									    async: false, //同步
									    success : function(result){ 
										    	if(result == "have"){   //有標籤重複
										    		alert("已存在相同標籤"+"'"+tagCreate+"'");
										    	}else if(result){    //沒有標籤重複
										    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
										    	}else{ //false
										    		// alert("找無分類");	
										    	}
										    	// console.log(result);
								    		},
									    error : function(error){
										    	alert("傳送失敗");
										    	// console.log(error);
										    } 
									});							
								}
							} //---end productTag
							// // =====關閉光箱=====
					    	$('#tagCreateModal').modal('hide');	
						}
						
						
					});


					break;	
				case "6" : //刪除標籤
					alert("刪除標籤");
					for( var i=0 ; i<trProduct.length ;i++ ){
						var productId = trProduct.eq(i).find("td").eq(0).text();
						var tagProductNo = trProduct.eq(i).find("td").eq(4).find("input[name='tag_product_no']");
						for(var j=0 ; j<tagProductNo.length ; j++){
							var table1Write = "product";
							var table2Write = "tag_products_relate";
							var statusWrite = "刪除";
							var tagProductNoWrite = tagProductNo.eq(j).val();
							// console.log(productId,cateProductNoWrite);
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table1 : table1Write,table2 : table2Write,product_no : productId,status : statusWrite,tag_product_no : tagProductNoWrite},
					 			type : "POST",
							    cache : false,
							    success : function(result){
								    	if(result){ //true 
								    		// alert("標籤已刪除");	
								    		// location.reload();
								    		// productTableServer.draw();
								    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    } 
							});
						}
						
					}
					break;	
				case "7" :
					// alert("加入本月推薦");
					var cateNo = "90" ;
					for (var i=0 ; i<trProduct.length ;i++ ) {
						var productId = trProduct.eq(i).find("td").eq(0).text();
						var table1Write = "product";
						var table2Write = "category_products_relate";
						var statusWrite = "addCate";
						var cateNoWrite = cateNo;
						// console.log(productId);
						// console.log(cateNo);
						$.ajax({
							url:"framwork/productAjax.php",
							data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_no : cateNoWrite},
				 			type : "POST",
				 			dataType : "json",
						    cache : false,
						    async: false, //同步
						    success : function(result){
							    	if(result){
							    		// console.log(result);
							    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
							    	}else{ //false
							    		// alert("找無分類");
							    	}
					    		},
						    error : function(error){
							    	alert("傳送失敗");
							    	// console.log(error);
							    } 
						});
					}
			
					break;
				case "8" :
					// alert("移除本月推薦");
					for (var i=0 ; i<trProduct.length ;i++ ) {
						var productId = trProduct.eq(i).find("td").eq(0).text();
						var cateProductNo = trProduct.eq(i).find("td").eq(5).find("input[name='cateRelateNo']").val();
						var table1Write = "product";
						var table2Write = "category_products_relate";
						var statusWrite = "刪除";
						// alert(cateProductNo);
						$.ajax({
							url:"framwork/productAjax.php",
							data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_product_no : cateProductNo},
				 			type : "POST",
				 			dataType : "json",
						    cache : false,
						    async: false, //同步
						    success : function(result){
							    	if(result){
							    		// console.log(result);
							    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
							    	}else{ //false
							    		// alert("找無分類");
							    	}
					    		},
						    error : function(error){
							    	alert("傳送失敗");
							    	// console.log(error);
							    } 
						});
					}
					break;
				case "9" :
					$(this).attr("data-toggle","modal");
					$(this).attr("data-target","#activityCreateModal");

					// ---歸0 之前得設定----
					$("#activitySelect").val(null);
					$("#activityCreateNew").val(null);
					

					//計算欄位數量
					$(".activityCreateModalCount").text(trProduct.length);

					// -----建立現有行銷活動----
					$("button#activityCreateConfirm").unbind('click').click(function(){
						// alert("加入現有行銷活動");
						var cateNo = $("#activitySelect").val();
						if(cateNo.length == 0 ){ 
							alert("請選擇行銷活動項目");
							$("#activitySelect").focus();
						}else if(cateNo.length != 0){
							for(var i=0;i<trProduct.length ;i++ ){
								var productId = trProduct.eq(i).find("td").eq(0).text();
								
								for(var j=0 ;j<cateNo.length;j++){
									var table1Write = "product";
									var table2Write = "category_products_relate";
									var statusWrite = "addCate";
									var cateNoWrite = cateNo[j];
									// console.log(productId);
									// console.log(cateNoWrite);
									$.ajax({
										url:"framwork/productAjax.php",
										data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_no : cateNoWrite},
							 			type : "POST",
							 			dataType : "json",
									    cache : false,
									    async: false, //同步
									    success : function(result){
										    	if(result){
										    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
										    	}else{ //false
										    		// alert("找無分類");
										    	}
								    		},
									    error : function(error){
										    	alert("傳送失敗");
										    	// console.log(error);
										    } 
									});
								}
								
							}
							// // =====關閉光箱=====
					    	$('#activityCreateModal').modal('hide');
						}
					});

					// -----建立新的行銷活動----
					$("button#activityCreateNewConfirm").unbind('click').click(function(){
						// alert("加入新的行銷活動");
						var cateNewName = $("input#activityCreateNew").val().trim();
						if(cateNewName.length == 0 ){ 
							alert("請建立新的行銷活動項目名稱");
							$("input#activityCreateNew").focus();
						}else{
							// -----寫入資料庫-----
							var cateNewName = cateNewName.trim();
							var tableWrite = "category";
							var statusWrite = "新增";
							var cateParents = 63 ;
							$.ajax({
								url:"framwork/categoryAjax.php",
					 			data : {table : tableWrite,status : statusWrite,cate_name : cateNewName,cate_parents : cateParents},
					 			type : "POST",
							    cache : false,
							    success : function(result){
							    	if(result){ //true 
							    		productCateActivityNew(result);
							    	}else{ //false
							    		alert("有存在相同分類");
							    	}
					    		},
							    error : function(error){
							    	alert("傳送失敗");
							    } 
							});
						}
					});
					function productCateActivityNew(result){
						var cateNewNo = result ;
						for(var i=0;i<trProduct.length ;i++ ){
							var productId = trProduct.eq(i).find("td").eq(0).text();	
							var table1Write = "product";
							var table2Write = "category_products_relate";
							var statusWrite = "addCate";
							var cateNoWrite = cateNewNo;
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_no : cateNoWrite},
					 			type : "POST",
					 			dataType : "json",
							    cache : false,
							    async: false, //同步
							    success : function(result){
								    	if(result){
								    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    	// console.log(error);
								    } 
							});		
						}
						// // =====關閉光箱=====
					    $('#activityCreateModal').modal('hide');
					}


					break;
				case "10" :
					for (var i=0 ; i<trProduct.length ;i++ ) {
						var productId = trProduct.eq(i).find("td").eq(0).text();
						var cateProductNo = trProduct.eq(i).find("td").eq(5).find("input[name='cateRelateNo']");
						for (var j=0 ; j<cateProductNo.length ; j++) {
							var table1Write = "product";
							var table2Write = "category_products_relate";
							var statusWrite = "刪除";
							var cateProductNoWrite = cateProductNo.eq(j).val();
							$.ajax({
								url:"framwork/productAjax.php",
								data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productId,cate_product_no : cateProductNoWrite},
					 			type : "POST",
					 			dataType : "json",
							    cache : false,
							    async: false, //同步
							    success : function(result){
								    	if(result){
								    		// console.log(result);
								    		productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
								    	}else{ //false
								    		// alert("找無分類");
								    	}
						    		},
							    error : function(error){
								    	alert("傳送失敗");
								    	// console.log(error);
								    } 
							});
						}
						
					}
					break;
				case "11" : //開啟360 狀態
					for(var i=0 ; i<trProduct.length ;i++ ){
						var tableWrite = "product";
						var statusWrite = "update360";
						var product360Status = "1" ;
						var productId = trProduct.eq(i).find("td").eq(0).text();
						$.ajax({
							url:"framwork/productAjax.php",
							data : {table : tableWrite,status : statusWrite,product_no : productId,product_360_status : product360Status},
							type : "POST",
							dataType : "json",
							cache : false,
							async: false, //同步
							success : function(result){
									if(result){ //true 										
										productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
									}else{ //false
										// alert("找無分類");
									}
								},
							error : function(error){
									alert("傳送失敗");
								} 
						});
					}
					break;
				case "12" : //關避360 狀態
					for(var i=0 ; i<trProduct.length ;i++ ){
						var tableWrite = "product";
						var statusWrite = "update360";
						var product360Status = "0" ;
						var productId = trProduct.eq(i).find("td").eq(0).text();
						$.ajax({
							url:"framwork/productAjax.php",
							data : {table : tableWrite,status : statusWrite,product_no : productId,product_360_status : product360Status},
							type : "POST",
							dataType : "json",
							cache : false,
							async: false, //同步
							success : function(result){
									if(result){ //true 										
										productTableServer.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁
									}else{ //false
										// alert("找無分類");
									}
								},
							error : function(error){
									alert("傳送失敗");
								} 
						});
					}
					break;					
				default :
					alert("沒有選擇欲執行的項目");
					$(this).siblings("#productSelects").focus();
					break;	
			}   //--------end 執行的動作的選擇

		}else{
			alert("請選擇欲修改的欄位");
		}	//-----------end 修改的商品選擇	
	});

	// ----標籤批次分頁----
	$("a.tagclass").click(function(){
		$("a.tagclass").removeClass("active");
		$(this).addClass("active");
		var classActive = $(this).attr("class");
		if(classActive.search("tagHave") != -1){
			$("#tagNew").hide();
			$("#tagHave").show();
		}else{
			$("#tagHave").hide();
			$("#tagNew").show();
		}
		// console.log(classActive);
	});
	// ----分類(品牌/供應商)批次分頁----
	$("a.cateclass").click(function(){
		$("a.cateclass").removeClass("active");
		$(this).addClass("active");
		var classCateActive = $(this).attr("class");
		if(classCateActive.search("cateHave") != -1){
			$("#cateNew").hide();
			$("#cateHave").show();
		}else{
			$("#cateHave").hide();
			$("#cateNew").show();
		}
		// console.log(classCateActive);
	});
	// ----分類(行銷活動)批次分頁----
	$("a.activityclass").click(function(){
		$("a.activityclass").removeClass("active");
		$(this).addClass("active");
		var classCateActive = $(this).attr("class");
		if(classCateActive.search("activityHave") != -1){
			$("#activityNew").hide();
			$("#activityHave").show();
		}else{
			$("#activityHave").hide();
			$("#activityNew").show();
		}
	});







	// ======productEdit.php======




	// ====判斷是否有spec=====
	if($("#specTable").find("tr").length != 2){ //有spec 關閉加入spec提醒
		$("#specNone").hide();
	}else{ //沒有spec
		$("#specNone").show();
	};
	function specCheck(){
		if($("#specTable").find("tr").length != 2){ //有spec 關閉加入spec提醒
			$("#specNone").hide();
		}else{ //沒有spec
			$("#specNone").show();
		};
	}

	// ====判斷是否有加購價=====
	if($("#comboTable").find("tr").length != 2){ //有spec 關閉加入spec提醒
		$("#comboNone").hide();
	}else{ //沒有spec
		$("#comboNone").show();
	};
	function comboCheck(){
		if($("#comboTable").find("tr").length != 2){ //有spec 關閉加入spec提醒
			$("#comboNone").hide();
		}else{ //沒有spec
			$("#comboNone").show();
		};
	}
	//comboNone


	$("#productEditCancel").click(function(){
		location.href="product.php";
	});
	// ----規格新增---
	$("#specCreateConfirm").click(function(){
		var productNo = $("input[name='product_no']").val();
		var info = $("input[id='specInfo']").val();
		var describe = $("input[id='specDescribe']").val();
		var stock = $("input[id='productStock']").val();
		var price1 = $("input[id='productPrice1']").val();
		var price2 = $("input[id='productPrice2']").val();
		var price3 = $("input[id='productPrice3']").val();
		if(stock.trim() == ""){
			alert("沒有庫存");
			$("input[id='productStock']").focus();
		}else{
			if(price1.trim() == ""){
				alert("原價一定要填");
				$("input[id='productPrice1']").focus();
			}else{
				var table1Write = "product";
				var table2Write = "product_spec";
		 		var statusWrite = "規格新增";
				$.ajax({
					url:"framwork/productAjax.php",
					data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productNo,product_spec_info : info,product_spec_describe : describe,product_stock : stock,	product_spec_price1 : price1,product_spec_price2 : price2,product_spec_price3 : price3},
		 			type : "POST",
		 			dataType : "json",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		$("#specTable").append("<tr class='specCount'><td scope='row' class='text-center'><label class='checkbox-inline'><input id='inlineCheckbox1' type='checkbox' class='productSpecStatus' value='option1'></label></td><td>"+result[0]['product_spec_info']+"</td><td>"+result[0]['product_spec_describe']+"</td><td>"+result[0]['product_stock']+"</td><td>"+result[0]['product_spec_price1']+"</td><td>"+result[0]['product_spec_price2']+"</td><td>"+result[0]['product_spec_price3']+"</td><td class='text-center' ><input type='hidden' name='product_spec_no' value='"+result[0]['product_spec_no']+"'><span class='edit'><button class='productSpecEdit' data-toggle='modal' data-target='#producrSpecEdit'>編輯</button></span><span>|</span><span class='edit'><button class='productSpecDelete'>刪除</button></td></tr>");
				    		$('#producrSpecCreate').modal('hide');
				    		specFeedback();
				    		specCheck();
				    	}else{ //false
				    		// alert("找不到相同標籤");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}
		}
		
		
	});
	// ----規格刪除---
	$(".productSpecDelete").click(function(e){
		e.preventDefault();
		$("tr.specCurrent").removeClass("specCurrent");
		$(this).parents("tr").addClass("specCurrent");
		var productNo = $("input[name='product_no']").val();
		var productSpecNo = $(this).parents("td[class='text-center']").find("input[name='product_spec_no']").val();
		deleteSpec(productSpecNo,productNo);
	});

	// ----規格修改---
	$(".productSpecEdit").click(function(e){
		e.preventDefault();
		$("tr.specCurrent").removeClass("specCurrent");
		$(this).parents("tr").addClass("specCurrent");
		var productNo = $("input[name='product_no']").val();
		var productSpecNo = $(this).parents("td[class='text-center']").find("input[name='product_spec_no']").val();
		var productSpecInfo = $(this).parents("tr").children("td").eq(1).text();
		var productSpecDescribe = $(this).parents("tr").children("td").eq(2).text();
		var productSpecStock = $(this).parents("tr").children("td").eq(3).text();
		var productSpecPrice1 = $(this).parents("tr").children("td").eq(4).text();
		var productSpecPrice2 = $(this).parents("tr").children("td").eq(5).text();
		var productSpecPrice3 = $(this).parents("tr").children("td").eq(6).text();
		specEditFuntion(productSpecNo,productNo,productSpecInfo,productSpecDescribe,productSpecStock,productSpecPrice1,productSpecPrice2,productSpecPrice3);
	});
	// ----建立新規則後 重新註冊按鍵-----
	function specFeedback(){
		// ----規格刪除---
		$(".productSpecDelete").unbind('click').click(function(e){
			e.preventDefault();
			$("tr.specCurrent").removeClass("specCurrent");
			$(this).parents("tr").addClass("specCurrent");
			var productSpecNo = $(this).parents("td[class='text-center']").find("input[name='product_spec_no']").val();
			var productNo = $("input[name='product_no']").val();
			deleteSpec(productSpecNo,productNo);
		});

		// ----規格修改---
		$(".productSpecEdit").unbind('click').click(function(e){
			e.preventDefault();
			$("tr.specCurrent").removeClass("specCurrent");
			$(this).parents("tr").addClass("specCurrent");
			var productNo = $("input[name='product_no']").val();
			var productSpecNo = $(this).parents("td[class='text-center']").find("input[name='product_spec_no']").val();
			var productSpecInfo = $(this).parents("tr").children("td").eq(1).text();
			var productSpecDescribe = $(this).parents("tr").children("td").eq(2).text();
			var productSpecStock = $(this).parents("tr").children("td").eq(3).text();
			var productSpecPrice1 = $(this).parents("tr").children("td").eq(4).text();
			var productSpecPrice2 = $(this).parents("tr").children("td").eq(5).text();
			var productSpecPrice3 = $(this).parents("tr").children("td").eq(6).text();
			specEditFuntion(productSpecNo,productNo,productSpecInfo,productSpecDescribe,productSpecStock,productSpecPrice1,productSpecPrice2,productSpecPrice3);
		});
	};

	function deleteSpec(productSpecNo,productNo){
		var table1Write = "product";
		var table2Write = "product_spec";
 		var statusWrite = "規格刪除";
 		$.ajax({
			url:"framwork/productAjax.php",
			data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_spec_no : productSpecNo,product_no : productNo},
 			type : "POST",
 			dataType : "json",
		    cache : false,
		    success : function(result){
		    	if(result){ //true 
		    		alert("已刪除規格");
		    		$("tr.specCurrent").remove();
		    		specCheck();
		    	}else{ //false
		    		// alert("找不到相同標籤");
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		});
	};
	
	function specEditFuntion(productSpecNo,productNo,productSpecInfo,productSpecDescribe,productSpecStock,productSpecPrice1,productSpecPrice2,productSpecPrice3){
		// -----將spec欄位內容寫入編輯modal------
		$("#specInfoEdit").val(productSpecInfo);
		$("#specDescribeEdit").val(productSpecDescribe);
		$("#productStockEdit").val(productSpecStock);
		$("#productPrice1Edit").val(productSpecPrice1);
		$("#productPrice2Edit").val(productSpecPrice2);
		$("#productPrice3Edit").val(productSpecPrice3);
		// -------修改spec內容並儲存-----
		$("#specEditConfirm").unbind('click').click(function(){
			var productNoEdit = productNo;
			var productSpecNoEdit = productSpecNo;
			var productSpecInfo = $("#specInfoEdit").val();
			var productSpecDescribe = $("#specDescribeEdit").val();
			var productSpecStock = $("#productStockEdit").val();
			var productSpecPrice1 = $("#productPrice1Edit").val();
			var productSpecPrice2 = $("#productPrice2Edit").val();
			var productSpecPrice3 = $("#productPrice3Edit").val();
			var table1Write = "product";
			var table2Write = "product_spec";
	 		var statusWrite = "規格更新";
			$.ajax({
				url:"framwork/productAjax.php",
				data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productNoEdit,product_spec_no : productSpecNoEdit,product_spec_info : productSpecInfo,product_spec_describe : productSpecDescribe,product_stock : productSpecStock,	product_spec_price1 : productSpecPrice1,product_spec_price2 : productSpecPrice2,product_spec_price3 : productSpecPrice3},
	 			type : "POST",
	 			dataType : "json",
			    cache : false,
			    success : function(result){
			    	if(result){ //true 
			    		$("tr.specCurrent").children("td").eq(1).text(result[0]['product_spec_info']);
			    		$("tr.specCurrent").children("td").eq(2).text(result[0]['product_spec_describe']);
			    		$("tr.specCurrent").children("td").eq(3).text(result[0]['product_stock']);
			    		$("tr.specCurrent").children("td").eq(4).text(result[0]['product_spec_price1']);
			    		$("tr.specCurrent").children("td").eq(5).text(result[0]['product_spec_price2']);
			    		$("tr.specCurrent").children("td").eq(6).text(result[0]['product_spec_price3']);
			    		$('#producrSpecEdit').modal('hide');
			    	}else{ //false
			    		// alert("找不到相同標籤");
			    	}
			    },
			    error : function(error){
			    	alert("傳送失敗");
			    } 
			});
		});
	};

	// 加價購
	// 刪除
	$(".productComboDelete").click(function(e){
		if (confirm("確定要刪除加價購嗎?")) {
			e.preventDefault();
			$("tr.comboCurrent").removeClass("comboCurrent");
			$(this).parents("tr").addClass("comboCurrent");
			var productNo = $("input[name='product_no']").val();
			var combo_id = $(this).parents("td[class='text-center']").find("input[name='combo_id']").val();
			$.ajax({
				url:"framwork/productComboAjax.php",
				data : {"status": "saveDeleteCombo", "id" : combo_id, "rel_product_no" : productNo},
				type : "POST",
				dataType : "json",
				cache : false,
				success : function(result){
					if(result){ //true 
						$("tr.comboCurrent").remove();
						comboCheck();
					} else { //false
						// alert("找不到相同標籤");
					}
				},
				error : function(error){
					alert("傳送失敗");
				} 
			});
			
		}
	});
	// 新增
	$("#comboCreateConfirm").click(function(e){
		e.preventDefault();
		if ($("#comboProduct").val() == null || $("#comboProduct").val() == "") {
			alert("請選擇產品");
			return;
		}
		if ($("#comboProductSpec").val() == null || $("#comboProductSpec").val() == "") {
			alert("請選擇規格");
			return;
		}
		if ($("#comboProductPrice").val() == "") {
			alert("請輸入優惠");
			return;
		}
		if ($("#comboProductStart").val() == "") {
			alert("請輸入開始時間");
			return;
		}
		if ($("#comboProductStart").val() == "") {
			alert("請輸入結束時間");
			return;
		}
		var productNo = $("input[name='product_no']").val();
		$.ajax({
			url:"framwork/productComboAjax.php",
			data : {"status": "saveNewCombo", "rel_product_no" : productNo, "product_no" : $("#comboProduct").val(), "product_spec_no" : $("#comboProductSpec").val()
				, "product_spec_price" : $("#comboProductPrice").val(), "starttime" : $("#comboProductStart").val(), "endtime" : $("#comboProductEnd").val()},
			type : "POST",
			dataType : "json",
			cache : false,
			success : function(result){
				if(result){ //true 
					$("#comboTable").append("<tr class='comboCount'><td><a target='_blank' href='../product-detail.php?product_no="+$("#comboProduct").val()+"'>"+$("#comboProduct").val() + " / " + $("#comboProduct option:selected").text()+"</a></td><td>"+$("#comboProductSpec option:selected").text()+"</td><td>"+$("#comboProductStart").val() + " ~ " + $("#comboProductEnd").val()+"</td><td>"+$("#comboProductPrice").val()+"</td><td class='text-center' ><input type='hidden' name='combo_id' value='"+result.message+"'><input type='hidden' name='combo_product_no' value='" + $("#comboProduct").val() + "'><input type='hidden' name='combo_product_spec_no' value='" + $("#comboProductSpec").val() + "'><input type='hidden' name='combo_starttime' value='" + $("#comboProductStart").val() + "'><input type='hidden' name='combo_endtime' value='" + $("#comboProductEnd").val() + "'><span class='edit'><button class='productComboEdit' data-toggle='modal' data-target='#producrComboEdit'>編輯</button></span><span>|</span><span class='edit'><button class='productComboDelete'>刪除</button></td></tr>");
					$("#comboProduct").val("");
					getComboSpec('', 'comboProductSpec', '');
					$("#comboProductPrice").val("");
					$("#comboProductStart").val("");
					$("#comboProductEnd").val("");
					
					comboCheck();
					$('#producrComboCreate').modal('hide');
					//  重新綁定 修改/刪除按鈕
					productComboFeedback();
				}else{ //false
					// alert("找不到相同標籤");
				}
			},
			error : function(error){
				alert("傳送失敗");
			} 
		});
	
	});
	// 修改
	$(".productComboEdit").click(function(e){
		e.preventDefault();
		$("tr.comboCurrent").removeClass("comboCurrent");
		$(this).parents("tr").addClass("comboCurrent");
		var productNo = $("input[name='product_no']").val();
		var combo_id = $(this).parents("td[class='text-center']").find("input[name='combo_id']").val();
		var combo_product_no = $(this).parents("td[class='text-center']").find("input[name='combo_product_no']").val();
		var combo_product_spec_no = $(this).parents("td[class='text-center']").find("input[name='combo_product_spec_no']").val();
		var productComboStarttime = $(this).parents("td[class='text-center']").find("input[name='combo_starttime']").val();
		if (productComboStarttime != "") productComboStarttime = productComboStarttime.substr(0, 16).replace(" ", "T");
		var productComboEndtime = $(this).parents("td[class='text-center']").find("input[name='combo_endtime']").val();
		if (productComboEndtime != "") productComboEndtime = productComboEndtime.substr(0, 16).replace(" ", "T");
		var productComboPrice = $(this).parents("tr").children("td").eq(3).text();
		comboEditFuntion(productNo, combo_id, combo_product_no, combo_product_spec_no, productComboStarttime, productComboEndtime, productComboPrice);
		console.log("修改非新增");
	});
	// (加購價) 新增後重新綁定按鈕
	function productComboFeedback(){
		// 修改
		$(".productComboEdit").unbind('click').click(function(e){
			e.preventDefault();
			$("tr.comboCurrent").removeClass("comboCurrent");
			$(this).parents("tr").addClass("comboCurrent");
			var productNo = $("input[name='product_no']").val();
			var combo_id = $(this).parents("td[class='text-center']").find("input[name='combo_id']").val();
			var combo_product_no = $(this).parents("td[class='text-center']").find("input[name='combo_product_no']").val();
			var combo_product_spec_no = $(this).parents("td[class='text-center']").find("input[name='combo_product_spec_no']").val();
			var productComboStarttime = $(this).parents("td[class='text-center']").find("input[name='combo_starttime']").val();
			if (productComboStarttime != "") productComboStarttime = productComboStarttime.substr(0, 16).replace(" ", "T");
			var productComboEndtime = $(this).parents("td[class='text-center']").find("input[name='combo_endtime']").val();
			if (productComboEndtime != "") productComboEndtime = productComboEndtime.substr(0, 16).replace(" ", "T");
			var productComboPrice = $(this).parents("tr").children("td").eq(3).text();
			comboEditFuntion(productNo, combo_id, combo_product_no, combo_product_spec_no, productComboStarttime, productComboEndtime, productComboPrice);
			console.log("修改 新增後");
		});
		// 刪除
		$(".productComboDelete").click(function(e){
			if (confirm("確定要刪除加價購嗎?")) {
				e.preventDefault();
				$("tr.comboCurrent").removeClass("comboCurrent");
				$(this).parents("tr").addClass("comboCurrent");
				var productNo = $("input[name='product_no']").val();
				var combo_id = $(this).parents("td[class='text-center']").find("input[name='combo_id']").val();
				$.ajax({
					url:"framwork/productComboAjax.php",
					data : {"status": "saveDeleteCombo", "id" : combo_id, "rel_product_no" : productNo},
					type : "POST",
					dataType : "json",
					cache : false,
					success : function(result){
						if(result){ //true 
							$("tr.comboCurrent").remove();
							comboCheck();
						} else { //false
							// alert("找不到相同標籤");
						}
					},
					error : function(error){
						alert("傳送失敗");
					} 
				});
				
			}
		});
	};
	function comboEditFuntion(productNo, combo_id, combo_product_no, combo_product_spec_no, productComboStarttime, productComboEndtime, productComboPrice) {
		$("#comboProductEdit").val(combo_product_no);
		getComboSpec(combo_product_no, 'comboProductSpecEdit', combo_product_spec_no);
		$("#comboProductStartEdit").val(productComboStarttime);
		$("#comboProductEndEdit").val(productComboEndtime);
		$("#comboProductPriceEdit").val(productComboPrice);
		// -------修改加價購內容並儲存-----
		$("#comboEditConfirm").unbind('click').click(function(){
			if ($("#comboProductEdit").val() == null || $("#comboProductEdit").val() == "") {
				alert("請選擇產品");
				return;
			}
			if ($("#comboProductSpecEdit").val() == null || $("#comboProductSpecEdit").val() == "") {
				alert("請選擇規格");
				return;
			}
			if ($("#comboProductPriceEdit").val() == "") {
				alert("請輸入優惠");
				return;
			}
			if ($("#comboProductStartEdit").val() == "") {
				alert("請輸入開始時間");
				return;
			}
			if ($("#comboProductStartEdit").val() == "") {
				alert("請輸入結束時間");
				return;
			}
			$.ajax({
				url:"framwork/productComboAjax.php",
				data : {"status": "saveUpdateCombo", "id" : combo_id, "rel_product_no" : productNo, "product_no" : $("#comboProductEdit").val(), "product_spec_no" : $("#comboProductSpecEdit").val()
					, "product_spec_price" : $("#comboProductPriceEdit").val(), "starttime" : $("#comboProductStartEdit").val(), "endtime" : $("#comboProductEndEdit").val()},
				type : "POST",
				dataType : "json",
				cache : false,
				success : function(result){
					if(result){ //true 
						$("tr.comboCurrent").children("td").eq(0).html("<a target='_blank' href='../product-detail.php?product_no="+$("#comboProductEdit").val()+"'>"+$("#comboProductEdit").val() + " / " + $("#comboProductEdit option:selected").text()+"</a>");
						$("tr.comboCurrent").children("td").eq(1).text($("#comboProductSpecEdit option:selected").text());
						$("tr.comboCurrent").children("td").eq(2).text($("#comboProductStartEdit").val() + " ~ " + $("#comboProductEndEdit").val());
						$("tr.comboCurrent").children("td").eq(3).text($("#comboProductPriceEdit").val());
						$('#producrComboEdit').modal('hide');
					}else{ //false
						// alert("找不到相同標籤");
					}
				},
				error : function(error){
					alert("傳送失敗");
				} 
			});
		});
	}

	$("#productEditConfirm").click(function(){
		var productNo = $("input[name='product_no']").val();
		var modelId = $("input[name='model_id']").val();
		var productName = $("input[name='product_name']").val();
		var productSubtitle = $("input[name='product_subtitle']").val();
		var productPromote = $("input[name='product_promote']").val();
		var productDescribe = $("input[name='product_describe']").val();
		var productNote = $("textarea[name='product_note']").val();
		if ($('#summernote').summernote('codeview.isActivated')){
			$('#summernote').summernote('codeview.deactivate'); 
		}
		var productContent = $("div.note-editable").html();

		
		
		// for(var i=0 ; i<$("div.note-editable").find("img").length ; i++){
		// 	var imgSrc = $("div.note-editable").find("img").eq(i).attr("src");
		// 	var change = imgSrc.slice(3);
		// 	$("div.note-editable").find("img").eq(i).attr("src",change);
		// }


		// ----商品缺圖片----
		if($("input[id='productEditProductStatus']").prop("checked")){//true
			var productStatus ="1";
		}else{
			var productStatus ="2";
		}
		if($("#specTable").find("tr").length == 2){
			alert("需要設定商品規格");
		}else{ //有商品規格 ,取得規格 
			var specStatus = $("input[class='productSpecStatus']");
			// -----計算是否有商品綁定規格-----
			var specTrueCount=0;
			for(var i = 0 ; i< specStatus.length ;i++){
				if(specStatus.eq(i).prop("checked")){
					specTrueCount++;
				}
			}

			// ------計算商品所有規格的最低價格(產品sort使用)----
			var productPriceSort = 0 ;
			specStatus.each(function(){
				if($(this).prop("checked")){//true  狀態開啟
					var p1 = parseInt($(this).parents("tr").find("td").eq(4).text());
					var p2 = parseInt($(this).parents("tr").find("td").eq(5).text());
					var p3 = parseInt($(this).parents("tr").find("td").eq(6).text());
					if( p2 == 0 & p3 == 0){ //僅只有原價
						if( productPriceSort == 0){
							productPriceSort = p1;
						}else if(  productPriceSort != 0  && productPriceSort > p1 ){ //比較前一筆規格售價比現在的規格低
							productPriceSort = p1;
						}
					}else if( p3 !=0 ){ //有填優惠
						if( productPriceSort!=0 && productPriceSort > p3 || productPriceSort==0  ){ //比較前一筆規格售價比現在的規格低
							productPriceSort = p3;
						}						
					}else{  //沒有優惠但有售價
						if((productPriceSort!=0 && productPriceSort > p2) || productPriceSort==0 ){ //比較前一筆規格售價比現在的規格低
							productPriceSort = p2;
						}
					}	
				}	
			});

			if(specTrueCount==0){
				alert("需要勾選至少一種規格");
			}else{
				specStatus.each(function(){
					if($(this).prop("checked")){ //true  狀態開啟
						var productspecNo =$(this).parents("tr").find("input[name='product_spec_no']").val();
						var table1Write = "product";
						var table2Write = "product_spec";
				 		var statusWrite = "產品編輯更新";
				 		var productSpecStatus = "1";
				 		$.ajax({
							url:"framwork/productAjax.php",
							data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productNo,product_spec_no : productspecNo,model_id : modelId,product_name : productName,product_subtitle : productSubtitle,product_promote : productPromote,product_describe : productDescribe,product_content : productContent,product_note : productNote,product_status : productStatus,product_spec_status : productSpecStatus,product_price_sort : productPriceSort},
				 			type : "POST",
				 			dataType : "json",
				 			async : false,
						    cache : false,
						    success : function(result){
						    	if(result){ //true 
						    		// location.href="product.php";
						    	}else{ //false
						    		// alert("找不到相同標籤");
						    	}
						    },
						    error : function(error){
						    	alert("傳送失敗");
						    } 
						});
					}else{ //false 
						var productspecNo =$(this).parents("tr").find("input[name='product_spec_no']").val();
						// console.log(productspecNo);
						var table1Write = "product";
						var table2Write = "product_spec";
				 		var statusWrite = "規格重設";
				 		var productSpecStatus = "0";
				 		$.ajax({
							url:"framwork/productAjax.php",
							data : {table1 : table1Write,table2 : table2Write,status : statusWrite,product_no : productNo,product_spec_no : productspecNo,product_spec_status : productSpecStatus},
				 			type : "POST",
				 			dataType : "json",
						    cache : false,
						    success : function(result){
						    	if(result){ //true 
						    		location.reload();
						    	}else{ //false
						    		// alert("找不到相同標籤");
						    	}
						    },
						    error : function(error){
						    	alert("傳送失敗規格");
						    } 
						});
					}
				});

				location.reload();
			}
			

		}
		
	});


	// =======coupon.php=========
	$("#couponCreatebutton").click(function(){
		location.href="couponEdit.php";
	});


	// =======couponEdit.php=========
	$("#couponEditCancel").click(function(){
		location.href="coupon.php";
	});

	// -----優惠劵新增 修改------
	$("#couponEditConfirm").click(function(){
		var couponNo = $("input[name='coupon_no']").val();
		var couponName = $("input[name='coupon_name']").val().trim();
		var couponCode = $("input[name='coupon_code']").val().trim();
		var couponDiscount = $("input[name='coupon_discount']").val().trim();
		var couponStart = $("input[name='coupon_start']").val();
		var couponEnd = $("input[name='coupon_end']").val();
		var couponUsed = $("select#couponEditUsed").val();
		var couponTarget = $("select#couponEditTarget").val(); 
		var couponTimes = $("input[name='coupon_times']").val().trim(); 
		var couponStatus = $("input[class='coupon_status']").prop("checked");
		var codeReg = /^\w{1,}$/;
		if(couponStatus){ //true
			couponStatus=1;
		}else{ //false
			couponStatus=0;
		}
		var tableWrite = "coupon";
		if(couponName.length == 0){
			alert("請填寫優惠劵名稱");
			$("input[name='coupon_name']").focus();
		}else if( couponCode.length == 0){
			alert("請填寫優惠劵代碼");
			$("input[name='coupon_code']").focus();
		}else if(!codeReg.test(couponCode)){
			alert("請填寫英數碼");
			$("input[name='coupon_code']").focus();
		}else if(couponDiscount.length == 0){
			alert("請填寫折扣百分比");
			$("input[name='coupon_discount']").focus();
		}else if(couponStart == ""){
			alert("請選擇開始時間");
			$("input[name='coupon_start']").focus();
		}else if(couponEnd == ""){
			alert("請選擇結束時間");
			$("input[name='coupon_end']").focus();
		}else if(couponUsed == null){
			alert("請指定標籤");
			$("select#couponEditUsed").focus();
		}else if(couponTarget == null){
			alert("請選擇使用對象");
			$("select#couponEditTarget").focus();
		}else if(couponTimes.length == 0){
			alert("請輸入使用張數");
			$("input[name='coupon_times']").focus();
		}else{	
			if(couponNo == "" ){ //新增
				// console.log("沒有");
				var statusWrite = "新增";
				$.ajax({
					url:"framwork/couponAjax.php",
					data : {table : tableWrite,status : statusWrite,coupon_name:couponName,coupon_code:couponCode,coupon_discount:couponDiscount,coupon_start:couponStart,coupon_end:couponEnd,coupon_used:couponUsed,coupon_target:couponTarget,coupon_times:couponTimes,coupon_status:couponStatus},
		 			type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		alert("優惠碼新增成功");
				    		location.href="coupon.php";
				    		// console.log(result);
				    	}else{ //false
				    		alert("優惠碼已存在");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}else{
				// console.log("有");
				var statusWrite = "修改";
				$.ajax({
					url:"framwork/couponAjax.php",
					data : {table : tableWrite,status : statusWrite,coupon_no:couponNo,coupon_name:couponName,coupon_code:couponCode,coupon_discount:couponDiscount,coupon_start:couponStart,coupon_end:couponEnd,coupon_used:couponUsed,coupon_target:couponTarget,coupon_times:couponTimes,coupon_status:couponStatus},
		 			type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		// console.log(result);
				    		if(result == "有相同優惠劵"){
				    			alert("已經存在相同優惠劵");
				    		}else{
				    			alert("優惠劵更新成功");
				    			location.href="coupon.php";
				    		}
				    	}else{ //false
				    		alert("儲存失敗");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			} 
		}
	});


	// --------order.php-------
	// ----標籤批次分頁----
	$("a.orderClass").click(function(){
		$("a.orderClass").removeClass("active");
		$(this).addClass("active");
		var classActive = $(this).attr("class");
		if(classActive.search("orderProcess") != -1){
			$("#orderHistory").hide();
			$("#orderProcess").show();
		}else{
			$("#orderProcess").hide();
			$("#orderHistory").show();
		}
		// console.log(classActive);
	});

	// --------orderEdit.php-----
	$("#orderEditCancel").click(function(){
		location.href="order.php";
	});

		//確認狀態出貨 顯示出貨通知按鈕
	function deliveySend(){
		if( $("#orderStatusEdit").val() == 2 ){
			$(".deliverInforBtnShow").show();
		}else{
			$(".deliverInforBtnShow").hide();			
		}
	}
	deliveySend();
	$("#orderStatusEdit").change(function(){
		deliveySend();
	});

	$("#orderEditConfirm").click(function(){
		var orderNo = $("input[name='order_no']").val();
		var orderStatus = $("select[id='orderStatusEdit']").val();
		var orderPayStatus = $("select#orderPayStatusEdit").val();
		var adminMemo = $("textarea[id='adminMemoEdit']").val();

		if(orderPayStatus == 0){ //未收到款項
			if(orderStatus == 0 || orderStatus == 4){
				orderUpload();
			}else{
				alert("請確認是否已經付款");
				$("select[id='orderPayStatusEdit']").focus();
			}
		}else{
			if( orderStatus == 2){ //出貨通知
				if( $("#deliverSendBtn").prop("checked") == true ){
					if( confirm("請確認是否要發出貨通知的信") == true ){
						var deliverContent = $("input#productDeliver").val();
						var statusWrite = "sendDeliverMail";
						var orderName = $("input#orderRecipientEdit").val().trim();
						var orderEmail = $("input#orderEmailEdit").val().trim();
						$.ajax({
							url:"framwork/adminAjax.php",
							data : {status : statusWrite,mail_content: deliverContent,order_mail  : orderEmail ,order_name : orderName },
				 			type : "POST",
						    cache : false,
						    success : function(result){
						    	console.log(result);
						    },
						    error : function(error){
						    	alert("傳送失敗");
						    } 
						});
					}	
					orderUpload();
				}else{
					orderUpload();
				}
			}else{
				orderUpload();
			}	
		}


		function orderUpload(){
			tableWrite = "order_item";
			statusWrite = "修改";
			$.ajax({
					url:"framwork/orderAjax.php",
					data : {table : tableWrite,status : statusWrite,order_no:orderNo,order_status:orderStatus,order_pay_status:orderPayStatus,admin_memo:adminMemo},
		 			type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //true 
				    		// console.log(result);
				    		alert("儲存成功");
				    		location.href="order.php";
				    	}else{ //false
				    		alert("儲存失敗");
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
		};
		
	});


	// ======style.php=======
	$("#articleCreatebutton").click(function(){
		window.location.href = "styleEdit.php";
	});


	// ======styleEdit.php=====
	$("#articleEditCancel").click(function(){
		window.location.href = "style.php";
	});
	//上傳文章主要圖檔
	$("#articleImgUpload").unbind('click').click(function(e){
			e.preventDefault();
			$(this).parent().siblings("input[type='file']").click();
	});
	$("input[name='article_img']").unbind('change').change(function(){
		var file = $(this)[0].files[0] ;
		var content = '';
		
		content += 'File Name: '+file.name+'\n';
		content += 'File Size: '+file.size+' byte(s)\n';
		content += 'File Type: '+file.type+'\n';
		content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
		console.log(content);

		$("#articleImgdelete").show();
		var img = $(this).siblings("img.imgShow");
		
		// //====js 讀圖檔位置尚未儲存檔案=====
		var readFile = new FileReader();
		readFile.readAsDataURL(file);
		readFile.addEventListener('load',function(){
			img.attr("src",readFile.result);
		},false);

		var status = "styleImg";
		var formData = new FormData();
		formData.append('file', file); 
		formData.append('status', status);
		$.ajax({
	       url : 'framwork/imgUpload.php',
	       type : 'POST',
	       data : formData,
	       cache : false,
	       processData: false,  // tell jQuery not to process the data
	       contentType: false,  // tell jQuery not to set contentType
	       success : function(data) {
	           console.log(data);
	           $("#img").attr("src","../image/style/"+data);
	       }
		});
	});
	//刪除圖檔
	var imgArticleDelete ;
	$("#articleImgdelete").unbind('click').click(function(){
		// -----儲存圖檔路徑 在確認後執行刪除----
		imgArticleDelete = $(this).parent().siblings("#img").attr("src").slice(3).trim() ;
		// -----將網頁圖片拉掉----
		$(this).parent().siblings("#img").attr("src","../image/style/default.png");
		$(this).hide();
		
		var status = "article";
	 	$.ajax({
            data: {status , status,src : imgArticleDelete},
            type: "POST",
            url: "framwork/imgDelete.php", 
            cache: false,
            success: function(resp) {
              console.log(resp);
            }
        });
	});

	//儲存 or 建立新的文章
	$("#articleEditConfirm").click(function(){
		var articleNo = $("input[name='article_no']").val().trim();
		var articleTitle = $("input[name='article_title']").val().trim();
		var articleDescribe = $("textarea#articleEditDescribe").val().trim();
		var img = $("#img").attr("src").trim().slice(3);
		var articleOwner = $("input[name='article_owner']").val().trim();
		
		if ($('#summernoteArticle').summernote('codeview.isActivated')){
			$('#summernoteArticle').summernote('codeview.deactivate'); 
		}
		
		var articleContent = $("div.note-editable").html();
		var articleStatus = $("input#articleEditStatus").prop("checked");
		var articleTag = $("select#articleEditUsed").val();
		var articleTagUsed = $("input[name='tag_used']");
		if( articleStatus ){
			articleStatus=1;
		}else{
			articleStatus=0;
		}

		///舊有的標籤分類
		var tagUsed =[];
		if(articleTagUsed.length !=0 ){
			for (var i = 0 ; i < articleTagUsed.length ; i++) {
				tagUsed.push(articleTagUsed.eq(i).val() );
			}
		}
		
		if( articleNo.length == 0 ){ //新文章
			// alert("新文章");
			if( articleTitle.length == 0 ){
				alert("請填寫文章標題");
				$("input[name='article_title']").focus();
			}else if( articleDescribe.length == 0 ){
				alert("請填寫文章描述");
				$("input[name='article_describe']").focus();
			}else if( img == "image/style/default.png" ){
				alert("請填上傳文章圖片");
			}else if( articleOwner.length == 0 ){
				alert("請填寫文章編輯人員");
				$("input[name='article_owner']").focus();
			}else{
				var status = "create";
			 	$.ajax({
		            data: {status , status,article_title : articleTitle,article_content : articleContent,article_describe : articleDescribe,article_img : img,article_owner : articleOwner,article_status : articleStatus},
		            type: "POST",
		            url: "framwork/articleAjax.php", 
		            cache: false,
		            success: function(resp) {
		              if(resp){
		              	articleNo = resp ;
		              	// window.location.href = "style.php";
		              }else{
		              	alert("建立失敗");
		              }
		            }
		        }).done(function(){
		        	/// 重建立的ＩＤ在儲存 分類
		        	if( articleTag.length != 0 ){ //有選擇分類在執行
		        		var status = "updateTagRelate";
			        	$.ajax({
				            data: {status , status,article_no : articleNo,article_tag : articleTag,tag_used : tagUsed},
				            type: "POST",
				            url: "framwork/articleAjax.php", 
				            cache: false,
				            success: function(resp) {
				              if(resp){
				              	// console.log(resp);
				              	
				              }else{
				              	alert("建立失敗類別");
				              }
				            }
			        	})
		        	}
		        	location.reload();
		        	
		        });
			}
		}else{ //文章更新
			// alert("文章更新");
			if( articleTitle.length == 0 ){
				alert("請填寫文章標題");
				$("input[name='article_title']").focus();
			}else if( articleDescribe.length == 0 ){
				alert("請填寫文章描述");
				$("input[name='article_describe']").focus();
			}else if( img == "image/style/default.png" ){
				alert("請填上傳文章圖片");
			}else if( articleOwner.length == 0 ){
				alert("請填寫文章編輯人員");
				$("input[name='article_owner']").focus();
			}else{
				// console.log(articleTag);
				var status = "update";
			 	$.ajax({
		            data: {status , status,article_no : articleNo,article_title : articleTitle,article_content : articleContent,article_describe : articleDescribe,article_img : img,article_owner : articleOwner,article_status : articleStatus},
		            type: "POST",
		            url: "framwork/articleAjax.php", 
		            cache: false,
		            success: function(resp) {
		              if(resp){
		              	// window.location.href = "style.php";
		              }else{
		              	alert("更新失敗");
		              }
		            }
		        }).done(function(){
		        	/// 重建立的ＩＤ在儲存 分類
		        	if( articleTag.length != 0 ){ //有選擇分類在執行
		        		var status = "updateTagRelate";
			        	$.ajax({
				            data: {status , status,article_no : articleNo,article_tag : articleTag,tag_used : tagUsed},
				            type: "POST",
				            url: "framwork/articleAjax.php", 
				            cache: false,
				            success: function(resp) {
				              if(resp){
				              	// console.log(resp);
				              	
				              }else{
				              	alert("建立失敗類別");
				              }
				            }
			        	})
		        	}
		        	location.reload();
		        });
			}
		}
	});

	// banner.php 首頁banner
	//上傳圖片
	saveStatus = 0;
	orginImgSrc = []; //全域變數 紀錄點選修改圖檔前的原始黨
	tempImgSrc = []; //全域變數 紀錄點選修改圖檔後的更新黨
	$("#bannerEditModel #bannerUpload").unbind('click').click(function(e){
		e.preventDefault();
		// orginImgSrc = $(this).siblings("img.imgShow").attr("src").trim();
		orginImgSrc.push($(this).siblings("img.imgShow").attr("src").trim());
		$(this).siblings("input[type='file']").click();
	});
	$("#bannerEditModel input[name='banner']").unbind('change').change(function(){
		var bannerNo = $(this).parents("#bannerEditModel").find("input#banner_no").val().trim();
		var bannerNoReStr ="";
		//重拼命名前字串
		switch (bannerNo) {
			case "1":
				bannerNoReStr = "A";
				break;
			case "2":
				bannerNoReStr = "B";
				break;
			case "3":
				bannerNoReStr = "C";
				break;
			case "4":
				bannerNoReStr = "D";
				break;
			case "5":
				bannerNoReStr = "E";
				break;
			case "6":
				bannerNoReStr = "F";
				break;
			case "7":
				bannerNoReStr = "G";
				break;
			case "8":
				bannerNoReStr = "H";
				break;		
			default:
				break;
		}

		var file = $(this)[0].files[0] ;
		var content = '';
		
		content += 'File Name: '+file.name+'\n';
		content += 'File Size: '+file.size+' byte(s)\n';
		content += 'File Type: '+file.type+'\n';
		content += 'Last Modified: '+file.lastModifiedDate.toDateString()+'\n';
		// console.log(content);
		$(this).siblings("#bannerDelete").show();
		var img = $(this).siblings("img.imgShow");
		var imgBannerTemperary = $(this).siblings("img#temporaryBanner");
		// //====js 讀圖檔位置尚未儲存檔案=====
		var readFile = new FileReader();
		readFile.readAsDataURL(file);
		readFile.addEventListener('load',function(){
			imgBannerTemperary.attr("src",readFile.result);
		},false);

		var status = "bannerImg";
		var formData = new FormData();
		formData.append('file', file); 
		formData.append('imgOriginSrc', img.attr("src").trim() );
		formData.append('bannerNoReStr', bannerNoReStr);
		formData.append('status', status);
		$.ajax({
			url : 'framwork/imgUpload.php',
			type : 'POST',
			data : formData,
			cache : false,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(data) {
				// console.log(data);
				$("#Banner").attr("src","../"+data);
				// tempImgSrc = "../"+data;
				tempImgSrc.push("../"+data);
			}
		 });
	});	


	//確認更新設定鍵
    $("#bannerEditModel #editBannerConfirm").click(function(){
		saveStatus =1; //儲存狀態 刪除檔案辨識

		var bannerNo = $("input#banner_no").val().trim();
		var img = $("#Banner").attr("src").trim(); //更新後新的入徑 
        var link = $("input#bannerLink").val().trim();
        var blank = $("#blankStatus").bootstrapSwitch('state') == true ? 1 : 0 ;
		var status = $("input#bannerStatus").bootstrapSwitch('state') == true ? 1 : 0;
		var tableWrite = "banner" ;
		var statusWrite = "update" ;
		if( !img && status==1 ){
			//沒有圖檔狀態不可開啟
			alert("請確認banner圖已經更新才可將狀態開啟");
		}else{
			if( orginImgSrc.length != 0 ){
				//有更新圖檔,刪除舊的圖檔
				deleteImgFile(orginImgSrc);
			}else{
				// alert("沒有更新圖檔");
			}
			var imgReplace =  img.replace("../", "");
			//儲存檔案
			$.ajax({
				url:"framwork/bannerAjax.php",
				data : {table : tableWrite , status : statusWrite , banner_no : bannerNo , img : imgReplace ,link : link , blank : blank , banner_status : status },
				 type : "POST",
				 dataType : "json",
				cache : false,
				success : function(result){
						if(result){ //true 
							// ======server reload======
							if(result){
								bannerServerTable.ajax.reload( null, false ); 	//datatable reload 不跳回初始第一頁 datatableAdmin.js
								//關閉model 光箱
								$('#bannerEditModel').modal('hide');
							}
						}else{ //false
							// alert("找無分類");
						}
					},
				error : function(error){
						alert("傳送失敗");
					} 
			});
			
		}
	});
	//刪除圖檔按鈕
	$("#bannerEditModel #bannerDelete").click(function(){
		//儲存欲刪除陣列 ,儲存後正式刪除
		var imgSrc = $(this).siblings("#Banner").attr("src").trim();
		orginImgSrc.push(imgSrc);

		//initail 圖片按鈕
		$("#temporaryBanner").attr("src","") ;
		$("#Banner").attr("src","");
		$("#bannerDelete").hide();

	});
	//刪除圖檔
	function deleteImgFile( img ){
		img.forEach(function(e){
			var imgSrc = e.replace("../","").trim();
			var statusWrite = "banner";
			$.ajax({
				url : 'framwork/imgDelete.php',
				type : 'POST',
				data : {status : statusWrite, src : imgSrc},
				cache : false,
				success : function(result) {
					if(result){ //true
						// alert("成功檔案刪除");
					}else{ //刪除失敗

					}  
				}
			});
		});
		
	}

    //關閉banner 設定視窗狀態
    $('#bannerEditModel').on('hidden.bs.modal', function (e) {
		//判斷是否有更新過圖檔但是取消儲存
		if( saveStatus == 0){ //儲存狀態 ,更新的圖檔不用刪除
			if( tempImgSrc.length == 0 ){ 
				// alert("沒有更新過圖檔");
			}else{
				// alert("有更新過圖檔"+tempImgSrc);
				deleteImgFile(tempImgSrc);
			}
		}
		
	   //全域變數原始圖檔歸0
	   orginImgSrc = [];
	   //全域變數新增黨檔歸0
	   tempImgSrc = [];
	   //全域變數 儲存狀態歸0
	   saveStatus = 0;
	});
	

	// banner.php 首頁精選品牌
	var bsNo =$("select#bs").val();
	$("select#bs").change(function(){
		bsNo = $(this).val();
	});
	$("#selectBrandConfirm").click(function(){
		var statusWrite = "updateBrandSelet";
		var cateNo = bsNo;
		var table = "brand_select";
		$.ajax({
			url : 'framwork/bannerAjax.php',
			type : 'POST',
			data : {status : statusWrite, table : table,cate_no : cateNo},
			cache : false,
			success : function(result) {
				if(result){
					location.reload();
				}
			}
		});
	});

	// banner.php 免運設定
	var DBCart = parseInt($("#cartPrice").val());
	var DBCartCargo = parseInt($("#cartCargo").val());
	$("#cartPrice").change(function(){
		editCartBtn();
	});
	$("#cartCargo").change(function(){
		editCartBtn();
	});


	function editCartBtn(){
		var cartPrice = parseInt($("#cartPrice").val());
		var cartCargo = parseInt($("#cartCargo").val());
		if( cartPrice == DBCart  && cartCargo == DBCartCargo){
			$("#cartEditConfirm").attr("disabled" , true);
			$("#cartEditConfirm").addClass("buttonDisabled");
		}else{
			$("#cartEditConfirm").attr("disabled" , false);
			$("#cartEditConfirm").removeClass("buttonDisabled");
		}
	}
	
	$("#cartEditConfirm").click(function(){
		var statusWrite = "updateCartPrice";
		var cartPrice = parseInt($("#cartPrice").val().trim());
		var cartCargo = parseInt($("#cartCargo").val().trim());
		var table = "cart_set";
		$.ajax({
			url : 'framwork/bannerAjax.php',
			type : 'POST',
			data : {status : statusWrite, table : table,cart_price : cartPrice , cart_cargo : cartCargo},
			cache : false,
			success : function(result) {
				if(result){
					location.reload();
				}
			}
		});
	});

 	// ======bootsrap switch=====
 	$("[name='my-checkbox']").bootstrapSwitch(
 		"size","mini"
 	);


});

function getComboSpec(product_no, obj_id, default_value) {
	$.ajax({
		url:"framwork/productComboAjax.php",
		data : {"product_no" : product_no, "status" : "getComboSpec"},
		type : "POST",
		dataType : "json",
		cache : false,
		success : function(result){
			var tmpItem = '';
			if (result) { //true
				$(result).each(function(index, item){
					tmpItem += '<option value="' + item["product_spec_no"] + '">' + item["product_spec_no"] + ' / ' + item["product_spec_info"] + ' (商品庫存:' + item["product_stock"]+')</option>';
				});
			} else { //false
				
			}
			$("#" + obj_id).html(tmpItem);
			$("#" + obj_id).val(default_value);
		},
		error : function(error){
			alert("傳送失敗");
		} 
	});
}