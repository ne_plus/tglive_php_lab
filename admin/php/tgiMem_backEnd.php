<!-- ========會員管理======== -->
<!-- ======dataTable===== -->
<div class="container demo">
	<table id="testTable" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>會員編號</th>
                <th>姓名</th>
                <th>Email</th>
                <th>電話</th>
                <th>加入時間</th>
                <th>最後登入時間</th>
                <th></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
            	<th>會員編號</th>
                <th>姓名</th>
                <th>Email</th>
                <th>電話</th>
                <th>加入時間</th>
                <th>最後登入時間</th>
                <th></th>
        	</tr>
    	</tfoot>
    	<tbody>
    		<?php  
    		date_default_timezone_set("Asia/Taipei");
            include("../../config.php");
    		require_once('../../model/DB.php');
    		$db = new DB();
    		$query = $db->DB_Query("select * from member order by mem_no asc");
    		foreach($query as $key=>$value){

    	?>
    		<tr>
                <td><?php echo $value["mem_no"]; ?></td>
                <td><?php echo $value["mem_lastname"].$value["mem_firstname"]; ?></td>
                <td><?php echo $value["mem_mail"]; ?></td>
                <td><?php echo $value["mem_tel"]; ?></td>
                <td><?php echo date("Y-m-d H:i:s",$value["mem_createtime"]); ?></td>
                <td><?php echo $value["mem_lastLoginTime"]; ?></td>
                <td><span class="edit">編輯</span><span>|</span><span class="edit">訂單紀錄</span><span>|</span><sapn class="edit glyphicon glyphicon-trash"></sapn></td>
            </tr>
         <?php  
         	}
         ?>    
    	</tbody>
	</table>
</div>	