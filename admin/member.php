<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">會員</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">會員</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<div class="card">
             <!-- =====dataTable====== -->
			             <div class="demo">
							<table id="dataTable-member" class="display" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>編號</th>
						                <th>姓名</th>
						                <th>Email</th>
						                <th>電話</th>
						                <th>加入時間</th>
						                <th>最後登入時間</th>
						                <th>狀態</th>
						                <th></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
						            	<th>編號</th>
						                <th>姓名</th>
						                <th>Email</th>
						                <th>電話</th>
						                <th>加入時間</th>
						                <th>最後登入時間</th>
						                <th>狀態</th>
						                <th></th>
						        	</tr>
						    	</tfoot>
						    	<tbody>
						    		
						    	</tbody>
							</table>
						</div>	
             <!-- =====/dataTable====== -->
             	</div>
            </div> 
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>