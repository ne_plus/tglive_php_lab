<?php require_once("module/header.php"); ?>

<?php date_default_timezone_set("Asia/Taipei");?> 


      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">行銷分類</li>
          </ul>
        </div>
      </div>
      <section class="charts activity">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">行銷分類</h1>
          </header>
          <div class="row">        
				<!-- category==== -->
			<div class="col-12">
				<div class="card">
					<div class="container">
						<div class="cresateBar">
							<div class="row">
								<div class="col-8">
										<input type="text" class="form-control form-control-sm" id="createCateActivityName" placeholder="新增行銷分類">	
								</div>
								<div class="col-4 text-right">
										<button id="createActivityCate" type="button" class="btn-sm btn-outline-success">新增</button>
								</div>
							</div>
						</div>
						
						<table class="table table-striped ">
		                    <thead>
		                      <tr>
		                        <th>行銷活動名稱</th>	 
		                        <th></th>                       
		                      </tr>
		                    </thead>
		                    <tbody>
		                    	<?php  
							    	require_once('../model/DB.php');
							    	$db = new DB();
                              		$query = $db->DB_Query("select * from category where cate_parents = '63' and cate_level = 1 order by cate_sort asc"); 
                              		if(count($query) == 0){
                              	?>
                              	 <tr><td class="text-center" colspan="2">找無資料</td></tr>
                              	<?php	
                              		}else{
										foreach ($query as $key => $value) {
											$disableButton = '';
											// cate_no=90 (推薦新品)  cate_no = 94 (熱門商品) cate_no=49(VR專區) cate_no = 183 (人氣商品) 
											if( $value["cate_no"] == 90 || $value["cate_no"] == 94 || $value["cate_no"] == 49 || $value["cate_no"] == 183){
												$disableButton = 'disabled';
											}
								?>				
								<tr>
									<td class="cateName"><?php echo $value["cate_name"]; ?>
										<input type="hidden" name="cate_no" value="<?php echo $value["cate_no"]; ?>">
										<input type="hidden" name="cate_describe" value="<?php echo $value["cate_describe"]; ?>">
									</td>
									<td class="text-right"><span class="edit"><button class="cateEdit" data-toggle="modal" data-target="#cateActivityEditModel" <?php echo $disableButton; ?>>編輯</button></span><span class="edit"><button class="cateDelet" <?php echo $disableButton; ?>>刪除</button></span></td>
								</tr>	
	 
								<?php
										}
                              		}
                              		
                              	?>
							   
							    <!--  -->
		                    </tbody>
	                  	</table>  

                  </div>
				</div>
             </div>        	
            	<!-- category==== -->


			<!-- Modal -->
			<div class="modal fade" id="cateActivityEditModel" tabindex="-1" role="dialog" aria-labelledby="cateNewModel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="cateNewModel">分類編輯</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
				        <div class="form-group row">
			        		<label for="cateActivityNewName" class="col-3 col-form-label">活動名稱</label>
							<div class="col-9">
							  <input class="form-control" type="text" id="cateActivityNewName" placeholder="輸入行銷分類名稱">
							</div>
	                    </div>
	                    <div class="form-group row">
			        		<label for="cateDescribe" class="col-3 col-form-label">說明</label>
							<div class="col-9">
								<textarea id="cateDescribe" name="cate_describe" class="form-control" placeholder="請輸入分類描述(50字以內)" rows="3"></textarea>
							  <!-- <input id="cateDescribe" type="text" name="cate_describe" class="form-control" placeholder="請輸入分類描述"> -->
							</div>
	                    </div>
	                    <div class="form-group row">
			        		<label class="col-3 col-form-label">Banner</label>
							<div class="col-3 text-center">
							  <button id="bannerUpload" class="btn-sm btn-outline-secondary cateBannerUpload">檔案上傳</button>	
							  <input type="file" name="cate_banner" style="display:none;" class="form-control">
							  <img src="" id="temporaryBanner" class="img-fluid" style="margin: 10px 0;">
							  <img id="cateBanner" src=""  class="imgShow img-fluid" style="display:none;">
							   <button id="bannerDelete" style="display:none;" class="btn-sm btn-outline-danger cateBannerDelete">刪除</button>
							</div>
							<div class="col-6 imgInfo">
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸需求*</div>
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">1920x480<!-- <sub>px</sub> --></div>
                            </div>
	                    </div>
	
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
			        <button type="button" class="btn-sm btn-primary" id="editCateConfirm">儲存設定</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- Modal -->

			       
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>