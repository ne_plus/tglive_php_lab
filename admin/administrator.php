<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">管理員</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">管理員</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
                <div class="card">
            
                     <div class="demo">
                         <div id="createBox" class="row" style="margin-bottom:30px;">
                          <!-- Button trigger modal -->    
                            <div class="col text-right">
                              <button id="createAdmin" class="btn-sm btn-outline-success" data-toggle="modal" data-target="#exampleModal">新增管理者</button>
                            </div>  
                        </div>
                  <!-- =====dataTable====== -->
                				<table id="dataTable-admin" class="display" cellspacing="0" width="100%">
                					<thead>
                						<tr>
                							<th>編號</th>
                			                <th>信箱</th>
                			                <th>權限</th>
                			                <th>名稱</th>
                			                <th>加入時間</th>
                			                <th>最後登入時間</th>
                			                <th>狀態</th>
                			                <th></th>
                						</tr>
                					</thead>
                					<tfoot>
                						<tr>
                			            	<th>編號</th>
                			                <th>信箱</th>
                			                <th>權限</th>
                			                <th>名稱</th>			                
                			                <th>加入時間</th>
                			                <th>最後登入時間</th>
                			                <th>狀態</th>
                			                <th></th>
                			        	</tr>
                			    	</tfoot>
                			    	<tbody>
                			    		
                			    	</tbody>
                				</table>
        			      </div>	
             <!-- =====/dataTable====== -->
                </div>
            </div> 
          </div>
        </div>
      </section>
    

 <!-- =========bootsrap 4 lightbnox===== -->         
      <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">新增管理員</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="admCreateMail" class="col-2 col-form-label">帳號</label>
                      <div class="col-10">
                          <input class="form-control" type="email" id="admCreateMail" placeholder="example@mail.com">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="admCreatePsw" class="col-2 col-form-label">密碼</label>
                        <div class="col-10">
                          <input class="form-control" type="password" id="admCreatePsw" placeholder="英數六碼以上">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="admCreateName" class="col-2 col-form-label">名稱</label>
                        <div class="col-10">
                          <input class="form-control" type="text" id="admCreateName" placeholder="管理者名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="admAuthLevel" class="col-2 col-form-label">權限</label>
                        <div class="col-10">
                          <select class="form-control" id="admAuthLevel">
                              <option value="" disabled selected>選擇管理者權限</option>
                              <option value="1">產品 / 訂單</option>
                              <option value="2">報表 / 使用者 / 匯入匯出</option>
                              <option value="3">最高權限</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="admStatus" class="col-2 col-form-label">狀態</label>
                        <div class="col-10">
                          <div class="row">
                              <label class="col-12 col-form-label switchButtonColor"><input id="admStatus" type="checkbox" name="my-checkbox" checked></label>
                          </div>
                          
                          
                        </div>
                    </div>            
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="admCreateConfirm" type="button" class="btn-sm btn-primary">確認新增</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>         
<!-- =========bootsrap lightbnox===== -->




      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>