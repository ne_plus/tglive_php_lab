<?php require_once("module/header.php"); ?>
<?php require_once("../controller/articleControl.php"); 

if( isset($_REQUEST["article_no"]) ){ //文章修改
    $articleNo = $_REQUEST["article_no"];
    $table = "article";
    $result = query($table,$articleNo);
    $jsonContent = json_encode($result["article_content"]);

    $pageTitle = "編輯文章";
    $articleNo = $result['article_no'];
    $articleTitle = $result['article_title'];
    $articleDescribe = $result['article_describe'];
    $articleOwner = $result['article_owner'];
    $createtime = date("Y-m-d H:i:s",$result['article_createtime']);
    if (trim($result['article_img']) != null || trim($result['article_img']) != ''){ 
      $imgStatus = "";
      $img = '../'.trim($result['article_img']);
    }else{ 
      $imgStatus = "display:none";
      $img = '../image/style/default.png';
    }
    if( $result['article_status'] == 1 ){ 
      $status = "checked"; 
    }else{
      $status = "";
    }
}else{ //文章新增
    $pageTitle = "新增文章";
    $articleNo = "";
    $articleTitle = "";
    $articleDescribe = "";
    $imgStatus = "display:none";
    $img = '../image/style/default.png';
    $articleOwner = "";
    $status = "";
    $jsonContent ="''";
}



//文章標籤分類
$articleTag = articleTagQuery("50",$articleNo);
if( count($articleTag["use"]) !=0 ){ //有使用分類
  $articleUse = "";
}else{ //沒有使用的標籤
  $articleUse = "selected";
}
// echo "<pre>";
// print_r($articleTag);
// echo "</pre>";
?>


      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="style.php">居生活文章</a></li>
            <li class="breadcrumb-item active"><?php echo $pageTitle ; ?></li>
          </ul>
        </div>
      </div>
      <section class="forms">
        <div class="container-fluid">
          <header> 
            <h1 class="h3"><?php echo $pageTitle ; ?></h1>
          </header>
          <div class="row">
            <!-- ======form ===== -->
            <div class="col-lg-12">
              <div class="card">
                <div class="card-block">
                  <!-- <form class="form-horizontal"> -->
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditTitle"><span class="text-danger">*</span>文章標題</label>
                      <div class="col-sm-4">
                        <input id="articleEditTitle" type="text" name="article_title" class="form-control" value="<?php echo $articleTitle; ?>" placeholder="請輸入文章標題">
                        <input type="hidden" name="article_no" value="<?php echo $articleNo; ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditDescribe"><span class="text-danger">*</span>文章描述</label>
                      <div class="col-sm-10">
                        <textarea id="articleEditDescribe" name="article_describe" class="form-control" placeholder="請輸入文章描述(50字以內)" rows="5" maxlength="50"><?php echo $articleDescribe; ?></textarea>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditImg"><span class="text-danger">*</span>文章圖片</label>
                      <div class="col d-flex justify-content-start">
                          <div>
                            <div class="">
                              <button id="articleImgUpload" class="btn-sm btn-outline-secondary">檔案上傳</button>
                              <button id="articleImgdelete" style="<?php echo $imgStatus;?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                            </div>
                            <input id="fileImg" type="file" name="article_img" style="display:none;" class="form-control">                           
                            <img id="img" src="<?php echo $img;  ?>" class="imgShow img-fluid"> 
                          </div>                              
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="" class="col-sm-2 col-form-label"><span class="text-danger">*</span>文章內容</label>
                           <div class="col-sm-10">
                                <div id="summernoteArticle"></div>
                           </div>
                      </div>
                      <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditOwner"><span class="text-danger">*</span>編輯人員</label>
                      <div class="col-sm-4">
                        <input id="articleEditOwner" type="text" name="article_owner" class="form-control" value="<?php echo $articleOwner; ?>" placeholder="請輸入文章編輯人員">
                      </div>
                    </div>
                    <div class="line"></div>  
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditStatus"><span class="text-danger">*</span>文章狀態</label>
                      <div class="col-sm-10">
                          <div class="row">
                            <div class="col-12 col-form-label switchButtonColor"><input id="articleEditStatus" class="article_status" type="checkbox" name="my-checkbox" <?php  echo $status;  ?> ></div>
                          </div>          
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleEditUsed"><span class="text-danger">*</span>文章分類</label>
                      <div class="col-sm-4">
                        <select id="articleEditUsed" class="form-control" multiple>
                           <option value disabled <?php echo $articleUse; ?> >選擇文章分類</option>
                        <?php  
                          foreach ($articleTag["all"] as $key => $value) {
                            $select = "";
                            if( in_array($value["tag_no"],$articleTag["use"]["tag_no"]) ){
                              $select = "selected";
                            }
                        ?>
                          <option value="<?php echo $value["tag_no"]; ?>" <?php echo $select; ?>><?php echo $value["tag_name"]; ?></option>
                        <?php    
                          }
                        ?>
                        </select>
                        <?php  
                          if( $articleUse != "selected" ){ 
                            foreach ($articleTag["use"]["tag_no"] as $key => $value){
                        ?>
                          <input type="hidden" name="tag_used" value="<?php echo $value; ?>">    
                        <?php  
                            }  
                          }
                        ?>
                      </div>  
                    </div>
                    <div class="line"></div>
                    <?php if(isset($_REQUEST["article_no"])){ ?>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="articleCreateTime"><span class="text-danger">*</span>文章建立時間</label>
                      <div class="col-sm-4">
                        <input id="articleCreateTime" type="text" name="article_createtime" class="form-control" value="<?php echo $createtime; ?>" disabled>
                      </div>
                    </div>
                    <div class="line"></div> 
                    <?php } ?>                      
                    <div class="form-group row">
                      <div class="col-12 text-right">
                        <button id="articleEditCancel" type="button" class="btn-sm btn-secondary">取消</button>
                        <button id="articleEditConfirm" type="button" class="btn-sm btn-success"><?php if(isset($_REQUEST["article_no"])){echo "儲存編輯";}else{echo "新增確認";} ?></button>
                      </div>
                    </div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
            <!-- ======form======== -->
          </div>
        </div>
      </section>


      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
   <script type="text/javascript">
    $(document).ready(function(){
        
        var content=<?php echo $jsonContent; ?>;
        if(content != "" ){
          $("div.note-editable").html(content);  
          $("div.note-placeholder").css("display","none");
        }else{
        
        }
        
    });
     
  </script>


<?php require_once("module/footer.php"); ?>