<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">優惠劵管理</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">優惠劵管理</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<div class="card product">
			

             <!-- =====dataTable====== -->
			             <div class="demo">
			             	<div class="row" style="margin-bottom:30px;">			             		
				             	<div class="col text-right">
				             		<button id="couponCreatebutton" class="btn-sm btn-outline-success">新增優惠劵</button>
				             	</div>
			             	</div>
			             	

			             			             	
      							<table id="couponServerTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
      								<thead>
      									<tr>
      										<th>優惠卷名稱</th>
      										<th>代碼</th>
      		                <th>標籤</th>
      		                <th>折扣百分比</th>
      		                <th>開始時間</th>
      		                <th>結束時間</th>
      		                <th>狀態</th>	
      		                <th></th>					               
      									</tr>
      								</thead>
      								
      						    	<tbody>
      						    	</tbody>
      							</table>
      							
      						</div>	
                   <!-- =====/dataTable====== -->
               	</div>
              </div> 
            </div>
          </div>
	






      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>