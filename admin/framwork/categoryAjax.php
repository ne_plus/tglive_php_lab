<?php  
ob_start();
session_start();

include("../../controller/categoryControl.php");


// ====ip get======
if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();


switch ($_REQUEST["status"]) {
// ======查詢所有父層=======
	case "查詢" :
			
			echo json_encode(cateSearchFatherAll());
			
			break;	

//=======分類資訊========			
	case "分類資訊" :
			
			$cateNo=$_REQUEST["cate_no"];
			echo json_encode(cateInfo($cateNo));
			
			break;			
// ======create======= 
	case "新增" : 
			$table = $_REQUEST["table"];
			$cateName = $_REQUEST["cate_name"];	
			$cateParents=$_REQUEST["cate_parents"];
			if($cateParents == 63 || $cateParents == 61){ //行銷活動使用以及標籤分類
				$cateLevel = "1";
			}elseif($cateParents != 62){
				$cateLevel = "2";
			}else{
				$cateLevel = "1";
			}		
			$item = array(
			"cate_parents"=> $cateParents,
			"cate_name"=> $cateName,
			"cate_level"=> $cateLevel
			);
			// ------(執行方式1)有偵測 是否有相同cate_name 存在---
			// echo cateCreate($cateName,$table,$item);  //新增成功回傳cate_no
			
			// -----(執行方式2)直接更新-----
			echo cateCreate2($cateName,$table,$item);
			break;
// ======update=======
	case 'update360' : 
			$table = $_REQUEST["table"];
			$cateNo = $_REQUEST["cate_no"];
			$mainStatus = $_REQUEST['cate_360_main'];
			$item = array(
				"cate_no" => $cateNo,
				"cate_360_main"=> $mainStatus,
			);
			echo cate360primaryUpdate($cateNo,$table,$item);
			break;		
	case "修改" : 
			$table = $_REQUEST["table"];
			$cateName = $_REQUEST["cate_name"];
			$cateNo = $_REQUEST["cate_no"];	
			$cateParents =$_REQUEST["cate_parents"];
			if($cateParents == 63){ //行銷活動使用
				$cateLevel = "1";
			}elseif($cateParents != 62){
				$cateLevel = "2";
			}else{
				$cateLevel = "1";
			}
			$cate360url	= $_REQUEST["cate_360_url"];
			$cate360status	= $_REQUEST["cate_360_status"];
			$item = array(
				"cate_no" => $cateNo,
				"cate_parents"=> $cateParents,
				"cate_level"=> $cateLevel,
				"cate_name"=> $cateName,
				"cate_logo_img"=> $_REQUEST["cate_logo_img"],
				"cate_banner_img"=> $_REQUEST["cate_banner_img"],
				"cate_360_status"=> $cate360status,
				"cate_360_link"=> $cate360url,
				"cate_describe"=> $_REQUEST["cate_describe"]
			);
			echo updateCate($cateNo,$table,$item,$cateName);
			// echo json_encode(updateCate($cateNo,$table,$item,$cateName));
			break;
	case "父層修改" : 
			$table = $_REQUEST["table"];
			$cateName = $_REQUEST["cate_name"];
			$cateNo = $_REQUEST["cate_no"];	
			$cateParents =$_REQUEST["cate_parents"];
			if($cateParents != 62){ //父層轉變為其他子層
				$cateLevel = "2";
				$result =  cateSearch($cateNo);
				if($result){ ///有子層 先將子層轉為父層 在修改父層
					foreach ($result as $key => $value) {
						$cateNoChid =  $value["cate_no"];
						$itemChild = array(
							"cate_no"=> $cateNoChid,
							"cate_parents"=> "62",
							"cate_level"=> "1"
						);
						childToFatherCateUpdate($cateNoChid,$table,$itemChild);					
					}
				}
			}else{ //層級不變內容修改
				$cateLevel = "1";
			}	
			$item = array(
				"cate_no" => $cateNo,
				"cate_parents"=> $cateParents,
				"cate_level"=> $cateLevel,
				"cate_name"=> $cateName,
				"cate_logo_img"=> $_REQUEST["cate_logo_img"],
				"cate_banner_img"=> $_REQUEST["cate_banner_img"],
				"cate_describe"=> $_REQUEST["cate_describe"]
			);
			echo updateCate($cateNo,$table,$item,$cateName);
			// echo json_encode(updateCate($cateNo,$table,$item,$cateName));
			break;		
// ======delete=====
	case "刪除": 
			$table = $_REQUEST["table"];
			$cateNo = $_REQUEST["cate_no"];	
			$item = array(
				"cate_no" => $cateNo				
			);
			if(isset($_REQUEST["cate_level"]) == false){
				echo deleteCate($cateNo,$table,$item);
				break;
			}elseif($_REQUEST["cate_level"] == 2){ // 子層
				echo deleteCate($cateNo,$table,$item);
				break;			
			}else{ //父層
				
				// =======子層晉升為父層=====
				$result =  cateSearch($cateNo);

				if($result){ ///有子層 先將子層轉為父層在刪除父層

				// ======刪除父層=======	
				deleteCate($cateNo,$table,$item);	

					foreach ($result as $key => $value) {
						$cateNoChid =  $value["cate_no"];
						$itemChild = array(
							"cate_no"=> $cateNoChid,
							"cate_parents"=> "62",
							"cate_level"=> "1",
							"cate_sort"=> "0"
						);
						// print_r($itemChild);
						childToFatherCateUpdate($cateNoChid,$table,$itemChild);
					}
				echo true;
					
				}else{ ///沒有子層直接刪除父層
					// ======刪除父層=======
					echo deleteCate($cateNo,$table,$item);
				}
				break ;
			}
			
} //---end switch case

?>