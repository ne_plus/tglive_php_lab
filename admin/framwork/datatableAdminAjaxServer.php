<?php  
include("../../model/DB.php");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}

// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";



date_default_timezone_set("Asia/Taipei");
$sql = "select * from admin order by adm_no ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	$adm =[];
	if($result){
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$adm[$key]["adm_no"] = $value["adm_no"];
			$adm[$key]["adm_authLevel"] = $value["adm_authLevel"];
			$adm[$key]["adm_email"] = $value["adm_email"];
			$adm[$key]["adm_name"] = $value["adm_name"];
			$adm[$key]["adm_status"] = $value["adm_status"];
			$adm[$key]["adm_createtime"] = $value["adm_createtime"];
			$adm[$key]["adm_updatetime"] = $value["adm_updatetime"];
			$adm[$key]["adm_lastLoginTime"] = $value["adm_lastLoginTime"];
			$searchCheck =array($value["adm_no"],$value["adm_email"],$value["adm_name"],$value["adm_createtime"],$value["adm_updatetime"]);

			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($adm[$key]);
				}else{  //配對上相同字串
					$searchCount++;
				}
			}
			
		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}


		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($adm,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r(array_slice($adm,0,-1));
		// echo "</pre>";
		
	}else{
		$array["data"]=array_slice($adm,$start,$length);
		$jsonStr = json_encode($array);
		echo $jsonStr;
	}



?>