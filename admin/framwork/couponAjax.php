<?php  
ob_start();
session_start();

include("../../controller/couponControl.php");


switch ($_REQUEST["status"]) {
// ======create=======
	case "新增" :
			$table = $_REQUEST["table"];
			$couponName = $_REQUEST["coupon_name"];			
			$item = array(
				"coupon_name"=> $couponName,
				"coupon_code"=> $_REQUEST["coupon_code"],
				"coupon_discount"=>$_REQUEST["coupon_discount"],
				"coupon_start"=> strtotime($_REQUEST["coupon_start"]),
				"coupon_end"=>strtotime($_REQUEST["coupon_end"]),
				"coupon_used"=> $_REQUEST["coupon_used"],
				"coupon_target"=>$_REQUEST["coupon_target"],
				"coupon_status"=>$_REQUEST["coupon_status"],
				"coupon_times"=>$_REQUEST["coupon_times"]
			);

			// print_r($item) ;

			echo addCoupon($table,$couponName,$item);


			break;
// ======update=======
	case "修改" : 
			$table = $_REQUEST["table"];
			$couponNo = $_REQUEST["coupon_no"];
			$couponName = $_REQUEST["coupon_name"];			
			$item = array(
				"coupon_no"=> $couponNo,
				"coupon_name"=> $couponName,
				"coupon_code"=> $_REQUEST["coupon_code"],
				"coupon_discount"=>$_REQUEST["coupon_discount"],
				"coupon_start"=> strtotime($_REQUEST["coupon_start"]),
				"coupon_end"=>strtotime($_REQUEST["coupon_end"]),
				"coupon_used"=>$_REQUEST["coupon_used"],
				"coupon_target"=>$_REQUEST["coupon_target"],
				"coupon_status"=>$_REQUEST["coupon_status"],
				"coupon_times"=>$_REQUEST["coupon_times"]
			);
			echo editCoupon($table,$couponNo,$item,$couponName);
			break;
// ======delete=====
	case "刪除": 
			$table = $_REQUEST["table"];
			$couponNo = $_REQUEST["coupon_no"];
			echo deleteCoupon($table,$couponNo);
			break;
// =======category 刪除後 coupon 欄位寫入空值並停用=====
	case "分類刪除": 
			$table = $_REQUEST["table"];
			$cateNo = $_REQUEST["cate_no"];
		 	$item = array(
				"coupon_used"=>null,
				"coupon_status"=> 0 
				);
		 	
			 echo cateDelete($table,$cateNo,$item);
			// echo deleteCoupon($table,$couponNo);
			break;

} //---end switch case

?>