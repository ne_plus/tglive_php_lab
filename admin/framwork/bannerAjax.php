<?php  
ob_start();
session_start();

include("../../model/DB.php");

switch ($_REQUEST["status"]) {
// ======update=======
	case "update" : 
			$table = $_REQUEST["table"];
            $bannerNo = $_REQUEST["banner_no"];	
            $img = $_REQUEST["img"];
            $linkSrc = $_REQUEST["link"];
            $blankStatus = $_REQUEST["blank"] ;
            $bannerStatus = $_REQUEST["banner_status"] ;
			$item = array(
                "banner_no" => $bannerNo,
				"img" => $img,
				"link"=> $linkSrc,
				"blank"=> $blankStatus,
				"banner_status"=> $bannerStatus
            );
            $checkColumn =  array("banner_no");
            $db = new DB();
            $result = $db -> DB_Update($table,$item,$checkColumn);
            if( $result ){
                echo true;
            }else{
                echo false;
            }
            
            break;
     case "updateBrandSelet" : 
            $table = $_REQUEST["table"];
            $cateNo = $_REQUEST["cate_no"];
			$item = array(
                "bs_no" => 1,
				"cate_no" => $cateNo
            );
            $checkColumn =  array("bs_no");
            $db = new DB();
            $result = $db -> DB_Update($table,$item,$checkColumn);
            if( $result ){
                echo true;
            }else{
                echo false;
            }
            
            break;   
    case "updateCartPrice" : 
            $table = $_REQUEST["table"];
            $cartPrice = $_REQUEST["cart_price"];
            $cartCargo = $_REQUEST["cart_cargo"];
			$item = array(
                "cs_no" => 1,
                "cs_price" => $cartPrice,
                "cs_cargo" => $cartCargo
            );
            $checkColumn =  array("cs_no");
            $db = new DB();
            $result = $db -> DB_Update($table,$item,$checkColumn);
            if( $result ){
                echo true;
            }else{
                echo false;
            }
            
			break;                	
			
} //---end switch case

?>