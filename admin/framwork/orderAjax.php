<?php  
ob_start();
session_start();

include("../../controller/orderControl.php");

if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
// ====ip get======
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();


switch ($_REQUEST["status"]) {
// ======create=======
	case "新增" :
		$cate_no=$_REQUEST["cate_no"];
		foreach($cate_no as $key => $cateValue){
			$orderEmail = "jack@mail.com";
			$orderCreatetime = time();
			$orderGroup = $orderEmail.$orderCreatetime;
			$orderCargo = $cate_no[0] == $cateValue ? 90 : 0 ;
			// ------計算同一供應商內的消費總金額------
			$orderPrice = 0;
			foreach ($_SESSION["products"] as $productKey => $productValue) {
				if($_SESSION["products"][$productKey]["cate_no"] == $cateValue){
					$orderPrice += $_SESSION["products"][$productKey]["order_detail_quantity"]*$_SESSION["products"][$productKey]["order_detail_price"];
				}	
			}
			
			$item = array(
				"mem_no" => 355,
				"cate_no" => $cateValue,
				"cate_name" => "test2",
				"order_group" => $orderGroup,
				"order_email" => $orderEmail,
				"order_recipient" => "熊天",
				"order_address" => "台北市內湖", 
				"order_tel" => "0937930193",
				"order_price" => $orderPrice,
				"order_cargo" => $orderCargo,
				"coupon_code" => null,
				"order_discount" => null,
				"order_vat" => 0.05,
				"order_pay" => 0,
				"order_pay_status" => 0,
				"order_status" => 0,
				"order_delivery" => 0,
				"order_createtime" => $orderCreatetime,
				);

		// echo "<pre>";
		// print_r($item);
		// echo "</pre>";
		echo createOrder($item,$cateValue); 

		}
			
			break;

// ========update=============
	case "修改":
		$table = $_REQUEST["table"];
		$orderNo = $_REQUEST["order_no"];
		$orderStatus = $_REQUEST["order_status"];
		$orderPayStatus = $_REQUEST["order_pay_status"];
		$adminMemo = $_REQUEST["admin_memo"];

		$item = array(
				"order_no" => $orderNo ,
				"order_pay_status" => $orderPayStatus,
				"order_status" => $orderStatus,
				"admin_memo" => $adminMemo
				);
		// print_r($item);
		print_r(editOrderItem($table,$item,$orderNo));
	break;
} //---end switch case

?>