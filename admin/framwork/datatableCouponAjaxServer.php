<?php  
include("../../model/coupon.php");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}

// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";



date_default_timezone_set("Asia/Taipei");
$sql = "select * from coupon order by coupon_no ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	$coupon =[];
	if($result){
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$coupon[$key]["coupon_no"] = $value["coupon_no"];
			$coupon[$key]["coupon_name"] = $value["coupon_name"];
			$coupon[$key]["coupon_code"] = $value["coupon_code"];
			$coupon[$key]["coupon_discount"] = $value["coupon_discount"];
			// $coupon[$key]["coupon_used"] = $value["coupon_used"];
			$searchCheck =array($value["coupon_no"],$value["coupon_name"],$value["coupon_code"],$value["coupon_discount"]);
			if($value["coupon_used"] != null){
				$tagSql = "select * from tag where tag_no = ".$value["coupon_used"];
				$tagResult = $db->DB_Query($tagSql);
				$coupon[$key]["coupon_used"] = $tagResult[0]["tag_name"];
				array_push($searchCheck,$tagResult[0]["tag_name"]);

			}else{
				$coupon[$key]["coupon_used"] = null;
			}

			$coupon[$key]["coupon_status"] = $value["coupon_status"];
			$coupon[$key]["coupon_start"] = $value["coupon_start"];
			$coupon[$key]["coupon_end"] = $value["coupon_end"];
			$coupon[$key]["coupon_times"] = $value["coupon_times"];
			$coupon[$key]["coupon_target"] = $value["coupon_target"];
			
			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($coupon[$key]);
				}else{  //配對上相同字串
					$searchCount++;
				}
			}
			
		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}

		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($coupon,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r($array);
		// echo "</pre>";
		
	}else{
		$array["data"]=array_slice($coupon,$start,$length);
		$jsonStr = json_encode($array);
		echo $jsonStr;
	}



?>