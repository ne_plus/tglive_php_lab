<?php  
include("../../model/article.php");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}

// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";



date_default_timezone_set("Asia/Taipei");
$sql = "select * from article order by article_no ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	$article =[];
	if($result){
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$article[$key]["article_no"] = $value["article_no"];
			$article[$key]["article_title"] = $value["article_title"];
			$article[$key]["article_content"] = $value["article_content"];
			$article[$key]["article_describe"] = $value["article_describe"];
			$article[$key]["article_owner"] = $value["article_owner"];
			$article[$key]["article_status"] = $value["article_status"];
			$article[$key]["article_img"] = $value["article_img"];
			$article[$key]["article_createtime"] = $value["article_createtime"];
			$searchCheck =array($value["article_no"],$value["article_title"],$value["article_content"],$value["article_describe"],$value["article_owner"]);

			$articleRelatesql = "select * from tag_article_relate a join tag b on a.tag_no = b.tag_no  where a.article_no = ".$value["article_no"];
			$articleRelateResult = $db -> DB_query($articleRelatesql);
			foreach ($articleRelateResult as $tagkey => $tagvalue) {
				$article[$key]["article_tag"][$tagkey] = $tagvalue;
			}

			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($article[$key]);
				}else{  //配對上相同字串
					$searchCount++;
				}
			}
			
		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}

		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($article,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r($array);
		// echo "</pre>";
		
	}else{
		$array["data"]=array_slice($article,$start,$length);
		$jsonStr = json_encode($array);
		echo $jsonStr;
	}



?>