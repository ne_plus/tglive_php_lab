<?php  
include("../../model/DB.php");
include("../../class/productClass.php");
date_default_timezone_set("Asia/Taipei");


$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;


$date = isset($_REQUEST['date'] ) ? $_REQUEST['date'] : date("Y-m") ;
//資料庫 
$db = new DB();
$sql = "SELECT * from product_views where date_group = '$date' order by product_views.times desc";
$result = $db->DB_Query($sql);

//次數加總
$sumSql = "SELECT sum(times) from product_views where date_group = '$date' ";
$sumResult = $db->DB_Query($sumSql);
$data = [];

//商品名稱
if( $result ){

	foreach ($result as $key => $value) {
		//商品資訊
		$product = new productObj($value['product_no']);
		$productInfo = $product->brief();

		//商品品牌
		$brandSql = "SELECT * from category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$productInfo["product_no"]."' and b.cate_parents <> 63 and b.cate_parents <> 61";
		$brandResult = $db->DB_Query($brandSql);
		
		//儲存data
		$data[$key]["product"] = $productInfo["product_name"];
		$data[$key]["brand"] = $brandResult[0]["cate_name"];
		$data[$key]["month"] = $value["date_group"];
		$data[$key]["times"] = $value["times"];
		$data[$key]["percentage"] = $value["times"]/$sumResult[0]["sum(times)"]; 
	}



	$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>count($result));
	$array["data"]=array_slice($data,$start,$length);

	$jsonStr = json_encode($array);
	echo $jsonStr;
	
}else{
	$array["data"]=array_slice($data,$start,$length);
	$jsonStr = json_encode($array);
	echo $jsonStr;
}
?>