<?php  
include("../../model/DB.php");
date_default_timezone_set("Asia/Taipei");


$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;


//資料庫 
$db = new DB();
$sql = "SELECT * from banner order by banner_no asc";//  limit 0,5
$result = $db->DB_Query($sql);
$data =[];
if($result){
	//商品名稱
	foreach ($result as $key => $value) {
		
		//儲存data
		$data[$key]["banner_no"] = $value["banner_no"];
		$data[$key]["img"] = $value["img"];
		$data[$key]["link"] = $value["link"];
		$data[$key]["blank"] = $value["blank"];
		$data[$key]["banner_status"] = $value["banner_status"];
		$data[$key]["banner_update"] = $value["banner_update"]; 
	}



	$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>count($result));
	$array["data"]=array_slice($data,$start,$length);

	$jsonStr = json_encode($array);
	echo $jsonStr;
}else{
	$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>count($result));
	$array["data"]=array_slice($data,$start,$length);
	$jsonStr = json_encode($array);
	echo $jsonStr;
}
?>