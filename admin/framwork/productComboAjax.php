<?php  
	ob_start();
	session_start();
	date_default_timezone_set("Asia/Taipei");
	include("../../model/DB.php");
	$db = new DB();

	switch ($_REQUEST["status"]) {
		case "getComboSpec":
			$product_no = isset($_POST["product_no"]) ? $_POST["product_no"] : 0;
			$sql = sprintf("SELECT product_spec_no, product_spec_info, product_stock FROM product_spec WHERE product_no = %d", $product_no);
			echo json_encode($db->DB_Query($sql));
			break;
		case "saveDeleteCombo":
			$id = isset($_POST["id"]) ? $_POST["id"] : 0;
			$rel_product_no = isset($_POST["rel_product_no"]) ? $_POST["rel_product_no"] : 0;
			$data = array(
				"id" => $id,
				"rel_product_no" => $rel_product_no,
				"isdeleted" => 1,
				"updatetime" => date("Y-m-d H:i:s")
			);
			$chkdata = array("id");
			$db->DB_UpdateOnly("product_combo", $data, $chkdata);
			$returnArray = array();
			$returnArray["status"] = 1;
			$returnArray["message"] = "";
			echo json_encode($returnArray);
			break;
		case "saveNewCombo":
			$rel_product_no = isset($_POST["rel_product_no"]) ? $_POST["rel_product_no"] : 0;
			$product_no = isset($_POST["product_no"]) ? $_POST["product_no"] : 0;
			$product_spec_no = isset($_POST["product_spec_no"]) ? $_POST["product_spec_no"] : "";
			$product_spec_price = isset($_POST["product_spec_price"]) ? $_POST["product_spec_price"] : 0;
			$starttime = isset($_POST["starttime"]) ? $_POST["starttime"] : "";
			$endtime = isset($_POST["endtime"]) ? $_POST["endtime"] : "";
			$data = array(
				"rel_product_no" => $rel_product_no,
				"product_no" => $product_no,
				"product_spec_no" => $product_spec_no,
				"product_spec_price" => $product_spec_price,
				"starttime" => $starttime,
				"endtime" => $endtime,
				"createtime" => date("Y-m-d H:i:s"),
				"updatetime" => date("Y-m-d H:i:s")
			);
			$db->DB_Insert("product_combo", $data);
			
			$returnArray = array();
			$returnArray["status"] = 1;
			$returnArray["message"] = $db->lastId;
			echo json_encode($returnArray);
			break;
		case "saveUpdateCombo":
			$id = isset($_POST["id"]) ? $_POST["id"] : 0;
			$rel_product_no = isset($_POST["rel_product_no"]) ? $_POST["rel_product_no"] : 0;
			$product_no = isset($_POST["product_no"]) ? $_POST["product_no"] : 0;
			$product_spec_no = isset($_POST["product_spec_no"]) ? $_POST["product_spec_no"] : "";
			$product_spec_price = isset($_POST["product_spec_price"]) ? $_POST["product_spec_price"] : 0;
			$starttime = isset($_POST["starttime"]) ? $_POST["starttime"] : "";
			$starttime = str_replace("T", " ", $starttime);
			$endtime = isset($_POST["endtime"]) ? $_POST["endtime"] : "";
			$endtime = str_replace("T", " ", $endtime);
			$data = array(
				"id" => $id,
				"rel_product_no" => $rel_product_no,
				"product_no" => $product_no,
				"product_spec_no" => $product_spec_no,
				"product_spec_price" => $product_spec_price,
				"starttime" => $starttime,
				"endtime" => $endtime,
				"updatetime" => date("Y-m-d H:i:s")
			);
			$chkdata = array("id");
			$db->DB_UpdateOnly("product_combo", $data, $chkdata);
			$returnArray = array();
			$returnArray["status"] = 1;
			$returnArray["message"] = "";
			echo json_encode($returnArray);
			break;
	}
?>