<?php  
include("../../model/DB.php");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}

// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";



date_default_timezone_set("Asia/Taipei");
$sql = "select * from member order by mem_createtime ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	$mem =[];
	if($result){
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$mem[$key]["mem_no"] = $value["mem_no"];
			$mem[$key]["mem_mail"] = $value["mem_mail"];
			$mem[$key]["mem_firstname"] = $value["mem_firstname"];
			$mem[$key]["mem_lastname"] = $value["mem_lastname"];
			$mem[$key]["mem_tel"] = $value["mem_tel"];
			$mem[$key]["mem_status"] = $value["mem_status"];
			$mem[$key]["mem_createtime"] = $value["mem_createtime"];
			$mem[$key]["mem_lastLoginTime"] = $value["mem_lastLoginTime"];
			$searchCheck =array($value["mem_no"],$value["mem_mail"],$value["mem_firstname"],$value["mem_lastname"],$value["mem_createtime"],$value["mem_lastLoginTime"]);

			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($mem[$key]);
				}else{  //配對上相同字串
					$searchCount++;
				}
			}
			
		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}

		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($mem,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r($array);
		// echo "</pre>";
		
	}else{
		$array["data"]=array_slice($mem,$start,$length);
		$jsonStr = json_encode($array);
		echo $jsonStr;
	}



?>