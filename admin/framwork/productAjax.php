<?php  
ob_start();
session_start();

include("../../controller/productControl.php");

if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
// ====ip get======
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();



switch ($_REQUEST["status"]) {
// ======create=======
	case "新增" :
			if($_REQUEST["table2"] == "product"){  //產品新增
				$table=$_REQUEST["table2"];
				$item = array(
					"model_id" =>$_REQUEST["model_id"],
					"product_name"=>$_REQUEST["product_name"],
					"product_createtime"=>time()
					);
				echo proCreate($table,$item);
			}
			
			break;
	case "addCate" :	
			$table1 = $_REQUEST["table1"]; // product 
			$table2 = $_REQUEST["table2"]; 
			$productNo = $_REQUEST["product_no"];
			$cateNo =  $_REQUEST["cate_no"];
			$item = array(
				"cate_no" =>$cateNo,
				"product_no"=>$productNo
					);
			$result = proUpdateCate($table1,$table2,$item,$productNo,$cateNo);
			$jsonstr = json_encode($result);
			echo $jsonstr;
			break;	
// ======update=======
	case "修改" : 
			if(isset($_REQUEST["table2"]) && $_REQUEST["table2"] == "tag_products_relate"){ //標籤新增修改
				$table1 = $_REQUEST["table1"]; // product 
				$table2 = $_REQUEST["table2"]; 
				$productNo = $_REQUEST["product_no"];
				$tagNo =  $_REQUEST["tag_no"];
				$item = array(
					"tag_no" =>$tagNo,
					"product_no"=>$productNo
					);
				// echo proUpdateTag($table1,$table2,$item,$productNo,$tagNo);
				$jsonstr = json_encode(proUpdateTag($table1,$table2,$item,$productNo,$tagNo));
				echo $jsonstr;
				break;
			}elseif(isset($_REQUEST["table2"]) && $_REQUEST["table2"] == "category_products_relate"){ //分類新增修改
				$table1 = $_REQUEST["table1"]; // product 
				$table2 = $_REQUEST["table2"]; 
				$productNo = $_REQUEST["product_no"];
				$cateNo =  $_REQUEST["cate_no"];
				$item = array(
					"cate_no" =>$cateNo,
					"product_no"=>$productNo
					);
				// echo proUpdateCate($table1,$table2,$item,$productNo,$cateNo);
				$result = proUpdateCate($table1,$table2,$item,$productNo,$cateNo);
				$jsonstr = json_encode($result);
				// echo is_array($jsonstr);
				// echo $result;
				if(is_array($result) && $result[0]["cate_level"] == "0" ){ //只有父層
					$cate = array(
						"cate_product_no" => $result[0]["cate_product_no"],
						"cate"=> $result[0]["cate_name"]
						);
					echo json_encode($cate);
				}elseif(is_array($result) && $result[0]["cate_level"] == "1"){ //有父層
					$sql = "select * from category where cate_no = '".$result[0]["cate_parents"]."'" ;
					$CateFatherGet = proCateFatherQuery($sql);
					$cate = array(
						"cate_product_no" => $result[0]["cate_product_no"],
						"cate"=> $CateFatherGet[0]["cate_name"].">".$result[0]["cate_name"]
						);
					echo json_encode($cate);
				}else{ //有重複分類
					echo $jsonstr;
				}
				// echo $result[0]["cate_level"];
				break;
			}else{ //上下架修改單一欄位修改
				$table = $_REQUEST["table"];
				$productNo = $_REQUEST["product_no"];
				$productStatus = $_REQUEST["product_status"];	
				$item = array(
					"product_no" => $productNo,
					"product_status"=> $productStatus
				);
				if(proUpdate($table,$item,$productNo)){ //更新成功
					$jsonstr = json_encode(productQuery($table,$productNo)) ;
					echo $jsonstr;
				}else{ //找不到產品

				}
				break;		
			}
	case "update360" : 	
			$table = $_REQUEST["table"];
			$productNo = $_REQUEST["product_no"];
			$product360Status = $_REQUEST["product_360_status"];	
			$item = array(
				"product_no" => $productNo,
				"product_360_status"=> $product360Status
			);
			if(proUpdate($table,$item,$productNo)){ //更新成功
				$jsonstr = json_encode(productQuery($table,$productNo)) ;
				echo $jsonstr;
			}else{ //找不到產品

			}
			break;
// ======delete=====
	case "刪除": 
			$table1 = $_REQUEST["table1"]; // product 
			$table2 = $_REQUEST["table2"]; //product_cate or product_tag
			$productNo = $_REQUEST["product_no"]; 
			if($table2 == "category_products_relate"){ //分類刪除	
				$CateProductNo = $_REQUEST["cate_product_no"];
				
				echo deleteCate($table1,$table2,$productNo,$CateProductNo);
			}elseif($table2 == "tag_products_relate"){ //標籤刪除
				$tagProductNo = $_REQUEST["tag_product_no"];

				echo deleteTag($table1,$table2,$productNo,$tagProductNo);
			} 
			

			break ;
	
	case "產品編輯更新":
		$table1 = $_REQUEST["table1"];
		$table2 = $_REQUEST["table2"];
		$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$item = array(
					"product_no" => $productNo,
					"model_id" => $_REQUEST["model_id"],
					"product_name" => $_REQUEST["product_name"],
					"product_subtitle" => $_REQUEST["product_subtitle"],
					"product_promote" => $_REQUEST["product_promote"],
					"product_describe" => $_REQUEST["product_describe"],
					"product_content" => $_REQUEST["product_content"],
					"product_note" => $_REQUEST["product_note"],
					"product_status" => $_REQUEST["product_status"],
					"product_price_sort" => $_REQUEST["product_price_sort"],
				);
		if(proUpdate($table1,$item,$productNo)){//onlyUpdate
			$item2 = array(
				"product_spec_no" => $productSpecNo,
				"product_no" => $productNo,
				"product_spec_status" => $_REQUEST["product_spec_status"]
				);
			echo proEditSpecOnly($table1,$table2,$item2,$productNo,$productSpecNo); //true
			
		}else{ //找不到產品

		};
		break;

	case "規格重設":
		$table1 = $_REQUEST["table1"];
		$table2 = $_REQUEST["table2"];
		$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$item = array(
			"product_spec_no" => $productSpecNo,
			"product_no" => $productNo,
			"product_spec_status" => $_REQUEST["product_spec_status"]
			);
		echo proEditSpecOnly($table1,$table2,$item,$productNo,$productSpecNo); //true
	
		break;	
			

	case "規格新增":
		$table1 = $_REQUEST["table1"];
		$table2 = $_REQUEST["table2"];
		$productNo = $_REQUEST["product_no"];
		$item = array(
					"product_no" => $productNo,
					"product_spec_info" => $_REQUEST["product_spec_info"],
					"product_spec_describe" => $_REQUEST["product_spec_describe"],
					"product_stock" => $_REQUEST["product_stock"],
					"product_spec_price1" => $_REQUEST["product_spec_price1"],
					"product_spec_price2" => $_REQUEST["product_spec_price2"],
					"product_spec_price3" => $_REQUEST["product_spec_price3"]
				);
		
		$result = proCreateSpec($table1,$table2,$item,$productNo);
		echo json_encode($result);
		break;

	case "規格更新":
		$table1 = $_REQUEST["table1"];
		$table2 = $_REQUEST["table2"];
		$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$item = array(
					"product_spec_no" => $productSpecNo,
					"product_no" => $productNo,
					"product_spec_info" => $_REQUEST["product_spec_info"],
					"product_spec_describe" => $_REQUEST["product_spec_describe"],
					"product_stock" => $_REQUEST["product_stock"],
					"product_spec_price1" => $_REQUEST["product_spec_price1"],
					"product_spec_price2" => $_REQUEST["product_spec_price2"],
					"product_spec_price3" => $_REQUEST["product_spec_price3"]
				);
		
		$result = proEditSpec($table1,$table2,$item,$productNo,$productSpecNo);
		echo json_encode($result);
		break;

	case "規格刪除":
		$table1 = $_REQUEST["table1"];
		$table2 = $_REQUEST["table2"];
		$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		
		
		echo deleteSpec($table1,$table2,$productNo,$productSpecNo);
		
		break;					
} //---end switch case

?>