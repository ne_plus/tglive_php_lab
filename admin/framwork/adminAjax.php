<?php  
ob_start();
session_start();
date_default_timezone_set("Asia/Taipei");
include("../../controller/userControl.php");


// ====ip get======
if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();



switch ($_REQUEST["status"]) {
// ======create=======
	case "新增" :
			$table = $_REQUEST["table"];
			$email = $_REQUEST["adm_email"];
			if($_REQUEST["adm_status"] == "true"){ //true
				$adm_status = "1" ;
			}else{  //false
				$adm_status = "0" ;
			}
			$item = array(
				"adm_authLevel"=> $_REQUEST["adm_authLevel"],
				"adm_email"=> $email,
				"adm_password"=> $_REQUEST["adm_password"],
				"adm_name"=> $_REQUEST["adm_name"],
				"adm_status"=> $adm_status,
				"adm_createtime"=> time(),
				"adm_updatetime"=> null
				);

			// print_r($item) ;

			echo create($table,$email,$item);


			break;
// ======update=======
	case "修改" : 
			$table = $_REQUEST["table"];
			if($table == "admin"){
				$mail = $_REQUEST["adm_email"];
				if($_REQUEST["adm_status"] =="true"){
					$adm_status ="1";
				}else{
					$adm_status ="0";
				}

				$item = array(
				"adm_authLevel"=> $_REQUEST["adm_authLevel"],
				"adm_email"=> $mail,
				"adm_name"=> $_REQUEST["adm_name"],
				"adm_status"=> $adm_status,
				"adm_createtime"=> time(),
				"adm_updatetime"=> null
				);
				echo edit($table,$mail,$item);



			}elseif($table == "member"){
				$mail = $_REQUEST["mem_mail"];
				if($_REQUEST["mem_ststus"] == "true"){ //true
					$mem_ststus = "1" ;
				}else{ //false
					$mem_ststus = "0" ;
				}
				$item = array(
					"mem_mail"=> $mail,
					"mem_firstname"=> $_REQUEST["mem_firstname"],
					"mem_lastname"=> $_REQUEST["mem_lastname"],
					"mem_tel"=> $_REQUEST["mem_tel"],
					"mem_note"=> $_REQUEST["mem_note"],
					"mem_status"=> $mem_ststus,
					"mem_address"=> $_REQUEST["mem_address"],
					"mem_updatetime"=> null
				);

				echo edit($table,$mail,$item);
			}		
			
			break;
//發信(出貨通知)
	case "sendDeliverMail" :

			$orderName = $_REQUEST["order_name"];
			$orderMail = $_REQUEST["order_mail"];
			$mailcontent = json_decode($_REQUEST["mail_content"],true);
			
			//發送email
			$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$orderName.'</p><p>感謝您的訂購，我們已收到您的款項並按訂單編號順序為您安排出貨。您訂購的商品將在2-5個工作天內為您寄達，謝謝您的耐心等候！</p><p>您本次的訂購相關訊息如下：</p><table border="1" cellspacing="0" style="width:100%; font-size: 12px; text-align: center;"><tr><th style="background-color: #eee;padding: 20px 0;width: 25%;">訂單編號</th><th style="background-color: #eee;width: 25%;">下單時間</th><th style="background-color: #eee;width: 25%;">商品名稱</th><th style="background-color: #eee;width: 25%;">數量</th></tr>';
			foreach ($mailcontent as $key => $value) {
				$content .= '<tr style="color:#888;"><td style="padding:20px 0;">'.$value["order_group"].'</td><td>'.$value["order_time"].'</td><td>'.$value["product_name"].'</td><td><div><span></span><span>'.$value["product_quantity"].'</span></div></td></tr>';
			}

			// ===tgi 內容====
			$content .='</table><p>如需查詢訂單處理狀態，請至<a href="https://www.tgilive.com/" target="_blank" style="color:#00f;">TGiLive官網</a>登入後前往會員專區>訂單記錄，即可看到您的訂單明細內容。</p><p><strong>※若有任何訂單上處理的問題，歡迎您利用以下方式與我們聯繫：</strong></p><p>TGiLive客服信箱： <span><a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a></span></p><p>TGiLive居生活粉絲專頁： <span><a href="https://www.facebook.com/TGiLiveTW/" target="_blank" style="color:#00f;">https://www.facebook.com/TGiLiveTW/</a></span></p><p>我們的客服回覆時間： <span>週一至週五10:00am-18:00pm</span></p><p style="margin-top: 50px;">TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';

			$title ="TGiLive居生活商品出貨通知！";
			$receiverArray = array($orderMail,"info@tgilive.com","info@tgilive.co.uk");
			
			echo json_encode(sendMail($title,$content,$receiverArray,$orderName));
			break; 		

} //---end switch case

?>