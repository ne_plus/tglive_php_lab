<?php  
include("../../model/DB.php");
date_default_timezone_set("Asia/Taipei");

//輸入查詢區間 (data)
$now = date("Y-m-d");
$startStr = isset($_REQUEST['startTime'] ) ? $_REQUEST['startTime']." 23:59:59" : date("Y-m-d",strtotime($now.'-1 day'))." 23:59:59";
$endStr = isset($_REQUEST['endTime'] ) ? $_REQUEST['endTime']." 00:00:00" : date("Y-m-d",strtotime($now.'-7 day'))." 00:00:00";
$start = strtotime($startStr);
$end = strtotime($endStr);


//資料庫 
$db = new DB();
$sql = "SELECT * from order_item where order_createtime between $end  and $start order by order_createtime desc";
$result = $db->DB_Query($sql);

$data = [] ;
$countData = date("Y-m-d",$start);
$endData = date("Y-m-d",$end);

//計算總共比數
$i = 0 ;
do{
	$data["amChart"][$i]["date"] = $countData;
	$countData = date("Y-m-d",strtotime($countData.'-1 day'));
	$i ++ ;
}while(strtotime($countData) >= $end);

for ($d=0; $d < count($data["amChart"]); $d++) { 
	$data["amChart"][$d]["price"] = 0 ;
	foreach ($result as $key => $value) {
		if( $data["amChart"][$d]["date"] == date("Y-m-d",$value["order_createtime"]) ){
			$data["amChart"][$d]["price"] += intval($value["order_price"]+$value["order_cargo"]);


			// ====export data====
			$orderGroup = $value["order_group"];
			$orderNo = $value["order_no"];
			$dataSql = "SELECT * from order_detail a join product b on a.product_no = b.product_no where a.order_no = ".$orderNo;
			$dataResult = $db->DB_Query($dataSql);
			foreach ($dataResult as $datakey => $datavalue) {
				if( $datakey == 0 ){
					$data["export"][$orderGroup][$orderNo][$datakey]["cargo"] = $value["order_cargo"];
				}else{
					$data["export"][$orderGroup][$orderNo][$datakey]["cargo"] = 0;
				}
				if( $datavalue["order_detail_price_discount"] != null ){
					$discountPrice = intval($datavalue["order_detail_price_discount"]*$datavalue["order_detail_quantity"]);
				}else{
					$discountPrice = 0;
				}

				$data["export"][$orderGroup][$orderNo][$datakey]["date"] = $data["amChart"][$d]["date"];
				$data["export"][$orderGroup][$orderNo][$datakey]["order_group"] = $orderGroup;
				$data["export"][$orderGroup][$orderNo][$datakey]["order_no"] = $orderNo;
				$data["export"][$orderGroup][$orderNo][$datakey]["product_name"] = $datavalue["product_name"];
				$data["export"][$orderGroup][$orderNo][$datakey]["model_id"] = $datavalue["model_id"];
				$data["export"][$orderGroup][$orderNo][$datakey]["product_brand"] = $datavalue["cate_name"];
				$data["export"][$orderGroup][$orderNo][$datakey]["product_quantity"] = $datavalue["order_detail_quantity"];
				$data["export"][$orderGroup][$orderNo][$datakey]["coupon"] = $datavalue["coupon_name"];
				$data["export"][$orderGroup][$orderNo][$datakey]["order_detail_price_discount"] = $discountPrice;
				$data["export"][$orderGroup][$orderNo][$datakey]["product_price"] = $datavalue["order_detail_price"];
				$data["export"][$orderGroup][$orderNo][$datakey]["total_price"] = intval($datavalue["order_detail_quantity"]*$datavalue["order_detail_price"]);
				$data["export"][$orderGroup][$orderNo][$datakey]["order_status"] = $value["order_status"];
				
			}	
		}
	}
}

echo json_encode($data);

?>