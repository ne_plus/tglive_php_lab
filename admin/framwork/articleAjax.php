<?php  
ob_start();
session_start();

include("../../controller/articleControl.php");

if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
// ====ip get======
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();



switch ($_REQUEST["status"]) {
// ======新文章儲存======
	case "create":
		$table = "article";
		$articleTitle = $_REQUEST["article_title"];
		$articleContent = $_REQUEST["article_content"];
		$articleDescribe = $_REQUEST["article_describe"];
		$articleImg = $_REQUEST["article_img"];
		$articleOwner = $_REQUEST["article_owner"];
		$articleStatus = $_REQUEST["article_status"];
		$item = array(
			"article_title"=> $articleTitle,
			"article_content"=> $articleContent,
			"article_describe"=> $articleDescribe,
			"article_img"=> $articleImg,
			"article_owner"=> $articleOwner,
			"article_status"=> $articleStatus,
			"article_createtime"=> time(),
			);
		echo addArticle($table,$item);
	 	break;	
// ======文章更新======
	case "update":
		$table = "article";
		$articleNo = $_REQUEST["article_no"] ;
		$articleTitle = $_REQUEST["article_title"];
		$articleContent = $_REQUEST["article_content"];
		$articleDescribe = $_REQUEST["article_describe"];
		$articleImg = $_REQUEST["article_img"];
		$articleOwner = $_REQUEST["article_owner"];
		$articleStatus = $_REQUEST["article_status"];
		$item = array(
			"article_no" => $articleNo,
			"article_title"=> $articleTitle,
			"article_content"=> $articleContent,
			"article_describe"=> $articleDescribe,
			"article_img"=> $articleImg,
			"article_owner"=> $articleOwner,
			"article_status"=> $articleStatus
			);
		echo editArticle($table,$articleNo,$item);
	 	break;	
// ======建立文章分類=====
	case "updateTagRelate":
		$table = "tag_article_relate";
		$articleNo = $_REQUEST["article_no"] ;
		$tagNo = $_REQUEST["article_tag"];
		$tagUsed = isset($_REQUEST["tag_used"])? $_REQUEST["tag_used"] : null;
		$tagSave = [];
		$tagDelet = [];

		//要儲存的tag
		if( count($tagUsed) == 0 ){ //之前沒有綁定過tag
			foreach ($tagNo as $key => $value) {
				$tagSave[$key] = $value;
			}	
		}else{ //之前有綁定過tag
			//需要多存的tag
			foreach ($tagNo as $key => $value) {
				//比對是否有相同tag ID 除存多出來的 
				if(!in_array($value, $tagUsed)){
					$tagSave[$key] = $value;
				}
			}
			//需要移除的tag
			foreach ($tagUsed as $key => $value) {
				if(!in_array($value, $tagNo)){
					$tagDelet[$key] = $value;
				}
		 	}	
		}
		
		//存ＤＢ
		if( count($tagSave) !=0 ){ //儲存
			foreach ($tagSave as $key => $value) {
				$saveItem = array(
					"article_no" => $articleNo,
					"tag_no"=> $value
				);
				articleRelateCreate($table,$saveItem);
			}
			$saveResult="yes";
		}else{
			$saveResult = "no";
		}
		if( count($tagDelet) !=0 ){ //刪除
			foreach ($tagDelet as $key => $value) {
				$deleteItem = array(
					"article_no" => $articleNo,
					"tag_no"=> $value
				);
				articleRelateDelete($table,$deleteItem);
			}
			$deleteResult="yes";
		}else{
			$deleteResult = "no";
		}
	
		
		$result["save"]=$saveResult;
		$result["delete"]=$deleteResult;
		print_r($result);

	 	break;	 		 	
// ======delete=====
	case "delete": 
		$table = "article";
		$articleNo = $_REQUEST["article_no"];
		echo deleteArticle($table,$articleNo);
		break;			
} //---end switch case

?>