<?php  
ob_start();
session_start();

include("../../controller/tagControl.php");

if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
// ====ip get======
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();



switch ($_REQUEST["status"]) {
// ======標籤加入群組分類======
	case "addTagInCate":
		$table = $_REQUEST["table"];
		$cateNo = $_REQUEST["cateNo"];
		$tagNo = $_REQUEST["tagNo"];
		$item = array(
			"cate_no" => $cateNo,
			"tag_no" => $tagNo
			);
		echo addTagInCate($table,$item);
	 	break;	
// ======標籤移除群組分類======
	case "removeTagInCate":
		$table = $_REQUEST["table"];
		$cateTagNo = $_REQUEST["cateTagNo"];
		
		echo removeTagInCate($table,$cateTagNo);
	 	break;		 	
// ======search======
	case "searchOutCate" :
			$table = $_REQUEST["table"];
			$cateNo = $_REQUEST["cateNo"];
			
			echo json_encode(cateTagRelateQuery($cateNo));
		break;
	case "searchIncate"	:
			$table = $_REQUEST["table"];
			$cateNo = $_REQUEST["cateNo"];

			echo json_encode(cateTagRelateHaveQuery($cateNo));
		break;
// ======create=======
	case "新增" :
			$table = $_REQUEST["table"];
			$tagName = $_REQUEST["tag_name"];			
			$item = array(
			"tag_name"=> $tagName,
			"tag_sort"=> "1"
			);

			// print_r($item) ;

			echo addTag($table,$tagName,$item);


			break;
// ======product 新增並加入tag=====
	case "新增加入" :
			$table = $_REQUEST["table"];
			$tagName = $_REQUEST["tag_name"];			
			$item = array(
			"tag_name"=> $tagName,
			"tag_sort"=> "1"
			);

			echo addTagProduct($table,$tagName,$item);
			
			
			break;	

// ======update=======
	case "修改" : 
			$table = $_REQUEST["table"];
			$tagNo = $_REQUEST["tag_no"];
			$tagName = $_REQUEST["tag_name"];
			$item = array(
				"tag_no"=>$tagNo,
				"tag_name"=> $tagName,
				"tag_sort"=> "1"
			);
			echo editTag($table,$tagNo,$item,$tagName);
			break;
// ======delete=====
	case "刪除": 
			$table = $_REQUEST["table"];
			$tagNo = $_REQUEST["tag_no"];
			echo deleteTag($table,$tagNo);
			break;			
} //---end switch case

?>