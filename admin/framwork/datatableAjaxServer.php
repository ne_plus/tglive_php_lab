<?php  
include("../../model/product.php");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}


// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";

// ====分類收尋=====
$columns3Search = isset($_REQUEST['columns']['3']['search']['value'] ) ?  $_REQUEST['columns']['3']['search']['value'] : null;

// =====商品狀態搜尋=====
$columns6Search = isset($_REQUEST['columns']['6']['search']['value'] ) ?  $_REQUEST['columns']['6']['search']['value'] : null;


date_default_timezone_set("Asia/Taipei");
$sql = "select * from product order by product_no ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	if($result){
		$products = [];
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$products[$key]["product_no"] = $value["product_no"];
			$products[$key]["model_id"] = $value["model_id"];
			$products[$key]["product_name"] = $value["product_name"];
			$products[$key]["product_status"] = $value["product_status"];
			$products[$key]["product_360_status"] = $value["product_360_status"];
			$products[$key]["restricted_status"] = $value["restricted_status"];
			$products[$key]["product_updatetime"] = $value["product_updatetime"];
			$searchCheck = array($value["product_no"],$value["model_id"],$value["product_name"]);
			$sql = "select * from product where product_no=:product_no" ;
			$dic=array(":product_no"=>$value["product_no"]);
			$product = new Product($sql,$dic); //product DB initial
			// -------------標籤搜尋
			$tagRelate = $product->productRelateTag();
			if($tagRelate){ //有標籤存在
				foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
					$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
					$products[$key]["tag_product_no"][$keytagRelate] = $valuetagRelate["tag_product_no"];
					$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
					array_push($searchCheck,$valuetagRelate["tag_name"]);

					// ----收尋標籤分類----
					$cateGroupTagSql = "SELECT * FROM category a join category_tag_relate b on a.cate_no=b.cate_no join tag c on b.tag_no = c.tag_no where a.cate_parents = '61' and c.tag_no= ".$valuetagRelate["tag_no"];
					$cateGroupTagResult = $db->DB_Query($cateGroupTagSql);
					if($cateGroupTagResult){ //標籤有分類
						$products[$key]["cate_tag_no"][$keytagRelate] = $cateGroupTagResult[0]['cate_tag_no'];
						$products[$key]["cate_tag_groupName"][$keytagRelate] = $cateGroupTagResult[0]['cate_name'];
					}else{ //沒有標籤分類
						$products[$key]["cate_tag_groupName"][$keytagRelate] = false;
					}
					
				} 
			}else{ //沒有標籤
				// $products[$key]["tag_name"] = '無';//null ;
			}
			// -------------分類搜尋供應商/ 品牌
			$resultRelate = $product->productRelateCate();
			if($resultRelate){ //有商品分類
				foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
					if($valuecateRelate["cate_parents"]== 63){ //行銷活動
						$products[$key]["activity"][$keycateRelate]=$valuecateRelate["cate_name"];
						$products[$key]["activity_no"][$keycateRelate]=$valuecateRelate["cate_no"];
					}else{ //供應商/品牌
						$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
						$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
						$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
						$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
						if($valuecateRelate["cate_parents"] != 62 && $valuecateRelate["cate_parents"]!= 63){//有父層
								$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
								// $products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];  //有時會有亂碼造成json格式轉換異常,先行關閉
								$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
								$products[$key]["cate_product_no"][$keycateRelate] = $valuecateRelate["cate_product_no"];
								array_push($searchCheck,$resultCateParents[0]["cate_name"],$valuecateRelate["cate_name"]);
						}elseif($valuecateRelate["cate_parents"]!= 63){ //沒有父層
							$products[$key]["cate_father_name"] = '無';//null ;
							$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
							$products[$key]["cate_product_no"][$keycateRelate] = $valuecateRelate["cate_product_no"];
							array_push($searchCheck,$valuecateRelate["cate_name"]);
						}
					}
					
				}
			}else{ // 沒有商品分類
				// $products[$key]["cate"] = '無';//null;
			}
			// // -------------分類搜尋本月推薦
			// $resultMonth = $product->productRelateCateMonth();
			// if($resultMonth){ //有存在本月推薦分類 (單筆商品只會綁定一次本月推薦)
			// 	$products[$key]["month"] = "1";//$resultMonth[0]["cate_name"];
			// 	$products[$key]["monthRelateNo"] =$resultMonth[0]["cate_product_no"];
			// }else{ //沒有

			// }

			// -------------分類搜尋行銷活動
			$resultActivity = $product->productRelateCateActivity();
			if($resultActivity){ //有存在本月推薦分類
				foreach ($resultActivity as $activitykey => $activityvalue) {
					$products[$key]["activity"][$activitykey] = $activityvalue["cate_name"];
					$products[$key]["activityRelateNo"][$activitykey] = $activityvalue["cate_product_no"];
				}
			}else{ //沒有

			}



			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($products[$key]);
				}else{  //配對上相同字串
					$searchCount++;
				}
			}

			// ========分類搜尋 =======
			if(trim($columns3Search) != null ){
				if(isset($products[$key]["cate"])!=false){ //有商品分類
					if(strpos(strtolower(implode(",",array_values($products[$key]["cate"]))),strtolower(trim($columns3Search))) === false ){ //配對不上相同字串
						unset($products[$key]);
						// echo "沒配上";
					}else{  //配對上相同字串
						$searchCount++;
					}
				}else{ //沒有商品分類
					unset($products[$key]);
					// echo "沒有喔";
				}
				
			}


			// ========商品狀態搜尋 =======
			if(trim($columns6Search) != null ){	
				if(array_key_exists($key,$products)){ //判斷是否存在product 陣列
					if(strpos(strtolower($products[$key]["product_status"]),strtolower(trim($columns6Search))) === false ){ //配對不上相同字串
						unset($products[$key]);
						// echo "沒配上";
					}else{  //配對上相同字串
						$searchCount++;
					}
				}
				
			}

		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}

		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($products,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// // echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r($array);

		// echo "</pre>";
		
	}else{
		return "沒有";
	}



?>