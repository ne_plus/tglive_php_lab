<?php 
include("../../controller/productControl.php");

switch($_FILES["file"]["error"] ){
	case UPLOAD_ERR_OK : 

		if($_REQUEST["status"]=="product"){  //--------------------------------------------------產品
			// ------檔案上傳-----
			if( file_exists("../../image/product") == false){
				mkdir("../../image/product",0777); //make directory
			}

			$fileName = $_FILES["file"]["name"];
			// echo pathinfo($fileName, PATHINFO_EXTENSION), "<br>"; //抓副檔名

			$from = $_FILES["file"]["tmp_name"];
			$to = "../../image/product/".$_REQUEST["product_no"]."_".$_REQUEST["model_id"]."_".$_REQUEST["img_no"].".".pathinfo($fileName, PATHINFO_EXTENSION);
			copy( $from, $to);

			// ----儲存圖片路徑-----
			$table = "product";
			$productNo =$_REQUEST["product_no"];
			$item = array (
				"product_no" => $_REQUEST["product_no"], 
				$_REQUEST["img_no"] => "image/product/".$_REQUEST["product_no"]."_".$_REQUEST["model_id"]."_".$_REQUEST["img_no"].".".pathinfo($fileName, PATHINFO_EXTENSION),
				);
			if(proUpdate($table,$item,$productNo)){ //true
				echo "上傳成功<br>";
			}else{
				echo "上傳失敗";
			}
		}elseif($_REQUEST["status"]=="categoryLogo"){ //-----------------------------------------------分類
			// ------檔案上傳-----
			if( file_exists("../../image/category") == false){
				mkdir("../../image/category",0777); //make directory
			}
			$fileName = $_FILES["file"]["name"];
			$from = $_FILES["file"]["tmp_name"];
			$to = "../../image/category/".$_REQUEST["cate_no"]."_cateLogoImg.".pathinfo($fileName, PATHINFO_EXTENSION);
			if(copy( $from, $to)){
				//echo "上傳成功";
				echo $_REQUEST["cate_no"]."_cateLogoImg.".pathinfo($fileName, PATHINFO_EXTENSION);//回傳檔案路徑
			}else{
				echo "上傳失敗";
			}
		}elseif($_REQUEST["status"]=="categoryBanner"){ //
			// ------檔案上傳-----
			if( file_exists("../../image/category") == false){
				mkdir("../../image/category",0777); //make directory
			}
			$fileName = $_FILES["file"]["name"];
			$from = $_FILES["file"]["tmp_name"];
			$to = "../../image/category/".$_REQUEST["cate_no"]."_cateBannerImg.".pathinfo($fileName, PATHINFO_EXTENSION);
			if(copy( $from, $to)){
				//echo "上傳成功";
				echo $_REQUEST["cate_no"]."_cateBannerImg.".pathinfo($fileName, PATHINFO_EXTENSION);//回傳檔案路徑
			}else{
				echo "上傳失敗";
			}

		}else if( $_REQUEST["status"]=="styleImg" ){
			// ------檔案上傳-----
			if( file_exists("../../image/style") == false){
				mkdir("../../image/style",0777); //make directory
			}
			$fileName = $_FILES["file"]["name"];
			$from = $_FILES["file"]["tmp_name"];
			$to = "../../image/style/".$fileName;
			if(copy( $from, $to)){
				//echo "上傳成功";
				echo $fileName;//回傳檔案路徑
			}else{
				echo "上傳失敗";
			}
		}else if( $_REQUEST["status"]=="bannerImg" ){
			// ------檔案上傳-----
			if( file_exists("../../image/frontend/banner") == false){
				mkdir("../../image/frontend/banner",0777); //make directory
			}
			$fileName = $_FILES["file"]["name"];
			$from = $_FILES["file"]["tmp_name"];
			//命名拼字
			$originFile = $_REQUEST["imgOriginSrc"];
			$NewFileName = '';
			do {
				$NewFileName = $_REQUEST["bannerNoReStr"].rand(1,10).$fileName;
			} while ( strpos($originFile,$NewFileName) );

			$to = "../../image/frontend/banner/".$NewFileName;

			if(copy( $from, $to)){
				//echo "上傳成功";
				echo "image/frontend/banner/".$NewFileName;//回傳檔案路徑
			}else{
				echo "上傳失敗";
			}
		}
		
		
		break;
	case UPLOAD_ERR_INI_SIZE:
	    echo "上傳檔案太大,不能超過 ", ini_get("upload_max_filesize") , "<br>";	
	    break;
	case UPLOAD_ERR_FORM_SIZE:
	    echo "上傳檔案太大,不能超過 ", $_POST["MAX_FILE_SIZE"] , "<br>";			    
	    break;
    case UPLOAD_ERR_PARTIAL:
    	echo "上傳失敗<br>";
    	break;
    case UPLOAD_ERR_NO_FILE:
        echo "没有上傳檔案<br>";
        break;
    default:
        echo "上傳檔案失敗，錯誤代碼: ",$_FILES["error"],"請通知系統開發人員<br>";

}

// echo $_REQUEST["product_no"],"<br>";
// echo $_REQUEST["model_id"],"<br>";
// echo $_REQUEST["img_no"],"<br>";
// echo $_FILES["file"]["error"],"<br>";
// echo $_FILES["file"]["name"],"<br>";
// echo mb_convert_encoding($_FILES["file"]["name"],"big5","utf-8"),"<br>";
// echo $_FILES["file"]["tmp_name"],"<br>";
// echo $_FILES["file"]["size"],"<br>";
// echo $_FILES["file"]["type"],"<br>";

// $fileName = $_FILES["file"]["name"];
// echo pathinfo($fileName, PATHINFO_EXTENSION), "<br>"; //抓副檔名

 ?>    
