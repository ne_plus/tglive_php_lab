<?php  
include("../../model/order.php");
date_default_timezone_set("Asia/Taipei");
//webgolds 提供 PHP 陣列輸出 JSON格式參考範例
$draw = isset ( $_REQUEST['draw'] ) ? intval( $_REQUEST['draw'] ) : 0;
$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 10;
$search = isset($_REQUEST['search']['value'] ) ?  $_REQUEST['search']['value'] : null;
$searchCount = 0; //seach 計時器

if( $length == -1 ){//filter 全部
	$length = null;
}

// ===搜尋升降冪====
$dir = isset($_REQUEST['order'][0]["dir"]) ? $_REQUEST['order'][0]["dir"] : "desc";

// ====分類收尋=====
$columns2Search = isset($_REQUEST['columns']['2']['search']['value'] ) ?  $_REQUEST['columns']['2']['search']['value'] : null;

// ====下單區段(time)收尋=====
if( isset($_REQUEST['columns']['3']['search']['value'] ) ){
	if( $_REQUEST['columns']['3']['search']['value'] != null ){
		$columns3Search =  $_REQUEST['columns']['3']['search']['value']." 00:00:00" ; //開始時間
	}else{
		$columns3Search = null;
	}
}else{
	$columns3Search = null;
}
if( isset($_REQUEST['columns']['4']['search']['value'] ) ){
	if( $_REQUEST['columns']['4']['search']['value'] != null  ){
			$columns4Search =  $_REQUEST['columns']['4']['search']['value']." 23:59:59" ; //結束時間
	}else{
		$columns4Search =null;
	}
}else{
	$columns4Search =null;
}


$sql = "select * from order_item where order_status=3 or order_status=4 or order_status=5 order by order_no ".$dir;
	$db = new DB();
	$result = $db->DB_Query($sql);
	$order =[];
	if($result){
		$searchCheck = []; //for search 使用
		foreach ($result as $key => $value) {
			$multiCount = 0;//seach 計時器(Multi)
			$order[$key]["order_no"] = $value["order_no"];
			$order[$key]["mem_no"] = $value["mem_no"];
			$order[$key]["cate_no"] = $value["cate_no"];
			$order[$key]["cate_name"] = $value["cate_name"];
			$order[$key]["order_group"] = $value["order_group"];
			$order[$key]["order_email"] = $value["order_email"];
			$order[$key]["order_recipient"] = $value["order_recipient"];
			$order[$key]["order_address"] = $value["order_address"];
			$order[$key]["order_cargo"] = $value["order_cargo"];
			$order[$key]["order_price"] = $value["order_price"];
			$order[$key]["order_pay"] = $value["order_pay"];
			$order[$key]["order_pay_install"] = $value["order_pay_install"];
			$order[$key]["order_status"] = $value["order_status"];
			$order[$key]["order_createtime"] = $value["order_createtime"];
			// $order[$key]["order_memo"] = $value["order_memo"];
			// $order[$key]["admin_memo"] = $value["admin_memo"];

			
			$searchCheck =array($value["order_group"],$value["cate_name"],$value["order_recipient"]);
			
			
			// ========搜尋 search bar =======
			if(trim($search) != null ){
				if(strpos(strtolower(implode(",",array_values($searchCheck))),strtolower(trim($search))) === false){ //配對不上相同字串
					unset($order[$key]);
				}else{  //配對上相同字串
					if( $multiCount == 0 ){
						$searchCount++ ;
						$multiCount++ ;
					}
				}
			}

			// ========分類搜尋 =======
			if(trim($columns2Search) != null ){	
				//判斷資料前次搜尋是否有被刪除
				if( isset($order[$key]["order_status"]) ){ //存在
					//判斷第二次搜尋結果(multi)
					if(strpos(strtolower($order[$key]["order_status"]),strtolower(trim($columns2Search))) === false ){ //配對不上相同字串
						unset($order[$key]);
						// echo "沒配上";
					}else{  //配對上相同字串
						if( $multiCount == 0 ){
							$searchCount++;
							$multiCount++ ;
						}	
					}	
				}
			}

			// ========下單區段(time)收尋=======
			if(trim($columns3Search) != null && trim($columns4Search) != null){
				//判斷資料前次搜尋是否有被刪除	
				if( isset($order[$key]["order_createtime"]) ){	
					//判斷第三次搜尋結果(multi)		
					if( strtotime($columns3Search) <=  (int)$order[$key]["order_createtime"] && strtotime($columns4Search) >= (int)$order[$key]["order_createtime"] ){ //配對不上相同字串
						if( $multiCount == 0 ){
							$searchCount++;
							$multiCount++ ;
						}
					}else{  //未配對上相同字串
						unset($order[$key]);
					}	
				}	
			}

			
		}
		
		if($searchCount == 0){
			$recordsFiltered = count($result);
		}else{
			$recordsFiltered = $searchCount ;
		}

		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>$recordsFiltered,"search"=>$search);
		$array["data"]=array_slice($order,$start,$length);
		
		$jsonStr = json_encode($array);
		echo $jsonStr;

		// echo count($result);
		// echo $searchCount;
		// echo "<pre>";
		// print_r($array);
		// echo "</pre>";
		
	}else{
		$array = array("draw"=>$draw,"recordsTotal"=>count($result),"recordsFiltered"=>count($result));
		$array["data"]=array_slice($order,$start,$length);
		$jsonStr = json_encode($array);
		echo $jsonStr;
	}



?>