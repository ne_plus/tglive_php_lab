<?php require_once("module/header.php"); ?>
<?php require_once("../controller/orderControl.php"); 


if( isset($_REQUEST["order_group"]) ){ //有值
  $orderGroup = $_REQUEST["order_group"];
  $sql= "select * from order_item where order_group = :order_group";
  $data = array(":order_group"=>$orderGroup);
  $group  = new DB();
  $result = $group->DB_Query($sql,$data);
  // echo "<pre>";
  // print_r($result);
  // echo "</pre>";
  // echo $result[0]["order_recipient"];

  // -----查詢訂單商品明細-----
  // foreach ($result as $detailkey => $detailvalue) {
  //   echo "<pre>";
  //   print_r($detailvalue);
  //   echo "</pre>";
  //   $orderNo = $result[$detailkey]['order_no'];
    
  //   $detailsql = "select * from order_detail where order_no = :order_no";
  //   $detailData = array(":order_no"=>$orderNo);
  //   $detailResult = $group->DB_Query($detailsql,$detailData);
  //   echo "<pre>";
  //   print_r($detailResult);
  //   echo "</pre>";
  // }
  
}






?>



      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="order.php">訂單管理</a></li>
            <li class="breadcrumb-item active">查詢群組訂單</li>
          </ul>
        </div>
      </div>
      <section class="forms orderEdit">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">查詢群組訂單</h1>
          </header>
          <div class="row">
            <!-- ======form ===== -->
            <div class="col-lg-12">
              <div class="card">
                <div class="card-block">
                  <!-- <form class="form-horizontal"> -->
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderRecipientEdit"><span class="text-danger">*</span>買家姓名</label>
                      <div class="col-sm-10">
                       <div class="order_recipient"><?php echo $result[0]['order_recipient'] ;?></div>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderCreatetimeEdit"><span class="text-danger">*</span>下單時間</label>
                      <div class="col-sm-10">
                         <div class="order_createtime"></div>
                      </div>
                    </div>
                    <div class="line"></div> 
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderTelEdit"><span class="text-danger">*</span>電話</label>
                      <div class="col-sm-10">
                        <div class="order_tel"><?php echo $result[0]['order_tel']; ?></div>
                      </div>
                    </div>
                    <div class="line"></div> 
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderEmailEdit"><span class="text-danger">*</span>Email</label>
                      <div class="col-sm-10">
                        <div class="order_email"><?php echo $result[0]['order_email']; ?></div>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderCargoEdit"><span class="text-danger">*</span>運費</label>
                      <div class="col-sm-10">
                        <div class="order_cargo"><span>$</span><?php echo $result[0]['order_cargo']; ?></div>
                      </div>
                    </div>
                    <div class="line"></div>                            
                    
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderDetail"><span class="text-danger">*</span>訂購商品</label>
                      <div class="col-sm-10">
                        <?php 
                           foreach ($result as $itemkey => $itemvalue) {
                              $orderNo = $itemvalue['order_no']; 
                              $detailsql = "select * from order_detail where order_no = :order_no";
                              $detailData = array(":order_no"=>$orderNo);
                              $detailResult = $group->DB_Query($detailsql,$detailData);
                        ?>
                          <div class="row">
                              <div class="col-12">訂單編號:<span class="order_no"><?php echo $itemvalue["order_no"] ?></span>
                              <?php  
                                switch ($itemvalue['order_pay_status']){
                                  case "0": 
                                    echo '<span class="order_pay_status text-center" style="background-color: #ff0f00; width:80px; color:#fff;">尚未收到款項</span>';
                                    break;
                                  case "1": 
                                    echo '<span class="order_pay_status text-center" style="background-color: #d4edda; width:80px; color:#155724;">已收到款項</span>';
                                    break;  
                                }
                                switch ($itemvalue['order_status']){
                                  case "0": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #ff0f00; width:80px; color:#fff;">處理中</span>';
                                    break;
                                  case "1": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #fff3cd; width:80px; color:#856404;">收款確認</span>';
                                    break; 
                                  case "2": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #fff3cd; width:80px; color:#856404;">出貨</span>';
                                    break; 
                                  case "3": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #f8d7da; width:80px; color:#721c24;">退貨</span>';
                                    break; 
                                  case "4": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #f8d7da; width:80px; color:#721c24;">取消</span>';
                                    break; 
                                  case "5": 
                                    echo '&nbsp&nbsp<span class="order_pay_status text-center" style="background-color: #d4edda; width:80px; color:#155724;">完成</span>';
                                    break;          
                                }

                              ?>
                              </div>
                              <div class="orderDetailTable col-12" style="padding-top: 0;">
                                <table id="orderTable" class="table table-bordered table-sm">
                                  <thead>
                                    <tr style="background-color:#ccc;">
                                      <th>供應商</th>
                                      <th>品牌</th>
                                      <th>商品編號</th>
                                      <th>購買商品</th>
                                      <th>規格</th>
                                      <th>數量</th>
                                      <th>金額(單價＊數量)</th>
                                      <th>加購狀態</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  
                                    <?php 
                                      //   // ------收尋產品名稱跟編號-----
                                      $total=0;
                                      foreach ($detailResult as $detailkey => $detailvalue) {
                                        $sqlProduct = "select product_no,product_name,model_id from product where product_no = '".$detailvalue['product_no']."'";
                                        $productResult = $group->DB_Query($sqlProduct);
                                        $total +=$detailvalue['order_detail_price']*$detailvalue['order_detail_quantity'];
                                    ?>
                                    <tr>
                                      <td><?php echo $itemvalue['cate_name'] ;?></td>
                                      <td><?php echo $detailvalue['cate_name']  ;?></td>
                                      <td><?php echo $productResult[0]['model_id'] ;?></td>
                                      <td><?php echo $productResult[0]['product_name'] ;?></td>
                                      <td><?php echo $detailvalue['product_spec'] ;?></td>
                                      <td><?php echo $detailvalue['order_detail_quantity'] ;?></td>
                                      <td><span>$</span><span class="orderDetailPrice"><?php echo $detailvalue['order_detail_price']*$detailvalue['order_detail_quantity']; ?></span></td>
                                      <td><?php if($detailvalue['order_detail_additional'] == 1){?><span class="text-danger">加購商品</span><?php } ?></td>
                                  </tr>  
                                  <?php if($detailvalue["coupon_no"]){?>
                                    <tr  style="background-color: #f2dede;">
                                      <td colspan="3"></td>
                                      <td>折扣碼</td>
                                      <td><?php echo $detailvalue["coupon_code"]; ?></td>
                                      <td><?php echo ($detailvalue["coupon_discount"]*100)."%"; ?></td>
                                      <td><span>-$</span><span  class="discountPrice"><?php echo $detailvalue["order_detail_price_discount"]*$detailvalue['order_detail_quantity']; ?></span></td>
                                    </tr>
                                  <?php } //---end if 
                                      $total-=$detailvalue["order_detail_price_discount"]*$detailvalue['order_detail_quantity'];
                                    }//---end foreach result
                                  ?>  
                                  </tbody>
                                </table>
                                <div class="text-right"><span>總金額：</span><span>$</span><span class="orderTotalPrice"><?php echo $total; ?></span></div>

                              </div>
                          </div> 
                          <div class="text-right">
                            <span class="edit">
                              <form action="orderEdit.php" method="get"><input type="hidden" name="order_no" value="<?php echo $itemvalue['order_no']; ?>"><button class="orderEditButton">前往編輯訂單</button></form>
                            </span>    
                          </div> 
                          <div class="line"></div>
                          <?php } ?>

                      </div>
                    </div>
                    <div class="line"></div>
                     <div class="form-group row">
                      <label class="col-sm-2 form-control-label"><span class="text-danger">*</span>消費總金額</label>
                      <div class="col-sm-10">
                        <div><span>$</span><span id="total_price"></span></div>
                      </div>
                    </div>
                    <div class="line"></div> 
                    
                    <div class="form-group row">
                      <div class="col-12 text-right">
                        <button id="orderEditCancel" type="button" class="btn-sm btn-secondary">回上一頁</button>
                      </div>
                    </div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
            <!-- ======form======== -->
          </div>
        </div>
      </section>


      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

  <script type="text/javascript">
      var orderTime = '<?php echo $result[0]["order_createtime"] ?>';
      var date = new Date(parseInt(orderTime)*1000);
      var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
      // console.log(time);
      $("div.order_createtime").text(time);

      //-------計算群組訂單總金額------
      var totalPrice=parseInt('<?php echo $result[0]['order_cargo'] ;?>'); 
      $("span.orderTotalPrice").each(function(){
        totalPrice+=parseInt($(this).text());
      });
      $("span#total_price").text(totalPrice);


      // // -----計算單筆訂單總金額-----
      // var detailPrice =0;
      // var discountAfter = 0;
      // $(".orderDetailPrice").each(function(){
      //   $(this).text();
      //   detailPrice += parseInt($(this).text());
      // });
      // // ----是否有優惠卷----
      // <?php //if($result["coupon_no"]){
      // ?>  
      // discount = '<?php //echo $result["order_discount"]; ?>';
      // discountAfter = parseInt(detailPrice)*discount;
      // $("#discountPrice").text(discountAfter); 
      // <?php
      // } 
      // ?>
      // detailPrice-=discountAfter;

      // $("#orderTotalPrice").text(detailPrice);

  </script>

<?php require_once("module/footer.php"); ?>