<?php require_once("module/header.php"); ?>

      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">商品管理</li>
          </ul>
        </div>
      </div>
      <section class="charts">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">商品管理</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<div class="card product">
				
				<?php require_once("../../model/product.php") ;
					$db = new DB();
					$sql ="select * from category ";
					$cate = $db->DB_Query($sql);
				?>

             <!-- =====dataTable====== -->
			             <div class="demo">
			             	<div class="row">
			             		<div class="col-6 productSelectBox">
			             			<button id="productSelectAll" class="btn btn-outline-secondary">全選</button>
				             		<button id="productSelectNone" class="btn btn-outline-secondary">全不選</button>
				             		<select id="productSelects" class="form-control">
				             			<option value disabled selected>批次處理動作</option>
				             			<option value="1" >上架</option>
				             			<option value="2" >下架</option>
		                              	<option value="3" >加入分類</option>
		                              	<option value="4" >刪除分類</option>
		                              	<option value="5" >加入標籤</option>
		                              	<option value="6" >刪除標籤</option>
				             		</select>
				             		<button id="productSelectConfirm" class="btn btn-outline-success">確認</button>
								
				             	</div>
				             	<div class="col-6 text-right">
				             		<span>分類篩選</span>
				             		<select id="cateSelects" class="form-control">
				             			<option value="0" selected>所有商品</option>
				             			<?php	
											foreach ($cate as $key => $value) {
										?>
				             			<option value="<?php echo $value['cate_name']; ?>" ><?php echo $value['cate_name']; ?></option>
		                              	<?php }	?>
				             		</select>
				             		<button id="productCreatebutton" class="btn btn-outline-success">新增商品</button>
				             	</div>
			             	</div>
			             	

				            <?php 
								$sql ="select * from product ";
								$result = $db->DB_Query($sql);
							?>		
				           
			             			             	
							<table id="dataTableProduct" class="cell-border" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>編號</th>
						                <th>縮圖</th>
						                <th>產品名稱</th>
						                <th>分類</th>
						                <th>標籤</th>
						                <th>定價</th>
						                <th>售價</th>
						                <th>優惠</th>
						                <th>狀態</th>
						                <th>更新時間</th>
						                <th></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
						            	<th>編號</th>
						                <th>縮圖</th>
						                <th>產品名稱</th>
						                <th>分類</th>
						                <th>標籤</th>
						                <th>定價</th>
						                <th>售價</th>
						                <th>優惠</th>
						                <th>狀態</th>
						                <th>更新時間</th>
						                <th></th>
						        	</tr>
						    	</tfoot>
						    	<tbody>
									<?php foreach ($result as $key => $value) {
									?>

						    		<tr class="product">
						            	<td><?php echo $value["product_no"] ; ?></td>
						                <td>img</td>
						                <td><?php echo $value["product_name"] ; ?></td>
						                <td><?php 
						                	$sql = "select * from product where product_no=:product_no";
											$dic=array(":product_no"=>$value["product_no"]);
											$product = new Product($sql,$dic);
											$resultRelate = $product->productRelateCate();
											if($resultRelate){ //有商品分類
												foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
													// echo $valuecateRelate["cate_name"] ;

													if($valuecateRelate["cate_parents"] != 0){ //有父層
														$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
														echo $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"]."<br>";
														?>
														<input type="hidden" name="cateRelateNo" value="<?php echo $valuecateRelate['cate_product_no'] ?>">
														<?php
													}else{ //沒有父層	
														echo $valuecateRelate["cate_name"]."<br>";
														?>
														<input type="hidden" name="cateRelateNo" value="<?php echo $valuecateRelate['cate_product_no'] ?>">
														<?php
													} //---end if
												} //---end foreach	
											}else{ // 沒有商品分類
												echo "無";
											}
						                ?></td>
						                <td><?php 
						                	$tagRelate = $product->productRelateTag();
											if($tagRelate){ //有標籤存在
												foreach ($tagRelate as $tagRelatekey => $tagRelatevalue) {
													echo $tagRelatevalue["tag_name"],"<br>";
												?>
												<input type="hidden" name="tag_product_no" value="<?php echo $tagRelatevalue['tag_product_no']; ?>">
												<?php		
												}
											}else{ //沒有標籤
												echo "無" ;
											}
						                ?></td>
						                <td><?php echo $value["product_price1"] ; ?></td>
						                <td><?php echo $value["product_price2"] ; ?></td>
						                <td><?php echo $value["product_price3"] ; ?></td>
						                <td><?php if($value["product_status"] == 1){
						                		echo "銷售中";
						                	}else{
						                		echo "已下架";
						                	} ?></td>
						                <td><?php echo $value["product_updatetime"] ; ?></td>
						                <td class="editSection text-center">
						                <span class="edit"><button class="productEditButton">編輯</button></span><span>|</span><span class="edit"><button class="productStatusButton"><?php if($value["product_status"]==1){
						                	echo "下架" ;
						                	}else{
						                		echo "上架" ;
						                		} ?></button></span></td>
						        	</tr>

									<?php } ?>
						        	
						    	</tbody>
							</table>
						</div>	
             <!-- =====/dataTable====== -->
             	</div>
            </div> 
          </div>
        </div>
		
		
		<?php  
			$sql ="select * from category ";
			$cate = $db->DB_Query($sql);
		?>

	<!-- Modal 分類新增-->
        <div class="modal fade" id="cateCreateModal" tabindex="-1" role="dialog" aria-labelledby="cateCreateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="cateCreateModalLabel">加入分類</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-12">
                     	一共選擇了<span id="cateCreateModalCount"></span>筆商品,選擇你要加入的分類或是新增分類
                  </div>
                </div>
                <div class="form-group row">
                    <label for="cateSelect" class="col-2 col-form-label">分類</label>
                    <div class="col-10">
                      <select class="form-control" id="cateSelect" multiple>
                          <option value="" disabled selected>選擇分類</option>
                          <?php foreach ($cate as $key => $value) {
                          ?>
						  <option value="<?php echo $value["cate_no"]; ?>"><?php echo $value["cate_name"]; ?></option>
                          <?php
                          } ?>
                          
                      </select>
                    </div>
                </div>
                <div class="form-group row"><div style="margin: auto;">or</div></div>
                <div class="form-group row">
                    <label for="cateCreateNew" class="col-2 col-form-label">新增</label>
                    <div class="col-10">
                      <input class="form-control" type="text" id="cateCreateNew" placeholder="想要新增的分類">
                    </div>
                </div>
                    
              </div>  <!-- end modal-body -->
			<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                <button id="cateCreateConfirm" type="button" class="btn btn-primary">確認加入</button>
              </div>
            </div>   <!-- end modal-content -->
          </div>
        </div>

        <!-- Modal 分類新增-->

	 

		

		<?php  
			$sql ="select * from tag ";
			$tag = $db->DB_Query($sql);
		?>

	<!-- Modal 標籤新增-->
        <div class="modal fade" id="tagCreateModal" tabindex="-1" role="dialog" aria-labelledby="tagCreateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="tagCreateModalLabel">加入標籤</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-12">
                     	一共選擇了<span id="tagCreateModalCount"></span>筆商品,選擇你要加入的標籤或是新增標籤
                  </div>
                </div>
                <div class="form-group row">
                    <label for="tagSelect" class="col-2 col-form-label">標籤</label>
                    <div class="col-10">
                      <select class="form-control" id="tagSelect" multiple>
                          <option value="" disabled selected>選擇標籤</option>
                          <?php foreach ($tag as $key => $value) {
                          ?>
                          <option value="<?php echo $value['tag_no']; ?>"><?php echo $value['tag_name'] ?></option>
                          <?php	
                          } ?>
                          
                      </select>
                    </div>
                </div>
                <div class="form-group row"><div style="margin: auto;">or</div></div>
                <div class="form-group row">
                    <label for="tagCreateNew" class="col-2 col-form-label">新增</label>
                    <div class="col-10">
                      <input class="form-control" type="text" id="tagCreateNew" placeholder="想要新增的標籤">
                    </div>
                </div>
                    
              </div>  <!-- end modal-body -->
			<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                <button id="tagCreateConfirm" type="button" class="btn btn-primary">確認加入</button>
              </div>
            </div>   <!-- end modal-content -->
          </div>
        </div>

        <!-- Modal 標籤新增-->

      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

<script src="js/dataTableAdmin.js"></script>
<?php require_once("module/footer.php"); ?>