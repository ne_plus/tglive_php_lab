<?php require_once("module/header.php"); ?>
<?php require_once("../controller/couponControl.php"); 

if( isset($_REQUEST["coupon_no"]) ){ //有值
  $couponNo = $_REQUEST["coupon_no"];
  $sql= "select * from coupon where coupon_no = :coupon_no";
  $data = array(":coupon_no"=>$couponNo);
  $result = query($sql,$data);
  // print_r(query($sql,$data));
}else{

}

$couponGroupSql ="SELECT * FROM category a join category_tag_relate b on a.cate_no=b.cate_no join tag c on b.tag_no = c.tag_no WHERE cate_parents= 61 and cate_level =1 and cate_name='優惠劵'";
$db = new DB();
$tagResult = $db->DB_Query($couponGroupSql);

?>



      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="coupon.php">優惠劵管理</a></li>
            <li class="breadcrumb-item active"><?php if(isset($_REQUEST["coupon_no"])){echo "編輯優惠劵";}else{echo "新增優惠劵";} ?></li>
          </ul>
        </div>
      </div>
      <section class="forms">
        <div class="container-fluid">
          <header> 
            <h1 class="h3"><?php if(isset($_REQUEST["coupon_no"])){echo "編輯優惠劵";}else{echo "新增優惠劵";} ?></h1>
          </header>
          <div class="row">
            <!-- ======form ===== -->
            <div class="col-lg-12">
              <div class="card">
                <div class="card-block">
                  <!-- <form class="form-horizontal"> -->
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditName"><span class="text-danger">*</span>優惠劵名稱</label>
                      <div class="col-sm-4">
                        <input id="couponEditName" type="text" name="coupon_name" class="form-control" value="<?php if( isset($_REQUEST['coupon_no']) ){echo $result['coupon_name'];} ?>" placeholder="請輸入優惠劵名稱">
                        <input type="hidden" name="coupon_no" value="<?php if( isset($_REQUEST['coupon_no']) ){echo $result['coupon_no'];} ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditCode"><span class="text-danger">*</span>優惠劵代碼</label>
                      <div class="col-sm-4">
                        <input id="couponEditCode" type="text" name="coupon_code" class="form-control" value="<?php if( isset($_REQUEST['coupon_no']) ){echo $result['coupon_code'];} ?>" placeholder="請輸入優惠劵代碼">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditDiscount"><span class="text-danger">*</span>折扣百分比</label>
                      <div class="col-sm-2">
                        <input id="couponEditDiscount" type="number" name="coupon_discount" min="0" max="100" class="form-control" placeholder="請輸入折扣百分比" value="<?php if( isset($_REQUEST['coupon_no']) ){echo $result['coupon_discount'];} ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditStart"><span class="text-danger">*</span>開始時間</label>
                      <div class="col-sm-4">
                        <input id="couponEditStart" type="datetime-local" name="coupon_start" class="form-control" >
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditEnd"><span class="text-danger">*</span>結束時間</label>
                      <div class="col-sm-4">
                        <input id="couponEditEnd" type="datetime-local" name="coupon_end" class="form-control" >
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditUsed"><span class="text-danger">*</span>適用指定</label>
                      <div class="col-sm-2">
                        <select id="couponEditUsed" class="form-control">
                           <option value disabled selected>選則指定標籤</option>
                           <?php foreach ($tagResult as $key => $value) {
                           ?>
                           <option value="<?php echo $value['tag_no']; ?>"><?php echo $value['tag_name'];?></option>
                           <?php 
                           } ?>
                        </select>
                      </div>  
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="couponEditTarget"><span class="text-danger">*</span>可使用對象</label>
                      <div class="col-sm-2">
                        <select id="couponEditTarget" class="form-control">
                           <option value disabled selected>選擇使用對象</option>
                           <option value="0">全部使用者</option>
                           <option value="1">僅會員使用</option>
                           <option value="2">僅非會員使用</option>
                        </select>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="couponEditTimes" class="col-sm-2 col-form-label"><span class="text-danger">*</span>優惠劵張數限制</label>
                           <div class="col-sm-2">
                                <input id="couponEditTimes" type="number" name="coupon_times" min="0" class="form-control" placeholder="請輸入張數限制" value="<?php if( isset($_REQUEST['coupon_no']) ){echo $result['coupon_times'];} ?>">
                           </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="couponEditStatus" class="col-sm-2 col-form-label"><span class="text-danger">*</span>優惠劵狀態</label>
                           <div class="col-sm-10">
                              <div class="row">
                                    <div class="col-12 col-form-label switchButtonColor"><input id="couponEditStatus" class="coupon_status" type="checkbox" name="my-checkbox" <?php if( isset($_REQUEST['coupon_no']) ){if($result['coupon_status']==1){echo 'checked';};} ?> ></div>
                              </div>   
                           </div>
                    </div>
                    <div class="line"></div>
  
                    
                    <div class="form-group row">
                      <div class="col-12 text-right">
                        <button id="couponEditCancel" type="button" class="btn-sm btn-secondary">取消</button>
                        <button id="couponEditConfirm" type="button" class="btn-sm btn-success"><?php if(isset($_REQUEST["coupon_no"])){echo "儲存編輯";}else{echo "新增確認";} ?></button>
                      </div>
                    </div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
            <!-- ======form======== -->
          </div>
        </div>
      </section>


      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

  <script type="text/javascript">
    var start = "<?php if(isset($_REQUEST['coupon_no'])){ echo $result['coupon_start'];} ?>";
    var end = "<?php if(isset($_REQUEST['coupon_no'])){ echo $result['coupon_end'];} ?>";
    var dateStart = new Date(parseInt(start)*1000);
    var startTime = dateStart.getFullYear()+'-'+("0"+(dateStart.getMonth()+1)).slice(-2)+'-'+("0"+dateStart.getDate()).slice(-2)+'T'+("0"+dateStart.getHours()).slice(-2)+':'+("0"+dateStart.getMinutes()).slice(-2)+':'+("0"+dateStart.getSeconds()).slice(-2);
    var dateEnd = new Date(parseInt(end)*1000);
    var endTime = dateEnd.getFullYear()+'-'+("0"+(dateEnd.getMonth()+1)).slice(-2)+'-'+("0"+dateEnd.getDate()).slice(-2)+'T'+("0"+dateEnd.getHours()).slice(-2)+':'+("0"+dateEnd.getMinutes()).slice(-2)+':'+("0"+dateEnd.getSeconds()).slice(-2);

    var tagSelectNo = "<?php if(isset($_REQUEST['coupon_no'])){ echo $result['coupon_used'];} ?>";

    var couponTagrget = "<?php if(isset($_REQUEST['coupon_no'])){ echo $result['coupon_target'];} ?>";
    <?php if(isset($_REQUEST['coupon_no'])){
    ?>
    $("input[name='coupon_start']").val(startTime);
    $("input[name='coupon_end']").val(endTime);
    $("select#couponEditTarget").val(couponTagrget);
    $("select#couponEditUsed").val(tagSelectNo);
    <?php  
    } ?>
    
    $("input[name='coupon_start']").change(function(){
      var startTimeSelect = $(this).val();
      $("input[name='coupon_end']").val(null);
      var dateStartSelect = new Date(startTimeSelect);
      var dateEndFrom = dateStartSelect.getFullYear()+1+'-'+("0"+(dateStartSelect.getMonth()+1)).slice(-2)+'-'+("0"+dateStartSelect.getDate()).slice(-2)+'T'+("0"+dateStartSelect.getHours()).slice(-2)+':'+("0"+dateStartSelect.getMinutes()).slice(-2)+':'+("0"+dateStartSelect.getSeconds()).slice(-2);;
      // console.log(dateEndFrom);
      $("input[name='coupon_end']").attr("min",startTimeSelect);
      $("input[name='coupon_end']").attr("max",dateEndFrom);
    });

  </script>

<?php require_once("module/footer.php"); ?>