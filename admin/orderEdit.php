<?php require_once("module/header.php"); ?>
<?php require_once("../controller/orderControl.php"); 
date_default_timezone_set("Asia/Taipei");

if( isset($_REQUEST["order_no"]) ){ //有值
  $orderNo = $_REQUEST["order_no"];
  $sql= "select * from order_item where order_no = :order_no";
  $data = array(":order_no"=>$orderNo);
  $result = query($sql,$data);
  // echo "<pre>";
  // print_r($result);
  // echo "</pre>";

  // -----查詢訂單商品明細-----
  $detailResult = detailQuery($sql,$data,$orderNo);
  // echo "<pre>";
  // print_r($detailResult);
  // echo "</pre>";
}



$productDeliverMail = [];


?>



      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="order.php">訂單管理</a></li>
            <li class="breadcrumb-item active">編輯訂單</li>
          </ul>
        </div>
      </div>
      <section class="forms orderEdit">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">編輯訂單</h1>
          </header>
          <div class="row">
            <!-- ======form ===== -->
            <div class="col-lg-12">
              <div class="card">
                <div class="card-block">
                  <!-- <form class="form-horizontal"> -->
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderNoEdit"><span class="text-danger">*</span>訂購編號</label>
                      <div class="col-sm-2">
                        <input id="orderNoEdit" type="text" name="order_no" class="form-control form-control-sm" value="<?php echo $result['order_no'] ;?>" disabled>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderCreatetimeEdit"><span class="text-danger">*</span>下單時間</label>
                      <div class="col-sm-2">
                        <input id="orderCreatetimeEdit" type="text" name="order_createtime" class="form-control form-control-sm" value="" disabled>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderDetail"><span class="text-danger">*</span>訂購商品</label>
                      <div class="col-sm-10">
                          <div class="row">
                              <div class="card-block orderDetailTable" style="padding-top: 0;">
                                <table id="orderTable" class="table table-bordered table-sm">
                                  <thead>
                                    <tr style="background-color:#ccc;">
                                      <th>供應商</th>
                                      <th>品牌</th>
                                      <th>商品編號</th>
                                      <th>購買商品</th>
                                      <th>規格</th>
                                      <th>數量</th>
                                      <th>金額(單價＊數量)</th>
                                      <th>加購狀態</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                    $totalPrice = 0;
                                    foreach ($detailResult as $detailkey => $detailvalue) {
                                      $sqlProduct = "select * from product where product_no = '".$detailvalue['product_no']."'";
                                      $productDb = new DB();
                                      $productResult = $productDb->DB_Query($sqlProduct);
                                      // echo $productResult[0]['product_name'];
                                  ?>
                                  <tr>
                                    <td><?php echo $result['cate_name'] ;?></td>
                                    <td><?php echo $detailvalue['cate_name'] ;?></td>
                                    <td><?php echo $productResult[0]['model_id'] ;?></td>
                                    <td><?php echo $productResult[0]['product_name'] ;?></td>
                                    <td><?php echo $detailvalue['product_spec'] ;?></td>
                                    <td><?php echo $detailvalue['order_detail_quantity'] ;?></td>
                                    <td><span>$</span><span class="orderDetailPrice"><?php echo $detailvalue['order_detail_price']*$detailvalue['order_detail_quantity']; ?></span></td>
                                    <td><?php if($detailvalue['order_detail_additional'] == 1){?><span class="text-danger">加購商品</span><?php } ?></td>
                                  </tr>  
                                  <?php if($detailResult[$detailkey]["coupon_no"]){ //有
                                  ?>
                                  <tr  style="background-color: #f2dede;">
                                    <td colspan="3"></td>
                                    <td>折扣碼</td>
                                    <td><?php echo $detailvalue["coupon_code"]; ?></td>
                                    <td><?php echo ($detailvalue["coupon_discount"]*100)."%"; ?></td>
                                    <td><span>-$</span><span  id="discountPrice"><?php echo $detailvalue["order_detail_price_discount"]*$detailvalue['order_detail_quantity']; ?></span></td>
                                  </tr>
                                  <?php
                                    } //-----end if
                                    $totalPrice += ($detailvalue['order_detail_price']*$detailvalue['order_detail_quantity'])-($detailvalue["order_detail_price_discount"]*$detailvalue['order_detail_quantity']); 

                                     //出貨通知信件陣列
                                    $productDeliverMail[$detailkey]["order_group"] = $result["order_group"];
                                    $productDeliverMail[$detailkey]["order_time"] = date("Y-m-d H:i:s",$result["order_createtime"]);
                                    $productDeliverMail[$detailkey]["product_name"] = $productResult[0]['product_name'];
                                    $productDeliverMail[$detailkey]["product_quantity"] = $detailvalue['order_detail_quantity'];

                                  }  // ----end foreach
                                  ?>

                                  </tbody>
                                </table>
                                <div class="text-right"><span>總金額：</span><span>$</span><span id="orderTotalPrice"><?php echo $totalPrice; ?></span></div>

                              </div>
                          </div>  
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderRecipientEdit"><span class="text-danger">*</span>買家姓名</label>
                      <div class="col-sm-2">
                        <input id="orderRecipientEdit" type="text" name="order_recipient" class="form-control form-control-sm" value="<?php echo $result['order_recipient'] ;?>" disabled>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderAddressEdit"><span class="text-danger">*</span>收貨地址</label>
                      <div class="col-sm-10">
                        <input id="orderAddressEdit" type="text" name="order_address" class="form-control form-control-sm" value="<?php echo $result['order_address'] ; ?>" disabled>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderTelEdit"><span class="text-danger">*</span>電話</label>
                      <div class="col-sm-4">
                        <input id="orderTelEdit" type="text" name="order_tel" class="form-control form-control-sm"
                        value="<?php echo $result['order_tel']; ?>" disabled>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderEmailEdit"><span class="text-danger">*</span>Email</label>
                      <div class="col-sm-4">
                        <input id="orderEmailEdit" type="text" name="order_email" class="form-control form-control-sm" disabled value="<?php echo $result['order_email']; ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderPayEdit"><span class="text-danger">*</span>付款方式</label>
                      <div class="col-sm-2">
                        <input id="orderPayEdit" type="text" name="order_pay" class="form-control form-control-sm" disabled value="<?php 
                          if($result['order_pay'] == 0){
                            echo "信用卡";
                          }elseif($result['order_pay'] == 1){
                            echo "ATM轉帳" ;
                          }elseif($result['order_pay'] == 2){
                            echo "超商付款" ;
                          }elseif($result['order_pay'] == 3){
                            echo "信用卡分期付款(" . $result['order_pay_install'] . "期)" ;
                          }elseif($result['order_pay'] == 4){
                            echo "現金匯款" ;
                          }
                         ?>">
                      </div><div class="col-sm-8"></div> 
                      <label class="col-sm-2 form-control-label" for="orderPayStatusEdit"><span class="text-danger">*</span>付款狀態</label>
                      <div class="col-sm-2">
                        <select id="orderPayStatusEdit" class="form-control form-control-sm">
                          <option value="0">尚未收到款項</option>
                          <option value="1">已收到款項</option>
                        </select>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="orderDeliveryEdit"><span class="text-danger">*</span>物流方式</label>
                      <div class="col-sm-2">
                        <input id="orderDeliveryEdit" type="text" name="order_delivery" class="form-control form-control-sm" disabled value="<?php 
                          if($result['order_delivery'] == 0){
                            echo "宅配";
                          }elseif($result['order_delivery'] == 1){
                            echo "超取";
                          }
                        ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                       <label for="orderStatusEdit" class="col-sm-2 col-form-label"><span class="text-danger">*</span>訂單狀態</label>
                       <div class="col-sm-2">
                            <select id="orderStatusEdit" class="form-control form-control-sm">
                                <option value="0">處理中</option>
                                <option value="1">收款確認</option>
                                <option value="2">出貨</option>
                                <option value="3">退貨</option>
                                <option value="4">取消</option>
                                <option value="5">完成</option>
                            </select>
                       </div>
                       <div class="col-sm-8">   
                          <div class="row">
                            <div class="col-12 deliverInforBtnShow">
                              <span id="deliverInfo" style="vertical-align: middle;">出貨通知</span>
                              <label class="col-form-label switchButtonColor"><input id="deliverSendBtn" type="checkbox" name="my-checkbox" ></label>
                              <input type="hidden" name="productDeliver-mail" id="productDeliver" value='<?php echo json_encode($productDeliverMail); ?>'>
                            </div>  
                          </div>
                       </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="orderMemoEdit" class="col-sm-2 col-form-label"><span class="text-danger">*</span>客戶備註</label>
                           <div class="col-sm-4">
                               <textarea id="orderMemoEdit" class="form-control form-control-sm" rows="5" placeholder="客戶訂購時留言備註" disabled><?php echo $result['order_memo']; ?></textarea>
                           </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="adminMemoEdit" class="col-sm-2 col-form-label"><span class="text-danger">*</span>後台備註</label>
                           <div class="col-sm-4">
                               <textarea id="adminMemoEdit" class="form-control form-control-sm" rows="5" placeholder="後台人員備註訂單處理狀態,前台無法檢視" ><?php echo $result['admin_memo']; ?></textarea>
                           </div>
                    </div>
                    <div class="line"></div>
  
                    
                    <div class="form-group row">
                      <div class="col-12 text-right">
                        <button id="orderEditCancel" type="button" class="btn-sm btn-secondary">取消</button>
                        <button id="orderEditConfirm" type="button" class="btn-sm btn-success">儲存編輯</button>
                      </div>
                    </div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
            <!-- ======form======== -->
          </div>
        </div>
      </section>


      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

  <script type="text/javascript">
      var orderTime = '<?php echo $result["order_createtime"] ?>';
      var date = new Date(parseInt(orderTime)*1000);
      var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);
      // console.log(time);
      $("#orderCreatetimeEdit").val(time);

      var orderPayStatus = '<?php echo $result["order_pay_status"] ?>';
      $("select#orderPayStatusEdit").val(orderPayStatus);

      var orderStatus = '<?php echo $result["order_status"] ?>';
      $("#orderStatusEdit").val(orderStatus);


      $("select#orderPayStatusEdit").change(function(){
        if($(this).val() == 0){
          $("select[id='orderStatusEdit']").val(0);
          $("select[id='orderStatusEdit'] option").eq(0).removeAttr("disabled");
        }else{
          if($("select#orderStatusEdit").val() == 0){
            $("select[id='orderStatusEdit'] option").eq(0).attr("disabled","");
            $("select[id='orderStatusEdit']").val(1);
          }else{
            $("select[id='orderStatusEdit'] option").eq(0).attr("disabled","");
          }      
        }
      });
      

      // -----計算訂單總金額-----
      // var detailPrice =0;
      // var discountAfter = 0;
      // $(".orderDetailPrice").each(function(){
      //   $(this).text();
      //   detailPrice += parseInt($(this).text());
      // });
      // // ----是否有優惠卷----
      // <?php //if($result["coupon_no"]){
      // ?>  
      // discount = '<?php //echo $result["order_discount"]; ?>';
      // discountAfter = parseInt(detailPrice)*discount;
      // $("#discountPrice").text(discountAfter); 
      // <?php
      // } 
      // ?>
      // detailPrice-=discountAfter;

      // $("#orderTotalPrice").text(detailPrice);

  </script>

<?php require_once("module/footer.php"); ?>