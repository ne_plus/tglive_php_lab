<?php require_once("module/header.php"); ?>
<?php require_once("../controller/productControl.php"); 

$db = new DB();

$productNo = $_REQUEST["product_no"]; 
$table = "product";
// ====product DB====

$result = productQuery($table,$productNo);
$jsonContent = json_encode($result["product_content"]);

// 加購價 搜尋 只有同供應商 （找尋商品 供應商和品牌)
$cateQuery = findBrandsInfo($productNo);

// 如果商品沒有綁定供應商/品牌 轉出product.php
if(!isset($cateQuery['vcate_no'][0])){
?>
<script>
  alert('沒有設定供應商/品牌');
  location.href="product.php"
</script>
<?php
}
$cateVenderID = $cateQuery['vcate_no'][0] ;
$cateBrandIdSql = 'SELECT * FROM category WHERE `cate_parents` = '.$cateVenderID;
$relateBrandIds = $db->DB_Query($cateBrandIdSql);
$relateBrandIdsOBJ = [];
$getBrandIdQueryProducts = [];
foreach ($relateBrandIds as $brandKey => $brandValue ){
  $relateBrandIdsOBJ[$brandKey] = $brandValue['cate_no'];
  $brandProducts = get_brandId_queryProducts($brandValue['cate_no']);
  foreach ( $brandProducts as $brandProductsKey => $brandProductsValue) {
    array_push($getBrandIdQueryProducts,$brandProductsValue);
  } 
}

// ====product_spec DB====
$result2 = productSpecQuery($table,$productNo);

// ====product_combo DB====
$result3 = productComboQuery($productNo);

// echo "<pre>";
// var_dump($result2);
// // echo count($result2);
// echo "</pre>";
date_default_timezone_set("Asia/Taipei");
$defaultTime = date("Y-m-d").'T'.date("H:i");
?>
<script src="js/image.js"></script>


      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="product.php">商品管理</a></li>
            <li class="breadcrumb-item active">編輯商品</li>
          </ul>
        </div>
      </div>
      <section class="forms">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">商品編輯</h1>
          </header>
          <div class="row">
            <!-- ======form ===== -->
            <div class="col-lg-12">
              <div class="card">
                <div class="card-block">
                  <!-- <form class="form-horizontal"> -->
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditModelId"><span class="text-danger">*</span>商品編號</label>
                      <div class="col-sm-10">
                        <input id="productEditModelId" type="text" name="model_id" class="form-control" value="<?php echo $result['model_id'] ;?>">
                        <input type="hidden" name="product_no" value="<?php echo $result['product_no'] ; ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditProductName"><span class="text-danger">*</span>商品名稱</label>
                      <div class="col-sm-10">
                        <input id="productEditProductName" type="text" name="product_name" class="form-control" value="<?php echo $result['product_name'] ;?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditProductSubtitle">商品副標</label>
                      <div class="col-sm-10">
                        <input id="productEditProductSubtitle" type="text" name="product_subtitle" class="form-control" placeholder="請輸入商品副標" value="<?php echo $result['product_subtitle'] ;?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditProductPromote">促銷文字</label>
                      <div class="col-sm-10">
                        <input id="productEditProductPromote" type="text" name="product_promote" placeholder="促銷文字顯示於標題下方,紅色文字說明折扣或促銷資訊" class="form-control" value="<?php echo $result['product_promote'] ;?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditProductDescribe">商品描述</label>
                      <div class="col-sm-10">
                        <input id="productEditProductDescribe" type="text" name="product_describe" placeholder="輸入簡短商品說明文字 (30字以內)" class="form-control" value="<?php echo $result['product_describe'] ;?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="">商品圖片</label>
                      <div class="col-sm-10">
                        <div class="row">
                            <div class="col text-center">
                              <p>main</p>
                              <div class="col">
                                <button id="img1Upload" class="btn-sm btn-outline-secondary imgUpload">檔案上傳</button>
                              </div>
                              <input id="fileImg1" type="file" name="product_img" style="display:none;" class="form-control">
                              <div class="col imgInfo my-1">
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸*</div>
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800</div>
                              </div>
                              <img id="img1" src="<?php 
                                    if(trim($result['img1']) != null || trim($result['img1']) != ''){
                                      echo sprintf('../%s',trim($result['img1']));
                                    }
                              ?>" class="imgShow img-fluid"> 
                              <div class="col imgdeletebutton">
                                <button id="img1delete" style="<?php if (trim($result['img1']) == null || trim($result['img1']) == ''){ echo 'display: none;';}?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                              </div>
                            </div>
                            <div class="col text-center">
                              <p>2nd</p>
                              <div class="col">
                                <button id="img2Upload" class="btn-sm btn-outline-secondary imgUpload">檔案上傳</button>
                              </div>
                              <input id="fileImg2" type="file" name="product_img" style="display:none;" class="form-control">
                              <div class="col imgInfo my-1">
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸*</div>
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800</div>
                              </div>
                              <img id="img2" src="<?php 
                                    if(trim($result['img2']) != null || trim($result['img2']) != ''){
                                      echo sprintf('../%s',trim($result['img2']));
                                    }
                              ?>" class="imgShow img-fluid">
                              <div class="col imgdeletebutton">
                                <button id="img2delete" style="<?php if (trim($result['img2']) == null || trim($result['img2']) == ''){ echo 'display: none;';}?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                              </div>   
                            </div>
                            <div class="col text-center">
                              <p>3th</p>
                              <div class="col">
                                <button id="img3Upload" class="btn-sm btn-outline-secondary imgUpload">檔案上傳</button>
                              </div>
                              <input id="fileImg3" type="file" name="product_img" style="display:none;" class="form-control">
                              <div class="col imgInfo my-1">
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸*</div>
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800</div>
                              </div>
                              <img id="img3" src="<?php 
                                    if(trim($result['img3']) != null || trim($result['img3']) != ''){
                                      echo sprintf('../%s',trim($result['img3']));
                                    }
                              ?>" class="imgShow img-fluid">    
                              <div class="col imgdeletebutton">
                                <button id="img3delete" style="<?php if (trim($result['img3']) == null || trim($result['img3']) == ''){ echo 'display: none;';}?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                              </div>   
                            </div>
                            <div class="col text-center">
                              <p>4th</p>
                              <div class="col">
                                <button id="img4Upload" class="btn-sm btn-outline-secondary imgUpload">檔案上傳</button>
                              </div>
                              <input id="fileImg4" type="file" name="product_img" style="display:none;" class="form-control">
                              <div class="col imgInfo my-1">
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸*</div>
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800</div>
                              </div>
                              <img id="img4" src="<?php 
                                    if(trim($result['img4']) != null || trim($result['img4']) != ''){
                                      echo sprintf('../%s',trim($result['img4']));
                                    }
                              ?>" class="imgShow img-fluid">
                              <div class="col imgdeletebutton">
                                <button id="img4delete" style="<?php if (trim($result['img4']) == null || trim($result['img4']) == ''){ echo 'display: none;';}?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                              </div>   
                            </div>
                            <div class="col text-center">
                              <p>5th</p>
                              <div class="col">
                                <button id="img5Upload" class="btn-sm btn-outline-secondary imgUpload">檔案上傳</button>
                              </div>
                              <input id="fileImg5" type="file" name="product_img" style="display:none;" class="form-control">
                              <div class="col imgInfo my-1">
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸*</div>
                                <div style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800</div>
                              </div>
                              <img id="img5" src="<?php 
                                    if(trim($result['img5']) != null || trim($result['img5']) != ''){
                                      echo sprintf('../%s',trim($result['img5']));
                                    }
                              ?>" class="imgShow img-fluid">
                              <div class="col imgdeletebutton">
                                <button id="img5delete" style="<?php if (trim($result['img5']) == null || trim($result['img5']) == ''){ echo 'display: none;';}?>" class="btn-sm btn-outline-danger imgdelete">刪除</button>
                              </div>   
                            </div>
                        </div> 
                      <!--   <div class="col-12 text-right" style="margin-top: 40px;">
                            <button type="button" class="btn-sm btn-outline-success">上傳</button>
                        </div> -->
                      </div>  
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" for="productEditProductNote">商品備註</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="productEditProductNote" name="product_note" rows="5" placeholder="純文字(此處前台不會顯示，僅後台管理員檢視)" ><?php echo $result['product_note'] ;?></textarea>
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                           <label for="" class="col-sm-2 col-form-label">商品內容</label>
                           <div class="col-sm-10">
                                <div id="summernote"></div>
                           </div>
                      </div>
                      <div class="line"></div>
                      <div class="form-group row">
                           <label for="productEditProductStatus" class="col-sm-2 col-form-label">商品狀態<br>(上架/下架)</label>
                           <div class="col-sm-10">
                              <div class="row">
                                <div class="col-12 col-form-label switchButtonColor"><input id="productEditProductStatus" class="product_status" type="checkbox" name="my-checkbox" <?php if($result['product_status'] == 1){
                                   echo "checked"; } ?> ></div>
                              </div>          
                           </div>
                      </div>
                      
                      <div class="line"></div>
                      <div class="form-group row">
                           <label class="col-sm-2 col-form-label"><span class="text-danger">*</span>商品規格</label>
                           <div class="col-sm-10 text-right">
                              <button type="button" class="btn-sm btn-outline-success" data-toggle="modal" data-target="#producrSpecCreate">新增規格</button>
                           </div>
                      </div>
                      <!-- <div class="line"></div> -->
                     
                      <div class="form-group row">
                          <div class="col-sm-12">
                              <div class="row">
                                  <div class="card-block productSpecTable">
                                    <table id="specTable" class="table table-bordered table-sm">
                                      <thead>
                                        <tr style="background-color:#eee;">
                                          <th style="width:40px;">勾選</th>
                                          <th>規格</th>
                                          <th>規格說明</th>
                                          <th>庫存</th>
                                          <th>定價</th>
                                          <th>售價</th>
                                          <th>優惠</th>
                                          <th  style="width:150px;"></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                       <?php 
                                      if($result2){ //有規格
                                          foreach ($result2 as $key => $value) {
                                      ?>
                                        <tr class="specCount">
                                          <td scope="row" class="text-center">
                                              <label class="checkbox-inline">
                                                <input id="inlineCheckbox1" class="productSpecStatus" type="checkbox" value="option1"
                                                <?php if($value["product_spec_status"]==1){
                                                  echo "checked"; } ?> >
                                              </label>
                                          </td>
                                          <td><?php echo $value["product_spec_info"]; ?></td>
                                          <td><?php echo $value["product_spec_describe"]; ?></td>
                                          <td><?php echo $value["product_stock"] ;?></td>
                                          <td><?php echo $value["product_spec_price1"] ;?></td>
                                          <td><?php echo $value["product_spec_price2"] ;?></td>
                                          <td><?php echo $value["product_spec_price3"] ;?></td>
                                          <td class="text-center" >
                                            <input type="hidden" name="product_spec_no" value="<?php echo $value["product_spec_no"] ; ?>">
                                            <span class="edit"><button class="productSpecEdit" data-toggle="modal" data-target="#producrSpecEdit">編輯</button></span><span>|</span>
                                            <span class="edit"><button class="productSpecDelete">刪除</button>
                                          </td>
                                        </tr>
                                     <?php 
                                          }    
                                      }
                                      ?>
                                        <tr id="specNone"><td class="text-center" colspan="8"><div class="alert alert-danger" role="alert"><strong>此商品無任何規格設定!</strong> 請新增商品規格</div></td></tr>
                                      </tbody>
                                    </table>
                                  </div>
                              </div>          
                           </div>
                      </div>
                      
					<div class="line"></div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><span class="text-danger">*</span>加價購</label>
						<div class="col-sm-10 text-right">
							<button type="button" class="btn-sm btn-outline-success" data-toggle="modal" data-target="#producrComboCreate">新增加價購</button>
						</div>
                    </div>
					<div class="form-group row">
                          <div class="col-sm-12">
                              <div class="row">
                                  <div class="card-block productComboTable">
                                    <table id="comboTable" class="table table-bordered table-sm">
                                      <thead>
                                        <tr style="background-color:#eee;">
										  <!-- <th style="width:40px;">勾選</th> -->
                                          <th>品名</th>
                                          <th>規格</th>
                                          <th>加購期間</th>
                                          <th>加購價</th>
                                          <th style="width:150px;"></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                       <?php 
                                      if($result3){ //有加購價
                                          foreach ($result3 as $key => $value) {
                                      ?>
                                        <tr class="comboCount">
										  <!-- <td scope="row" class="text-center">
                                              <label class="checkbox-inline">
                                                <input id="inlineCheckbox1" class="productComboStatus" type="checkbox" value="option1"
                                                <?php //if($value["product_combo_status"]==1){
                                                  //echo "checked"; } ?> >
                                              </label>
                                          </td> -->
                                          <td><a target="_blank" href="../product-detail.php?product_no=<?=$value["product_no"]?>"><?php echo $value["product_no"] . ' / ' . $value["product_name"]; ?></a></td>
                                          <td><?php echo $value["product_spec_no"] . ' / ' . $value["product_spec_info"] . ' (商品庫存:'.$value["product_stock"].')'; ?></td>
                                          <td><?php echo $value["starttime"] . ' ~ ' . $value["endtime"] ;?></td>
                                          <td><?php echo $value["product_spec_price"] ;?></td>
                                          <td class="text-center" >
                                            <input type="hidden" name="combo_id" value="<?php echo $value["id"] ; ?>">
                                            <input type="hidden" name="combo_product_no" value="<?php echo $value["product_no"] ; ?>">
                                            <input type="hidden" name="combo_product_spec_no" value="<?php echo $value["product_spec_no"] ; ?>">
                                            <input type="hidden" name="combo_starttime" value="<?php echo $value["starttime"] ; ?>">
                                            <input type="hidden" name="combo_endtime" value="<?php echo $value["endtime"] ; ?>">
                                            <span class="edit"><button class="productComboEdit" data-toggle="modal" data-target="#producrComboEdit">編輯</button></span><span>|</span>
                                            <span class="edit"><button class="productComboDelete">刪除</button>
                                          </td>
                                        </tr>
                                     <?php 
                                          }    
                                      }
                                      ?>
                                        <tr id="comboNone"><td class="text-center" colspan="10"><div class="alert alert-danger" role="alert"><strong>此商品無加購價設定!</strong></div></td></tr>
                                      </tbody>
                                    </table>
                                  </div>
                              </div>          
                           </div>
                      </div>
  
                    
					
					<div class="line"></div>
                    <div class="form-group row">
                      <div class="col-12 text-right">
                        <button id="productEditCancel" type="button" class="btn-sm btn-secondary">取消</button>
                        <button id="productEditConfirm" type="button" class="btn-sm btn-success">儲存編輯</button>
                      </div>
                    </div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
            <!-- ======form======== -->
          </div>
        </div>
      </section>

        <!-- ========modal========== -->

            <!-- Modal 新增 -->
            <div class="modal fade" id="producrSpecCreate" tabindex="-1" role="dialog" aria-labelledby="specNew" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="specNew">新增商品規格</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="specInfo" class="col-2 col-form-label">規格</label>
                      <div class="col-10">
                          <input class="form-control" type="text" id="specInfo" name="product_spec_info" placeholder="輸入產品規格">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="specDescribe" class="col-2 col-form-label">說明</label>
                        <div class="col-10">
                          <input class="form-control" type="text" id="specDescribe" name="product_spec_describe" placeholder="輸入規格說明文字">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="productStock" class="col-2 col-form-label">庫存</label>
                        <div class="col-10">
                          <input class="form-control" type="number" id="productStock" name="product_stock" placeholder="輸入庫存量">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice1">定價</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice1"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice2">售價</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice2"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">優惠</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice3"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="specCreateConfirm" type="button" class="btn-sm btn-primary">確認新增</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>

            <!-- Modal 規格編輯 -->
            <div class="modal fade" id="producrSpecEdit" tabindex="-1" role="dialog" aria-labelledby="specEdit" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="specEdit">修改商品規格</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="specInfoEdit" class="col-2 col-form-label">規格</label>
                      <div class="col-10">
                          <input class="form-control" type="text" id="specInfoEdit" name="edit_product_spec_info" placeholder="輸入產品規格">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="specDescribeEdit" class="col-2 col-form-label">說明</label>
                        <div class="col-10">
                          <input class="form-control" type="text" id="specDescribeEdit" name="edit_product_spec_describe" placeholder="輸入規格說明文字">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="productStockEdit" class="col-2 col-form-label">庫存</label>
                        <div class="col-10">
                          <input class="form-control" type="number" id="productStockEdit" name="edit_product_stock" placeholder="輸入庫存量">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice1Edit">定價</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice1Edit"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice2Edit">售價</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice2Edit"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3Edit">優惠</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="productPrice3Edit"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="specEditConfirm" type="button" class="btn-sm btn-primary">確認新增</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>

			<!-- Modal 加價購新增 -->
            <div class="modal fade" id="producrComboCreate" tabindex="-1" role="dialog" aria-labelledby="comboNew" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="specNew">新增加價購</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="specInfo" class="col-2 col-form-label">產品</label>
                      <div class="col-10">
						<select class="form-control" id="comboProduct" name="product_combo_info" onchange="getComboSpec(this.value, 'comboProductSpec', '')">
							<option value="">請選擇產品</option>
							<?php
                // a. 所有供應商底下加購價
								$sql ="select * from product";
								$comboProduct = $db->DB_Query($sql);
								foreach ($comboProduct as $key => $value) {
                  echo '<option value="' . $value["product_no"] . '">' . $value["product_name"] . '</option>';
                }

                // b. 同供應商底下加購價  
                // foreach ($getBrandIdQueryProducts  as $key => $value) { 
								// 	echo '<option value="' . $value["product_no"] . '">' . $value["product_name"] . '</option>';
								// }
							?>
						</select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="specDescribe" class="col-2 col-form-label">規格</label>
                        <div class="col-10">
                          <select class="form-control" id="comboProductSpec" name="product_combo_spec">
							<option value="">請選擇規格</option>
						  </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">加購價</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="comboProductPrice"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">開始時間</label>
                        <div class="col-10">
                            <input type="datetime-local" class="form-control" id="comboProductStart" value='<?=$defaultTime?>'/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">結束時間</label>
                        <div class="col-10">
                            <input type="datetime-local" class="form-control" id="comboProductEnd" value='<?=$defaultTime?>'/>
                        </div>
                    </div>
                    
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="comboCreateConfirm" type="button" class="btn-sm btn-primary">確認新增</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>

            <!-- Modal 加價購編輯 -->
            <div class="modal fade" id="producrComboEdit" tabindex="-1" role="dialog" aria-labelledby="comboEdit" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="specEdit">修改加價購</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label for="specInfo" class="col-2 col-form-label">產品</label>
                      <div class="col-10">
						<select class="form-control" id="comboProductEdit" name="product_combo_info_edit" onchange="getComboSpec(this.value, 'comboProductSpecEdit', '')">
							<option value="">請選擇產品</option>
							<?php
                // a. 所有供應商底下加購價
								$sql ="select * from product";
								$comboProduct = $db->DB_Query($sql);
								foreach ($comboProduct as $key => $value) {
                  echo '<option value="' . $value["product_no"] . '">' . $value["product_name"] . '</option>';
                }  

                // b. 同供應商底下加購價  
                // foreach ($getBrandIdQueryProducts  as $key => $value) {   
								// 	echo '<option value="' . $value["product_no"] . '">' . $value["product_name"] . '</option>';
								// }
							?>
						</select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="specDescribe" class="col-2 col-form-label">規格</label>
                        <div class="col-10">
                          <select class="form-control" id="comboProductSpecEdit" name="product_combo_spec_edit">
							<option value="">請選擇規格</option>
						  </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">優惠</label>
                        <div class="col-10">
                          <div class="input-group"><span class="input-group-addon">$</span>
                            <input type="number" class="form-control" id="comboProductPriceEdit"><span class="input-group-addon">.00</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">開始時間</label>
                        <div class="col-10">
                            <input type="datetime-local" class="form-control" id="comboProductStartEdit" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="productPrice3">結束時間</label>
                        <div class="col-10">
                            <input type="datetime-local" class="form-control" id="comboProductEndEdit" />
                        </div>
                    </div>
                  </div>  <!-- end modal-body -->
                  <div class="modal-footer">
                    <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
                    <button id="comboEditConfirm" type="button" class="btn-sm btn-primary">確認修改</button>
                  </div>
                </div>   <!-- end modal-content -->
              </div>
            </div>
		<!-- ========modal========== -->

      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

  <script type="text/javascript">
    $(document).ready(function(){
        // var content = '<?php //echo $result["product_content"]; ?>' ;
        var content=<?php echo $jsonContent; ?>;
        if(content != "" ){
          $("div.note-editable").html(content);  
          $("div.note-placeholder").css("display","none");
        }else{
          // console.log("跑錯邊");
        }
        
        // JSON.parse(res)
        // console.log(content);
    });
  </script>

<?php require_once("module/footer.php"); ?>