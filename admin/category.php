<?php require_once("module/header.php"); 
		date_default_timezone_set("Asia/Taipei");
		require_once('../model/DB.php');
		$db = new DB();

		// 360 品牌
		$query360 = $db->DB_Query("select * from category where cate_360_status = 1 and cate_level = 2 order by cate_updatetime");
		$query360primary = $db->DB_Query("select * from category where cate_360_status = 1 and cate_360_main = 1 and cate_level = 2");
	
		$primary360ID = '';
		if (count($query360primary) == 0) {
			$primary360ID = 0 ;// 沒有設定
		} else {
			$primary360ID = $query360primary[0]['cate_no'] ;
		}
?>

<?php ini_set ("soap.wsdl_cache_enabled", "0"); //去除圖片會有抓取cache 暫存圖檔 讓網頁重新抓取?> 


      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">品牌管理</li>
          </ul>
        </div>
      </div>
      <section class="charts category">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">品牌管理</h1>
          </header>
          <div class="row">        
				<!-- category==== -->
			<div class="col-12">
				<div class="card">
					<div class="container">
						<div class="cresateBar">
							<div class="row mb-5">
								<div class="col-12">
									<h2 class="h3 bg-danger text-white text-center py-2">360 主打品牌</h2>	
									<div class="d-flex justify-content-between align-items-center">
											<div class="col-8">
													<div class="row">
															<select name="primary360Sel" class="form-control" id="primary360">
																<option value="" disabled selected="">選擇品牌</option>
																<?php foreach ($query360 as $key360 => $value360) { ?>
																<option value="<?php echo $value360["cate_no"] ?>" <?php if($value360['cate_360_main'] == 1){echo "selected";}?>><?php echo $value360["cate_name"]; ?></option>
																<?php } ?>
															</select> 
														  <input type="hidden" id="currentPrimary360ID" value="<?= $primary360ID ?>">               
													</div>
											</div>
											<div class="select360PrimaryBtn ml-2">
													<button id="selectPrimary360Confirm" type="button" class="btn-sm btn-outline-success buttonDisabled text-white" disabled>儲存更新</button>
											</div>                        
									</div>
								</div>
								
							</div>


							<h2 class="h3 bg-warning text-white text-center py-2">供應商 & 品牌設定</h2>
							<div class="row">
								<div class="col-4">
										<input type="text" class="form-control form-control-sm" id="createCateName" placeholder="新增分類">	
								</div>
								<div class="col-4">
										<select class="form-control form-control-sm" id="cate_level">
			                              	<option value="" disabled selected="">選擇上層分類</option>
			                              	<option value="62" >第一層分類</option>
			                              	<?php  
					                    	
			                              		$query = $db->DB_Query("select * from category where cate_parents = '62' and cate_level = 1 order by cate_sort asc"); 
			                              		foreach ($query as $key => $value) {
			                              	?>
			                              	<option value="<?php echo $value["cate_no"] ?>" ><?php echo $value["cate_name"]; ?></option>
			                              	<?php		
			                              		}
			                              	?>
			                          	</select>
								</div>
								<div class="col-4 text-right">
										<button id="createCate" type="button" class="btn-sm btn-outline-success">新增</button>
								</div>
							</div>
						</div>
						
						<div id="accordion" role="tablist">
					
						  			<!-- ==========父層============ -->
						  <?php 
				    	    	$query = $db->DB_Query("select * from category where cate_parents = '62' and cate_level = 1 order by cate_sort asc"); 
				    	    	for($i=0;$i<count($query);$i++){   	
						  ?>
						  <div class="catebox" style="margin-bottom:20px">
						    <div class="card-header" role="tab" id="<?php echo 'heading'.($i+1); ?>"> 
						    	<div class="row cateFather">
							    	<div class="col-6">
							    		<h5 class="mb-0">
								        	<a class="cateFatherInfo" data-toggle="collapse" href="<?php echo'#collapse'.($i+1); ?>" aria-expanded="true" aria-controls="<?php echo 'collapse'.($i+1); ?>" ><i class="fa fa-angle-down"></i><img src="<?php if($query[$i]['cate_logo_img'] != ""){
								        		echo '../'.$query[$i]['cate_logo_img'];
								        		}else{ echo '../image/category/default.png';} ?>" class="cateImg img-fluid" ><?php echo $query[$i]["cate_name"]; ?><input type="hidden" name="cate_no" value="<?php echo $query[$i]['cate_no']; ?>"></a>
									    </h5>
							    	</div>
							    	<div class="col-6 text-right">
							    		<span class="edit"><button class="cateFatherEdit" data-toggle="modal" data-target="#cateEditModel">編輯</button></span><span class="edit"><button class="cateFatherDelete">刪除</button></span>
							    	</div>
								</div>
						    </div>
						    <div id="<?php echo 'collapse'.($i+1); ?>" class="collapse" role="tabpanel" aria-labelledby="<?php echo 'heading'.($i+1); ?>" data-parent="#accordion">
										<!-- ==========子層============ -->
								<?php 
										$sql ="select * from category where cate_parents= '".$query[$i]["cate_no"]."' order by cate_sort asc";
					    	    		$child = $db->DB_Query($sql);
					    	    		for($j=0;$j<count($child);$j++){
								 ?>	

							      <div class="row cate-level">
							      	<!-- <div class="col-sm-1"></div> -->
						    		<div class="col-6">
							      	 	<div class="card-body"><img src="<?php if($child[$j]['cate_logo_img'] != ""){
								        		echo '../'.$child[$j]['cate_logo_img'];
								        		}else{ echo '../image/category/default.png';}  ?>" class="cateImg img-fluid" ><?php echo $child[$j]["cate_name"] ; ?><input type="hidden" name="cate_no" value="<?php echo $child[$j]['cate_no']; ?>"></div>
							      	</div>
							      	<div class="col-6 text-right" >
												<?php if ($child[$j]['cate_360_main'] == 1) {?> 
													<span class="badge badge-pill badge-danger">主打</span>
												<?php } ?>
												<?php if ($child[$j]['cate_360_status'] == 1) {?> 
												<span class="badge badge-pill badge-primary">360品牌</span>
												<?php } ?>
						      			<span class="edit"><button class="cateChildEdit" data-toggle="modal" data-target="#cateEditModel">編輯</button></span><span class="edit"><button class="cateDelete">刪除</button></span>
						      		</div>
						    	</div>
						    	<?php  
									}  //<------end 子層搜尋
						    	?>
						    	<div class="line"></div>
						    </div>
						   </div> 
						    <?php   	
							} //<-----end 父層搜尋
						    ?>

						</div>
                  </div>
				</div>
             </div>        	
            	<!-- category==== -->


			<!-- Modal -->
			<div class="modal fade" id="cateEditModel" tabindex="-1" role="dialog" aria-labelledby="cateNewModel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="cateNewModel">分類編輯</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
				        <div class="form-group row">
			        		<label for="cateNewName" class="col-3 col-form-label">分類名稱</label>
							<div class="col-9">
							  <input class="form-control" type="text" id="cateNewName" placeholder="輸入新的分類名稱">
							</div>
	                    </div>
	                    <div class="form-group row">
			        		<label for="cateEditFatherSelect" class="col-3 col-form-label">分類層級</label>
							<div class="col-9">
							  <select class="form-control" id="cateEditFatherSelect">
								<option value disabled selected>選擇上層分類</option>
								<option value="62" >第一層分類</option>
							  </select>
							</div>
	                    </div>
	                    <div class="line"></div>
	                     <div class="form-group row">
			        		<label class="col-3 col-form-label">Logo</label>
							<div class="col-3 text-center">
						 	  <button id="logoUpload" class="btn-sm btn-outline-secondary cateLogoUpload">檔案上傳</button>	
							  <input type="file" name="cate_logo" style="display:none;" class="form-control">
							  <img src="" id="temporaryLogo" class="img-fluid" style="margin: 10px 0;">
							  <img id="cateLogo" src=""  class="imgShow img-fluid" style="display:none;">
							  <button id="logoDelete" style="display:none;" class="btn-sm btn-outline-danger cateLogoDelete">刪除</button>
							</div>
							<div class="col-6 imgInfo">
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸需求*</div>
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">800x800(去背)</div>
                            </div>
	                    </div>
	                    <div class="line"></div>
	                    <div class="form-group row">
			        		<label class="col-3 col-form-label">Banner</label>
							<div class="col-3 text-center">
							  <button id="bannerUpload" class="btn-sm btn-outline-secondary cateBannerUpload">檔案上傳</button>	
							  <input type="file" name="cate_banner" style="display:none;" class="form-control">
							  <img src="" id="temporaryBanner" class="img-fluid" style="margin: 10px 0;">
							  <img id="cateBanner" src=""  class="imgShow img-fluid" style="display:none;">
							   <button id="bannerDelete" style="display:none;" class="btn-sm btn-outline-danger cateBannerDelete">刪除</button>
							</div>
							<div class="col-6 imgInfo">
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">*圖片尺寸需求*</div>
                                <div class="text-center" style="color:rgba(0,0,0,.4);transform: scale(.9);transform-origin:center;">1920x480<!-- <sub>px</sub> --></div>
                            </div>
	                    </div>
	                    <div class="line"></div>
								
								<div class="form-group row" id="cate360area">
									<label for="cate360Status" class="col-3 col-form-label">360 狀態</label>
									<div class="col-8">
										<div class="row">
										<label class="col-12 col-form-label switchButtonColor"><input id="cate360Status" type="checkbox" name="my-checkbox"></label>
										</div>
									</div>

									<label for="cate360url" class="col-3 col-form-label">360 網址</label>
									<div class="col-9">
										<input class="form-control" type="text" id="cate360url" placeholder="輸入連結網址">
									</div>

									
								</div>
								<div class="line"></div>

	                    <div class="form-group row">
			        		<label for="cateDescribe" class="col-3 col-form-label">說明</label>
							<div class="col-9">
								<textarea id="cateDescribe" name="cate_describe" class="form-control" placeholder="請輸入分類描述(50字以內)" rows="3"></textarea>
							  <!-- <input id="cateDescribe" type="text" name="cate_describe" class="form-control" placeholder="請輸入分類描述"> -->
							</div>
	                    </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn-sm btn-secondary" data-dismiss="modal">取消</button>
			        <button type="button" class="btn-sm btn-primary" id="editCateNameConfirm">儲存設定</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- Modal -->

			       
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>


<?php require_once("module/footer.php"); ?>