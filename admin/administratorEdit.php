<?php 
require_once("module/header.php"); 
require_once('../model/DB.php');
date_default_timezone_set("Asia/Taipei");
$db = new DB();
$sql = "select * from admin where adm_no ='".$_REQUEST['ID']."'" ;
$result = $db->DB_Query($sql);
?>
      <div class="breadcrumb-holder">   
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="administrator.php">管理員</a></li>
            <li class="breadcrumb-item active">管理員資料編輯</li>
          </ul>
        </div>
      </div>
      <section class="charts administratorEdit">
        <div class="container-fluid">
          <header> 
            <h1 class="h3">管理員資料編輯</h1>
          </header>
          <div class="row">
            <div class="col-lg-12">
            	<!-- =====card===== -->
            	<div class="card">
	                <div class="card-block">
	                	<div class="form-group row">
		                      <label class="col-2 col-form-label">管理員編號：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo $_REQUEST["ID"]; ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label class="col-2 col-form-label">管理員帳號：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input id="adm_email" class="form-control" type="text" value="<?php echo $result[0]['adm_email']; ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label class="col-2 col-form-label">加入日期：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo date("Y-m-d H:i:s",$result[0]["adm_createtime"]); ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label class="col-2 col-form-label">最後登入時間：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      	 		<label  class="col-12 col-form-label"><input class="form-control" type="text" value="<?php echo date("Y-m-d H:i:s",$result[0]["adm_lastLoginTime"]) ; ?>" disabled></label>
		                      	 	</div>
		                      </div>
	                    </div>
	                	<div class="form-group row">
		                      <label for="nameEdit" class="col-2 col-form-label"><span class="text-danger">*</span>名稱：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      			<label  class="col-12 col-form-label"><input class="form-control" type="text" id="nameEdit" value=<?php echo $result[0]["adm_name"]; ?>></label>	
		                          	</div>
		                      </div>
	                    </div>
	                    <div class="form-group row">
		                      <label for="admAuthLevelEdit" class="col-2 col-form-label">權限：</label>
		                      <div class="col-4">
		                      		<div class="row">
		                      			<label  class="col-12 col-form-label">
		                      				<select class="form-control" id="admAuthLevelEdit">
				                              	<option value="" disabled>選擇管理者權限</option>
				                              	<option value="1" >產品 / 訂單</option>
				                              	<option value="2" >報表 / 使用者 / 匯入匯出</option>
				                              	<option value="3" >最高權限</option>
				                          	</select>
				                          	<script type="text/javascript">
				                          		 $("#admAuthLevelEdit").val(<?php echo $result[0]["adm_authLevel"]; ?>);
				                          	</script>
		                      			</label>
		                      		</div>
		                      		
		                      </div>
	                    </div>
	                    <div class="form-group row">
	                         <label for="admStatus" class="col-2 col-form-label">狀態</label>
	                         <div class="col-3">
	                         		<div class="row">
	                         			<label  class="col-12 col-form-label switchButtonColor">
	                         				 <input id="admStatus" type="checkbox" name="my-checkbox" 
							  				<?php if($result[0]['adm_status']){echo "checked" ;}else{} ?> >
	                         			</label>
	                         		</div>
	                         </div>
	                    </div>
	                    <div class="modal-footer">
		                    <button id="admCancel" type="button" class="btn-sm btn-secondary">取消</button>
		                    <button id="admUpdate" type="button" class="btn-sm btn-success">儲存編輯</button>
	                  	</div>
	                </div>
              </div>
            	<!-- =====card===== -->
            </div> 
          </div>
        </div>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Your company &copy; 2017-2019</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">Ne-Plus</a></p>
              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>

<?php require_once("module/footer.php"); ?>