<?php require_once("module/header.php"); 

// 免運設定
$cartSql ="SELECT * from cart_set where cs_no = 1";
$cartPriceResult = $db -> DB_Query($cartSql);
$cartPrice = $cartPriceResult[0]['cs_price']; //免運門檻
$cartCargo = $cartPriceResult[0]['cs_cargo']; //運費金額
?>
<script>
	var cartPrice = parseInt("<?php echo $cartPrice; ?>");
</script>

	<section class="purchase">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_cart_cart?>
			
		</ol>
		<div class="container">
			
			<div class="row cartMenu">
				<div class="col-4 step active">
					<li><span>1.</span> <?=$lang_cart_cart_list?></li>
				</div>
				<div class="col-4 step">
					<li><span>2.</span> <?=$lang_cart_fill_form?></li>
				</div>
				<div class="col-4 step">
					<li><span>3.</span> <?=$lang_cart_order_complete?></li>
				</div>
				
			</div><!-- .cartMenu.row -->
			<h3><?=$lang_cart_mycart?></h3>	
			
			<div id="cart" class="row">
				<div class="col-12 table-responsive">
					<table class="table" style='border-bottom: 1px solid #e9ecef;'> <!-- style="display: none;" -->
					  <thead id="thead-rwd-hide">
					  <!-- =======桌機======== -->
					    <tr>
					      <th scope="col"><?=$lang_cart_item?></th>
					      <th scope="col"><?=$lang_cart_product?></th>
					      <th scope="col"><?=$lang_cart_spec2?></th>
					      <th scope="col"><?=$lang_cart_brand?></th>
					      <th scope="col"><?=$lang_cart_qty?></th>
					      <th scope="col"><?=$lang_cart_order_unitprice?></th>
					      <th scope="col"><?=$lang_cart_order_fee?></th>
					      <th scope="col"></th>
					    </tr>
					  </thead>
					  <tbody>
					  <?php
					  if(count($orderList)==0){ //沒有購物清單
					  ?>
					  <tr class="text-center"><td colspan="8" style="height:100px;line-height: 100px;"><?=$lang_cart_empty2?></td></tr>
					  <?php	
					  }
					  else {
					   $productCountArr ; //計算combo 數量不重複(同產品不同規格的商品只會出現一次加購選項)
					  foreach ($orderList as $key => $value) {
						$keyCombo = $key;
					  	$table = "product";	
						$productOrderShow = productQuery($table,$value["product_no"]);
						$specOrderShow = findSpecInfo($value["product_spec_no"]);
						$brandOrderShow = findBrandsInfo($value["product_no"]);
						$couponTagShow = findCounponUseTag($value["product_no"]);

						$price = '';
				      	if($specOrderShow[0]["product_spec_price2"] ==0 && $specOrderShow[0]["product_spec_price3"] ==0){ //原價
				      		$price = $specOrderShow[0]["product_spec_price1"] ;
				      	}else{ //有折扣
				      		if($specOrderShow[0]["product_spec_price3"] !=0){
				      			$price = $specOrderShow[0]["product_spec_price3"];
				      		}else{
				      			$price = $specOrderShow[0]["product_spec_price2"];
				      		}
				      	}
				      	

				      	//判斷如果商品再點選時發現庫存已經缺貨
				      	if($specOrderShow[0]["product_stock"] < $value["quanty"]){ //商品數量大於庫存
				      		$quanty = $specOrderShow[0]["product_stock"];
				      	}else{
				      		$quanty = $value["quanty"];
				      	}

				      	// ==商品小計===
				      	$charge = ((int)$price*(int)$quanty);

					  ?>	 
					  <!-- =======桌機======== -->
					  	 <tr class="orderItem">
					      <th scope="row" class="img-rwd"><img src="<?php echo $productOrderShow['img1']; ?>" style="max-width:80px;"></th>
					      <td class="orderName" style="vertical-align: middle;">
					      	<a href="product-detail.php?product_no=<?php echo $productOrderShow['product_no'];?>"><?php echo $productOrderShow["product_name"]; ?></a>
					      	<!-- ===rwd==== -->
					      	<div class="order-rwd my-2"><?php echo $specOrderShow[0]["product_spec_info"]; ?></div>
					      	<div class="order-rwd"><?=$lang_product_currency?> <span ><?php echo $charge; ?></span><span class="couponDiscountShow" style="display:none;color:#f00;">-<?=$lang_product_currency?> <span class="couponDiscountPriceShow"></span></span></div>
					      </td>
					      <td class="orderSpec" style="vertical-align: middle;"><?php echo $specOrderShow[0]["product_spec_info"]; ?></td>
					      <td class="orderBrand" style="vertical-align: middle;"><?php echo  $brandOrderShow["brand_name"][0]; ?></td>
					      <td class="orderQty" style="vertical-align: middle;"><input class="orderQuantySelect form-control form-control-sm" type="number" name="quanty" value='<?php echo $quanty ; ?>' min="1" max="<?php echo $specOrderShow[0]["product_stock"]; ?>" style="max-width: 60px;"></td>
					      <td class="orignPrice" style="vertical-align: middle;"><?=$lang_product_currency?> <span><?php  echo $price;?></span></td>
					      <td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span class="charge"><?php echo $charge; ?></span><span class="couponDiscountShow" style="display:none;">-<?=$lang_product_currency?> <span class="couponDiscountPriceShow"></span></span></td>
					      <td style="vertical-align: middle;">
					      	<button class="cancelOrderBtn" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_delete?>"><i class="fa fa-times" aria-hidden="true"></i></button>
					      	<input type="hidden" name="product_no" value="<?php echo $productOrderShow['product_no'];?>">
					      	<input type="hidden" name="product_name" value="<?php echo $productOrderShow["product_name"];?>">
					      	<input type="hidden" name="product_spec_no" value="<?php echo $value['product_spec_no'];?>">
					      	<input type="hidden" name="product_spec_info" value="<?php echo $specOrderShow[0]["product_spec_info"];?>">
					      	<input type="hidden" name="price" value="<?php echo $price; ?>">
					      	<input type="hidden" name="bcate_no" value="<?php echo $brandOrderShow["bcate_no"][0];?>">
					      	<input type="hidden" name="bcate_name" value="<?php echo $brandOrderShow["brand_name"][0];?>">	
					      	<input type="hidden" name="vcate_no" value="<?php echo $brandOrderShow["vcate_no"][0];?>">
					      	<input type="hidden" name="vcate_name" value="<?php echo $brandOrderShow["vendor_name"][0];?>">			
					      	<input type="hidden" name="coupon_no" value="">
					      	<input type="hidden" name="coupon_name" value="">
					      	<input type="hidden" name="coupon_code" value="">
					      	<input type="hidden" name="coupon_discount" value="">
					      	<input type="hidden" name="order_detail_price_discount" value="">	      	
					      	
							<div style="display: none;" class="<?php echo 'couponArea'; ?>">
					      	<?php foreach ($couponTagShow as $couponkey => $couponvalue){ ?>
					      		<input type="hidden" name="couponCodeUse" value="<?php echo $couponvalue['coupon_code']; ?>">
					      	<?php } ?>
					      	</div>	
							
					      </td>
						<!-- ======rwd===== -->
							<div class="orderNmae-rwd"><a href="product-detail.php?product_no=<?php echo $productOrderShow['product_no'];?>"><?php echo $productOrderShow["product_name"]; ?></a></div>

					    </tr>
					   <?php
						// 加價購
						$comboProducts = get_comboProducts_by_productId($productOrderShow['product_no']);
						if ($comboProducts && count($comboProducts) >= 1 && !isset($productCountArr[$value['product_no']]['combo']) ) {
							$productCountArr[$value['product_no']]['combo'] = 1; // 
							?>
							<tr>
								<td colspan="8" class="border-top-0 pt-0">
								<?php
								//foreach ($comboProducts as $key => $value) {
									?>
									<!-- <table class="table table-striped">
										<tr>
											<td><input type="checkbox" class="addRemoveComboToCart" data-key="<?=$value["id"]?>" data-charge="<?=$value["product_spec_price"]?>" <?=(in_array($value["id"], $orderComboList)) ? 'checked' : ''?> />加購</td>
											<th scope="row" class="img-rwd"><img src="<?=$value["img1"]?>" style="max-width:80px;"></th>
											<td><img src="" style="max-width: 100px;" /></td>
											<td class="orderName" style="vertical-align: middle;">
												<?=$value["product_name"]?><br />
												<?=$value["product_spec_info"]?> -->
												<!-- ===rwd==== -->
												<!-- <div class="order-rwd my-2"><?=$value["product_spec_info"]?></div>
												<div class="order-rwd"><?=$lang_product_currency?> <span><?=$value["product_spec_price"]?></span></div>
											</td>
											<td class="orderSpec" style="vertical-align: middle;"><?=$value["product_spec_info"]?></td>
											<td class="orderSpec" style="vertical-align: middle;"><?=$value["product_spec_price"]?></td>
										</tr>
									</table> -->
									<?php
								// }
								?>

								<div class="w-100 row border border-light border-top-0 bg-light" style="margin:auto;">	
									<div class="col-12 comporducts p-0">
										<div class="comporductsTitle bg-secondary text-white text-center border-0 ">
											可加購商品
										</div>
									</div>
								<?php
								 foreach($comboProducts as $key => $value) {
									$comboorderQty = 0;
									if(isset($_COOKIE["comboorders"])){
										$comboorders = json_decode($_COOKIE["comboorders"], true);
										if(in_array($value["id"], $orderComboList) && count($comboorders)!=0){
											$comboorderQty = $comboorders[$value["id"]]['quanty']; 
										} 
									}
									?>
										<div class="comporducts col-lg-6">
											<div class="d-flex align-items-center border-light">
												<div class="comporductsItem-select">
													<input type="radio" name="comboSelect-<?=$keyCombo?>" class="addRemoveComboToCart" data-key="<?=$value["id"]?>" data-charge="<?=$value["product_spec_price"]?>"  data-selected="<?=(in_array($value["id"], $orderComboList)) ? 'on' : ''?>" <?=(in_array($value["id"], $orderComboList)) ? 'checked' : ''?> />
												</div>
												<div class="comporductsItem-img">
													<img src="<?=$value["img1"]?>" class="img-fluid">
												</div>
												<div class="comporductsItem-desc comporductsItem-withOption">
													<div class="comporductsItem-title">
														<a href="product-detail.php?product_no=<?=$value["product_no"]?>"><?=$value["product_name"]?></a>
													</div>
													<div class="comporductsItem-specInfo text-secondary">規格: <?=$value["product_spec_info"]?></div>
													<!-- <div >數量: 1</div> -->
													<div class="text-secondary">加購價: $<?=$value["product_spec_price"]?></div>
											<?php if(in_array($value["id"], $orderComboList)) { ?>		
													<div class="text-danger">費用: $<span class="comporducts-price"><?=(int)$value["product_spec_price"]*(int)$comboorderQty?></span> (加購價*數量)</div>
											<?php } ?>	
												</div>
											<?php if(in_array($value["id"], $orderComboList)) { ?>	
												<div class="comporductsItem-option d-flex flex-column justify-content-between flex-fill">
													<input class="comporductsItemQty form-control form-control-sm" type="number" name="quanty" value='<?php echo $comboorderQty; ?>' min="1" max="<?php echo $quanty; ?>" style="max-width: 60px;">
													<input type="hidden" class="comboMax" value="<?=(int)$value["product_stock"]?>">
												</div>

												<!-- 加購資料儲存session -->
												<div class="d-none comporductsTemp">
													<input type="hidden" name="comporducts_id" value="<?=$value["id"]?>">
													<input type="hidden" name="comporducts_product_no" value="<?=$value["product_no"]?>">
													<input type="hidden" name="comporducts_product_name" value="<?=$value["product_name"]?>">
													<input type="hidden" name="comporducts_product_spec_info" value="<?=$value["product_spec_info"]?>">
													<input type="hidden" name="comporducts_product_spec_no" value="<?=$value["product_spec_no"]?>">
													<input type="hidden" name="comporducts_price" value="<?=(int)$value["product_spec_price"]?>">
													<input type="hidden" name="comporducts_quanty" value="<?=(int)$comboorderQty?>">
													<input type="hidden" name="comporducts_vendorNo" value="<?=$value["vendor_no"];?>">
													<input type="hidden" name="comporducts_vendorName" value="<?=$value["parent_name"];?>">
													<input type="hidden" name="comporducts_cateNo" value="<?=$value["cate_no"];?>">
													<input type="hidden" name="comporducts_cateName" value="<?=$value["cate_name"];?>">
												</div>
												
											<?php } ?>	
											</div>
										</div>
									<?php
								}
								?>
								</div>

								</td>
							</tr>
							<?php
						}
						?>
					  <?php } //end foreach loop 
						}//--- end if 判斷是否有購物清單
					  ?>
					  </tbody>
					</table>
					
					<?php  
						 if(count($orderList) !=0){ //有購物清單
					?>
					<div class="row">
					<div class="col-12 col-md-8 ">
						<img src="image/frontend/320x100.jpg"  width="100%" style="max-width: 320px;margin-top: 60px;"/>
					</div>
					
					<div class="col-12 col-md-4">
						
							<table class="table orderFinal text-right">
							  <thead >
							    <tr >
							      <th class="text-center" colspan="3"><?=$lang_cart_calc?></th>
							    </tr>
							  </thead>
							  <tbody>
							  	<tr>
							  		<td colspan="3">
						    			<div class="input-group">
										  <input name="coupon_code_keyup" type="text" class="form-control form-control-sm" placeholder="<?=$lang_cart_couponcode?>" aria-label="" aria-describedby="basic-addon1"> &nbsp;
										  <div class="input-group-append">
										    <button id="couponBtn" class="btn btn-outline-secondary btn-sm" type="button"><?=$lang_confirm?></button>
										    <input type="hidden" name="coupon_no_currentUse" value="">
										  </div>
										</div>
							    	</td>
							  	</tr>
							    <tr>
								    
							      <th class="sum-row"><?=$lang_cart_transfee?>：</th>
							      <td></td>
							      <td class="cargoUse"><span><?=$lang_product_currency?> </span><span id="orderTotalCargo"><?php echo (int)$cartCargo ;?></span></td>
							      <td class="cargoNoUse" style="display: none;"><span></span><span><?=$lang_cart_notransfee?></span></td>
							    </tr>
								<tr class="coupon" style="display: none;">
									<th class="sum-row"><?=$lang_cart_coupon?>：</th>
									<td class="text-center"><span class="couponUseName"></span><p id="couponUseFailInfo" class="mt-2"></p></td>
									<td class="couponUse" style="vertical-align: middle;">
										<span>-</span><span><?=$lang_product_currency?> </span><span id="orderCouponDiscount"></span>
									</td>
							    </tr>
							    <tr>
							      <th class="sum-row" ><?=$lang_cart_total?>：</th>
							      <td></td>
							      <td style="color:#db363d; width: 100px;"><span><?=$lang_product_currency?> </span><span id="orderTotalPrice"></span></td>
							    </tr>
							  </tbody>
							</table>
						
					</div><!-- col-6 -->
					</div><!-- row -->
					
					<?php } ?>
				</div><!-- .table-responsive -->
				
					<?php if(count($orderList) !=0){ //有購物清單 ?>
				<a class="btn btn-outline-secondary btn-sm submitBtn" id="cartBtnConfrim" href="deliver.php" role="button"><?=$lang_cart_fill_form?></a>
					<?php } ?>
			</div><!-- .cart -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>