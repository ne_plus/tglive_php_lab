<?php require_once("module/header.php"); 
	// ---會員資訊----
	if(isset($_SESSION["memNo"])){
		include("Frontframwork/memInfo.php");	
	}else{
		header( 'Location:index.php');
	}

	//---訂單茲訓----
	include("Frontframwork/memHistoryDataServer.php");
?>

	<section class="purchase" style="margin-bottom: 0;">
<!--
		<div class="main-menu_memSideBar">
          <ul id="side-main-menu" class="side-menu list-unstyled mt-3">                  
            <li><a href="memcenter.php" class="memberSelect"><i class="fa fa-user" aria-hidden="true"></i> <span><?=$lang_member_info?><</span></a></li>
            <li><a href="memhistory.php" class="adminSelect  active"><i class="fa fa-history" aria-hidden="true"></i> <span>購物紀錄</span></a></li>
          </ul>
        </div>
-->
        <div class="memcenter">
			<div class="container">
				<div class="row" >	
					<div class="col-12">						
						<h3><?=$lang_member_history2?></h3>
						<div class="row">
							<div class="col-12">
								<div id="accordion" role="tablist">
									<div class="row mb-2">
										<div class="historyCount col-5" style="line-height: 30px;"><?=$lang_member_totalrecord_1?>: <?php echo $array["recordsTotal"]; ?> <?=$lang_member_totalrecord_2?></div>
										 <div class="col-7 text-right">
										 	<form class="form-inline justify-content-end" id="searchOrderForm" action="" method="get">
				     							 <input class="form-control form-control-sm  searchOrder" type="text" placeholder="<?=$lang_cart_search_hint?>" aria-label="Search" name="orderSearch" value="<?php if(isset($_REQUEST["orderSearch"])){echo $_REQUEST["orderSearch"];} ?>">
				      								<button id="searchOrderButton"><i class="fas fa-search searchIcon"></i></button>
				    						</form>
										 </div>
									</div>
								<?php		
									if(count($order) != 0 ){ 
										foreach ($order as $key => $value) {
											$orderStatus = "";	
											switch ($value["order_status"]){
				                                  case "0": 
				                                    $orderStatus = $lang_member_order_status_0;
				                                    break;
				                                  case "1": 
				                                    $orderStatus = $lang_member_order_status_1;
				                                    break; 
				                                  case "2": 
				                                    $orderStatus = $lang_member_order_status_2;
				                                    break; 
				                                  case "3": 
				                                    $orderStatus = $lang_member_order_status_3;
				                                    break; 
				                                  case "4": 
				                                    $orderStatus = $lang_member_order_status_4;
				                                    break; 
				                                  case "5": 
				                                    $orderStatus = $lang_member_order_status_5;
				                                    break;          
			                                }
										
								?>
								  <div class="card shop-history">
								    <div class="card-header" role="tab" id="<?php echo 'heading'.$key; ?>">
								    	<div class="row">
								    		<div class="col-lg col-12">
												<div><?=$lang_member_order_status?> : </div><div class="order_group mb-2"><?php echo $value["order_group"]; ?></div>
											</div>
											<div class="col-lg col-12">
												<div><?=$lang_cart_order_time?> : </div><div class="order_createtime mb-2"><?php echo date("Y-m-d H:i:s",$value["order_createtime"]); ?></div>
											</div>
											<div class="col-lg col-12">
												<div><?=$lang_member_receiver?> : </div><div class="order_recipient mb-2"><?php echo $value["order_recipient"]; ?></div>
											</div>
											<div class="col-lg col-12">
												<div><?=$lang_cart_transfee?> : </div><div class="order_cargo mb-2"><span><?=$lang_product_currency?> </span><?php echo (int)$value["order_cargo"]; ?></div>
											</div>
											<div class="col-lg col-12">
												<div><?=$lang_cart_order_price?> : </div><div class="order_price mb-2"><span><?=$lang_product_currency?> </span><?php echo (int)$value["order_price"]+(int)$value["order_cargo"]; ?></div>
											</div>
											<div class="col-lg col-12">
												<div><?=$lang_cart_order_status?> : </div><div class="order_status mb-2"><?php echo $orderStatus; ?></div>
											</div>
											<div class="col-lg col-12 text-right">
												<a class="detailBtn mb-1" data-toggle="collapse" href="<?php echo '#collapse'.$key; ?>" aria-expanded="true" aria-controls="<?php echo 'collapse'.$key; ?>">
										        <?=$lang_cart_order_detail?>
										        </a>
		
											</div>
								    	</div>  

								    </div>
								    <div id="<?php echo 'collapse'.$key; ?>" class="collapse" role="tabpanel" aria-labelledby="<?php echo 'heading'.$key; ?>" data-parent="#accordion">
								      <div class="card-body">
								      	<div class="row">
									      		<div class="col-12">
										      		<?php
											      		$payment_name = array($lang_cart_choose_payby_cc,$lang_cart_choose_payby_cc,$lang_cart_choose_payby_store,$lang_cart_choose_payby_cc_installment,$lang_cart_choose_payby_atm);
											      		
											      		if ($value["order_pay"] == 2){
																			      		//超商付款
																			      		$sql_payment_info = "SELECT * FROM `ecpay_receive_log` WHERE MerchantTradeNo = ".$value["order_group"]." and RtnCode = 10100073 ";
																			      		$payment_info_resule = $db->DB_Query($sql_payment_info);	
																			      		
																			      		if (count($payment_info_resule) > 0){
																									$payment_info =  sprintf("%s:%s",$lang_paymentinfo_cvc,$payment_info_resule[0]["CustomField1"]);
																								}
																			      		
							                  } else if ($value["order_pay"] == 4){
															      	//	ATM付款
																								$sql_payment_info = "SELECT * FROM `ecpay_receive_log` WHERE MerchantTradeNo = ".$value["order_group"]." and RtnCode = 2 ";
																								$payment_info_resule = $db->DB_Query($sql_payment_info);	
																								if (count($payment_info_resule) > 0){
																									$payment_info =  sprintf("%s:%s,%s:%s",$lang_paymentinfo_bank,$payment_info_resule[0]["CustomField2"],$lang_paymentinfo_atm,$payment_info_resule[0]["CustomField3"]);
																								}
							                  }
										      		?>
											<span><?=$lang_footer_payinfo?> : </span><span class="order_payment"><?php echo $payment_name[$value["order_pay"]]; ?></span>	<span class="order_payment_info"><?=$payment_info; ?></span>	
											</div>
											
								      		<div class="col-12">
												<span><?=$lang_cart_address?> : </span><span class="order_address"><?php echo $value["order_address"]; ?></span>	
											</div>
											<div class="col-12">
												<span><?=$lang_cart_invoice_infoemail?> : </span><span class="order_email"><?php echo $value["order_email"]; ?></span>
											</div>
											<div class="col-12">
												<span><?=$lang_member_phone2?> : </span><span class="order_email"><?php echo $value["order_tel"]; ?></span>
											</div>
								      	</div>
								      	<div class="line"></div>
						<?php  
							
								foreach ($value["detail"] as $orderDeatilkey => $orderDeatilvalue) {						
									$prcie = (int)$orderDeatilvalue["order_detail_price"]*(int)$orderDeatilvalue["order_detail_quantity"]-(int)$orderDeatilvalue["order_detail_price_discount"]*(int)$orderDeatilvalue["order_detail_quantity"];
							
						?>
								        	<div class="orderDtailBox col-12 mb-3">
								        		<div class="row">
								        			<div class="col-lg col-12"><a href="product-detail.php?product_no=<?php echo $orderDeatilvalue['product_no']; ?>"><span><?php echo $productResult[0]["product_name"]; ?></a></span> <?php if($orderDeatilvalue['order_detail_additional'] == 1){?><div class='w-100  mb-1'><span class="badge badge-info">加購商品</span></div><?php } ?> </div>
										        	<div class="col-lg col-sm-6"><?=$lang_cart_brand?> : <span><?php echo $orderDeatilvalue["cate_name"]; ?></span></div>
										        	<div class="col-lg col-sm-6"><?=$lang_cart_spec?> : <span><?php echo $orderDeatilvalue["product_spec"]; ?></span></div>
										        	<div class="col-lg col-sm-6"><?=$lang_cart_qty?> : <span><?php echo $orderDeatilvalue["order_detail_quantity"]; ?></span></div>
										        	<div class="col-lg col-sm-6"><?=$lang_cart_sum?> : <?=$lang_product_currency?> <span><?php echo $prcie; ?></span></div>
									        	</div>
								        	</div>
								        		
								        	
								     
						<?php  
									 } //----end foreach ($orderDeatilResult)
						?>							        
	

								      </div>
								    </div>

								  </div> <!-- end .card -->									
					    <?php 
									}//end foreach
						?>
						<div class="col-12">
            
				            <div class="shop_pagination_area mt-3">
				            	<?php 
									if(strpos($_SERVER['QUERY_STRING'],'pageNo='.$array["pageNo"])){
										// echo "有分頁";
										$path = str_replace('&pageNo='.$array["pageNo"],'',$_SERVER['QUERY_STRING']);
									}else{
										// echo "沒有"; 
										$path = $_SERVER['QUERY_STRING'];
									} 
								?>
				                <nav aria-label="Page navigation example">
								  <ul class="pagination justify-content-center">
								    <li class="page-item <?php if($array['pageNo'] == 1 ){echo 'disabled';} ?>">
								      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]-1); ?>" aria-label="Previous">
								        <span aria-hidden="true">&laquo;</span>
								        <span class="sr-only">Previous</span>
								      </a>
								    </li>
								    <?php 
								    		if($array["totalPage"] > 7){ //頁數大於 7 
								    			if($array["pageNo"]<5){ 
								    				for($p=1 ; $p<=5 ; $p++){
								    					if( $p == $array["pageNo"]){ //當頁
									?>	
												<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
									<?php
														}else{
									?>
												<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
									<?php					
														}	    					
								    				}
								    ?>
								    			<li class="page-item disabled"><a class="page-link" >...</a></li>
								    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $array["totalPage"]; ?>"><?php echo $array["totalPage"]; ?></a></li>
								    <?php				
								    			}else if(($array["totalPage"]-3) <= $array["pageNo"]){
								    ?>
								    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
								    			<li class="page-item disabled"><a class="page-link" >...</a></li>
								    <?php
								    				for($p=($array["totalPage"]-4) ; $p<=$array["totalPage"] ; $p++){
								    					if( $p == $array["pageNo"]){ //當頁
								    ?>
								    			<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
								    <?php						
								    					}else{
								    ?>
												<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
								    <?php						
								    					}							    					
								    				}
								    			}else if($array["pageNo"]>=5){
								    ?>
								    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo "1"; ?>"><?php echo "1"; ?></a></li>
								    			<li class="page-item disabled"><a class="page-link" >...</a></li>
								    <?php				
								    				for($p=($array["pageNo"]-1) ; $p<=($array["pageNo"]+1) ; $p++){
								    					if( $p == $array["pageNo"]){ //當頁
								    ?>
												<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>	
								    <?php						
								    					}else{
								    ?>
												<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
								    <?php								
								    					}	
								    				}
								    ?>
								    			<li class="page-item disabled"><a class="page-link" >...</a></li>
								    			<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $array["totalPage"]; ?>"><?php echo $array["totalPage"]; ?></a></li>
								    <?php				
								    			}
								  
								    		}else{
									    		for($p=1 ; $p<=$array["totalPage"] ; $p++){
												if( $p == $array["pageNo"]){ //當頁
									?>
											<li class="page-item active"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
									<?php				
													}else{
									?>
											<li class="page-item"><a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo $p; ?>"><?php echo $p ; ?></a></li>
									<?php			
													} 
												} //----end for 
											} //----end if	
								    ?>
								    <li class="page-item <?php if($array['pageNo'] == $array['totalPage']){echo 'disabled';} ?>">
								      <a class="page-link" href="?<?php echo $path.'&';?>pageNo=<?php echo ($array["pageNo"]+1); ?>" aria-label="Next">
								        <span aria-hidden="true">&raquo;</span>
								        <span class="sr-only">Next</span>
								      </a>
								    </li>
								  </ul>
								</nav>
				            </div>
				        </div>					
				
						<?php
					    		}elseif( isset($_REQUEST["orderSearch"]) ){ //有搜尋訂單但找不到內容   
					    ?>
								<div class="card">
					    			<div class="card-body text-center">
					    				<div class="col-12 py-5"><?=$lang_member_nohistory?></div>
					    				<div class="col-12"><a role="button" href="index.php" class="btn btn-outline-success"><?=$lang_member_goshop?></a></div>
					    			</div>
					    		</div>


						<?php  
								}else{
						?>
					    		<div class="card">
					    			<div class="card-body text-center">
					    				<div class="col-12 py-5"><?=$lang_member_nohistory2?></div>
					    				<div class="col-12"><a role="button" href="index.php" class="btn btn-outline-success"><?=$lang_member_goshop?></a></div>
					    			</div>
					    		</div>
						 <?php  

								}	//end if					 
						 ?>
								</div>		

							</div>
						</div>
						
						
					</div><!-- .col-lg-12 -->
					
					
					<!-- <a class="btn btn-outline-secondary btn-sm submitBtn" href="payment.php" role="button">填寫付款資訊</a> -->
				</div>	<!-- .info-->

				
			</div><!-- .container -->
		</div> <!-- .memcenter -->	
	</section>			

<?php require_once("module/footer.php"); ?>