<?php 

	require_once("model/DB.php"); 

	//UTF-8編碼
	header('Content-Type: text/html; charset=utf-8');

	/**
	*    Credit信用卡付款 & CVS超商代碼產生訂單
	*/
    //載入SDK
    include('plugIn/ecpay/ECPay.Payment.Integration.php');
    try {
        
    	$obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = ecpay_web;	//服務位置
        $obj->HashKey     = ecpay_hashkey;		//測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = ecpay_hashiv;		//測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = ecpay_merchantid;	//測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1';				//CheckMacValue加密類型，請固定填入1，使用SHA256加密

        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = isset($_POST["order_no"]) ? $_POST["order_no"] : "";
		$total_price = isset($_POST["total_price"]) ? sprintf("%d", $_POST["total_price"]) : 0;
		$pay = isset($_POST["pay"]) ? sprintf("%d", $_POST["pay"]) : 0;
		if ($pay != 0 && $pay != 2 && $pay != 3 && $pay != 4) $pay = 0;
		
        $obj->Send['ReturnURL']         = HTTP_SERVER . "/payment-ecpay-receive.php" ;    //付款完成通知回傳的網址
        $obj->Send['OrderResultURL']         = HTTP_SERVER . "/memhistory.php" ;    //付款完成通知回傳的網址
        $obj->Send['ClientBackURL']         = HTTP_SERVER . "/memhistory.php" ;    //付款完成導回按鈕
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                          //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
        $obj->Send['TotalAmount']       = $total_price;                              //交易金額
        $obj->Send['TradeDesc']         = "居生活優質商品" ;                          //交易描述

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "居生活優質商品", 'Price' => (int)$total_price,
                   'Currency' => "元", 'Quantity' => (int) "1", 'URL' => ""));

		if ($pay == 0 || $pay == 3) {
			//Credit信用卡分期付款延伸參數(可依系統需求選擇是否代入)
			//以下參數不可以跟信用卡定期定額參數一起設定
			$obj->Send['ChoosePayment']     = ECPay_PaymentMethod::Credit ;              //付款方式:Credit
			$obj->Send['IgnorePayment']     = ECPay_PaymentMethod::GooglePay ;           //不使用付款方式:GooglePay
			$pay_install = isset($_POST["pay_install"]) ? sprintf("%d", $_POST["pay_install"]) : 0;
			if ($pay != 3 || ($pay_install != 3 && $pay_install != 6 && $pay_install != 12)) $pay_install = 0;
			$obj->SendExtend['CreditInstallment'] = $pay_install;    //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
			$obj->SendExtend['Redeem'] = false ;           //是否使用紅利折抵，預設false
			$obj->SendExtend['UnionPay'] = false;          //是否為聯營卡，預設false;

			//Credit信用卡定期定額付款延伸參數(可依系統需求選擇是否代入)
			//以下參數不可以跟信用卡分期付款參數一起設定
			// $obj->SendExtend['PeriodAmount'] = '' ;    //每次授權金額，預設空字串
			// $obj->SendExtend['PeriodType']   = '' ;    //週期種類，預設空字串
			// $obj->SendExtend['Frequency']    = '' ;    //執行頻率，預設空字串
			// $obj->SendExtend['ExecTimes']    = '' ;    //執行次數，預設空字串
		}
        elseif ($pay == 2) {
			//CVS超商代碼延伸參數(可依系統需求選擇是否代入)
			$obj->Send['ChoosePayment']     = ECPay_PaymentMethod::CVS ;                  //付款方式:CVS超商代碼
			$obj->SendExtend['Desc_1']            = '';      //交易描述1 會顯示在超商繳費平台的螢幕上。預設空值
			$obj->SendExtend['Desc_2']            = '';      //交易描述2 會顯示在超商繳費平台的螢幕上。預設空值
			$obj->SendExtend['Desc_3']            = '';      //交易描述3 會顯示在超商繳費平台的螢幕上。預設空值
			$obj->SendExtend['Desc_4']            = '';      //交易描述4 會顯示在超商繳費平台的螢幕上。預設空值
			$obj->SendExtend['PaymentInfoURL']    = HTTP_SERVER . "/payment-ecpay-receive.php" ;      //預設空值
			//$obj->SendExtend['ClientRedirectURL'] =  HTTP_SERVER . "/memhistory.php" ;  ;      //預設空值
			$obj->SendExtend['StoreExpireDate']   = '';      //預設空值
		}
        elseif ($pay == 4) {
			//ATM延伸參數(可依系統需求選擇是否代入)
			$obj->Send['ChoosePayment']     = ECPay_PaymentMethod::ATM ;                  //付款方式:ATM
			$obj->SendExtend['ExpireDate']            = '3';      //交易描述1 會顯示在超商繳費平台的螢幕上。預設空值
			$obj->SendExtend['PaymentInfoURL']    = HTTP_SERVER . "/payment-ecpay-receive.php" ;      //預設空值
			//$obj->SendExtend['ClientRedirectURL'] =  HTTP_SERVER . "/memhistory.php" ;       //預設空值
		}
		
        # 電子發票參數
        
        $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
        $obj->SendExtend['RelateNumber'] = "IV".time();
        $obj->SendExtend['CustomerID'] = isset($_POST["order_invoice_no"]) ? $_POST["order_invoice_no"] : "";
        $obj->SendExtend['CustomerIdentifier'] = isset($_POST["order_invoice_no"]) ? $_POST["order_invoice_no"] : "";
        if (isset($_POST["order_invoice_title"]) && $_POST["order_invoice_title"] != "") $obj->SendExtend['CustomerName'] = isset($_POST["order_invoice_title"]) ? $_POST["order_invoice_title"] : "";
        $obj->SendExtend['CustomerAddr'] = isset($_POST["order_member_address"]) ? preg_replace("/[^A-Za-z0-9 ]/", "", $_POST["order_member_address"] ): "";
        $obj->SendExtend['CustomerPhone'] = isset($_POST["order_member_tel"]) ? $_POST["order_member_tel"] : "";
        $obj->SendExtend['CustomerEmail'] = isset($_POST["order_member_email"]) ? $_POST["order_member_email"] : "";
        $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
        $obj->SendExtend['Print'] = isset($_POST["order_invoice_print"]) ? $_POST["order_invoice_print"] : "";
        $obj->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach ($obj->Send['Items'] as $info)
        {
            array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
        }
        $obj->SendExtend['InvoiceRemark'] = '';
        $obj->SendExtend['DelayDay'] = '7';
        $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto submit至ECPay)
        $obj->CheckOut();

    
    } catch (Exception $e) {
    	echo $e->getMessage();
    }
?>