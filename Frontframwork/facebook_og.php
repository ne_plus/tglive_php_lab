<?php  

// // =====check og webside address====
if(strpos(strtolower($_SERVER["REQUEST_URI"]),"product-detail.php?")){ //產品內頁
	$productNoOg = $_REQUEST["product_no"] ;
	$url = HTTP_SERVER.$_SERVER["REQUEST_URI"];
	get_og_by_products($url,$productNoOg);
}elseif(strpos(strtolower($_SERVER["REQUEST_URI"]),"brands.php?")){ //品牌內頁
	$cateNoOg =  $_REQUEST['cate_no']; //網址列抓取品牌標號
	$url = HTTP_SERVER.$_SERVER["REQUEST_URI"];
	get_og_by_brands($url,$cateNoOg);
}elseif(strpos(strtolower($_SERVER["REQUEST_URI"]),"styles-detail.php?")){ //文章內頁
	$articleNoOg =  $_REQUEST['pid']; //網址列抓取品牌標號
	$url = HTTP_SERVER.$_SERVER["REQUEST_URI"];
	get_og_by_article($url,$articleNoOg);
}else{ //首頁
	$url = HTTP_SERVER;
	$logoImg =  HTTP_SERVER.'/image/frontend/LogoC_og.jpg';
	index_og($url,$logoImg);
}
// =====check og webside address end====

function get_og_by_article($url,$pid){
	$db = new DB();
	$articleSelectOgSql = "select * from article where article_no= $pid ";
	$articleSelectOgResult = $db -> DB_Query($articleSelectOgSql);

	// ga meta
	echo '<meta name="description" content="'.$articleSelectOgResult[0]['article_describe'].'" />';
	echo '<meta name="keyword" content="'.$articleSelectOgResult[0]['article_title'].'" />';	

	// fb og 
	echo '<meta property="og:title" content="'.$articleSelectOgResult[0]['article_title'].'" />';
	echo '<meta property="og:description" content="'.$articleSelectOgResult[0]['article_describe'].'" />';
	echo '<meta property="og:image" content="'.HTTP_SERVER.'/'.$articleSelectOgResult[0]['article_img'].'" />';
	echo '<meta property="og:image:width" content="400" />';
	echo '<meta property="og:image:height" content="400" />';
	echo '<meta property="og:site_name" content="'.$articleSelectOgResult[0]['article_title'].'" />';
	echo '<meta property="og:type" content="website" />';
	echo '<meta property="og:locale" content="zh_TW" />';
	echo '<meta property="og:url" content="'.$url.'" />';
};
function get_og_by_brands($url,$bid){
	$db = new DB();
	$cateSelectOgSql = "select * from category where cate_no= $bid ";
	$cateSelectOgResult = $db -> DB_Query($cateSelectOgSql);

	// ga meta
	echo '<meta name="description" content="'.$cateSelectOgResult[0]['cate_describe'].'" />';
	echo '<meta name="keyword" content="'.$cateSelectOgResult[0]['cate_name'].'" />';	

	// fb og 
	echo '<meta property="og:title" content="'.$cateSelectOgResult[0]['cate_name'].'" />';
	echo '<meta property="og:description" content="'.$cateSelectOgResult[0]['cate_describe'].'" />';
	echo '<meta property="og:image" content="'.HTTP_SERVER.'/'.$cateSelectOgResult[0]['cate_logo_img'].'" />';
	echo '<meta property="og:image:width" content="400" />';
	echo '<meta property="og:image:height" content="400" />';
	echo '<meta property="og:site_name" content="'.$cateSelectOgResult[0]['cate_name'].'" />';
	echo '<meta property="og:type" content="website" />';
	echo '<meta property="og:locale" content="zh_TW" />';
	echo '<meta property="og:url" content="'.$url.'" />';
};

function get_og_by_products($url,$pid){
	$productSelectOgsql ="select * from product where product_status =1 and product_no = ".$pid;
	$productselectOgresult = productQueryAll($productSelectOgsql);	

	// ga meta
	echo '<meta name="description" content="'.$productselectOgresult[0]['product_describe'].'" />';
	echo '<meta name="keyword" content="'.$productselectOgresult[0]['product_name'].'" />';	

	// fb og 
	echo '<meta property="og:title" content="'.$productselectOgresult[0]['product_name'].'" />';
	echo '<meta property="og:description" content="'.$productselectOgresult[0]['product_describe'].'" />';
	echo '<meta property="og:image" content="'.HTTP_SERVER.'/'.$productselectOgresult[0]['img1'].'" />';
	echo '<meta property="og:image:width" content="400" />';
	echo '<meta property="og:image:height" content="400" />';
	echo '<meta property="og:site_name" content="'.$productselectOgresult[0]['product_name'].'" />';
	echo '<meta property="og:type" content="website" />';
	echo '<meta property="og:locale" content="zh_TW" />';
	echo '<meta property="og:url" content="'.$url.'" />';
};

function get_og_by_styles($sid){
	
};

function index_og($url,$logoImg){
	// ga meta
	echo '<meta name="description" content="TGiLive居生活, 品味而居, 質感生活, 我們不只是追求精選好物的電商平台，而是給你完整的品味生活質感提案" />';
	echo '<meta name="keyword" content="TGiLive, 居生活, 設計, 好物, 生活, 美學, 品味質感, 選物, 電商, 精品, 寵物, 時尚, 親子, 體驗, VR, 虛擬實境, 眾籌, 集資, 募資" />';	


	// fb og 
	echo '<meta property="og:title" content="TGIlive居生活" />';
	echo '<meta property="og:description" content="TGiLive居生活, 品味而居, 質感生活, 我們不只是追求精選好物的電商平台，而是給你完整的品味生活質感提案" />';
	echo '<meta property="og:image" content="'.$logoImg.'" />';
	echo '<meta property="og:image:width" content="600" />';
	echo '<meta property="og:image:height" content="200" />';
	echo '<meta property="og:site_name" content="TGIlive居生活" />';
	echo '<meta property="og:type" content="website" />';
	echo '<meta property="og:locale" content="zh_TW" />';
	echo '<meta property="og:url" content="'.$url.'" />';
}

?>