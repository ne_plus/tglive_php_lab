<?php  
ob_start();
session_start();
date_default_timezone_set("Asia/Taipei");
include("../controller/orderControl.php");

if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
// ====ip get======
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();

switch ($_REQUEST["status"]) {
// ======create=======
	case "createOrderItem" :
		$cate_no=$_REQUEST["cate_no"];
		$cargoInfo = $_REQUEST["cargo_info"];
		$cargoPrice = $_REQUEST['cargo_price'];
		$Info = $_REQUEST["info"];

		$result ="";
		$memNo = $Info[0];
		$orderEmail = $Info[1];
		$orderRecipient =  $Info[2];
		$orderAddress = $Info[3];
		$orderTel = $Info[4];
		$orderMemo = $Info[5];
		$pay = $Info[6];
		$pay_install = $Info[7];
		$orderUniStatus = $_REQUEST["order_uni_status"];
		$orderUniNo = $_REQUEST["order_uni_no"];
		$orderUniTitle = $_REQUEST["order_uni_title"];
		$orderInvoiceAddress = $_REQUEST["order_invoice_address"];
		$orderCreatetime = time();
		$orderGroup = $memNo.$orderCreatetime;
		foreach($cate_no as $key => $cateValue){		
			if($cargoInfo == 0){ //免運
				$orderCargo = 0;
			}else{//計算運費 
				$orderCargo = $cate_no[0] == $cateValue ? $cargoPrice : 0 ;
			}
			
			// ------計算同一供應商內的消費總金額------
			$orderPrice = 0;
			foreach ($_SESSION["orders"] as $productKey => $productValue) {
				foreach ($productValue as $key => $value) {
					if($value["vendor_no"] == $cateValue){
						$orderPrice += (int)$value["price"]*(int)$value["quanty"];
						$vendorName = $value["vendor_name"];
						if($value["coupon_no"] != '' ){
							$orderPrice -= (int)$value["order_price_discount"]*(int)$value["quanty"];
						}
					}
				}
			}

			// 加購價 
			if(isset($_SESSION["orderCombos"])){
				foreach ($_SESSION["orderCombos"] as $comboKey => $comboValue){
					if($comboKey == $cateValue) {
						foreach($comboValue as $comboKey2 => $comboValue2)
						$vendorName = $comboValue2["vendor_name"];
						$orderPrice += (int)$comboValue2['price']*(int)$comboValue2['quanty'];
					}
				}
			}
			
			$item = array(
				"mem_no" => $memNo,
				"cate_no" => $cateValue,
				"cate_name" => $vendorName,
				"order_group" => $orderGroup,
				"order_email" => $orderEmail,
				"order_recipient" => $orderRecipient,
				"order_address" => $orderAddress,
				"order_memo" => $orderMemo,
				"order_tel" => $orderTel,
				"order_price" => $orderPrice,
				"order_cargo" => $orderCargo,
				"order_uni_status" => $orderUniStatus,
				"order_uni_no" => $orderUniNo,
				"order_uni_title" => $orderUniTitle,
				"order_invoice_address" => $orderInvoiceAddress,
				"order_pay" => $pay,
				"order_pay_install" => $pay_install,
				"order_pay_status" => 0,
				"order_status" => 0,
				"order_delivery" => 0,
				"order_createtime" => $orderCreatetime,
				);

			if(createOrder($item,$cateValue)){ //true
				$result = "ok";
			}else{
				$result = "error";
			} 
		}
			
		if($result == "ok"){ //訂單成功儲存
			echo $orderGroup ;
		}else{
			echo false;
		}
			
		break;

	// recharge 更新info 
	case "orderInfoChange" :
		$db = new DB();	
		$table = "order_item";
		$orderNo = $_REQUEST["order_no"];
		$pay = $_REQUEST["pay"];
		$orderInvoiceAddress = $_REQUEST["order_invoice_address"];	
		// $adminMemo = urldecode($_REQUEST["admin_memo"]);

		$checkColumn = array("order_group");
		$data = array(
				"order_group" => $orderNo ,
				"order_invoice_address" => $orderInvoiceAddress,
				"order_pay" => $pay
				);
		// print_r($item);
		echo $db -> DB_UpdateOnly($table,$data,$checkColumn);
		
			break;
// ========查詢=============
	case "查詢":
		
	break;
} //---end switch case




?>