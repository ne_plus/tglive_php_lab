<?php  
if(isset($_REQUEST['tag_no']) == false){ //沒值
	$searchTag = null;
}else{
	$searchTag =[];
	foreach ($_REQUEST['tag_no'] as $key => $value) {
		array_push($searchTag,$value);
	}
}

if(isset($_REQUEST['tag_no']) === false && isset($_REQUEST['sort']) === false && isset($_REQUEST['pricefilter']) === false){ //沒有標籤過濾的產品也沒有價錢排序
	//計算總數量
	$sqlCount ="select count(*) from product where product_status = 1 order by product_price_sort desc"; // 
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

  	//抓取資料庫比數 每頁9筆
	$sql ="select * from product where product_status = 1 order by product_price_sort desc limit $pageStart,$recPerPage";
}elseif(isset($_REQUEST['tag_no']) === false && isset($_REQUEST['sort']) === true && isset($_REQUEST['pricefilter']) === false){ //沒有標籤過濾的產品有價錢排序
	//計算總數量
	$sqlCount ="select count(*) from product where product_status = 1 order by product_createtime desc"; // 
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

  	// ===排序方式===
  	if($_REQUEST['sort'] == "pricetop"){
  		//抓取資料庫比數 每頁9筆
		$sql ="select * from product where product_status = 1 order by product_price_sort desc limit $pageStart,$recPerPage";
  	}elseif($_REQUEST['sort'] == "pricelow"){
  		//抓取資料庫比數 每頁9筆
		$sql ="select * from product where product_status = 1 order by product_price_sort asc limit $pageStart,$recPerPage";
  	}elseif($_REQUEST['sort'] == "time"){
  		//抓取資料庫比數 每頁9筆
		$sql ="select * from product where product_status = 1 order by product_createtime desc limit $pageStart,$recPerPage";
  	}

}elseif(isset($_REQUEST['tag_no']) == true  && isset($_REQUEST['sort']) === false){ //有tag 的篩選也沒有價錢排序
	$tagNo = $searchTag;
	$tagBind = [];
	foreach ($tagNo as $tagkey => $tagvalue) {
		array_push($tagBind, "tag_no = ".$tagvalue);
	}
	$str = implode(" or ",$tagBind);

	$sqlCount = sprintf("select count(DISTINCT a.product_no) from tag_products_relate a join product b on a.product_no = b.product_no where b.product_status=1 and (%s) ORDER BY a.product_no DESC",$str);
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

	
	$sql = sprintf("select * from tag_products_relate a join product b on a.product_no = b.product_no where (%s) and b.product_status=1 group by a.product_no ORDER BY b.product_price_sort DESC limit $pageStart,$recPerPage",$str);
}elseif(isset($_REQUEST['tag_no']) == true  && isset($_REQUEST['sort']) === true){ //有tag 的篩選也有價錢排序
	$tagNo = $searchTag;
	$tagBind = [];
	foreach ($tagNo as $tagkey => $tagvalue) {
		array_push($tagBind, "tag_no = ".$tagvalue);
	}
	$str = implode(" or ",$tagBind);

	$sqlCount = sprintf("select count(DISTINCT a.product_no) from tag_products_relate a join product b on a.product_no = b.product_no where b.product_status=1 and (%s) ORDER BY a.product_no DESC",$str);
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;


  	// ===排序方式===
  	if($_REQUEST['sort'] == "pricetop"){
  		//抓取資料庫比數 每頁9筆
		$sql = sprintf("select * from tag_products_relate a join product b on a.product_no = b.product_no where (%s) and b.product_status=1 group by a.product_no ORDER BY b.product_price_sort DESC limit $pageStart,$recPerPage",$str);
  	}elseif($_REQUEST['sort'] == "pricelow"){
  		//抓取資料庫比數 每頁9筆
		$sql = sprintf("select * from tag_products_relate a join product b on a.product_no = b.product_no where (%s) and b.product_status=1 group by a.product_no ORDER BY b.product_price_sort asc limit $pageStart,$recPerPage",$str);
  	}elseif($_REQUEST['sort'] == "time"){
  		//抓取資料庫比數 每頁9筆
		$sql = sprintf("select * from tag_products_relate a join product b on a.product_no = b.product_no where (%s) and b.product_status=1 group by a.product_no ORDER BY b.product_createtime DESC limit $pageStart,$recPerPage",$str);
  	}		
}elseif(isset($_REQUEST['pricefilter']) === true && isset($_REQUEST['sort']) === false ){ //沒有tag有價錢篩選
	//計算總數量
	switch($_REQUEST['pricefilter']){
		case "500":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort<=500 "; 
		break;
		case "500to1000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 501 and 1000 "; 
		break;
		case "1000to2000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 1001 and 2000 "; 
		break;
		case "2000to3000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 2001 and 3000 "; 
		break;
		case "3000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort>=3001 "; 
		break;
	}
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	// echo $totalRecord;
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

  	//抓取資料庫比數 每頁9筆
  	switch($_REQUEST['pricefilter']){
		case "500":
			$sql ="select * from product where product_status = 1 and product_price_sort<=500 order by product_price_sort desc limit $pageStart,$recPerPage"; 
		break;
		case "500to1000":
			$sql ="select * from product where product_status = 1 and product_price_sort between 501 and 1000 order by product_price_sort desc limit $pageStart,$recPerPage"; 
		break;
		case "1000to2000":
			$sql ="select * from product where product_status = 1 and product_price_sort between 1001 and 2000 order by product_price_sort desc limit $pageStart,$recPerPage"; 
		break;
		case "2000to3000":
			$sql ="select * from product where product_status = 1 and product_price_sort between 2001 and 3000 order by product_price_sort desc limit $pageStart,$recPerPage"; 
		break;
		case "3000":
			$sql ="select * from product where product_status = 1 and product_price_sort>=3001 order by product_price_sort desc limit $pageStart,$recPerPage"; 
		break;
	}
	

}elseif(isset($_REQUEST['pricefilter']) === true && isset($_REQUEST['sort']) === true ){ //沒有tag有價錢篩選 以及價錢排序
	//計算總數量
	switch($_REQUEST['pricefilter']){
		case "500":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort<=500 "; 
		break;
		case "500to1000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 501 and 1000 "; 
		break;
		case "1000to2000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 1001 and 2000 "; 
		break;
		case "2000to3000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort between 2001 and 3000 "; 
		break;
		case "3000":
			$sqlCount ="select count(*) from product where product_status = 1 and product_price_sort>=3001 "; 
		break;
	}
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	// echo $totalRecord;
  	//每頁有幾筆
  	$recPerPage = 40;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

  	//抓取資料庫比數 每頁9筆
  	switch($_REQUEST['pricefilter']){
		case "500":
			//價錢排序
			if($_REQUEST['sort'] == "pricetop"){
		  		//抓取資料庫比數 每頁9筆
				$sql ="select * from product where product_status = 1 and product_price_sort<=500 order by product_price_sort desc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "pricelow"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort<=500 order by product_price_sort asc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "time"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort<=500 order by product_createtime desc limit $pageStart,$recPerPage";
		  	}	
		break;
		case "500to1000":
			//價錢排序
			if($_REQUEST['sort'] == "pricetop"){
		  		//抓取資料庫比數 每頁9筆
				$sql ="select * from product where product_status = 1 and product_price_sort between 501 and 1000 order by product_price_sort desc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "pricelow"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 501 and 1000 order by product_price_sort asc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "time"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 501 and 1000 order by product_createtime desc limit $pageStart,$recPerPage";
		  	}	
		break;
		case "1000to2000":
			//價錢排序
			if($_REQUEST['sort'] == "pricetop"){
		  		//抓取資料庫比數 每頁9筆
				$sql ="select * from product where product_status = 1 and product_price_sort between 1001 and 2000 order by product_price_sort desc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "pricelow"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 1001 and 2000 order by product_price_sort asc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "time"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 1001 and 2000 order by product_createtime desc limit $pageStart,$recPerPage";
		  	}	 
		break;
		case "2000to3000":
			//價錢排序
			if($_REQUEST['sort'] == "pricetop"){
		  		//抓取資料庫比數 每頁9筆
				$sql ="select * from product where product_status = 1 and product_price_sort between 2001 and 3000 order by product_price_sort desc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "pricelow"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 2001 and 3000 order by product_price_sort asc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "time"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort between 2001 and 3000 order by product_createtime desc limit $pageStart,$recPerPage";
		  	}	 
		break;
		case "3000":
			//價錢排序
			if($_REQUEST['sort'] == "pricetop"){
		  		//抓取資料庫比數 每頁9筆
				$sql ="select * from product where product_status = 1 and product_price_sort>=3001 order by product_price_sort desc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "pricelow"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort>=3001 order by product_price_sort asc limit $pageStart,$recPerPage";
		  	}elseif($_REQUEST['sort'] == "time"){
		  		//抓取資料庫比數 每頁9筆
				$sql = "select * from product where product_status = 1 and product_price_sort>=3001 order by product_createtime desc limit $pageStart,$recPerPage";
		  	}	 
		break;
	}
	

}


$resultProduct = $db->DB_Query($sql);
if($resultProduct){
	$products = [];
	foreach ($resultProduct as $key => $value) {
		$products[$key]["product_no"] = $value["product_no"];
		$products[$key]["product_name"] = $value["product_name"];
		$products[$key]["product_subtitle"] = $value["product_subtitle"];
		$products[$key]["img1"] = $value["img1"];
		$products[$key]["product_status"] = $value["product_status"];
		$products[$key]["product_price_sort"] = $value["product_price_sort"];
		$sql = "select * from product where product_no=:product_no" ;
		$dic=array(":product_no"=>$value["product_no"]);
		$product = new Product($sql,$dic); //product DB initial
		// -------------標籤搜尋
		$tagRelate = $product->productRelateTag();
		if($tagRelate){ //有標籤存在
			foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
				$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
				$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
			}
			 
		}else{ //沒有標籤
			$products[$key]["tag_name"] = null ;
			$products[$key]["tag_no"] =null ;
		}

		// -------------分類搜尋
		
		$resultRelate = $product->productRelateCate();
		if($resultRelate){ //有商品分類
			foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
				$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
				$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
				$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
				$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
				if($valuecateRelate["cate_parents"] != 0 ){//有父層
						$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
						$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
						$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
				}else{ //沒有父層
					$products[$key]["cate_father_name"] = null ;
					$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
				}
			}

		}else{ // 沒有商品分類
			$products[$key]["cate"] = null;
		}

		// ---------產品規格-----
		$resultSpec = $product->productSpecUseInfo();
		if($resultSpec){ //使用中的規格
			foreach ($resultSpec as $keySpec => $valueSpec){
				$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
				$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
				if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
					$products[$key]["product_spec_price_discount"][$keySpec]= null ;
				}else{ //有折扣
					$priceDiscount = $valueSpec["product_spec_price2"];
					if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
						$priceDiscount = $valueSpec["product_spec_price3"];
						$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
					}elseif($valueSpec["product_spec_price3"] == 0){
						$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
					}else{
						$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
					}
				}
				
			}
			
		}else{ //沒有規格
			$products[$key]["product_stock"][$keySpec] = null;
		}

	}
	$array = array("recordsTotal"=>$totalRecord);
	$array["totalPage"] = $totalPage;
	$array["pageNo"] = $pageNo;
	$array["data"] = $products;
	// echo "<pre>";
	// print_r($array);
	// echo "</pre>";
}else{
	$products = [];
	$array = array("recordsTotal"=>$totalRecord);
	$array["totalPage"] = $totalPage;
	$array["pageNo"] = $pageNo;
	$array["data"] = $products;
	// echo json_encode($array);
}



?>