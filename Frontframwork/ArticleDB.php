<?php  

// include("../model/DB.php");
date_default_timezone_set("Asia/Taipei");

class Article{
	public $array = []; //文章陣列
	public $article_count; //query總數量
	public $page_no; //當前頁數
	public $total_page; //總共頁數
	public $tag_article_all = [];//文章所有的標籤分類
	public $pidSql ; //pid組合SQL字串
	public $pageSql ; //page組合SQL字串
	public $status ; // article 文章搜尋 or tag 標籤搜尋文章

	public function __construct($status=null , $pid=null , $tagId=null , $page=null  , $recPerPage=null ){
		//1.initial DB connnect and Query ;
		$db = new DB();
		if( $status=="article" ){
			if( $pid ){
				$pidSql = "and article_no = ".$pid ;
			}else{
				$pidSql = null;
			}
			$this->pidSql = $pidSql;
		}elseif( $status=="tag" ){
			$tagidSql = " and tag_no = ".$tagId;
			$this->tagidSql = $tagidSql;
		}
		$this->status = $status;
		
		
		// 計算抓取數量
		if( $status == "article" ){
			$sqlCount = "SELECT count(*) FROM article where article_status = 1 " .$pidSql ;
		}elseif( $status == "tag" ){
			$sqlCount = "SELECT count(*) FROM tag_article_relate a left join article b on a.article_no = b.article_no where b.article_status = 1 " .$tagidSql ;
		}

		$countQuery = $db -> DB_Query($sqlCount);
		$this->article_count = $countQuery[0]["count(*)"];


		//2.pagenation loop;
		
		//當前頁數
		$pageNo = isset($page)? $page : 1;
		$this->page_no = $pageNo;
		//總共query數量
		$totalRecord = $this->article_count;
		//每頁有幾筆
		$recPerPage = isset($recPerPage)? $recPerPage : 5;
		//共有幾頁
		$totalPage = ceil($totalRecord/$recPerPage);
		$this->total_page = $totalPage;

		$pageStart = ($pageNo-1) * $recPerPage;
		$pageSql = "limit $pageStart,$recPerPage";
	
		$this->pageSql = $pageSql;
	}

	public function articleQuery(){
		// query result;
		$db = new DB();
		if( $this->status == "article"){
			$sql = "SELECT * FROM article where article_status = 1 " .$this->pidSql." order by article_createtime desc ".$this->pageSql;
		}elseif( $this->status == "tag" ){
			$sql = "SELECT * FROM article a left join tag_article_relate b on a.article_no = b.article_no  where a.article_status = 1 " .$this->tagidSql." order by a.article_createtime desc ".$this->pageSql;
		}
		
		$query = $db -> DB_Query($sql);
		foreach ($query as $key => $value) {
			//文章list
			array_push($this->array , $value);
			//文章標籤分類
			$tagRelatesql = "SELECT * FROM tag_article_relate a left join tag b on a.tag_no = b.tag_no where a.article_no = ".$value["article_no"];
			$tagRelateQuery = $db -> DB_Query($tagRelatesql);
			$this->array[$key]["tag_article_relate"] = $tagRelateQuery;
		}
		return $this->array; 
	}


	public function pagenation(){
		$result = array(
			"recordsTotal" => $this->article_count,
			"totalPage" =>  $this->total_page,
			"pageNo" => $this->page_no,
			);
		return $result;
	}

	public function tagArticleRelateQueryAll(){
		$db = new DB();
		$sql = "SELECT count(*),b.tag_name,b.tag_no  FROM tag_article_relate a left join tag b on a.tag_no = b.tag_no left join article c on a.article_no = c.article_no where c.article_status=1 group by a.tag_no desc";
		$result = $db -> DB_Query($sql);
		return $result;
	}
	public function getTagInfo($tagNo){
		$db = new DB();
		$sql = "SELECT * FROM tag where tag_no = ".$tagNo;
		$result = $db -> DB_Query($sql);
		return $result;
	}
	public function getArticleLatest($count=null){
		$articleCount = isset($count)? " limit 0 , ".$count : null;
		$db = new DB();
		$sql = "SELECT * FROM article where article_status = 1  order by article_createtime desc ".$articleCount;
		$result = $db -> DB_Query($sql);
		return $result;
	}
}


?>