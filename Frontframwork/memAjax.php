<?php  
ob_start();
session_start();
date_default_timezone_set("Asia/Taipei");
include("../controller/userControl.php");


// ====ip get======

 
if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}

function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();


switch ($_REQUEST["status"]) {
	case "memMailCheck":

		$table = "member";
		$email = $_REQUEST["mem_email"];
		$check = checkMemMail($table,$email);

		if($check){
			echo json_encode($check);
		}else{
			echo false;
		}
		
		break;

	case "memCreate" :
		$table = "member";	
		$email = $_REQUEST["mem_email"];
		$psw = $_REQUEST["mem_password"];
		$memTel = $_REQUEST["mem_tel"];
		$memLastName = $_REQUEST["mem_lastname"];
		$memFirstName = $_REQUEST["mem_firstname"];
		$memAddress = $_REQUEST["mem_address"];

		$item = array(
			"mem_mail" => $email,
			"mem_password"=> md5($psw),
			"mem_firstname"=> $memFirstName,
			"mem_lastname"=> $memLastName ,
			"mem_tel" =>$memTel,
			"mem_address" =>$memAddress,
			"mem_status" => 1, 
			"mem_createtime" =>time(), 
			"mem_loginIP"=> $ipAdd
			);
		echo json_encode(create($table,$email,$item));

		break;

	case "memInfoEdit":
		$table = $_REQUEST["table"];
		$memNo = $_REQUEST["mem_no"];
		$memMail = $_REQUEST["mem_mail"];
		$memTel = $_REQUEST["mem_tel"];
		$memLastName = $_REQUEST["mem_lastname"];
		$memFirstName = $_REQUEST["mem_firstname"];
		$memAddress = $_REQUEST["mem_address"];

		$item = array(
			"mem_no" => $memNo,
			"mem_firstname"=> $memFirstName,
			"mem_lastname"=> $memLastName ,
			"mem_tel" =>$memTel,
			"mem_address" =>$memAddress
		);
		$result = edit($table,$memMail,$item);
		if($result){
			// =====修改session======
			$_SESSION["memFirstName"] =$memFirstName;
			$_SESSION["memLastName"] =$memLastName;
			echo  true; //成功
		}else{
			echo  false; //更新失敗
		}

		break;
	//========FB 登入一 有帳號存在==========
	case "memStatusEditFB":
		$table = "member";	
		$memNo = $_REQUEST["mem_no"];
		$memMail = $_REQUEST["mem_email"];
		$memLastName = $_REQUEST["mem_lastname"];
		$memFirstName = $_REQUEST["mem_firstname"];
		$memFBid = $_REQUEST["mem_FBid"];

		//session 方式
		$_SESSION["memNo"] = $memNo;
		$_SESSION["memMail"] = $memMail;
		$_SESSION["memFirstName"] =$memFirstName;
		$_SESSION["memLastName"] =$memLastName;

		// ---儲存 最後登入時間-----
		$item= array(
			"mem_no" => $memNo,
			"mem_lastLoginTime" =>time(),
			"mem_token" => $memFBid,
			"mem_loginIP" => $ipAdd
		);

		if(edit($table,$memMail,$item)){
			echo true;
		}else{
			echo false;
		}

		break;
	//========FB 登入二 新註冊==========
	case "memCreateFB":
		$table = "member";	
		$memMail = $_REQUEST["mem_email"];
		$memLastName = $_REQUEST["mem_lastname"];
		$memFirstName = $_REQUEST["mem_firstname"];
		$memFBid = $_REQUEST["mem_FBid"];

		$item = array(
			"mem_mail" => $memMail,
			"mem_FBid"=>$memFBid,
			"mem_firstname"=> $memFirstName,
			"mem_lastname"=> $memLastName ,
			"mem_status" => 1, 
			"mem_token" => $memFBid,
			"mem_createtime" =>time(), 
			"mem_loginIP"=> $ipAdd
			);
		$result = create($table,$memMail,$item);
		$memNo = $result["mem_id"];
		//session 方式
		$_SESSION["memNo"] = $memNo;
		$_SESSION["memMail"] = $memMail;
		$_SESSION["memFirstName"] =$memFirstName;
		$_SESSION["memLastName"] =$memLastName;
		

		break;	
	//=======發送mail==========
	case "sendOrderMail":		
		$orderNo = $_REQUEST["order_no"];
		$totalPrice = $_REQUEST["total_price"];
		$cargo = $_REQUEST["cargo_info"] == 0 ? 0 : $_REQUEST["cargo_price"];
		$orderName =  $_REQUEST["order_name"] ;
		$orderTel =  $_REQUEST["order_tel"] ;
		$orderAddress =  $_REQUEST["order_address"] ;
		$orderEmail =  $_REQUEST["order_email"] ;
		$orderPay_install =  $_REQUEST["order_pay_install"] ;
		// $orderPay =  $_REQUEST["order_pay"] ;
		switch ($_REQUEST["order_pay"]) {
			case '0':
				$orderPay ="信用卡付款";
				break;
			case '2':
				$orderPay ="超商付款";
				break;
			case '3':
				$orderPay ="信用卡分期-".$orderPay_install."期";
				break;
			case '4':
				$orderPay ="現金匯款";
				break;
			default:
				break;
		}


		$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$orderName.'</p><p>感謝您對TGiLive居生活的支持，此信件是通知您我們已收到您的訂購需求，且訂單已經成立，我們將在收到您的付款後，盡快處理您的訂單。</p><p>下方為您本次的訂單明細 : </p><table border="1" cellspacing="0" style="width:100%; font-size: 12px;margin-bottom:15px"><tr><th colspan="4" style="padding: 20px 0;    background-color: #ccc;">訂單明細</th></tr><tr><th colspan="4" style="background-color: #eee;">訂單編號 : <span>'.$orderNo.'</span></th></tr><tr><th style="background-color: #eee;">姓名</th><td style="color:#888;">'.$orderName.'</td><th style="background-color: #eee;">訂單日期</th><td style="color:#888;">'.date('Y-m-d',time()).'</td></tr><tr><th style="background-color: #eee;">電話</th><td style="color:#888;">'.$orderTel.'</td><th style="background-color: #eee;">付款方式</th><td style="color:#888;">'.$orderPay.'</td></tr><tr><th style="background-color: #eee;">收件地址</th><td style="color:#888;">'.$orderAddress.'</td><th style="background-color: #eee;">訂單狀態</th><td style="color:#888;">訂單處理中</td></tr></table><table border="1" cellspacing="0" style="width:100%; font-size: 12px; text-align: center;"><tr><th colspan="5" style="padding: 20px 0;background-color: #ccc;">訂單商品明細</th></tr><tr><th style="background-color: #eee;">商品品名</th><th style="background-color: #eee;">規格</th><th style="background-color: #eee;">數量</th><th style="background-color: #eee;">價格</th><th style="background-color: #eee;">總計</th></tr>';
		$finalPrice = 0; 
		$productCountArr ; //計算combo 數量不重複(同產品不同規格的商品只會出現一次加購選項)
		// ===訂單明細====
		foreach ($_SESSION["orders"] as $key => $value){ 
			foreach ($value as $productkey => $productvalue) {	
				$price = (int)$productvalue['quanty']* (int)$productvalue['price'];
				$discount = (int)$productvalue['quanty']* (int)$productvalue['order_price_discount'];
				$totalPrice = $price - $discount;
				$finalPrice += $totalPrice;
				$content .='<tr style="color:#888;"><td><a href="https://www.tgilive.com/product-detail.php?product_no='.$productvalue['product_no'].'">'.$productvalue['product_name'].'</a></td><td>'.$productvalue['product_spec_info'].'</td><td>'.$productvalue['quanty'].'</td><td><div><span>NT. </span><span>'.$price.'</span></div>';
				if($discount !=0){
					$content .='<div style="color:#f00;"><span>-NT. </span><span>'.$discount.'</span></div>';
				}
				$content .='</td><td><span>NT. </span><span>'.$totalPrice.'</span></td></tr>';

				// 加價購
				$orderComboList = [];
				if(isset($_COOKIE["comboorders"])){ //有存在加價購清單
					$comboorders = json_decode($_COOKIE["comboorders"], true);
					foreach ($comboorders as $orderskey => $ordersvalue) {
						if (!in_array($orderskey, $orderComboList) && $ordersvalue["quanty"] >= 1)
							array_push($orderComboList, $orderskey);
					}
				}
				$comboProducts = get_comboProducts_by_productId($productvalue['product_no']);
				if ($comboProducts && count($comboProducts) >= 1) {
					foreach ($comboProducts as $key2 => $value2) {
						if (in_array($value2["id"], $orderComboList) && !isset($productCountArr[$productvalue['product_no']]['combo']) ) {
							$productCountArr[$productvalue['product_no']]['combo'] = 1; //
							//計算總金額
							$price = (int)$value2["product_spec_price"]*(int)$comboorders[$value2["id"]]['quanty'];
							$finalPrice += $price;
							$content .='<tr style="color:#888;">';
							$content .='<td><a href="https://www.tgilive.com/product-detail.php?product_no='. $value2['product_no'] .'">'. $value2["product_name"] .'</a> <span style="color:red;">(加購商品)</span></td>';
							$content .='<td>'. $value2["product_spec_info"] .'</td><td>'. (int)$comboorders[$value2["id"]]['quanty'] .'</td><td><div><span>NT. </span><span>'.$price.'</span></div>';
							$content .='</td><td><span>NT. </span><span>'. (int)$value2["product_spec_price"]*(int)$comboorders[$value2["id"]]['quanty'] .'</span></td></tr>';
						}
					}
				}
				
			}		
		}
		
		
		$content .= '<tr><th colspan="4" style="text-align: right">商品合計</th><td style="color:#888;"><span>NT. </span><span>'.$finalPrice.'</span></td></tr><tr><th colspan="4" style="text-align: right">郵寄/宅配運費</th><td style="color:#888;">';
		if($cargo == 0){
			$content .= "免運費";
		}else{
			$content .='<span>NT. </span><span>'.$cargo.'</span>';
		}
		$content .= '</td></tr><tr><th colspan="4" style="text-align: right">訂單總計</th><td><span>NT. </span><span>'.((int)$finalPrice+(int)$cargo).'</span></td></tr></table>';
		
		// ===tgi 內容====
		$content .='<ul><li>為了保護您的個人資料安全，即日起我們將不在信函中顯示完整訂購人資料明細，如需查詢訂單處理狀態，請至<a href="https://www.tgilive.com/" target="_blank" style="color:#00f;">TGiLive官網</a>登入後前往會員專區>訂單記錄，即可查詢您的訂單內容與配送狀態。</li><li>超商代收-代碼的繳費期限為7天，請務必於期限內進行繳款。請妥善保留您的超商付款代碼。若您無法取得超商繳費代碼。請與居生活客服人員聯繫</li><li>請於收到虛擬帳號3日內進行ATM或銀行APP轉帳繳款，若您無法取得或遺失虛擬帳號，請與居生活客服人員聯繫，或重新下單。</li><li>若有任何訂單上處理的問題，歡迎您利用以下方式與我們聯繫：</li><p>TGiLive居生活粉絲專頁： <span><a href="https://www.facebook.com/TGiLiveTW/" target="_blank" style="color:#00f;">https://www.facebook.com/TGiLiveTW/</a></span></p><p>TGiLive客服信箱： <span><a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a></span></p><p>我們的客服回覆時間： <span>週一至週五10:00am-18:00pm</span></p></ul><p>◎TGiLive居生活防範詐騙聲明：</p><ol><li>我們所有訊息以Email及粉絲專頁私訊為主，不會任意打電話給會員。</li><li>不會傳簡訊或電話通知您誤設分期、簽錯單據、訂單重複等等。</li><li>我們不會要求您操作任何ATM機器。</li><li>接到疑似詐騙電話，切勿聽信前往操作提款機或回撥電話，或直接撥打165反詐騙專線。</li><li>請勿直接回覆信件，若有訂單或帳務問題，請利用上述方式直接與TGiLive居生活聯繫。</li></ol><p>※請注意！TGiLive居生活官網所有付款服務只有「一次付清」單一選項，絕不會誤設成分期付款，若接獲不明電話以此詐騙，請馬上報警或撥打165反詐騙專線。</p><p style="margin-top: 50px;">TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';


		$title ="TGI Live 訂單通知";
		// $receiverArray = array($orderEmail,"info@tgilive.com","info@tgilive.co.uk");
		$receiverArray = array($orderEmail);

		$systemDefault = receiverCompany;
		foreach ($systemDefault as $key => $value) {
			array_push($receiverArray, $value);
		}
		echo json_encode(sendMail($title,$content,$receiverArray,$orderName));
			break;	


	case "pswChange":
		$table = $_REQUEST["table"];
		$memNo = $_REQUEST["mem_no"];
		$memMail = $_REQUEST["mem_mail"];
		$memPsw = $_REQUEST["mem_password"];
		$memName = $_REQUEST["mem_name"];

		$count = strlen( $memPsw );
		$replace = "";
		for ($i=4; $i < $count ; $i++) { 
			$replace .= "*";
		}
		$pswSend  = substr_replace ( $memPsw ,  $replace , 2 , $count-4 );

		$item = array(
			"mem_no" => $memNo,
			"mem_password"=>md5($memPsw)
		);
		$result = edit($table,$memMail,$item);
		if($result){
			$title ="TGI Live會員密碼更新";
			// $content = "<p><span>TGI live 登入密碼 : </span><span>$tempPsw</span></p><p>請登入後重新設定密碼</p>" ;
			$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$memName.'</p><p>您帳號的密碼已變更</p><p><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">新密碼 :&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">'.$pswSend.'</span></p><p>※提醒：此信件為系統發出信件，若有問題請請來信至客服信箱（ <a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a> ），我們將盡快為您處理，謝謝。</p><p>TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';
			
			$receiverArray = array($memMail);
			echo json_encode(sendMail($title,$content,$receiverArray,$memName));
			// echo  true; //成功
		}else{
			echo  false; //更新失敗
		}
		break;
	case "createTempPsw":
		//建立新的密碼並儲存
		$table = "member";
		$email = $_REQUEST["mem_email"];
		$memNo = $_REQUEST["mem_no"];
		$memName = $_REQUEST["mem_name"];
		$tempPsw = "temp".rand(10,10000);

		$item = array(
			"mem_no" => $memNo,
			"mem_password" => md5($tempPsw)
		);
		$result = edit($table,$email,$item);

		if($result){ //true
			//發送email
			$title ="TGI Live會員密碼重置";
			// $content = "<p><span>TGI live 登入密碼 : </span><span>$tempPsw</span></p><p>請登入後重新設定密碼</p>" ;
			$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$memName.'</p><p>當您收到這封 Email 是因為您提出忘記密碼的申請，請利用下方系統隨機產生的臨時密碼，前往官網後台登入並更新您的密碼，以確保您的帳戶資料安全。</p><p><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">臨時密碼 :&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">'.$tempPsw.'</span></p><p>提醒您：</p><ol><li>如果您沒有變更密碼，請馬上用此密碼登入後，修改密碼。</li><li>如果您無法利用此密碼登入或有其他問題，請勿直接回覆本信件，請來信至客服信箱（ <a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a> ），我們將盡快為您處理，謝謝。</li></ol><p>TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';

			$receiverArray = array($email);
			echo json_encode(sendMail($title,$content,$receiverArray,$memName));
			// echo $memName ;

		}else{ //更新失敗
			echo false;
		}
		
		
	 	break;	
	case "sendOrderCheckOutMail": 
		
		$now = date("Y-m-d H:i:s");
		$memName =  $_REQUEST["order_name"];
		$orderNo =  $_REQUEST["order_no"];
		$price = $_REQUEST["order_price"];
		$orderEmail = $_REQUEST["order_mail"];

		//發送email
		$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 120px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$memName.'</p><p style="margin:30px 0;">感謝您在TGiLive居生活訂購我們的商品！您的訂單已經付款成功，我們將立即為您安排出貨！下方為您的付款資訊：</p><p style="margin:5px 0;"><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">訂單編號：&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">'.$orderNo.'</span></p><p style="margin:5px 0;"><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">交易金額：&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">'.$price.'</span></p><p style="margin:5px 0;"><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">交易完成時間：&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;PingFang TC&quot;; font-size: medium;">'.$now.'</span></p><p>如需查詢訂單處理狀態，請至<a href="https://www.tgilive.com/" target="_blank" style="color:#00f;">TGiLive官網</a>登入後前往會員專區>訂單記錄，即可查詢您的訂單內容與配送狀態。</p><p style="font-weight: bold; ">※一般商品將於2~5個工作天以宅配方式送達（不含六日及國定假日），商品頁標示為「預購」商品，將以實際出貨或製作日標示為主，出貨時您將會收到系統送出的出貨通知。</p><p style="font-weight: bold; ">※提醒：此信件為系統發出信件，若有問題請請來信至客服信箱（ <a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a> ），我們將盡快為您處理，謝謝。</p><p>我們的客服回覆時間： 週一至週五10:00am-18:00pm</p><p style="margin:40px 0;">TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';

		$title ="TGiLive居生活訂單付款成功！";

		// $receiverArray = array("jack@ne-plus.com");
		// $receiverArray = array($orderEmail,"info@tgilive.com","info@tgilive.co.uk");
		$receiverArray = array($orderEmail);

		$systemDefault = receiverCompany;
		foreach ($systemDefault as $key => $value) {
			array_push($receiverArray, $value);
		}
		echo json_encode(sendMail($title,$content,$receiverArray,$memName));
		break;	


	case "newMemMail":  //公司內部信件
		
		$memName =  $_REQUEST["mem_lastname"].$_REQUEST["mem_firstname"];
		$memEmail = $_REQUEST["mem_email"];
		$memTel = $_REQUEST["mem_tel"];

		//發送email
		$content = '<p>TGiLive居生活新註冊客戶：<span></span></p><p>姓名：<span>'.$memName.'</span></p><p>Email：<span>'.$memEmail.'</span></p><p>電話：<span>'.$memTel.'</span></p>';

		$title ="新加入會員通知";


		$receiverArray = array("info@tgilive.com","info@tgilive.co.uk");	
		// $receiverArray = array("jack@ne-plus.com");	
		echo json_encode(sendMail($title,$content,$receiverArray,$memName));
		break;		
	 	//=======發送mail==========


	case "newMemMailEDM" : //新會員接收會員通知信
		$memEmail = $_REQUEST["mem_email"];
		$memName = $_REQUEST["mem_lastname"].$_REQUEST["mem_firstname"];
		//EDM內容
		include('../Frontframwork/EDM/TGiLiveFirstMember.php');
		//EDM內容

		// $content; //edm 裡內容
		$title ="[TGILive 居生活] 新加入會員通知";
		$receiverArray = array( $memEmail );
		echo json_encode(sendMail($title,$content,$receiverArray,$memName));
		break;
	default:

		break;
}



?>