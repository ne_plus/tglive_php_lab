<?php 
include("../model/product.php");

$start = isset($_REQUEST['start'] ) ? $_REQUEST['start']  : 0;
$length = isset($_REQUEST['length'] ) ?  $_REQUEST['length'] : 9;
// $searchTag = isset($_REQUEST['searchTag']) ?  $_REQUEST['searchTag'] : null;
// $_REQUEST['searchTag'] =array(17);
if(isset($_REQUEST['searchTag']) == false){ //沒值
	$searchTag = null;
}else{
	$searchTag =[];
	foreach ($_REQUEST['searchTag'] as $key => $value) {
		array_push($searchTag,$value);
	}
}
$searchCount = 0; //seach 計時器

$db = new DB();
date_default_timezone_set("Asia/Taipei");
// $sql = "select * from product product_status = 1 order by product_no ".$dir;
if(isset($_REQUEST['searchTag']) == false){
	//計算總數量
	$sqlCount ="select count(*) from product where product_status = 1 order by product_createtime desc"; // 
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 9;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

  	//抓取資料庫比數 每頁9筆
	$sql ="select * from product where product_status = 1 order by product_createtime desc limit $pageStart,$recPerPage";
	
}else{ //有tag 的篩選
	$tagNo = $searchTag;
	$tagBind = [];
	foreach ($tagNo as $tagkey => $tagvalue) {
		array_push($tagBind, "tag_no = ".$tagvalue);
	}
	$str = implode(" or ",$tagBind);

	$sqlCount = sprintf("select count(DISTINCT a.product_no) from tag_products_relate a join product b on a.product_no = b.product_no where b.product_status=1 and (%s) ORDER BY a.product_no DESC",$str);
	$statement = $db->pdo->query($sqlCount);
  	$row = $statement->fetch(PDO::FETCH_NUM);
  	$totalRecord = $row[0];
  	//每頁有幾筆
  	$recPerPage = 9;
  	//共有幾頁
  	$totalPage = ceil($totalRecord/$recPerPage);

  	if(isset($_REQUEST["pageNo"])==false){
  		$pageNo=1;
  	}else{ 
		$pageNo=$_REQUEST["pageNo"];
  	}
  	$pageStart = ($pageNo-1) * $recPerPage;

	
	$sql = sprintf("select * from tag_products_relate a join product b on a.product_no = b.product_no where (%s) and b.product_status=1 group by a.product_no ORDER BY a.product_no DESC limit $pageStart,$recPerPage",$str);
}


$result = $db->DB_Query($sql);

if($result){
		$products = [];
		foreach ($result as $key => $value) {
			$products[$key]["product_no"] = $value["product_no"];
			$products[$key]["product_name"] = $value["product_name"];
			$products[$key]["product_subtitle"] = $value["product_subtitle"];
			// $products[$key]["product_promote"] = $value["product_promote"];
			// $products[$key]["product_describe"] = $value["product_describe"];
			$products[$key]["img1"] = $value["img1"];
			// $products[$key]["img2"] = $value["img2"];
			// $products[$key]["img3"] = $value["img3"];
			// $products[$key]["img4"] = $value["img4"];
			// $products[$key]["img5"] = $value["img5"];
			// $products[$key]["product_content"] = $value["product_content"];
			$products[$key]["product_status"] = $value["product_status"];
			$sql = "select * from product where product_no=:product_no" ;
			$dic=array(":product_no"=>$value["product_no"]);
			$product = new Product($sql,$dic); //product DB initial
			// -------------標籤搜尋
			$tagRelate = $product->productRelateTag();
			if($tagRelate){ //有標籤存在
				foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
					$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
					$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
				}
				 
			}else{ //沒有標籤
				$products[$key]["tag_name"] = null ;
				$products[$key]["tag_no"] =null ;
			}

			// -------------分類搜尋
			
			$resultRelate = $product->productRelateCate();
			if($resultRelate){ //有商品分類
				foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
					$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
					$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
					$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
					$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
					if($valuecateRelate["cate_parents"] != 0 ){//有父層
							$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
							$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
							$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
					}else{ //沒有父層
						$products[$key]["cate_father_name"] = null ;
						$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
					}
				}

			}else{ // 沒有商品分類
				$products[$key]["cate"] = null;
			}

			// ---------產品規格-----
			$resultSpec = $product->productSpecUseInfo();
			if($resultSpec){ //使用中的規格
				foreach ($resultSpec as $keySpec => $valueSpec){
					$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
					$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
					if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
						// $products[$key]["product_spec_price2"][$keySpec] = null ;
						// $products[$key]["product_spec_price3"][$keySpec] = null ;
						$products[$key]["product_spec_price_discount"][$keySpec]= null ;
					}else{ //有折扣
						$priceDiscount = $valueSpec["product_spec_price2"];
						if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
							$priceDiscount = $valueSpec["product_spec_price3"];
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}elseif($valueSpec["product_spec_price3"] == 0){
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}else{
							$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
						}
						// $products[$key]["product_spec_price2"][$keySpec] = $valueSpec["product_spec_price2"] ;
						// $products[$key]["product_spec_price3"][$keySpec] = $valueSpec["product_spec_price3"] ;
					}
					
				}
				
			}else{ //沒有規格
				$products[$key]["product_stock"][$keySpec] = null;
			}



			
			// -----標籤篩選----
			// if($searchTag != null){
			// 	if($products[$key]["tag_no"] != null){
			// 		$countTagFilter = 0;
			// 		foreach ($searchTag as $tagkey => $tagvalue) {
			// 			if(in_array( $tagvalue, $products[$key]["tag_no"])){
			// 				$countTagFilter++;
			// 				// echo "have";
			// 				// echo $tagvalue;
			// 			}else{
			// 				// echo "NG";
			// 				// echo $tagvalue;
			// 			}
			// 		}
			// 		if($countTagFilter == 0){
			// 			unset($products[$key]);
			// 		}
			// 	}else{
			// 		unset($products[$key]);
			// 	}		
			// }else{
			// 	// echo "no";
			// }

		}
		$array = array("recordsTotal"=>$totalRecord);
		$array["totalPage"] = $totalPage;
		$array["pageNo"] = $pageNo;
		// $array["data"]=array_slice($products,$start,count($products));
		$array["data"] = $products;
		// $array["data"] = $products;
		// return  $products;
		echo json_encode($array);
		// echo "<pre>";
		// echo $length;
		// echo count($products);
		// print_r($products);
		// echo "</pre>";
	}else{
		$products = [];
		$array = array("recordsTotal"=>$totalRecord);
		$array["totalPage"] = $totalPage;
		$array["pageNo"] = $pageNo;
		// $array["data"]=array_slice($products,$start,count($products));
		$array["data"] = $products;
		echo json_encode($array);
		// return "沒有";
	}

?>