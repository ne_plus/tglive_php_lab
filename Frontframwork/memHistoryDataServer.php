<?php  

if(isset($_SESSION["memNo"])){
	$memNo = $_SESSION["memNo"] ;
}

if(isset($_REQUEST["orderSearch"]) && $_REQUEST["orderSearch"] != "") { //單筆收尋

	$groupSql ="select mem_no , order_group , order_email , order_recipient , order_tel , order_memo , order_address , order_status , order_pay , order_createtime ,order_invoice_address from order_item where mem_no=".$memNo." and order_group = ".$_REQUEST["orderSearch"]." group by order_group ";
	$statement = $db->DB_Query($groupSql);

	$totalRecord = count($statement);
	//每頁有幾筆
	$recPerPage = 5;
	//共有幾頁
	$totalPage = ceil($totalRecord/$recPerPage);

	if(isset($_REQUEST["pageNo"])==false){
		$pageNo=1;
	}else{ 
	$pageNo=$_REQUEST["pageNo"];
	}
	$pageStart = ($pageNo-1) * $recPerPage;


}else{ //所有歷史紀錄

	//計算總數量
	$sqlCount ="SELECT * FROM order_item WHERE mem_no=".$memNo." group by order_group"; 
	$statement = $db->DB_Query($sqlCount);

	$totalRecord = count($statement);
	//每頁有幾筆
	$recPerPage = 5;
	//共有幾頁
	$totalPage = ceil($totalRecord/$recPerPage);

	if(isset($_REQUEST["pageNo"])==false){
		$pageNo=1;
	}else{ 
	$pageNo=$_REQUEST["pageNo"];
	}
	$pageStart = ($pageNo-1) * $recPerPage;


	//抓取資料庫比數 每頁5筆

	$groupSql ="select mem_no , order_group , order_email , order_recipient , order_tel , order_memo , order_address , order_status , order_pay , order_createtime , order_invoice_address from order_item where mem_no=".$memNo." group by order_group order by order_createtime desc limit $pageStart,$recPerPage ";
	// $sql = "select * from order_item where mem_no=".$memNo." and ( order_status=0 or order_status=1 or order_status=2 ) order by order_createtime desc"; // 
}


$db = new DB();
$groupResult = $db->DB_Query($groupSql);
$order =[];
if($groupResult){		
	foreach ($groupResult as $key => $value) {
		$order[$key]["mem_no"] = $value["mem_no"];
		$order[$key]["order_group"] = $value["order_group"];
		$order[$key]["order_email"] = $value["order_email"];
		$order[$key]["order_tel"] = $value["order_tel"];
		$order[$key]["order_memo"] = $value["order_memo"];
		$order[$key]["order_recipient"] = $value["order_recipient"];
		$order[$key]["order_address"] = $value["order_address"];
		$order[$key]["order_pay"] = $value["order_pay"];
		$order[$key]["order_status"] = $value["order_status"];
		$order[$key]["order_createtime"] = $value["order_createtime"];
		$order[$key]["order_invoice_address"] = $value["order_invoice_address"];
		$order[$key]["order_price"] = 0 ;
		$order[$key]["order_cargo"] = 0 ;
	
		// ====有群組定單=====
		$anotherGroupSql = "select order_no , order_price , order_cargo from order_item where order_group=".$value["order_group"]; // 	
		$anotherGroupResult = $db->DB_Query($anotherGroupSql);
		foreach ($anotherGroupResult as $anotherGroupkey => $anotherGroupvalue) {
			$order[$key]["order_price"] += (int)$anotherGroupvalue["order_price"];
			$order[$key]["order_cargo"] += (int)$anotherGroupvalue["order_cargo"];

			// ====收尋orderDetail====
			$orderDetailSql =  "select * from order_detail where order_no=".$anotherGroupvalue["order_no"];
			$setailResult = $db->DB_Query($orderDetailSql);
			foreach ($setailResult as $setailResultkey => $setailResulvalue) {
				$order[$key]["detail"][$setailResulvalue["order_detail_no"]] = $setailResulvalue;
			}
		}

		
	}
	
	

	// print_r($array);
	// $jsonStr = json_encode($order);
	
	// echo "<pre>";
	// print_r($order);
	// echo "</pre>";
	
}
	
$array = array("recordsTotal"=>$totalRecord);
$array["totalPage"] = $totalPage;
$array["pageNo"] = $pageNo;


?>