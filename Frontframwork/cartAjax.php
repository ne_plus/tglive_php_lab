<?php  
ob_start();
session_start();
date_default_timezone_set("Asia/Taipei");
include("../controller/productControl.php");



// $productCate = $_REQUEST["cate_no"];
if(isset($_REQUEST["product_no"])){ //存在
	$productNO = $_REQUEST["product_no"];
}

// $productSpec = $_REQUEST["product_spec"];
$status = $_REQUEST["status"];


switch ($status) {
	case 'addCartOnly':   //快速加入購物車 (如果有存在多個規格 存入第一個規格)
		$table="product"; 

		//spec 搜尋
		$spec = productHaveSpecQuery($table,$productNO);
		$specNo = $spec[0]["product_spec_no"];
		if( $spec[0]["product_spec_price3"] == 0 && $spec[0]["product_spec_price2"] == 0 ){ //原價
			$specPrice = $spec[0]["product_spec_price1"];
		}else{ //有折扣
			if($spec[0]["product_spec_price3"] != 0 ){
				$specPrice = $spec[0]["product_spec_price3"];
			}else{
				$specPrice = $spec[0]["product_spec_price2"];
			}
		}
		// echo "<pre>";
		// print_r($spec);
		// echo $specPrice;
		// echo "</pre>";

		//品牌收尋
		$brand = productCateQuery($table,$productNO);
		$brandCateProductNo = $brand[0]["cate_product_no"];
		$brandCateParentsNo = $brand[0]["cate_parents"];
		$brandCateNo = $brand[0]["cate_no"];
		$brandCateName = $brand[0]["cate_no"];
		echo "<pre>";
		print_r($brand);
		echo "</pre>";


		// $_SESSION["products"][$productNO][$productSpec]=$productSpec; //建立3維陣列 並設傳入的psn為索引key値,不會造成重複的內容傳入
		// $_SESSION["products"][$productNO][$productSpec]["order_detail_price"]="1000"; //最終售價
		// $_SESSION["products"][$productNO][$productSpec]["cate_no"]=$productCate; //品牌編號
		// $_SESSION["products"][$productNO][$productSpec]["cate_name"]="這是測試"; //品牌名稱
		// $_SESSION["products"][$productNO][$productSpec]["order_detail_quantity"]=1; //數量

		break;

	case "addCartDetail";
		$array =[];
		$productNo = $_REQUEST["product_no"];
		$productName = $_REQUEST["product_name"];
		$cateNo = $_REQUEST["cate_no"];
		$cateName = $_REQUEST["cate_name"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$quanty = $_REQUEST["quanty"];
		$price = $_REQUEST["price"];
		
		//確認是否有cookie確認碼存在(header儲存用於檢查)
        if (!isset($_COOKIE["CookieCheck"])){ //瀏覽器關閉cookie 功能改用session存
           	$_SESSION["orders"][$productNo][$productSpecNo]["product_name"] = $productName;
           	$_SESSION["orders"][$productNo][$productSpecNo]["product_no"] = $productNo;
			$_SESSION["orders"][$productNo][$productSpecNo]["price"]= $price; //最終售價
			$_SESSION["orders"][$productNo][$productSpecNo]["cate_no"]= $cateNo; //品牌編號
			$_SESSION["orders"][$productNo][$productSpecNo]["cate_name"]= $cateName; //品牌名稱
			$_SESSION["orders"][$productNo][$productSpecNo]["product_spec_no"]= $productSpecNo;
			$_SESSION["orders"][$productNo][$productSpecNo]["quanty"]=$quanty; //數量

			echo false;
        }else{  //使用cookie 儲存購物車訂單資訊
			if(!isset($_COOKIE["orders"])){  //沒有其他訂單資訊直接儲存
				$array[$productNo][$productSpecNo]["product_name"] =  $productName;
	           	$array[$productNo][$productSpecNo]["product_no"] =  $productNo;
				$array[$productNo][$productSpecNo]["cate_no"] =  $cateNo;
				$array[$productNo][$productSpecNo]["cate_name"] =  $cateName;
				$array[$productNo][$productSpecNo]["product_spec_no"] =  $productSpecNo;
				$array[$productNo][$productSpecNo]["quanty"] =  $quanty;
				$array[$productNo][$productSpecNo]["price"] =  $price;
			
				setcookie("orders",json_encode($array),time()+(60*60*24*30),"/");

				echo true;
			}else{ //有其他訂單資訊
				$orderlist = json_decode($_COOKIE["orders"], true) ;
				$orderlist[$productNo][$productSpecNo]["product_name"] =  $productName;
	           	$orderlist[$productNo][$productSpecNo]["product_no"] =  $productNo;
				$orderlist[$productNo][$productSpecNo]["cate_no"] =  $cateNo;
				$orderlist[$productNo][$productSpecNo]["cate_name"] =  $cateName;
				$orderlist[$productNo][$productSpecNo]["product_spec_no"] =  $productSpecNo;
				$orderlist[$productNo][$productSpecNo]["quanty"] =  $quanty;
				$orderlist[$productNo][$productSpecNo]["price"] =  $price;
				
				setcookie("orders",json_encode($orderlist),time()+(60*60*24*30),"/");
	  		
				echo true;
			}

			
        }        
        break;
    case "editCartDetail";  
    	$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$quanty = $_REQUEST["quanty"];

    	$orderlist = json_decode($_COOKIE["orders"], true) ;
    	$orderlist[$productNo][$productSpecNo]["quanty"] = $quanty;

    	setcookie("orders",json_encode($orderlist),time()+(60*60*24*30),"/");
	  		
		echo true;

		break;
	case "addCartCombo";
		$array =[];
		$combo_id = $_REQUEST["combo_id"];
		
		//確認是否有cookie確認碼存在(header儲存用於檢查)
        if (!isset($_COOKIE["CookieCheck"])){ //瀏覽器關閉cookie 功能改用session存
           	$_SESSION["comboorders"][$combo_id]["quanty"] = 1; //數量

			echo false;
        }else{  //使用cookie 儲存購物車訂單資訊
			if(!isset($_COOKIE["comboorders"])){  //沒有其他訂單資訊直接儲存
				$array[$combo_id]["quanty"] =  1;
			
				setcookie("comboorders",json_encode($array),time()+(60*60*24*30),"/");

				echo true;
			}else{ //有其他訂單資訊
				$combolist = json_decode($_COOKIE["comboorders"], true) ;
				$combolist[$combo_id]["quanty"] = 1;
				
				setcookie("comboorders",json_encode($combolist),time()+(60*60*24*30),"/");
	  		
				echo true;
			}
        }        
        break;
	case "removeCartCombo";  
    	$combo_id = $_REQUEST["combo_id"];
		$combolist = json_decode($_COOKIE["comboorders"], true) ;
		$combolist[$combo_id]["quanty"] = 0;
		unset($combolist[$combo_id]);

    	setcookie("comboorders",json_encode($combolist),time()+(60*60*24*30),"/");
	  		
		echo true;

		break;
	case "editCartCombo":
		$combo_id = $_REQUEST["combo_id"];
		$quanty = $_REQUEST["quanty"];
		$combolist = json_decode($_COOKIE["comboorders"], true) ;
		$combolist[$combo_id]["quanty"] = $quanty;
		setcookie("comboorders",json_encode($combolist),time()+(60*60*24*30),"/");

		echo true;
		break;	
	case 'cartCheck':
		$orderList = [] ;
		if(isset($_COOKIE["orders"])){ //有存在購物清單
			$orders = json_decode($_COOKIE["orders"], true);
			
			foreach ($orders as $orderskey => $ordersvalue) {
				foreach ($ordersvalue as $key => $value) {
					$table = "product";
					$productOrderShow = productQuery($table,$value["product_no"]);
					$specOrderShow = findSpecInfo($value["product_spec_no"]);
					
					$value['product_name']=$productOrderShow["product_name"];
					$value['product_no']=$productOrderShow["product_no"];
					$value['product_subtitle']=$productOrderShow["product_subtitle"];
					$value['img1']=$productOrderShow["img1"];
					$value['product_spec_info']=$specOrderShow[0]["product_spec_info"];
					if($specOrderShow[0]["product_spec_price3"]==0 && $specOrderShow[0]["product_spec_price2"] == 0){  //原價
						$value['price']= $specOrderShow[0]["product_spec_price1"];
					}else{ //有折扣
						if($specOrderShow[0]["product_spec_price3"] != 0 ){
							$value['price']= $specOrderShow[0]["product_spec_price3"];
						}else{
							$value['price']= $specOrderShow[0]["product_spec_price2"];
						}
					}
					array_push($orderList,$value);
				}		
			}
		}else{ //沒有購物清單

		}
		echo  json_encode($orderList);
		break;

	case "clearOldcartBeforeCheckOut":	
		if(isset($_SESSION["orders"])){ //存在舊的紀錄 刪除
			unset($_SESSION["orders"]);
		}

		// 加購價session 
		if(isset($_SESSION["orderCombos"])){ //存在舊的紀錄 刪除
			unset($_SESSION["orderCombos"]);
		}

		echo true;
		break;
	case 'cartBeforeCheckOut':	//儲存DB前先行轉存session		
		$productName = $_REQUEST["product_name"];
		$productNo = $_REQUEST["product_no"];
		$productSpecNo = $_REQUEST["product_spec_no"];
		$productSpecInfo = $_REQUEST["product_spec_info"];
		$price = $_REQUEST["price"]; 
		$cateNo = $_REQUEST["bcate_no"];
		$cateName = $_REQUEST["bcate_name"];
		$quanty = $_REQUEST["quanty"];
		$vendorNo = $_REQUEST["vcate_no"];
		$vendorName = $_REQUEST["vcate_name"];
		$couponNo = $_REQUEST["coupon_no"];
		$couponName = $_REQUEST["coupon_name"];
		$couponCode = $_REQUEST["coupon_code"];
		$couponDiscount = $_REQUEST["coupon_discount"];
		$orderPriceDiscount = $_REQUEST["order_price_discount"];

		$_SESSION["orders"][$productNo][$productSpecNo]["product_name"] = $productName;
       	$_SESSION["orders"][$productNo][$productSpecNo]["product_no"] = $productNo;
		$_SESSION["orders"][$productNo][$productSpecNo]["price"]= $price; //最終售價(單價)
		$_SESSION["orders"][$productNo][$productSpecNo]["cate_no"]= $cateNo; //品牌編號
		$_SESSION["orders"][$productNo][$productSpecNo]["cate_name"]= $cateName; //品牌名稱
		$_SESSION["orders"][$productNo][$productSpecNo]["product_spec_no"]= $productSpecNo;
		$_SESSION["orders"][$productNo][$productSpecNo]["product_spec_info"]= $productSpecInfo;
		$_SESSION["orders"][$productNo][$productSpecNo]["quanty"]=$quanty; //數量
		$_SESSION["orders"][$productNo][$productSpecNo]["vendor_no"]=$vendorNo;
		$_SESSION["orders"][$productNo][$productSpecNo]["vendor_name"]=$vendorName;
		$_SESSION["orders"][$productNo][$productSpecNo]["coupon_no"]=$couponNo;
		$_SESSION["orders"][$productNo][$productSpecNo]["coupon_name"]=$couponName;
		$_SESSION["orders"][$productNo][$productSpecNo]["coupon_code"]=$couponCode;
		$_SESSION["orders"][$productNo][$productSpecNo]["coupon_discount"]=$couponDiscount;
		$_SESSION["orders"][$productNo][$productSpecNo]["order_price_discount"]=$orderPriceDiscount;

		echo true; //儲存成功
		break;
	case 'cartComboBeforeCheckOut':	//加購價 儲存DB前先行轉存session	
		$product_combo_ID = $_REQUEST['id'];
		$productNo = $_REQUEST["product_no"];
		$productName = $_REQUEST["product_name"];
		$specInfo = $_REQUEST["spec_info"];
		$specNo = $_REQUEST["spec_no"];
		$price = $_REQUEST["price"]; 
		$quanty = $_REQUEST["quanty"];
		$vendorNo = $_REQUEST["vcate_no"];
		$vendorName = $_REQUEST["vcate_name"];
		$cateNo = $_REQUEST["cate_no"];
		$cateName = $_REQUEST["cate_name"];

		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["combo_ID"] = $product_combo_ID;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["product_no"] = $productNo;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["product_name"]= $productName;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["price"]= $price; //最終售價(單價)
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["quanty"]=$quanty; //數量
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["spec_no"]=$specNo;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["spec_info"]=$specInfo;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["vendor_no"]=$vendorNo;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["vendor_name"]=$vendorName;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["cate_no"]=$cateNo;
		$_SESSION["orderCombos"][$vendorNo][$product_combo_ID]["cate_name"]=$cateName;

		echo true; //儲存成功
		break;	
	case "checkSessionKey";	
		// ===儲存session 讓deliver.php 知道是從cart.php來的====
		$_SESSION["checkSessionKey"] = "ok";
		break;
	case "checkSessionKey-repay";	
		// ===儲存session 讓歷史清單裡未結帳的訂單重新結帳 
		$_SESSION["checkSessionKey-repay"] = "ok";
		break;	
	case "deleteCartDetail"	:
		$productNo = $_REQUEST['product_no'];
		$productSpecNo = $_REQUEST['product_spec_no'];

		if(isset($_COOKIE["orders"])){ //有存在購物清單
			$orderlist = json_decode($_COOKIE["orders"], true) ; 

			unset($orderlist[$productNo][$productSpecNo]);
			
			if(count($orderlist[$productNo]) == 0 ){
				unset($orderlist[$productNo]);
			}

			setcookie("orders",json_encode($orderlist),time()+(60*60*24*30),"/");


			// 刪除加購價cookie
			$orderComboList = [];
			if(isset($_COOKIE["comboorders"])){
				$comboorders = json_decode($_COOKIE["comboorders"], true);
				foreach ($comboorders as $orderskey => $ordersvalue) {
					if (!in_array($orderskey, $orderComboList) && $ordersvalue["quanty"] >= 1)
						array_push($orderComboList, $orderskey);
				}
			}
			$comboProducts = get_comboProducts_by_productId($productNo);
			if ($comboProducts && count($comboProducts) >= 1) {
				foreach ($comboProducts as $key2 => $value2) {
					if (in_array($value2["id"], $orderComboList)) {
						unset($comboorders[$value2["id"]]);
					}
				}		
			}	
				
			setcookie("comboorders",json_encode($comboorders),time()+(60*60*24*30),"/");

			echo true;

			
		}else{ //沒有購物清單
			echo false ; 
		}
		

		break;
	case "quantyAndCouponEdit":
		$db= new DB();
		//1.修改庫存
		foreach ($_SESSION["orders"] as $key => $value) {
			foreach ($value as $speckey => $specvalue) {
				$quanty = $specvalue["quanty"];
				$productNo = $specvalue["product_no"];
				$productSpecNo = $specvalue["product_spec_no"];
				$Quantysql = "UPDATE product_spec set product_stock=product_stock - ".(int)$quanty." where product_spec_no = ".$productSpecNo;
				$db -> exec($Quantysql);

				//1-2.修改庫存加價購
				$orderComboList = [];
				$orderComboListQty = [];
				if(isset($_COOKIE["comboorders"])){
					$comboorders = json_decode($_COOKIE["comboorders"], true);
					foreach ($comboorders as $orderskey => $ordersvalue) {
						if (!in_array($orderskey, $orderComboList) && $ordersvalue["quanty"] >= 1)
							array_push($orderComboList, $orderskey);
							$orderComboListQty[$orderskey]['quanty'] = $ordersvalue["quanty"];
					}
				}
				$comboProducts = get_comboProducts_by_productId($productNo);
				if ($comboProducts && count($comboProducts) >= 1) {
					foreach ($comboProducts as $key2 => $value2) {
						if (in_array($value2["id"], $orderComboList)) {
							$comboquanty = $orderComboListQty[$value2["id"]]['quanty'];
							$comboproductNo = $value2["product_no"];
							$comboproductSpecNo = $value2["product_spec_no"];
							$comboQuantysql = "UPDATE product_spec set product_stock=product_stock - ".(int)$comboquanty." where product_spec_no = ".$comboproductSpecNo;
							$db -> exec($comboQuantysql);
						}
					}		
				}	
			}
		}
		//1-2.修改庫存加價購
		// foreach ($_SESSION["comboorders"] as $key => $value) {
		// 	foreach ($value as $speckey => $specvalue) {
		// 		$quanty = 1;
		// 		$productNo = $specvalue["product_no"];
		// 		$productSpecNo = $specvalue["product_spec_no"];
		// 		$Quantysql = "UPDATE product_spec set product_stock=product_stock - ".(int)$quanty." where product_spec_no = ".$productSpecNo;
		// 		$db -> exec($Quantysql);
		// 	}
		// }


		//2.如有使用優惠劵 扣除優惠卷使用額度
		$couponCount = 0;
		foreach ($_SESSION["orders"] as $key => $value) {
			foreach ($value as $speckey => $specvalue) {
				if($specvalue["coupon_no"] != ""){ //有優惠劵使用
					$couponCount++;
					$couponNo = $specvalue["coupon_no"];
				}
			}
		}
		if($couponCount != 0){ //有使用優惠卷
			$couponUseSql = "UPDATE coupon set coupon_times=coupon_times - ".(int)"1"." where coupon_no = ".$couponNo;
			$db -> exec($couponUseSql);
		}

		break;	
	case "clearAll":
		// a. 一般訂單-------------------------------------
		// 1.刪除session暫存
		if(isset($_SESSION["orders"])){ //存在舊的紀錄 刪除
			unset($_SESSION["orders"]);
		}else{
			echo "session don`t have";
		}
		// 2.刪除cookie清單
		if(isset($_COOKIE["orders"])){ //有存在購物清單
			setcookie("orders","",time()-3600,"/");
		}else{
			echo "cookie don`t have";
		}

		// b. 加購價--------------------------------------
		// 1.刪除session暫存
		if(isset($_SESSION["orderCombos"])){ //存在舊的紀錄 刪除
			unset($_SESSION["orderCombos"]);
		}else{
			echo "comboorders session don`t have";
		}
		// 2.刪除cookie清單
		if(isset($_COOKIE["comboorders"])){ //有存在購物清單
			setcookie("comboorders","",time()-3600,"/");
		}else{
			echo "comboorders cookie don`t have";
		}

		echo true;
		break; 	
	default:
		
		break;
}



// print_r($productCate);
?>