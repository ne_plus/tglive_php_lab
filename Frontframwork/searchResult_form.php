<?php  

// 4. 組合Sql搜尋方式
$searchSQLStr = '';
for($s=0 ; $s<count($searchArrFinal) ; $s++ ) {
	if($s == 0){
		$searchSQLStr = "a.product_name like '%".$searchArrFinal[$s]."%' or a.product_content like '%".$searchArrFinal[$s]."%' or c.tag_name like '%".$searchArrFinal[$s]."%'";
	}else{
		$searchSQLStr .= " or a.product_name like '%".$searchArrFinal[$s]."%' or a.product_content like '%".$searchArrFinal[$s]."%' or c.tag_name like '%".$searchArrFinal[$s]."%'";
	}
}

//計算總數量
$sqlCount =sprintf("SELECT * FROM product a left join tag_products_relate b on a.product_no=b.product_no left join tag c on b.tag_no = c.tag_no  WHERE a.product_status=1 and ( %s ) group by a.product_no",$searchSQLStr); 
$statement = $db->pdo->query($sqlCount);
// $row = $statement->fetch(PDO::FETCH_NUM);
$row = $statement->rowCount();
$totalRecord = $row;//$row[0]
//每頁有幾筆
$recPerPage = 12;
//共有幾頁
$totalPage = ceil($totalRecord/$recPerPage);

if(isset($_REQUEST["pageNo"])==false){
	$pageNo=1;
}else{ 
$pageNo=$_REQUEST["pageNo"];
}
$pageStart = ($pageNo-1) * $recPerPage;

	//抓取資料庫比數 每頁12筆
$sql =sprintf("SELECT * FROM product a left join tag_products_relate b on a.product_no=b.product_no left join tag c on b.tag_no = c.tag_no  WHERE a.product_status=1 and ( %s ) group by a.product_no limit $pageStart,$recPerPage ",$searchSQLStr);




$resultProduct = $db->DB_Query($sql);
if($resultProduct){
	$products = [];
	foreach ($resultProduct as $key => $value) {
		$products[$key]["product_no"] = $value["product_no"];
		$products[$key]["product_name"] = $value["product_name"];
		$products[$key]["product_subtitle"] = $value["product_subtitle"];
		$products[$key]["img1"] = $value["img1"];
		$products[$key]["product_status"] = $value["product_status"];
		$products[$key]["product_price_sort"] = $value["product_price_sort"];
		$sql = "select * from product where product_no=:product_no" ;
		$dic=array(":product_no"=>$value["product_no"]);
		$product = new Product($sql,$dic); //product DB initial
		// -------------標籤搜尋
		$tagRelate = $product->productRelateTag();
		if($tagRelate){ //有標籤存在
			foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
				$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
				$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
			}
			 
		}else{ //沒有標籤
			$products[$key]["tag_name"] = null ;
			$products[$key]["tag_no"] =null ;
		}

		// -------------分類搜尋
		
		$resultRelate = $product->productRelateCate();
		if($resultRelate){ //有商品分類
			foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
				$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
				$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
				$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
				$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
				if($valuecateRelate["cate_parents"] != 0 ){//有父層
						$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
						$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
						$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
				}else{ //沒有父層
					$products[$key]["cate_father_name"] = null ;
					$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
				}
			}

		}else{ // 沒有商品分類
			$products[$key]["cate"] = null;
		}

		// ---------產品規格-----
		$resultSpec = $product->productSpecUseInfo();
		if($resultSpec){ //使用中的規格
			foreach ($resultSpec as $keySpec => $valueSpec){
				$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
				$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
				if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
					$products[$key]["product_spec_price_discount"][$keySpec]= null ;
				}else{ //有折扣
					$priceDiscount = $valueSpec["product_spec_price2"];
					if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
						$priceDiscount = $valueSpec["product_spec_price3"];
						$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
					}elseif($valueSpec["product_spec_price3"] == 0){
						$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
					}else{
						$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
					}
				}
				
			}
			
		}else{ //沒有規格
			$products[$key]["product_stock"][$keySpec] = null;
		}

	}
	$array = array("recordsTotal"=>$totalRecord);
	$array["totalPage"] = $totalPage;
	$array["pageNo"] = $pageNo;
	$array["data"] = $products;
	// echo "<pre>";
	// print_r($array);
	// echo "</pre>";
}else{
	$products = [];
	$array = array("recordsTotal"=>$totalRecord);
	$array["totalPage"] = $totalPage;
	$array["pageNo"] = $pageNo;
	$array["data"] = $products;
	// echo json_encode($array);
}


?>