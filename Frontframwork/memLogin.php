<?php  
ob_start();
session_start();
date_default_timezone_set("Asia/Taipei");
include("../controller/userControl.php");


// ====ip get======
if (!function_exists('eregi'))
{
    function eregi($pattern, $string)
    {
        return preg_match('/'.$pattern.'/i', $string);
    }
}
function get_real_ip(){
	 $ip=false;
	 if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	  	$ip = $_SERVER["HTTP_CLIENT_IP"];
	 }
	 if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  	$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip){
		array_unshift($ips, $ip); $ip = FALSE;
		}
		for($i = 0; $i < count($ips); $i++){
			if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])){
				$ip = $ips[$i];
				break;
			}
		}
	 }
	 return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
$ipAdd = get_real_ip();

// $table = $_REQUEST["table"];
$table = $_REQUEST["table"];
$email = $_REQUEST["mem_email"];
$psw = md5($_REQUEST["mem_password"]);

$check = query($table,$email,$psw);
if($check){
	if($check["mem_status"] == 0){
		echo json_encode("denied"); //停權
	}else{
		$feedback =[];
		$memNo = $check["mem_no"];
		$memMail = $check["mem_mail"];
		$memFirstName = $check["mem_firstname"];
		$memLastName = $check["mem_lastname"];
		$memToken = md5($memNo.$memMail);


		$feedback["memFirstName"]=$memFirstName;
		$feedback["memLastName"]=$memLastName;

	
		//session 方式
		$_SESSION["memNo"] = $memNo;
		$_SESSION["memMail"] = $memMail;
		$_SESSION["memFirstName"] =$memFirstName;
		$_SESSION["memLastName"] =$memLastName;

		// ---儲存 最後登入時間-----
		$item= array(
			"mem_no" => $memNo,
			"mem_lastLoginTime" =>time(),
			"mem_token" => $memToken,
			"mem_loginIP" => $ipAdd
		);
		if(edit($table,$memMail,$item)){

			// =====儲存cookie memToken====
			setcookie("mem_token",$memToken,time()+(60*60*24*30*12),"/");
			echo json_encode($feedback); //成功
		}else{
			echo  false; //更新失敗
		}
	}
}else{
	echo false;//"無帳號"
};

?>