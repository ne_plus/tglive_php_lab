<?php require_once("module/header.php"); 


include("Frontframwork/ArticleDB.php");
$pid = isset( $_REQUEST["pid"] )? $_REQUEST["pid"] : null ; //文章ID
$page = isset( $_REQUEST["page"] )? $_REQUEST["page"] : null ; //頁面
$tagId = isset( $_REQUEST["tag_no"] )? $_REQUEST["tag_no"] : null ; //標籤id
$status = isset( $_REQUEST["tag_no"] )? "tag" : "article"; //文章ID搜尋文章 or 標籤搜尋所有文章
$recPerPage = 8; //每一分頁要幾篇內容
$article = new Article( $status , $pid , $tagId , $page , $recPerPage ); 
$articles = $article -> articleQuery(); 
$page = $article-> pagenation();
$tagAll = $article->tagArticleRelateQueryAll() ;
$latest = $article->getArticleLatest(5); //可以填寫需要抓取的數量

if( isset( $_REQUEST["tag_no"] ) ){
	$tagInfo = $article->getTagInfo( $_REQUEST["tag_no"] ) ;
}


?>


	<section class="styleshow marginTop100">
					<ol class="breadcrumb">
						  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
						  <?php  
						  	if(  !isset( $_REQUEST["tag_no"] ) ){ 
						  ?>
						  <li class="breadcrumb-item active"><?=$lang_menu_style?></li>
						  <?php		
						  	}else{
						  ?>
						  <li class="breadcrumb-item">
						  	<a href="products.php"><?=$lang_menu_style?></a>
						  	<form action="styles.php" method="get"></form>
						  </li>
						  <li class="breadcrumb-item active"><?php echo $tagInfo[0]["tag_name"] ?></li>
						  <?php		
						  	}
						  ?>
						  
					</ol>
			
		<div class="container mt-3">
			<div class="row">		
				<div class="col-12 col-lg-8">
					<div class="page">
						<?php  
							foreach ($articles as $key => $value) {
						?>
						<div class="style-box row">					
							<div class="style-img col-12 col-md-7">
								<a href="styles-detail.php?pid='<?php echo $value["article_no"] ?>'">
									<div class="style-img-box">
										<img src="<?php echo $value["article_img"]; ?>">
										<div class="style-hover-box">
											<div class="style-hover-box-inner">
												<div>Read more</div>
												<div class="lineUp"></div>
												<div class="lineRight"></div>
												<div class="lineLeft"></div>
												<div class="lineDown"></div>
											</div>
										</div>
									</div>
								</a>	
							</div>
							<div class="style-content d-flex flex-wrap flex-column align-content-center col-12 col-md-5">
								<div class="title"><?php echo $value["article_title"]; ?></div>
								<div class="subtitle"><span><?php echo date("Y-m-d",$value["article_createtime"]); ?></span><span class="mx-1">|</span><span>words by <?php echo $value["article_owner"]; ?></span></div>
								<div>
									<?php  
										foreach ($value["tag_article_relate"] as $tagkey => $tagvalue) {
									?>
									<span class="article-tag"><a href="?tag_no=<?php echo $tagvalue['tag_no']; ?>">#<?php echo $tagvalue['tag_name']; ?></a></span>	
									<?php  
										} //end tag for loop
									?>
								</div>
								<div class="article my-2"><?php echo $value["article_describe"]; ?>...</div>
								<div>
									<a href="styles-detail.php?pid='<?php echo $value["article_no"] ?>'">&lt; Read more &gt;</a>
								</div>
							</div>
						</div>
						<?php } //end articles for loop?>
					</div>
					



					<div class="shop_pagination_area mt-3">
	                    <nav aria-label="Page navigation example">
						  <ul class="pagination justify-content-center">
						    <li class="page-item pgPre <?php if( $page['pageNo'] == 1 ){echo 'disabled';} ?>">
						      <a class="page-link" href="?page=<?php echo ($page['pageNo']-1); ?>" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						        <span class="sr-only">Previous</span>
						      </a>
						    </li>

						    <?php 
						    	for($p = 1 ; $p <= $page["totalPage"] ; $p++){ 
						    		if($p == $page["pageNo"]){
						    ?>
							<li class="page-item active page <?php echo "pg".$p ?>"><a class="page-link" href="<?php echo "?page=$p" ;?>"><?php echo $p; ?></a></li>
						    <?php			
						    		}else{
						    ?>
							<li class="page-item page <?php echo "pg".$p ?>"><a class="page-link" href="<?php echo "?page=$p" ;?>"><?php echo $p; ?></a></li>	
							<?php
								}
							}
						    ?>
						    <li class="page-item pgNext <?php if( $page['totalPage'] == $page['pageNo'] ){echo 'disabled';} ?>" >
						      <a class="page-link" href="?page=<?php echo ($page['pageNo']+1); ?>" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						        <span class="sr-only">Next</span>
						      </a>
						    </li>
			

						  </ul>
						</nav>
	                </div>                
				</div>

				<div class="col-12 col-lg-4">
					<div class="article-history">
						<div class="history-title latest">
							<?=$lang_style_latest?>
						</div>
						<div class="history-pass row">
							<?php  
								foreach ($latest as $key => $value) {
								
							?>
							<div class="history-img col-4 col-lg-5 mb-3">
								<a href="styles-detail.php?pid=<?php echo  $value['article_no']; ?>">
									<div class="history-img-box">									
										<img src="<?php echo $value["article_img"]; ?>" >
										<div class="style-hover-box">
											<div class="style-hover-box-inner">
												<div>Read more</div>
												<div class="lineUp"></div>
												<div class="lineRight"></div>
												<div class="lineLeft"></div>
												<div class="lineDown"></div>
											</div>
										</div>									
									</div>
								</a>
							</div>
							<div class="history-content col-8 col-lg-7">
								<div class="content-box">
									<a href="styles-detail.php?pid=<?php echo  $value['article_no']; ?>"><?php echo $value["article_title"]; ?></a>
									<div class="history-content-time"><?php echo date("Y-m-d",$value["article_createtime"]); ?>
										
									</div>
								</div>
							</div>
							<?php } ?>
						</div>

						<div class="history-title index">
							<?=$lang_style_index?>
						</div>	
						<div class="history-index row">
						<?php  
							foreach ($tagAll as $key => $value) {
						?>
							<div class="col-12"><a href="?tag_no=<?php echo $value['tag_no']; ?>"><span><?php echo $value["tag_name"]; ?></span><span>(<?php echo $value["count(*)"]; ?>)</span></a></div>
						<?php } ?>	
						</div>	

						<!-- <div class="history-title top">
							top hit
						</div>
						<div class="history-top row">
							<div class="top-content col-12">
								<div class="content-box"><a href="">三種戶外冒險必備的錦囊妙計</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">薑母鴨的冬天 VS 羊肉爐的四季</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">gogoro & GOGOGO Fire in the Hole</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">三種戶外冒險必備的錦囊妙計</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">薑母鴨的冬天 VS 羊肉爐的四季</a></div>
							</div>
							<div class="top-content col-12">
								<div class="content-box"><a href="">gogoro & GOGOGO Fire in the Hole</a></div>
							</div>
						</div> -->
							
					</div>
				</div>
                

			</div>
		</div>
	</section>
	
  
<?php require_once("module/footer.php"); ?>