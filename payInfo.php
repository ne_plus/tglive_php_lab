<?php require_once("module/header.php"); ?>
<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_cart_payway?>
			
		</ol>
		<div class="container">
			
			
			<div class="row">
                <h3><?=$lang_cart_payway?></h3>
                 <p>目前付款方式有：信用卡一次付清、信用卡3、6、12期0利率、現金匯款、超商付款</p>
                <div class="subtitle">信用卡一次付清、分期</div>
               <p>本站接受 VISA、Master、JCB、銀聯卡，輸入資料後等待銀行授權，填入信用卡背面簽名欄旁末三碼數字，以利訂單處理快速又安全。採用SSL 128 bits 最安全加密機制。</p>
              <p> 全館刷卡 3 期 0 利率；結帳金額滿 3,000 元以上享 6 期 0 利率；結帳金額滿 10,000 元以上享 12 期 0 利率。</p>
<p>合作銀行：中國信託、台新、玉山、富邦、遠東、永豐、國泰世華、華南、日盛、樂天、安泰、聯邦、兆豐、台中商銀、上海銀行、凱基、匯豐、星展、新光、合庫、彰銀、一銀、元大、陽信、台灣企銀、華泰、三信商銀
</p>
<p>本站接受 VISA、Master、JCB、銀聯卡，輸入資料後等待銀行授權，填入信用卡背面簽名欄旁末三碼數字，以利訂單處理快速又安全。 採用SSL 128 bits 最安全加密機制。
</p>
               <span class="highlight">※請款日由依各家信用卡公司而定。</span>
               

                <div class="subtitle">超商付款</div>
                <p>可選擇711、全家、OK、萊爾富付款，超商代碼繳費期限為7天，請務必於期限內進行付款，若您無法取得或遺失超商繳費代碼，請與居生活客服人員聯繫，或重新下單。</p>
                 <p>操作流程可參閱<a href="https://www.ecpay.com.tw/service/pay_way_cvpay" target="_blank">ECPAY</a></p>
                
                <ul class="nav nav-tabs"  role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="seven-tab" href="#seven" data-toggle="tab" role="tab" aria-controls="seven" aria-selected="true">7-11</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="family-tab" href="#family" data-toggle="tab" role="tab" aria-controls="family" aria-selected="false">全家</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="hilife-tab" href="#hilife" data-toggle="tab" role="tab" aria-controls="hilife" aria-selected="false">萊爾富</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="ok-tab" href="#ok" data-toggle="tab" role="tab" aria-controls="ok" aria-selected="false">OK</a>
                    </li>
                </ul>  
                <div class="tab-content" id="myTabContent" style="margin-bottom: 30px">
                    <div class="card tab-pane fade show active" style="border-top: none;" id="seven" role="tabpanel" aria-labelledby="seven-tab">
                        <div class="card-body">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon_a.jpg"  alt="First slide">
                                        <div class="text-center step-info my-2">1.請選擇代碼輸入</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon_b.jpg" alt="Second slide">
                                        <div class="text-center step-info my-2">2.輸入繳費代碼。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon_c.jpg" alt="Three slide">
                                        <div class="text-center step-info my-2">3.資料傳輸中。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon_d.jpg" alt="Forth slide">
                                        <div class="text-center step-info my-2">4.確認金額、項目無誤後，列印繳費單。</div>
                                    </div>
                                     <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon_e.jpg" alt="Forth slide">
                                        <div class="text-center step-info my-2">5.臨櫃繳費。</div>
                                    </div>
                                     <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/7-11/ibon6.jpg" style="max-width: 200px" alt="Forth slide">
                                        <div class="text-center step-info my-2">6.取得繳費收據。</div>
                                    </div>
                                     
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>   
                    <div class="card tab-pane fade" style="border-top: none;" id="family" role="tabpanel" aria-labelledby="family-tab">
                        <div class="card-body">
                            <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block" src="image/frontend/paycode/family/fam001.png"  alt="First slide">
                                        <div class="text-center step-info my-2">1.選擇「繳費」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam002.jpg" alt="Second slide">
                                        <div class="text-center step-info my-2">2.選擇「代碼繳費」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam003.jpg" alt="Three slide">
                                        <div class="text-center step-info my-2">3.請詳細閱讀下列條款。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam004.jpg" alt="Forth slide">
                                        <div class="text-center step-info my-2">4.輸入繳費代碼(共14碼)。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam005.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">5.再次確認繳費代碼。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam006.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">6.確認金額、項目無誤後，列印繳費單。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/family/fam007.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">7.臨櫃繳費。</div>
                                    </div>
                                    
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>  
                    <div class="card tab-pane fade" style="border-top: none;" id="hilife" role="tabpanel" aria-labelledby="hilife-tab">
                        <div class="card-body">
                            <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_1.jpg"  alt="First slide">
                                        <div class="text-center step-info my-2">1.選擇「代收繳款」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_2.jpg" alt="Second slide">
                                        <div class="text-center step-info my-2">2.選擇「網路交易」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_3.jpg" alt="Three slide">
                                        <div class="text-center step-info my-2">3.選擇「ECPay綠界科技」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_4.png" alt="Forth slide">
                                        <div class="text-center step-info my-2">4.選擇「代碼繳費」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_5.png" alt="Fifth slide">
                                        <div class="text-center step-info my-2">5.輸入「繳費代碼」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_6.png" alt="Fifth slide">
                                        <div class="text-center step-info my-2">6.請詳細閱讀下列條款。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_7.png" alt="Fifth slide">
                                        <div class="text-center step-info my-2">7.再次確認交易明細。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/hi-life/life_8.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">8.繳費單例印中，請持繳費單臨櫃繳款。</div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>  
                    <div class="card tab-pane fade" style="border-top: none;" id="ok" role="tabpanel" aria-labelledby="ok-tab">
                        <div class="card-body">
                            <div id="carouselExampleIndicators4" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go1.jpg"  alt="First slide">
                                        <div class="text-center step-info my-2">1.選擇「繳費」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go2.jpg" alt="Second slide">
                                        <div class="text-center step-info my-2">2.選擇「代碼繳費」。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go3.jpg" alt="Three slide">
                                        <div class="text-center step-info my-2">3.請詳細閱讀下列條款。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go4.jpg" alt="Forth slide">
                                        <div class="text-center step-info my-2">4.輸入繳費代碼(共14碼)。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go5.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">5.再次確認繳費代碼。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go6.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">6.確認金額、項目無誤。</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block" src="image/frontend/paycode/ok/ok-go7.jpg" alt="Fifth slide">
                                        <div class="text-center step-info my-2">7.列印繳費單後，臨櫃繳費。</div>
                                    </div>
                                    
                                </div> <!-- carousel-inner -->
                                <a class="carousel-control-prev" href="#carouselExampleIndicators4" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators4" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div><!-- carouselExampleIndicators4 -->
                        </div><!-- card-body -->
                    </div>  <!--  card    -->  
                </div><!-- tab-content -->
				  <div class="subtitle">現金匯款</div>
                <p>訂單成立後會產生專屬的付款虛擬帳號，請於三日內匯款。若您無法取得或遺失虛擬帳號，請與居生活客服人員聯繫，或重新下單。</p>
				<p>操作流程可參閱<a href="https://www.ecpay.com.tw/Service/pay_qa_atm_acntr" target="_blank">ECPAY</a></p>
				<ul>注意事項 ：
					<li>選擇結帳頁任一銀行之金融卡並至同銀行ATM提款機進行轉帳享 0 元手續費；至他行ATM提款機則須支付跨行手續費 15 元。</li>
					<li>若無以結帳頁上提供銀行之金融卡，可任意選擇或點選其他金融機構進行付款，須支付跨行手續費 15 元。</li>
					<li>例：玉山金融卡在玉山ATM提款機轉帳享 0 元手續費；玉山金融卡在非玉山ATM提款機轉帳收取 15 元手續費 / 筆。</li>
					<li>ATM櫃員機，可選擇轉帳/轉出(繳費單筆上限3萬元)或繳費(無上限3萬元限制)之功能按鈕；若為使用第一銀行ATM櫃員機，請選擇「繳費」按鈕。</li> 
					<li>適用【繳費】功能之銀行ATM櫃員機：台新銀行、玉山銀行、中國信託、華南銀行、第一銀行、富邦銀行、台灣銀行、土地銀行、彰化銀行、永豐銀行、國泰世華銀行。( 無上限3萬元限制 )</li>
					<li>各銀行ATM繳款帳號，若金額錯誤、逾期繳費、重覆繳款，是經由銀行端機制進行檢核ATM繳款帳號資訊，綠界科技無法進行金額錯誤、逾期繳費、重覆繳款的訂單阻擋。</li>
					<li>ATM櫃員機繳款帳號如須使用網路銀行轉帳，須自行向銀行申請開通「非約定轉帳」功能，如網銀無法使用請改用實體櫃員機進行轉帳。</li>
				</ul>
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>	
    <style>
        .pages .container .nav-tabs li{
            margin:0;
            margin-bottom: -1px;
        }
        .parcode-step{
            background-color: rgba(255,200,207,.3);
            display: inline-block;
            padding: 10px;
            border-radius: 5px;
            color: #f67f90;
            margin-bottom: 10px;
        }
        .step-info{
            font-size:16px;
        }
        .carousel img{
            margin:auto;
            width:100%;
        }
        @media screen and ( min-width : 992px ){
            .carousel img{
                width:60%;
            }
        } 
        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            height: 100px;
            width: 100px;
            outline: black;
            background-size: 100%, 100%;
            border-radius: 50%;
            background-image: none;
        }

        .carousel-control-next-icon:after
        {
            content: '>';
            font-size: 55px;
            color: #f67f90;
        }

        .carousel-control-prev-icon:after {
            content: '<';
            font-size: 55px;
            color: #f67f90;
        }
    </style>		

<?php require_once("module/footer.php"); ?>