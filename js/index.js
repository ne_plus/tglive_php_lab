$(document).ready(function(){
//RWD
 $("#nav-button-rwd").click(function(){
 	$(".nav-rwd").slideToggle();
 });
 $("#cancelButton-rwd").click(function(){
 	$(".nav-rwd").slideToggle();
 });
 $(".brandHoverTrigger-rwd").click(function(e){
 	e.preventDefault();
 	$(".brandHover-rwd").slideToggle();
 	var angleTransform = $(this).find(".fa-angle-left");
 	var classStr = $(".brandHover-rwd").attr("class");
 	if( classStr.indexOf("active")!= -1 ){
 		angleTransform.css({transform:"rotateZ(0deg)",transition:".5s"});
 		$(".brandHover-rwd").removeClass("active");
 	}else{
 		angleTransform.css({transform:"rotateZ(-90deg)",transition:".5s"});
 		$(".brandHover-rwd").addClass("active");
 	}
 });
 $("#cartListCheck-rwd").click(function(e){
 	e.preventDefault();

 	$(".cartList-rwd").css("right","0");
 	
 });
 $("#cancelCartButton-rwd").click(function(){
 	$(".cartList-rwd").css("right","-110%");
 });
 $("#brandsBtn-rwd").click(function(){
 	$(".productshow.brands .sidebar").animate({left:'15px'},500);
 });
 $("#cancelBrandsButton-rwd").click(function(){
 	$(".productshow.brands .sidebar").animate({left:'-110vw'},500);
 });
 $(window).resize(function(){
 	rwd();
 });		
 function rwd(){
 	if($(window).width() > 991 ){
 		$(".nav-rwd").slideUp();
 		$(".cartList-rwd").css("right","-110%");
 		$(".product-items .product-name").css("font-size","14px");
 		$(".product-items .price-new").css("font-size","16px");
 		$(".product-items .price-old").css("color","#000");
 	}else{
 		$(".product-items .product-name").css("font-size","12px");
 		$(".product-items .price-new").css("font-size","14px");
 		$(".product-items .price-old").css("color","#999");
 	}
 };
 rwd(); //initial



// -----導覽列----
	$("header .brandHoverAct").hover(function(){
		// console.log($(this));
		$(this).addClass("mouseon");
	},function(){
		$(this).removeClass("mouseon");
	});
	var webLocation = window.location.href;
	if(webLocation.match("/memcenter.php") != null || webLocation.match("/memhistory.php") !=null){
			
	}else{
		$(window).scroll(function(){
			var bannerSpace = $("header").height(); 
			var bannerSpaceMinus = "-"+bannerSpace;
			var mouseOnCheck = $("header .brandHoverAct");
			if($(window).scrollTop()>bannerSpace){
				$(window).mousewheel(function(e){
					if(e.deltaY<0 && $(window).scrollTop()>bannerSpace && !(mouseOnCheck.attr("class").search("mouseon") != -1)){ //滑鼠往下滑 (以及確認是否有hover 狀態)
						$("header").css("top",bannerSpaceMinus+"px");
					}else{ //滑鼠往上滑
						$("header").css("top",0);
					};
				});
				$(".gotop").show(500);
			}else{
				$(".gotop").hide(500);
				$("header").css("top",0);
			}
		});
	}
	

	// $("button.navbar-toggler").click(function(){
	// 	// $("header").animate();
	// 	$("header").css("height","320px");
	// });

// -----導覽列選單----
	$("header .brandDetail > a").hover(function(){
		// console.log($(this).parent().attr("id"));
		var currentImg = ".img"+$(this).parent().attr("id");
		// // console.log(currentImg);
		$("header .imgbrand").hide();
		$("header .imgbrand").css("opacity","0");
		$(currentImg).show();
		$(currentImg).css({
			opacity :'1',
			transition : '2s'
		});
	},function(){
		$("header .imgbrand").css("transition","0s");
	});

// -----登入畫面----
	//RWD-----
	$("#login-rwd").click(function(e){
		e.preventDefault();
		$(this).attr("data-toggle","modal");
		$(this).attr("data-target","#loginBox");
	});
	//RWD-----


	$("#login").click(function(){
		$(this).attr("data-toggle","modal");
		$(this).attr("data-target","#loginBox");

		$("#loginBox .label-custom").removeClass("active");
		if($("#loginBox .modal-body form #login-username").val().trim().length != 0 ){ //有輸入值(帳號)
			$("#loginBox .modal-body form #login-username").siblings(".label-custom").addClass("active");
			$("#loginBox .modal-body form .login-username-describe").show(300);
		}

		if($("#loginBox .modal-body form #login-password").val().trim().length != 0){ //有輸入值(密碼)
			$("#loginBox .modal-body form #login-password").siblings(".label-custom").addClass("active");
			$("#loginBox .modal-body form .login-password-describe").show(300);
		}
	});
	
	$("#loginBox .modal-body form input").click(function(){
		$("#loginBox .label-custom").removeClass("active");
		var loginLabel = $(this).siblings(".label-custom");
		if(loginLabel.attr("class").search("active") != -1){
			loginLabel.removeClass("active");
		}else{			
			loginLabel.addClass("active");
		}

		if($("#loginBox .modal-body form #login-username").val().trim().length != 0 ){ //有輸入值(帳號)
			$("#loginBox .modal-body form #login-username").siblings(".label-custom").addClass("active");
		}

		if($("#loginBox .modal-body form #login-password").val().trim().length != 0){ //有輸入值(密碼)
			$("#loginBox .modal-body form #login-password").siblings(".label-custom").addClass("active");
		}
	});

	$("#loginBox .modal-body form #login-username").keyup(function(){
		if($("#loginBox .modal-body form #login-username").val().trim().length != 0 ){ //有輸入值(帳號)
			$("#loginBox .modal-body form .login-username-describe").show(300);
			$("#loginBox .modal-body form #login-username").siblings(".label-custom").addClass("active");
		}
	});	
	$("#loginBox .modal-body form #login-password").keyup(function(){
		if($("#loginBox .modal-body form #login-password").val().trim().length != 0){ //有輸入值(密碼)
			$("#loginBox .modal-body form .login-password-describe").show(300);
			$("#loginBox .modal-body form #login-password").siblings(".label-custom").addClass("active");
		}
	});	

	// ====登入按鈕====
	$("#loginButton").click(function(){
		var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 
		var pswReg = /^\w{6,}$/; //password
		var userAcount = $("#loginBox .modal-body form #login-username").val().trim();
		var userPassword = $("#loginBox .modal-body form #login-password").val().trim();
		if(emailReg.test(userAcount) === false ){ //帳號格式驗證
			swal({
				  title: "請輸入E-Mail",
				  icon: "warning",
			}).then( function(confrim) {
				  if (confrim) {
				    $("#loginBox .modal-body form #login-username").siblings(".label-custom").addClass("active");
					$("#loginBox .modal-body form #login-username").select();
				  } 
			});	
		}else if(pswReg.test(userPassword) === false ){ //密碼格式驗證
			swal({
				  title: "請輸入6位數以上英數密碼",
				  icon: "warning",
			}).then( function(confrim) {
				  if (confrim) {
				    $("#loginBox .modal-body form #login-password").siblings(".label-custom").addClass("active");
					$("#loginBox .modal-body form #login-password").select();
				  } 
			});
		}else{ 
			var tableWrite = "member";
			var statusWrite = "查詢" ;
			var mem_emailWrite = userAcount;
			var mem_passwordWrite = userPassword;

			// ======如果沒有帳號重複建立新的會員====
			var memCheck = "";

			var statusCheck = "memMailCheck"; 
			$.ajax({
				url:"Frontframwork/memAjax.php",
				data : {status : statusCheck,mem_email : mem_emailWrite},
				type : "get",
				async : false,
			    cache : false,
			    success : function(result){
			    		if(result){ //true 有重複帳號
			    			memCheck =  1; //帳號重複
			    		}else{ // false 新用戶
			    			memCheck =  2; //新用戶
			    		}
					},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});
			if( memCheck ==1 ){ //mail是會員
				
				// 查詢密碼是否正確
				$.ajax({
					url : "Frontframwork/memLogin.php",
					data : {table : tableWrite,status : statusWrite,mem_email : mem_emailWrite,mem_password : mem_passwordWrite},
					type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //登入成功
				    		var success = JSON.parse(result);
				    		if(success == "denied"){
				    			swal({
								  title: "帳號已停權",
								  icon: "error",
								});
				    		}else{
				    			//關閉光箱
					    		$("#loginBox").modal('hide');

					    		// 寫入會員欄位
					    		$("li#memCenter").find("span.last-name").text(success["memLastName"]);
					    		$("li#memCenter").find("span.first-name").text(success["memFirstName"]);

					    		//登入登出狀態切換
					    		$("li#login").hide();
					    		$("li#memCenter").show();
					    		$("li#logout").show();

					    		location.reload();
				    		}
				    		
				    	}else{ //false 找不到帳號(因前面已經驗證過mail帳號有存在,這裡顯示密碼錯誤)
				    		swal({
							  title: "密碼錯誤",
							  icon: "error",
							});
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}else{ //非會員
				swal({
				  title: "帳號錯誤",
				  icon: "error",
				});
			}



			
		}
	});


	// ====登出按鈕====
	//RWD
	$("#logout-rwd").click(function(e){
		e.preventDefault();
		
		$.ajax({
			url : "Frontframwork/memLogout.php",
			type : "POST",
			async: false,
		    cache : false,
		    success : function(result){	
		    	// location.reload();
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		}).done(function() {
		  fbstatusCheck();
		});
	});
	//RWD end



	$("a#logout").click(function(e){
		e.preventDefault();
		$.ajax({
			url : "Frontframwork/memLogout.php",
			type : "POST",
			async: false,
		    cache : false,
		    success : function(result){
		    	//登入登出狀態切換
	    		$("li#login").show();
	    		$("li#memCenter").hide();
	    		$("li#logout").hide();

	    		// 寫入會員欄位
	    		$("li#memCenter").find("span.last-name").text("");
	    		$("li#memCenter").find("span.first-name").text("");
		    	
		    	// location.reload();
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		}).done(function(){
			fbstatusCheck();
		});
	});

	// 註冊
		//header
	$("#registerButton").click(function(){
		location.replace('register.php');
	});
		// register.php
	$("#registerConfirmButton").click(function(e){
		e.preventDefault();
		var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 
		var pswReg = /^\w{6,}$/; //password
		var userMail = $("input#userAcountCreate").val().trim();
		var userLastName = $("input#userLastNameCreate").val().trim();
		var userFirstName = $("input#userFirstNameCreate").val().trim();
		var userTel = $("input#userPhoneCreate").val().trim();
		var userAddr = $("input#userAddressCreate").val().trim();
		var userPsw = $("input#userPswCreate").val().trim();
		var userRePsw = $("input#userPswConfrimCreate").val().trim();

		$("input#userLastNameCreate").css("backgroundColor","");
		$("input#userFirstNameCreate").css("backgroundColor","");
		$("input#userPhoneCreate").css("backgroundColor","");	
		//帳號重複確認
		var memCheck = '';
		if( registerMemCheck(userMail) ){
			memCheck = 1 ; //新會員
		}else{
			memCheck = 2 ; //會員帳號重複
		}

		if( emailReg.test(userMail) === false ){
			swal({
			  title: "請輸入有效E-Mail",
			  icon: "error",
			}).then( function(confrim) {
				$("input#userAcountCreate").select();
			});
		}else if( memCheck == 2 ){
			swal({
				title: "帳號已經使用，請再次確認",
				icon: "error",
			}).then( function(confrim) {
				$("input#userAcountCreate").select();
			});
		}else if( pswReg.test(userPsw) === false  ){
			swal({
				title: "請輸入英數碼六位數以上",
				icon: "error",
			}).then( function(confrim) {
				$("input#userPswCreate").select();
			});
			return ;
		}else if( userPsw != userRePsw ){
			swal({
				title: "請確認輸入的'確認密碼'與'密碼'相同",
				icon: "error",
			}).then( function(confrim) {
				$("input#userPswConfrimCreate").select();
			});
			return;
		}else if( userLastName == '' || userFirstName == '' || userTel == '' ){
			swal({
				title: "請填妥必填資訊",
				icon: "error",
			  }).then( function(confrim) {
				  if(userLastName == ''){$("input#userLastNameCreate").css("backgroundColor","#f00");}
				  if(userFirstName == ''){$("input#userFirstNameCreate").css("backgroundColor","#f00");}
				  if(userTel == ''){$("input#userPhoneCreate").css("backgroundColor","#f00");}
			  });
		}else{
			//新增會員
			var newMemNo = registerMemCreate( userMail , userPsw , userLastName , userFirstName , userAddr , userTel );
			//發信
			newMemSendMail( userMail , userLastName , userFirstName , userTel ); //公司內部通知 and userEDM
			//儲存cookie 和session 
			memSessionAndCookie( userMail , userPsw );
			//reload to index
			location.reload();
		}

	});

	function registerMemCheck( mail ){
		var memCheck = "";

		var statusCheck = "memMailCheck"; 
		// =====檢查是否有重複帳號=====
		$.ajax({
			url:"Frontframwork/memAjax.php",
			data : {status : statusCheck,mem_email : mail},
			type : "get",
			async : false,
			cache : false,
			success : function(result){
					if(result){ //true 有重複帳號
						memCheck =  false; //帳號重複
					}else{ // false 新用戶
						memCheck =  true; //新用戶
					}
				},
			error : function(error){
					alert("傳送失敗");
				} 
		});
		return memCheck;
	}

	function registerMemCreate( mail , psw , lastName , firstName , address , tel ){
		var memNo = "";

		var statusCreate = "memCreate";
		$.ajax({
			url:"Frontframwork/memAjax.php",
			data : {status : statusCreate,mem_email : mail,mem_password : psw,mem_lastname : lastName, mem_firstname : firstName,mem_address : address,mem_tel : tel},
			type : "get",
			async : false,
			cache : false,
			success : function(result){
					var member = JSON.parse(result);
					memNo = member["mem_id"];
				},
			error : function(error){
					alert("傳送失敗");
				} 
		});	
		return memNo;
	}

	function newMemSendMail( mail , lastName , firstName , tel ){
		//發送mail 給管理員
		var statusNewMemCreate = "newMemMail";
		$.ajax({
			url:"Frontframwork/memAjax.php",
			data : {status : statusNewMemCreate,mem_email : mail,mem_lastname : lastName, mem_firstname : firstName,mem_tel : tel},
			type : "get",
			async : false,
			cache : false,
			success : function(result){
				},
			error : function(error){
				} 
		});
		//發送給個別用戶
		var statusNewMemCreate = "newMemMailEDM";
		$.ajax({
			url:"Frontframwork/memAjax.php",
			data : {status : statusNewMemCreate,mem_email : mail,mem_lastname : lastName, mem_firstname : firstName,mem_tel : tel},
			type : "get",
			async : false,
			cache : false,
			success : function(result){
				},
			error : function(error){
				} 
		});
	}
	function memSessionAndCookie( email , psw ){
		// ======儲存cookie 和 session=====
		var tableWrite = "member";
		var statusWrite = "查詢" ;
		var mem_emailWrite = email;
		var mem_passwordWrite = psw;
		$.ajax({
			url : "Frontframwork/memLogin.php",
			data : {table : tableWrite,status : statusWrite,mem_email : mem_emailWrite,mem_password : mem_passwordWrite},
			type : "POST",
			cache : false,
			success : function(result){
				if(result){ //登入成功
					var success = JSON.parse(result);
					if(success == "denied"){
						swal({
						title: "帳號已停權",
						icon: "error",
						});
					}else{   			
						// location.reload();
					}
					
				}else{ //false 找不到帳號
					swal({
					title: "帳號/密碼錯誤",
					icon: "error",
					});
				}
			},
			error : function(error){
				alert("傳送失敗");
			} 
		});
	}



	//fb登出狀態確認(如果使用fb登入)
	function fbstatusCheck(){
		var storage = sessionStorage;
		FB.getLoginStatus(function(response) {
	      if (response.status === 'connected') { //使用fb登入
	      	FB.logout(function(response) 
	        {
		        // user is now logged out

		        //清除session
		        storage.removeItem("FBLoginStatus");
		        window.location.reload();
	        });
	      }else{ //未使用fb登入
	      	//清除session
		    storage.removeItem("FBLoginStatus");
	      	window.location.reload();
	      } 

	      
	    });
	}

	//忘記密碼(header登入選單)
	$("a#forgot-pass").click(function(){
		var userAcount = $("#loginBox .modal-body form #login-username").val().trim();
		forgot(userAcount);
	});

	//忘記密碼(購物頁面登入選單)
	$("a#forgot-pass-shop").click(function(e){
		e.preventDefault();
		var userAcount = $("#LoginEmail").val().trim();
		forgot(userAcount);
	});

	//忘記密碼function
	function forgot(userAcount){
		var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 	
		var statusCheck = "memMailCheck"; 
			$.ajax({
				url:"Frontframwork/memAjax.php",
				data : {status : statusCheck,mem_email : userAcount},
				type : "get",
				async : false,
			    cache : false,
			    success : function(result){
			    		if(result){ //回傳值 有重複帳號
			    			var obj = JSON.parse(result);
			    			var userNo = obj["mem_no"];
			    			var userName = obj["mem_lastname"]+obj["mem_firstname"];
			    			swal("確定要重設密碼?", {
							  dangerMode: true,
							  buttons: [ "取消" , "確認!"],
							}).then( function(confrim) {
							  if (confrim) {
							  	var statusTemp = "createTempPsw";
							    $.ajax({
									url : "Frontframwork/memAjax.php",
									type : "POST",
									data : { status : statusTemp,mem_email : userAcount,mem_no : userNo,mem_name : userName},
									async: false,
								    cache : false,
								    success : function(tempresult){
							    		swal("請至信箱確認登入密碼").then( function(confrim) {
							    			location.reload();
							    		});
								    },
								    error : function(error){
								    	alert("傳送失敗");
								    } 
								}); 
							  }

							});
			    		}else{ // false 新用戶
			    			$str=  "帳號 '"+userAcount+"' 不是TGI Live會員";
			    			swal( $str, {
							  buttons: false,
							  timer: 3000,
							});
			    		}
					},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});
	}

// activity.php
 // anchor
 	$(".anchor").click(function(e){
 		e.preventDefault();
 		$(this).find("form").submit();
 	});


 	var activitySection = $(".activity-banner");
 	for (var a = 0; a < activitySection.length; a++) {
 		activitySection.eq(a).not(activitySection.eq(0)).css("marginTop","5px");
 	}

 	// 產品出現Scroll
 	var rowBanner = $("section.activityshow .activitySection > .activityBannerSection");
 	var rowProduct = $("section.activityshow .activitySection > .activityProductSection"); 
 	var rowProductItem = $("section.activityshow .activitySection > .activityProductSection .activityProductItem"); 
 	//活動區塊計算
 	var activityCount = rowBanner.length;
 	if( activityCount > 1){
 		for (var b = 0; b < activityCount; b++) {
 			rowBanner.eq(b).not(rowBanner.eq(0)).css({"opacity":"0"});
 		}
 	}
 	//商品數量計算
 	for (var p=3 ; p<rowProductItem.length ; p++){
 		rowProductItem.eq(p).css({"opacity":"0","top":"50px"});
 	}

 	$(window).scroll(function(){
 		var headerHeight , breadcrumbHeight , rowBannerHeight =[],rowProductHeight=[],rowProductItemHeight,showActive=[];
 		// 導覽列高度
 		headerHeight = $("header").height()+3;
 		// 麵包屑高度
 		breadcrumbHeight = $(".breadcrumb").height()+10;		
 		// banner區塊 高度
 		for(var b=0 ; b< activityCount ;b++){
 			rowBannerHeight.push(rowBanner.eq(b).height());
 		}
 		// 產品區塊 高度
 		for(var p=0 ; p< rowProduct.length ;p++){
 			rowProductHeight.push(rowProduct.eq(p).height());
 		}
 		// 個別產品數量 高度
 		rowProductItemHeight = rowProductItem.height();

 		var sh = headerHeight + breadcrumbHeight - rowProductItemHeight;
 		for (var s = 0; s < activityCount; s++) {
 			sh +=  rowBannerHeight[s] + rowProductHeight[s] ;
 			showActive.push(sh);
 		}
 		// 其他行銷活動區域展開
 		for (var a = 0; a < activityCount; a++) {
 			if( $(window).width() < 768 ){
 				if($(window).scrollTop() > showActive[a] ){
		 			rowBanner.eq(a+1).animate({"opacity":"1"},500,function(){
		 				$(this).find(".activity-banner-title-background").animate({"left":"-40px"},500,function(){
					     	$(this).parent().find("span").animate({"opacity":"1"},500);
					     });
		 			});
		 		}
 			}else{
 				if($(window).scrollTop() > showActive[a] ){
		 			rowBanner.eq(a+1).animate({"opacity":"1"},500,function(){
		 				$(this).find(".activity-banner-title-background").animate({"left":"-75px"},500,function(){
					     	$(this).parent().find("span").animate({"opacity":"1"},500);
					     });
		 			});
		 		}
 			}
 			
	 		//商品區塊展開
	 		var rowCount = rowProductHeight[a] / rowProductItemHeight;
	 		
	 		if( activityCount != 1){ //所有行銷活動
	 			if( $(window).scrollTop() > showActive[a] - ( rowProductItemHeight * rowCount ) ){
	 				rowProductItem.eq(a*9).animate({"opacity":"1","top":"0"},500,function(){});	
	 				rowProductItem.eq(a*9+1).delay(200).animate({"opacity":"1","top":"0"},500,function(){});	
	 				rowProductItem.eq(a*9+2).delay(400).animate({"opacity":"1","top":"0"},500,function(){});	
		 			rowProductItem.eq(a*9+3).delay(600).animate({"opacity":"1","top":"0"},500,function(){});		
		 			rowProductItem.eq(a*9+4).delay(800).animate({"opacity":"1","top":"0"},500,function(){});
	 				rowProductItem.eq(a*9+5).delay(1000).animate({"opacity":"1","top":"0"},500,function(){});
					rowProductItem.eq(a*9+6).delay(1200).animate({"opacity":"1","top":"0"},500,function(){});
		 			rowProductItem.eq(a*9+7).delay(1400).animate({"opacity":"1","top":"0"},500,function(){});			 				
	 				rowProductItem.eq(a*9+8).delay(1600).animate({"opacity":"1","top":"0"},500,function(){});
		 		}
	 		}else{ //單一活動 (多筆資料)
	 			for (var c = rowCount ; c >= 3; c--) {
	 				if(c == rowCount ){
	 					if( $(window).scrollTop() > showActive[a] - ( rowProductItemHeight * c ) ){
			 				rowProductItem.eq(a*9).animate({"opacity":"1","top":"0"},500,function(){});	
			 				rowProductItem.eq(a*9+1).delay(200).animate({"opacity":"1","top":"0"},500,function(){});	
			 				rowProductItem.eq(a*9+2).delay(400).animate({"opacity":"1","top":"0"},500,function(){});	
				 			rowProductItem.eq(a*9+3).delay(600).animate({"opacity":"1","top":"0"},500,function(){});		
				 			rowProductItem.eq(a*9+4).delay(800).animate({"opacity":"1","top":"0"},500,function(){});
			 				rowProductItem.eq(a*9+5).delay(1000).animate({"opacity":"1","top":"0"},500,function(){});
							rowProductItem.eq(a*9+6).delay(1200).animate({"opacity":"1","top":"0"},500,function(){});
				 			rowProductItem.eq(a*9+7).delay(1400).animate({"opacity":"1","top":"0"},500,function(){});			 				
			 				rowProductItem.eq(a*9+8).delay(1600).animate({"opacity":"1","top":"0"},500,function(){});
			 				// console.log("c=4",Math.abs(c - rowCount));
				 		}
	 				}else{
	 					var r  = Math.abs( rowCount - c ) ;
	 					if( $(window).scrollTop() > showActive[a] - ( rowProductItemHeight * (c-2*r) ) ){
			 				rowProductItem.eq(r*9).animate({"opacity":"1","top":"0"},500,function(){});	
			 				rowProductItem.eq(r*9+1).delay(200).animate({"opacity":"1","top":"0"},500,function(){});	
			 				rowProductItem.eq(r*9+2).delay(400).animate({"opacity":"1","top":"0"},500,function(){});	
				 			rowProductItem.eq(r*9+3).delay(600).animate({"opacity":"1","top":"0"},500,function(){});		
				 			rowProductItem.eq(r*9+4).delay(800).animate({"opacity":"1","top":"0"},500,function(){});
			 				rowProductItem.eq(r*9+5).delay(1000).animate({"opacity":"1","top":"0"},500,function(){});
							rowProductItem.eq(r*9+6).delay(1200).animate({"opacity":"1","top":"0"},500,function(){});
				 			rowProductItem.eq(r*9+7).delay(1400).animate({"opacity":"1","top":"0"},500,function(){});			 				
			 				rowProductItem.eq(r*9+8).delay(1600).animate({"opacity":"1","top":"0"},500,function(){});
				 		}
	 				}
	 			}
	 			
	 			
	 		}

	 		
 		}
 		
 	});
 	


// -----gotop----
	 $(".gotop").click(function(){
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
    });
	

	
    // ----提示語----
	$('[data-toggle="tooltip"]').tooltip();


	//商品內頁連結
	$(".content-hover").click(function(){
		// console.log($(this).parents("form"));
		$(this).parents("form").submit();
	});
	


	//商品搜尋
		//關鍵字顯示
	$("form#searchForm").on('mouseenter mouseleave',function(){
		$(this).find(".recommendTag-area").toggleClass("recommendTag-area-visible");		
	});	

		//關鍵字顯示RWD
	$("form#searchForm-rwd").on('mouseenter mouseleave',function(){
		$(this).find(".recommendTag-area").toggleClass("recommendTag-area-visible");		
	});
	
	
	


	
	$("form#searchForm").submit(function(e){
		e.preventDefault();
		var searchStr = $(this).find("input.searchAll").val();
		if(searchStr.trim().length == 0){
			swal({
				  title: "請輸入想要查尋的商品名稱",
				  icon: "warning",
			}).then( function(confrim) {
				  if (confrim) {
					$("input.searchAll").focus();
				  } 
			});	
		}else{
			$("form#searchForm")[0].submit();
		}
	});

	// 搜尋結果result.php
	var searchResult = $("#searchResult").find("#search-title").text();
	$(".searchAll").val(searchResult.trim());

});


//圖檔loading完後執行
$(window).on('load', function () {
     // alert($(".activity-banner-title").length);
     //行銷活動 activity.php
    if( $(window).width() < 768 ){
 		$(".activity-banner-title-background").eq(0).animate({"left":"-40px"},500,function(){
	     	$(".activity-banner-title").eq(0).find("span").animate({"opacity":"1"},500);
	     });
 	}else{
 		$(".activity-banner-title-background").eq(0).animate({"left":"-75px"},500,function(){
	     	$(".activity-banner-title").eq(0).find("span").animate({"opacity":"1"},500);
	     });
 	}
     
 });