$(document).ready(function(){

	$("#memInfoEditBtn").click(function(){
	
		$("input#memberLastName").attr("disabled",false);
		$("input#memberFirstName").attr("disabled",false);
		$("input#memberPhone").attr("disabled",false);
		$("input#memberAddress").attr("disabled",false);
	
		$("button#memPswChangeBtn").hide();
		$("#pswArea").hide();

		// $("button#memInfoEditBtn").hide();
		$("button#memInfoConfirmBtn").show();
		$("button#memInfoCancelBtn").show();
	});
	$("button#memInfoCancelBtn").click(function(){
		$("input#memberLastName").attr("disabled",true);
		$("input#memberFirstName").attr("disabled",true);
		$("input#memberPhone").attr("disabled",true);
		$("input#memberAddress").attr("disabled",true);
		$("button#memInfoConfirmBtn").hide();
		$("button#memInfoCancelBtn").hide();
		$("button#memPswChangeBtn").hide();
		$("#pswArea").hide();
		location.reload();
	});

	$("button#memInfoConfirmBtn").click(function(){
		var memLastName = $("input#memberLastName").val().trim();
		var memFirstName = $("input#memberFirstName").val().trim();
		var memTel = $("input#memberPhone").val().trim();
		var memAddress = $("input#memberAddress").val().trim();
		var memMail = $("input#memberEmail").val().trim().trim();
		var memNo = $("input#memberNo").val().trim().trim();

		var tableWrite = "member";
		var statusWrite = "memInfoEdit"	;
		$.ajax({
			url : "Frontframwork/memAjax.php",
			data : {table : tableWrite,status : statusWrite,mem_mail : memMail,mem_no : memNo,mem_lastname : memLastName,mem_firstname : memFirstName,mem_tel : memTel,mem_address : memAddress},
			type : "POST",
		    cache : false,
		    success : function(result){
		    	if(result){ //更新成功
		    		swal({
					  title: "成功更新資料",
					  icon: "success",
					}).then( function(confrim) {
					  location.reload();
					});
		    	}else{ //false 找不到帳號
		    		swal({
					  title: "更新失敗",
					  icon: "error",
					});
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		});
		
	});

	// =====重設密碼====
	$("button#memPswEditBtn").click(function(){
		swal("重設密碼").then( function(confrim) {
			$("#pswArea").show();
			$("input#memberPSWChange").val("");
			$("input#memberPSWChangeAgain").val("")
			$("button#memPswChangeBtn").show();
			$("button#memInfoCancelBtn").show();

			$("input#memberLastName").attr("disabled",true);
			$("input#memberFirstName").attr("disabled",true);
			$("input#memberPhone").attr("disabled",true);
			$("input#memberAddress").attr("disabled",true);
			$("button#memInfoConfirmBtn").hide();						    		
		});
		
	});

	$("#memPswChangeBtn").click(function(){
		var newPsw = $("input#memberPSWChange").val().trim();
		var newPswAgain = $("input#memberPSWChangeAgain").val().trim();
		var pswReg =  /^(?=^.{6,}$)((?=.*[0-9])(?=.*[a-z|A-Z]))^.*$/; //password

		var memMail = $("input#memberEmail").val().trim().trim();
		var memNo = $("input#memberNo").val().trim().trim();
		var memName = $("input#memberLastName").val().trim() + $("input#memberFirstName").val().trim();

		if( pswReg.test(newPsw) === false ){
				swal({
				  title: "請輸入英數碼六位數以上",
				  icon: "error",
				}).then( function(confrim) {
					$("input#memberPSWChange").select();
				});
				return;
			}else if( newPsw != newPswAgain){
				swal({
				  title: "請確認輸入的'確認密碼'與'密碼'相同",
				  icon: "error",
				}).then( function(confrim) {
					$("input#memberPSWChangeAgain").select();
				});
				return;
			}else{
				var tableWrite = "member";
				var statusWrite = "pswChange";
				$.ajax({
					url : "Frontframwork/memAjax.php",
					data : {table : tableWrite,status : statusWrite,mem_mail : memMail,mem_no : memNo,mem_password : newPsw , mem_name : memName},
					type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //更新成功
				    		swal({
							  title: "成功更新密碼",
							  text: "請使用新密碼重新登入!",
							  icon: "success",
							}).then( function(confrim) {
							//成功後登出 
								$.ajax({
									url : "Frontframwork/memLogout.php",
									type : "POST",
									async: false,
								    cache : false,
								    success : function(result){
								    	
								    	location.reload();
								    },
								    error : function(error){
								    	alert("傳送失敗");
								    } 
								});	 
							});
				    	}else{ //false 找不到帳號
				    		swal({
							  title: "更新失敗",
							  icon: "error",
							});
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}
		
	});


});