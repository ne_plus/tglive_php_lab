$(document).ready(function(){
	// 建立session紀錄第一次登入狀態
	var storage = sessionStorage;
	var FBStatus = storage.getItem('FBLoginStatus');
	console.log(FBStatus);


	 // This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
	    console.log('statusChangeCallback');
	    // The response object is returned with a status field that lets the
	    // app know the current login status of the persopren.
	    // Full docs on the response object can be found in the documentation
	    // for FB.getLoginStatus().
	    if (response.status === 'connected') {
	        // Logged into your app and Facebook.
	        // login(response.authResponse.accessToken);
	        // console.log(response.authResponse.accessToken);

	        if(FBStatus != 1){
	        	loginAPI();
	        }
	        
	        console.log('The person is logged into Facebook and your app');
	    } else if (response.status === 'not_authorized') {
	        // The person is logged into Facebook, but not your app.
	        console.log('The person is logged into Facebook, but not your app');
	    } else {
	        // The person is not logged into Facebook, so we're not sure if
	        // they are logged into this app or not.
	        console.log("The person is not logged into Facebook");
	    }
	}

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
	  FB.init({
	    appId      : '432812180472781',
	    cookie     : true,  // enable cookies to allow the server to access 
	                        // the session
	    xfbml      : true,  // parse social plugins on this page
	    version    : 'v2.12' // use version 2.2
	  });

	  // Now that we've initialized the JavaScript SDK, we call 
	  // FB.getLoginStatus().  This function gets the state of the
	  // person visiting this page and can return one of three states to
	  // the callback you provide.  They can be:
	  //
	  // 1. Logged into your app ('connected')
	  // 2. Logged into Facebook, but not your app ('not_authorized')
	  // 3. Not logged into Facebook and can't tell if they are logged into
	  //    your app or not.
	  //
	  // These three cases are handled in the callback function.

	  FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	  });

  };

  // Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
    function loginAPI() 
    {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=id,name,email,first_name,last_name,picture', function(response) {
          console.log('Successful login for: ' + response.name);
          // console.log('firstName: ' + response.first_name);
          // console.log('LastName: ' + response.last_name);
          // console.log('email: ' + response.email);
          // console.log(response);


          // =====fb登入步驟 check DB & save====
          	// ======如果沒有帳號重複建立新的會員====
			var memCheck = "";
			var memNO = "";
			var memFirstName = "";
			var memLastName = "";
			var mem_emailWrite = response.email;
			var mem_FBID = response.id;
			var statusCheck = "memMailCheck"; 
			$.ajax({
				url:"Frontframwork/memAjax.php",
				data : {status : statusCheck,mem_email : mem_emailWrite},
				type : "get",
				async : false,
			    cache : false,
			    success : function(result){
			    		if(result){ //true 有重複帳號
			    			memCheck =  1; //帳號重複
			    			var user = JSON.parse(result);
			    			memNO = user["mem_no"];
			    			memFirstName = user["mem_firstname"];
			    			memLastName = user["mem_lastname"];
			    		}else{ // false 新用戶
			    			memCheck =  2; //新用戶
			    			memFirstName = response.first_name;
			    			memLastName = response.last_name;
			    		}
					},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			}).done(function(){
				if( memCheck == 1 ){ //DB有存在此會員 直接登入
					var DBstatusCheck = "memStatusEditFB";
					$.ajax({
						url:"Frontframwork/memAjax.php",
						data : {status : DBstatusCheck,mem_email : mem_emailWrite,mem_no : memNO,mem_firstname : memFirstName,mem_lastname : memLastName, mem_FBid : mem_FBID},
						type : "post",
						async : false,
					    cache : false,
					    success : function(result){
					    		//登入完後 儲存session紀錄
								storage.setItem("FBLoginStatus",1);
							},
					    error : function(error){
						    	alert("傳送失敗");
						    } 
					}).done(function(){

						window.location.reload();
					});
				}else{ //新註冊 FB登入 儲存DB 資料
					var DBstatusCheck = "memCreateFB";
					$.ajax({
						url:"Frontframwork/memAjax.php",
						data : {status : DBstatusCheck,mem_email : mem_emailWrite,mem_firstname : memFirstName,mem_lastname : memLastName,mem_FBid : mem_FBID},
						type : "post",
						async : false,
					    cache : false,
					    success : function(result){
					    		console.log(result);
					    		//登入完後 儲存session紀錄
								storage.setItem("FBLoginStatus",1);
							},
					    error : function(error){
						    	alert("傳送失敗");
						    } 
					}).done(function(){

						window.location.reload();
					});
					
				}
				
			});
			

        });
    }



    // custom fb login button
    function fbLogin()
    {
        // FB 第三方登入，要求公開資料與email
        FB.login(function(response)
        {
            statusChangeCallback(response);
            console.log(response);
        }, {scope: 'public_profile,email'});
    }
    // custom fb logout button
    function fbLogout() 
    {
        FB.logout(function(response) 
        {
        // user is now logged out
        alert('已成功登出!');
        window.location.reload();
        });
    }

    // ====導覽列FB登入====
    $("#fbLoginButton").click(function(){
    	fbLogin();
    });

    // ====購物車頁面FB登入====
    $("#shopFBLoginBtn").click(function(){
    	//儲存session 讓deliver.php 指認由cart.php過去的內容
		var checkStatus ="checkSessionKey";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			type : "get",
			data : {status : checkStatus },
		    cache : false,
		    success : function(result){
		    	
		    },
		    error : function(error){
		    	alert("key失敗");
		    } 
		}).done(function(){
			
		});
		// ====前往FB連結登入====
		fbLogin();
    });

});