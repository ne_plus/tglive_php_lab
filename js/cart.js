$(document).ready(function(){

// // =====加入購物車按鈕(快速)=====
// $(".add-cart").click(function(e){
// 	e.stopPropagation(); //取消capture狀態或是 bubble狀態
// 	var productName = $(this).parents(".content-hover").find(".product-name").text().trim();
// 	var productNO = $(this).parents(".content-hover").siblings("input[name='product_no']").val();
// 	var status ="addCartOnly";
// 	// var price = $(this).parents(".content-hover").find()
// 	// console.log(productNO);
// 	$.ajax({
// 		url : "Frontframwork/cartAjax.php",
// 		data : {product_no : productNO,status : status ,product_name : productName},
// 		type : "get",
// 	    cache : false,
// 	    success : function(result){
// 	    	if(result){ //登入成功
// 	    		console.log(result);
// 	    	}else{ //false 找不到帳號
	    		
// 	    	}
// 	    },
// 	    error : function(error){
// 	    	alert("傳送失敗");
// 	    } 
// 	});
// });

//加入購物車(商品內頁)
$("#addToCart").click(function(){
	addToCart($(this),false);
});
$("#addToCartAndgoPayment").click(function(){
	addToCart($(this),true);
});
function addToCart(obj,goCart){
	var productNo = obj.siblings("input[name='product_no']").val();
	var productName = obj.siblings("input[name='product_name']").val();
	var brandNo = obj.siblings("input[name='cate_no']").val();
	var brandName = obj.siblings("input[name='cate_name']").val();
	var productSpecNo = $("select#productSpecSelect").val();
	var productQty = '';
	var productPrice = '';
	var productQtyMax = '';
	$(".product_spec_wrap .form").each(function(){
		if($(this).attr("id").search("specNo"+productSpecNo) != -1){
			productQty = $(this).find("input.qty").val();
			productPrice = $(this).find("span.finalPrice").text().trim();
			productQtyMax = $(this).find("input.qty").attr('max');
		}
	});
	if(parseInt(productQty) > parseInt(productQtyMax)){
		swal({
		  title: "庫存只有"+productQtyMax,
		  icon: "warning",
		}).then( function(confrim){
		  if (confrim) {
		    $("input.qty").focus();
		  } 
		});
	}else{
		var status = "addCartDetail";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {product_no : productNo,status : status ,product_name : productName,cate_no : brandNo , cate_name : brandName ,product_spec_no :productSpecNo,quanty : productQty,price : productPrice},
			type : "get",
		    cache : false,
		    success : function(result){
			    if (goCart){
				    location.replace("cart.php");
			    }else{
				  	if(result){ 
			    		swal({
						  title: "已加入購物車",
						  icon: "success",
						}).then( function(confrim) {
						  checkOrderCookie();
						  location.reload();
						});
			    	}else{ 
			    		alert("section");
			    	}  
			    }
		    },
		    error : function(error){
			    if (goCart){
			    	location.replace("cart.php");
			    }else{
				   alert("傳送失敗"); 
			    }
		    } 
		});
	}
}
$("#addComboToCart").click(function(){
	addComboToCart($(this), false);
});
// 購物車 加購價 
$(".addRemoveComboToCart").click(function(){
	// if ($(this).is(":checked")) addComboToCart($(this), true);
	// else removeComboToCart($(this), true);	
	var selected = $(this);
	var radioName = $(this).attr('name');
	$("input[name='"+radioName+"']").each(function(){
		removeComboFromProduct($(this));
	});
	if(!($(this).attr("data-selected") == 'on')){
		// addComboFromProduct(selected);
		addComboToCart($(this), true);
	}else{
		location.replace("cart.php");
	}
});
// 購物車 加購價 end

// 商品內頁 加購價
$(".addComboProductBeforeToCart").click(function(){
	var selected = $(this);
	// if ($(this).is(":checked")) addComboFromProduct($(this));
	// else removeComboFromProduct($(this));

	$("input[name='comboSelect']").each(function(){
		removeComboFromProduct($(this));
	});
	addComboFromProduct(selected);
})
function addComboFromProduct(obj){
	var combo_id = obj.attr("data-key");
	var status = "addCartCombo";
	$.ajax({
		url : "Frontframwork/cartAjax.php",
		data : {"status" : status, "combo_id" : combo_id},
		type : "get",
		cache : false,
		success : function(result){
			console.log(combo_id,"新增")
		},
		error : function(error){
			alert("傳送失敗"); 
		} 
	});
}
function removeComboFromProduct(obj){
	var combo_id = obj.attr("data-key");
	var status = "removeCartCombo";
	$.ajax({
		url : "Frontframwork/cartAjax.php",
		data : {"status" : status, "combo_id" : combo_id},
		type : "get",
		cache : false,
		async: false,
		success : function(result){
			console.log(combo_id,"移除")
		},
		error : function(error){
			alert("傳送失敗"); 
		}
	});
}
// 商品內頁 加購價 end

function addComboToCart(obj,goCart){
	var productNo = obj.siblings("input[name='product_no']").val();
	var combo_id = obj.attr("data-key");
	var status = "addCartCombo";
	$.ajax({
		url : "Frontframwork/cartAjax.php",
		data : {"product_no" : productNo, "status" : status, "combo_id" : combo_id},
		type : "get",
		cache : false,
		success : function(result){
			if (goCart){
				location.replace("cart.php");
			} else {
				if(result){ 
					swal({
					  title: "已加入購物車",
					  icon: "success",
					}).then( function(confrim) {
					  checkOrderCookie();
					  location.reload();
					});
				}else{ 
					alert("section");
				}  
			}
		},
		error : function(error){
			if (goCart) {
				location.replace("cart.php");
			} else {
			   alert("傳送失敗"); 
			}
		} 
	});
}
function removeComboToCart(obj,goCart){
	var combo_id = obj.attr("data-key");
	var status = "removeCartCombo";
	$.ajax({
		url : "Frontframwork/cartAjax.php",
		data : {"status" : status, "combo_id" : combo_id},
		type : "get",
		cache : false,
		success : function(result){
			if (goCart){
				location.replace("cart.php");
			} else {
				if(result){ 
					swal({
					  title: "已移除購物車",
					  icon: "success",
					}).then( function(confrim) {
					  checkOrderCookie();
					  location.reload();
					});
				}else{ 
					alert("section");
				}  
			}
		},
		error : function(error){
			if (goCart) {
				location.replace("cart.php");
			} else {
			   alert("傳送失敗"); 
			}
		} 
	});
}

function checkOrderCookie(){ // 購物車下拉式選單
	var status = "cartCheck";
	$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {status : status},
			type : "get",
		    cache : false,
		    success : function(result){
	    		var obj = JSON.parse(result);
	    		if(obj.length>0){ //有訂單資訊
	    			$("#cartListCount").text(obj.length);
	    			$(".cartInfo").remove();
	    			$(".orderListBox").remove();
	    			$("button#checkoutButton").show();
	    			for (var i = 0 ;i<obj.length ;  i++) {
	    				
	    				// ----方法一字串縮減---
	    				// var product_name = '';
	    				// if(obj[i]['product_name'].length > 5){
	    				// 	product_name = obj[i]['product_name'].slice(0,4)+'...';
	    				// }else{
	    				// 	product_name = obj[i]['product_name'];
	    				// }
	    				// var specInfo = '';
	    				// if(obj[i]['product_spec_info'].length > 2){
	    				// 	specInfo = obj[i]['product_spec_info'].slice(0,2)+'...';
	    				// }else{
	    				// 	specInfo = obj[i]['product_spec_info'];
	    				// }

	    				// var createStr='<div class="orderListBox my-2"><span class="orderimg"><img src="'+obj[i]['img1']+'" class="img-fluid" style="max-width: 35px; max-height: 35px;"></span><span class="orderName">'+product_name+'</span><span class="orderSpec">'+specInfo+'</span></div>';

	    				// -----方法二-----
	    				var createStr='<div class="orderListBox my-2"><div class="cartItem-img"><img src="'+obj[i]['img1']+'" class="img-fluid"></div><div class="cartItem-desc"><div class="cartItem-title"><a href="product-detail.php?product_no='+obj[i]['product_no']+'">'+obj[i]['product_name']+'</a></div><div class="cartItem-subtitle">'+obj[i]['product_subtitle']+'</div><div class="cartItem-specInfo">'+obj[i]['product_spec_info']+'</div></div></div>';
	    				$(createStr).insertBefore($("button#checkoutButton"));
	    			}
	    		};
	    		// console.log(obj);
		    	
		    },
		    error : function(error){
		    	//alert("傳送失敗");
		    } 
		});
}



// 購物袋---前往結帳
$("button#checkoutButton").click(function(){
	location.replace("cart.php");
});

//修改購物資訊(數量)
$("input.orderQuantySelect").change(function(){
	var productNo = $(this).parents("tr").find("input[name='product_no']").val();
	var productSpecNo = $(this).parents("tr").find("input[name='product_spec_no']").val();
	var quanty = $(this).val();
	var status = "editCartDetail";
	var quantyMax = $(this).attr("max");
	if(parseInt(quanty) > parseInt(quantyMax)){ //數量大於庫存
		swal({
		  title: "庫存只有"+quantyMax,
		  icon: "warning",
		}).then( function(confrim){
		  if (confrim) {
		    location.reload();
		  } 
		});
	}else{
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {product_no : productNo,status : status ,product_spec_no : productSpecNo,quanty : quanty},
			type : "get",
		    cache : false,
		    success : function(result){
		    	if(result){  
		    		// checkOrderCookie(); //移除購物車(導覽列清單)
		    		location.reload();
		    	}else{ 
		    		alert("section");
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		});
	}
	
});

// 加購價調整數量
$(".comporductsItemQty").change(function(){
	var comporductsID = $(this).parents(".comporducts").find(".comporductsTemp input[name='comporducts_id']").val();
	var quanty = $(this).val();
	var quantyMax = $(this).attr("max");
	var comboMaxDB = $(this).siblings('.comboMax').val();
	var status = "editCartCombo";
	if(parseInt(quanty) > parseInt(quantyMax) || parseInt(quanty) > parseInt(comboMaxDB)){
		swal({
			title: "商品數量太多",
			icon: "warning",
		  }).then( function(confrim){
			if (confrim) {
			  location.reload();
			} 
		  });
	}else{
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {combo_id : comporductsID,status : status ,quanty : quanty},
			type : "get",
		    cache : false,
		    success : function(result){
		    	if(result){  
		    		// checkOrderCookie(); //移除購物車(導覽列清單)
		    		location.reload();
		    	}else{ 
		    		alert("section");
		    	}
		    },
		    error : function(error){
		    	alert("傳送失敗");
		    } 
		});
	}
});


//金額總計
countTotalPrice();//inital
function countTotalPrice(){
	var cartPriceDB = (typeof(cartPrice) == 'undefined' )?  null : cartPrice ; //from DB 免運門檻

	var totalPrice = 0;
	$("span.charge").each(function(){
		totalPrice += parseInt($(this).text());
	});
    
	// $(".addRemoveComboToCart").each(function(){
	// 	if ($(this).is(":checked"))
	// 		totalPrice += parseInt($(this).attr("data-charge"));
	// });
	$("span.comporducts-price").each(function(){
		totalPrice += parseInt($(this).text());
	})
	
	
    if($("span#orderCouponDiscount").text() != ""){ //有優惠卷
    	totalPrice += -parseInt($("span#orderCouponDiscount").text());
    }


	if(totalPrice >= cartPriceDB ){//免運 //全館免運totalPrice >= 0
		$("td.cargoUse").hide();
		$("td.cargoNoUse").show();
	}else{ 
		$("td.cargoUse").show();
		$("td.cargoNoUse").hide();
		totalPrice += parseInt($("#orderTotalCargo").text());
	}
	
	$("span#orderTotalPrice").text(totalPrice);
}

//使用coupon
$("button#couponBtn").click(function(){
	var couponCode = $("input[name='coupon_code_keyup']").val();	
	var status = "check";

	// ====確認是否有此優惠卷存在=====
	$.ajax({
		url : "Frontframwork/couponAjax.php",
		data : {coupon_code : couponCode.trim(),status : status },
		type : "get",
	    cache : false,
	    success : function(result){
	    	if(result){  
	    		couponComparison(result);
	    		// ----紀錄使用之coupon no-----
	    		$("input[name='coupon_no_currentUse']").val(JSON.parse(result)['coupon_no']);
	    	}else{ 
	    		swal({
				  title: "沒有'"+couponCode+"'優惠劵代碼",
				  icon: "error",
				}).then( function(confrim) {
				  if (confrim) {
				    // $("input[name='coupon_code_keyup']").focus();
				    location.reload();
				  } 
				});
		    }
	    },
	    error : function(error){
	    	alert("傳送失敗");
	    } 
	});		
});

function couponComparison(result){
	var couponInfo = JSON.parse(result);
	var couponCode = couponInfo['coupon_code'];
	var couponNo = couponInfo['coupon_no'];
	var couponName = couponInfo['coupon_name'];
	var couponDiscount = parseInt(couponInfo['coupon_discount']);

	$("span.couponDiscountShow").each(function(){
		$(this).hide();
	});

	$("input[name='product_no']").each(function(){
		var productOriginalPrice = parseInt($(this).parents("td").siblings("td.orignPrice").find("span").text());

		// =====歸0舊的coupon紀錄====
		$(this).siblings("input[name='coupon_no']").val('');
		$(this).siblings("input[name='coupon_code']").val('');
		$(this).siblings("input[name='coupon_discount']").val('');
		$(this).siblings("input[name='order_detail_price_discount']").val('');


		//=====紀錄當下輸入的coupon折扣=====
		$(this).siblings(".couponArea").find("input[name='couponCodeUse']").each(function(){
			if(couponCode == $(this).val()){ //產品有優惠卷的標籤存在	
				if( parseInt($(this).parents("td").find("input[name='coupon_discount']").val()) > couponDiscount || $(this).parents("td").find("input[name='coupon_discount']").val() == ""){ 
					$(this).parents("td").find("input[name='coupon_discount']").val(couponDiscount);
					$(this).parents("td").find("input[name='coupon_no']").val(couponNo);
					$(this).parents("td").find("input[name='coupon_code']").val(couponCode);
					$(this).parents("td").find("input[name='coupon_name']").val(couponName);
					var discountPrice = parseInt(((100-couponDiscount)/100)*productOriginalPrice) ;
					$(this).parents("td").find("input[name='order_detail_price_discount']").val(discountPrice);

				}				
			}	
		})


	//=====寫入coupon使用細節=====

		if($(this).siblings("input[name='coupon_no']").val() != ""){
			var disPrice = parseInt($(this).siblings("input[name='order_detail_price_discount']").val());
			var disQty = parseInt($(this).parents("tr").find("input[name='quanty']").val());
			$(this).parents("tr").find("span.couponDiscountPriceShow").text(disPrice*disQty);
			$(this).parents("tr").find("span.couponDiscountShow").show();
		}				

	});

	// 金額計算顯示折扣金額
	var priceDiscount = 0;
	$("input[name='order_detail_price_discount']").each(function(){
		priceDiscount += parseInt($(this).val() * $(this).parents("td").siblings("td.orderQty").find("input").val());
	});
	if(priceDiscount == 0){
		$("#couponUseFailInfo").text("沒有可使用的商品");
		$("#couponUseFailInfo").show();
	}else{
		$("#couponUseFailInfo").text("");
		$("#couponUseFailInfo").hide();
	}
	$(".coupon").find(".couponUseName").text(couponName);
	$(".coupon").find("#orderCouponDiscount").text(priceDiscount);
	$(".coupon").show();

	countTotalPrice();
}


//end 使用coupon


//購物車確認按鈕前往輸入資料
$("#cartBtnConfrim").click(function(e){
	e.preventDefault();
	var quantyCheck = 0;
	$("input.orderQuantySelect").each(function(){
		if($(this).val() == 0 ){
			quantyCheck++;
			var productName = $(this).parents("tr").find("input[name='product_name']").val();
			// var quantyMax = $(this).attr("max");
			swal({
			  title: "'"+productName+"'數量沒有選擇",
			  icon: "warning",
			}).then( function(confrim){
			  if (confrim) {
			    location.reload();
			  } 
			});
		}
	});

	if(quantyCheck == 0 ){ //沒有庫存問題

	//儲存session 讓deliver.php 指認由cart.php過去的內容
		var checkStatus ="checkSessionKey";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			type : "get",
			data : {status : checkStatus },
		    cache : false,
		    success : function(result){
		    	
		    },
		    error : function(error){
		    	// alert("傳送失敗");
		    	alert("key失敗");
		    } 
		});

	// ===清除舊的暫存訂單資訊====
		var status ="clearOldcartBeforeCheckOut";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			type : "get",
			data : {status : status },
		    cache : false,
		    success : function(result){
		    	if(result){  
		    		cartSession();
		    	}else{ 
		    		alert("clear error");
		    	}
		    },
		    error : function(error){
		    	// alert("傳送失敗");
		    	alert(error);
		    } 
		});
	}
	
	
});

function cartSession(){
	$("tr.orderItem").each(function(){
		var productNo = $(this).find("input[name='product_no']").val();
		var productName = $(this).find("input[name='product_name']").val();
		var productSpecNo = $(this).find("input[name='product_spec_no']").val();
		var productSpecInfo = $(this).find("input[name='product_spec_info']").val();
		var cateNo = $(this).find("input[name='bcate_no']").val();
		var cateName = $(this).find("input[name='bcate_name']").val();
		var price = $(this).find("input[name='price']").val();
		var quanty = $(this).find("input[name='quanty']").val();
		var vCateNo = $(this).find("input[name='vcate_no']").val();
		var vCateName = $(this).find("input[name='vcate_name']").val();
		var couponNo = $(this).find("input[name='coupon_no']").val();
		var couponName = $(this).find("input[name='coupon_name']").val();
		var couponCode = $(this).find("input[name='coupon_code']").val();
		var couponDiscount = $(this).find("input[name='coupon_discount']").val();
		var orderPriceDiscount = $(this).find("input[name='order_detail_price_discount']").val();
		
		var status ="cartBeforeCheckOut";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {product_no : productNo,status : status ,product_spec_no : productSpecNo,product_spec_info : productSpecInfo,product_name : productName,bcate_no : cateNo,bcate_name : cateName,price : price,quanty : quanty,vcate_no : vCateNo,vcate_name : vCateName,coupon_no : couponNo,coupon_name : couponName,coupon_code : couponCode,coupon_discount : couponDiscount,order_price_discount:orderPriceDiscount},
			type : "get",
		    cache : false,
		    async: false,
		    success : function(result){
		    	if(result){  
		    		
		    	}else{ 
		    		// alert("error");
		    		console.log(result);
		    	}
		    },
		    error : function(error){
		    	alert("section傳送失敗");
		    } 
		});
	});

	cartComboSession();
	// location.href = "deliver.php";
}

function cartComboSession() {
	$('.comporductsTemp').each(function(){
		var id = $(this).find("input[name='comporducts_id']").val();
		var productNo = $(this).find("input[name='comporducts_product_no']").val();
		var productName = $(this).find("input[name='comporducts_product_name']").val();
		var specInfo = $(this).find("input[name='comporducts_product_spec_info']").val();
		var specNo = $(this).find("input[name='comporducts_product_spec_no']").val();
		var price = $(this).find("input[name='comporducts_price']").val();
		var quanty = $(this).find("input[name='comporducts_quanty']").val();
		var vCateNo = $(this).find("input[name='comporducts_vendorNo']").val();
		var vCateName = $(this).find("input[name='comporducts_vendorName']").val();
		var cateNo = $(this).find("input[name='comporducts_cateNo']").val();
		var cateName = $(this).find("input[name='comporducts_cateName']").val();
		var status ="cartComboBeforeCheckOut";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			data : {id: id,product_no : productNo,spec_no : specNo,spec_info : specInfo,product_name : productName,status : status ,price : price,quanty : quanty,vcate_no : vCateNo,vcate_name : vCateName,cate_no : cateNo,cate_name :cateName},
			type : "get",
			cache : false,
			async: false,
			success : function(result){
				if(result){  
					
				}else{ 
					// alert("error");
					console.log(result);
				}
			},
			error : function(error){
				alert("section傳送失敗");
			} 
		});
	})
	

	location.href = "deliver.php";
}

//刪除購物資訊
$("button.cancelOrderBtn").click(function(){
	var productNo = $(this).siblings("input[name='product_no']").val();
	var productSpecNo = $(this).siblings("input[name='product_spec_no']").val();
	$(this).parents("tr").addClass("currentOrder");
	var status = "deleteCartDetail";
	$.ajax({
		url : "Frontframwork/cartAjax.php",
		data : {product_no : productNo,status : status ,product_spec_no : productSpecNo},
		type : "get",
	    cache : false,
	    success : function(result){
	    	if(result){  
	    		// checkOrderCookie(); //移除購物車(導覽列清單)
	    		// $("tr.currentOrder").remove();
	    		location.reload();
	    	}else{ 
	    		alert("section");
	    	}
	    },
	    error : function(error){
	    	alert("傳送失敗");
	    } 
	});
});

//會員登入按鈕(結帳頁面)
$("#shopLoginBtn").click(function(){
		var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 
		var pswReg = /^\w{6,}$/; //password
		var userAcount = $("#LoginEmail").val().trim();
		var userPassword = $("#LoginPassword").val().trim();
		if(emailReg.test(userAcount) === false ){ //帳號格式驗證
			swal({
				  title: "請輸入E-Mail",
				  icon: "warning",
			}).then( function(confrim){
				  if (confrim) {
					$("#LoginEmail").select();
				  } 
			});	
		}else if(pswReg.test(userPassword) === false ){ //密碼格式驗證
			swal({
				  title: "請輸入6位數以上英數密碼",
				  icon: "warning",
			}).then( function(confrim) {
				  if (confrim) {
					$("#LoginPassword").select();
				  } 
			});
		}else{ 
			var tableWrite = "member";
			var statusWrite = "查詢" ;
			var mem_emailWrite = userAcount;
			var mem_passwordWrite = userPassword;

			// ======如果沒有帳號重複建立新的會員====
			var memCheck = "";

			var statusCheck = "memMailCheck"; 
			$.ajax({
				url:"Frontframwork/memAjax.php",
				data : {status : statusCheck,mem_email : mem_emailWrite},
				type : "get",
				async : false,
			    cache : false,
			    success : function(result){
			    		if(result){ //true 有重複帳號
			    			memCheck =  1; //帳號重複
			    		}else{ // false 新用戶
			    			memCheck =  2; //新用戶
			    		}
					},
			    error : function(error){
				    	alert("傳送失敗");
				    } 
			});
			if( memCheck ==1 ){ //mail是會員
				$.ajax({
					url : "Frontframwork/memLogin.php",
					data : {table : tableWrite,status : statusWrite,mem_email : mem_emailWrite,mem_password : mem_passwordWrite},
					type : "POST",
				    cache : false,
				    success : function(result){
				    	if(result){ //登入成功
				    		var success = JSON.parse(result);
				    		if(success == "denied"){
				    			swal({
								  title: "帳號已停權",
								  icon: "error",
								});
				    		}else{
				    			
								//儲存session 讓deliver.php 指認由cart.php過去的內容
									var checkStatus ="checkSessionKey";
									$.ajax({
										url : "Frontframwork/cartAjax.php",
										type : "get",
										data : {status : checkStatus },
									    cache : false,
									    success : function(result){
									    	
									    },
									    error : function(error){
									    	// alert("傳送失敗");
									    	alert("key失敗");
									    } 
									}).done(function(){
										location.reload();
									});
					    		
				    		}
				    		
				    	}else{ //false 找不到帳號
				    		swal({
							  title: "密碼錯誤",
							  icon: "error",
							});
				    	}
				    },
				    error : function(error){
				    	alert("傳送失敗");
				    } 
				});
			}else{ //非會員
				swal({
				  title: "帳號錯誤",
				  icon: "error",
				});
			}	
		}
	});


//首購
$("button#firstShopBtn").click(function(){
	$(".infoBox").css("opacity","0");
	$(".infoBox").show(0);
	$(".checkMember").hide(0);
	$(".infoBox").animate({opacity:'1'},500);
});


// ====跳頁===
// var webLocation = window.location.href;
// if(webLocation.match("/deliver.php")!=null){
// 	$(window).on('beforeunload', function() {
// 	  return '你要放棄修改嗎？';
// 	});
// }else{

// }	


//確認發票資訊
$("input[name='invoiceSelect']").click(function(){
	$("div.invoiceInfo").show();
	$("input#orderUniNo").val("");
	$("input#orderInvoiceTitle").val("");
	$("input#orderInvoiceAddress").val("");
	$("input#person_invoice").prop("checked");
	if($("input#person_invoice").prop("checked") === true){
		$("div.companyInvoice").hide();
		$("div.invoice").show(300);
	}else{
		$("div.companyInvoice").show(300);
		$("div.invoice").show(300);
	};
});
$("input#invoiceCheck").click(function(){
	if($(this).prop("checked") === true){
		$("input#orderInvoiceAddress").val("");
		$("input#orderInvoiceAddress").css("backgroundColor","");
		$("input#orderInvoiceAddress").attr("disabled",true);
	}else{
		$("input#orderInvoiceAddress").attr("disabled",false);
	}
});
// 確認分期資訊
$("input[name='payway']").click(function(){
	if($("input#payway3").prop("checked") === true){
		$("div.installInfo").show();
	}else{
		$("div.installInfo").hide(300);
	};
});

$()

//確認購買
$("#checkoutBtn").click(function(e){
	e.preventDefault();
	var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 
	var pswReg =  /^(?=^.{6,}$)((?=.*[0-9])(?=.*[a-z|A-Z]))^.*$/; //password
	var vendorCateNo = [];
	var cargoPrice = parseInt($("span#cargoPrice").text());
	var totalPrice = parseInt($("span#TotalPrice").text());
	var memNOcheck = $("input#memberNo").val();
	var orderEmail = $("input#memberEmail").val().trim();
	
	var orderLastName = $("input#memberLastName").val().trim();
	var orderFirstName = $("input#memberFirstName").val().trim();
	// var orderRecipient = $("input#memberName").val().trim();
	var orderAddress = $("input#memberAddress").val().trim();
	var orderTel = $("input#memberPhone").val().trim();
	var orderMemo = $("textarea#purchaseMemo").val().trim() == '' ? null : $("textarea#purchaseMemo").val().trim();
	$("input[name='vendor_no[]']").each(function(){
		var cateNo = $(this).val();
		if(jQuery.inArray(cateNo,vendorCateNo) == -1){
			vendorCateNo.push(cateNo);
		}
	});
	
	var cargoInfo = $("input[name='cargo']").val(); // 1:有運費 0:沒有運費

	var paywaySelect = $("input[name='payway']"); //付款方式
	var pay = "" ;
	paywaySelect.each(function(){
		if($(this).prop("checked") == true){
			pay = $(this).val();
		}	
	});
	var pay_install = 0;
	if (pay == 3) {
		var pay_install = $("#orderIstallment").val(); // 分期期數
	}
	
	var personInvoiceCheckBtn = $("input#person_invoice"); //發票勾選個人發票
	var companyInvoiceCheckBtn = $("input#company_invoice"); //發票勾選公司發票
	var orderUniNo = $("input#orderUniNo").val().trim() ; //統一編號
	var orderUniTitle = $("input#orderInvoiceTitle").val().trim();//統編抬頭
	var orderInvoiceAddress = $("input#orderInvoiceAddress").val().trim(); //發票寄送位置

	var termCheckBtn = $("input#termCheck"); //服務條款勾選
	$("input#orderUniNo").css("backgroundColor","");
	$("input#orderInvoiceTitle").css("backgroundColor","");
	$("input#orderInvoiceAddress").css("backgroundColor","");
	
	// console.log(purchaseInfo);
	$("input#memberLastName").css("backgroundColor","");
	$("input#memberFirstName").css("backgroundColor","");
	$("input#memberAddress").css("backgroundColor","");
	$("input#memberPhone").css("backgroundColor","");
	if( emailReg.test(orderEmail) === false ){
		swal({
		  title: "請輸入有效E-Mail",
		  icon: "error",
		}).then( function(confrim) {
			$("input#memberEmail").select();
		});
	}else if(orderLastName == '' || orderFirstName == '' || orderAddress =='' || orderTel == ''){
		swal({
		  title: "請填妥資訊",
		  icon: "error",
		}).then( function(confrim) {
			if(orderLastName == ''){$("input#memberLastName").css("backgroundColor","#f00");}
			if(orderFirstName == ''){$("input#memberFirstName").css("backgroundColor","#f00");}
			if(orderAddress == ''){$("input#memberAddress").css("backgroundColor","#f00");}
			if(orderTel == ''){$("input#memberPhone").css("backgroundColor","#f00");}
		});
	}else if( !(pay == 0 || pay == 3 || pay == 2 || pay == 4) ){	// 0 信用卡全額, 3 信用卡分期
		swal({
		  title: "請勾選付款方式",
		  icon: "error",
		}).then( function(confrim) {
			paywaySelect.focus();
		});
	}else if( personInvoiceCheckBtn.prop("checked") ===false && companyInvoiceCheckBtn.prop("checked") === false){
		swal({
		  title: "請勾選發票種類",
		  icon: "error",
		}).then( function(confrim) {
			personInvoiceCheckBtn.focus();
		});
	}else if( personInvoiceCheckBtn.prop("checked") ===true && ( orderInvoiceAddress == "" && $("input#invoiceCheck").prop("checked") === false )){
		swal({
		  title: "請選擇發票發送地址",
		  icon: "error",
		}).then( function(confrim) {
			$("input#orderInvoiceAddress").focus();
		});
	}else if( companyInvoiceCheckBtn.prop("checked") === true && (( orderInvoiceAddress == "" && $("input#invoiceCheck").prop("checked") === false ) || orderUniNo == "" || !isValidTaxID(orderUniNo) || orderUniNo.length != 8 || orderUniTitle == "" ) ){
		swal({
		  title: "請填妥發票資訊",
		  icon: "error",
		}).then( function(confrim) {
			if(orderUniNo == "" || orderUniNo.length != 8 || !isValidTaxID(orderUniNo)) { $("input#orderUniNo").css("backgroundColor","#f00"); }
			if(orderUniTitle == "" ){$("input#orderInvoiceTitle").css("backgroundColor","#f00");}
			if(orderInvoiceAddress == "" && $("input#invoiceCheck").prop("checked") === false){$("input#orderInvoiceAddress").css("backgroundColor","#f00");}
		});
	}else if(termCheckBtn.prop("checked") == false ){
		swal({
		  title: "請勾選同意TGI服務條款",
		  icon: "error",
		}).then( function(confrim) {
		 	termCheckBtn.focus();
		});
	}else{
		//超商收款確認金額不得大於兩萬
		if( totalPrice > 20000 && pay ==2 ){
			swal({
				title: "超商付款金額不得大於2萬",
				icon: "error",
			  }).then( function(confrim) {
			   
			  });
			  return ;//結束 
		}

		//=====確認首購=====		
		if( memNOcheck == undefined ){ //首購 進行帳密確認並新增會員
			var psw = $("input#memberPassword").val().trim();
			var pswRecheck = $("input#memberPasswordAgain").val().trim();
			if( pswReg.test(psw) === false ){
				swal({
				  title: "請輸入英數碼六位數以上",
				  icon: "error",
				}).then( function(confrim) {
					$("input#memberPassword").select();
				});
				return;
			}else if( psw != pswRecheck){
				swal({
				  title: "請確認輸入的'確認密碼'與'密碼'相同",
				  icon: "error",
				}).then( function(confrim) {
					$("input#memberPasswordAgain").select();
				});
				return;
			}else{
				var firstUse = firstShop(orderEmail,psw,orderLastName,orderFirstName,orderAddress,orderTel);
				if(firstUse == 1){
					swal({
					  title: "有重複帳號",
					  text: "請確認是否已經為TGI Live會員",
					  icon: "error",
					  buttons: ["嘗試輸入其他Email", "登入會員"],
					}).then( function(confrim) {
						if(confrim){
							location.reload();
						}else{
							$("input#memberEmail").select();
						}
					});	
					return;	
				}
				memNO = firstUse;
				// return;
				
			}	
		}else{
			memNO = memNOcheck.trim();
		} 	
		// ====個人資訊=====
		var orderRecipient = orderLastName + orderFirstName;
		var purchaseInfo = [];
		purchaseInfo.push(memNO); //0
		purchaseInfo.push(orderEmail); //1
	 	purchaseInfo.push(orderRecipient); //2
		purchaseInfo.push(orderAddress); //3
		purchaseInfo.push(orderTel); //4
		purchaseInfo.push(orderMemo); //5
		purchaseInfo.push(pay); //6
		purchaseInfo.push(pay_install); //7
		// =====發票狀態=====
		var orderUniStatus ="";
		if(personInvoiceCheckBtn.prop("checked") ===true){
			orderUniStatus = 0;
		}else if(companyInvoiceCheckBtn.prop("checked") === true){
			orderUniStatus = 1;
		}
		if( orderUniNo == "" ){
			 orderUniNo = null;
		}
		if( orderUniTitle == "" ){
			orderUniTitle = null ;
		}
		if( orderInvoiceAddress == "" ){
			orderInvoiceAddress = "同商品配送地址";
		}
		 
		var status = "createOrderItem";	
		$.ajax({
			url:"Frontframwork/orderAjax.php",
			data : {status : status,cate_no : vendorCateNo,cargo_info : cargoInfo ,cargo_price : cargoPrice,info : purchaseInfo,order_uni_status : orderUniStatus,order_uni_no : orderUniNo,order_uni_title : orderUniTitle,order_invoice_address : orderInvoiceAddress},
			type : "get",
		    cache : false,
		    success : function(result){
			    	if(result){ //true 
			    		orderNo = result; //回傳訂單編號傳給弘揚
			    	}else{ //false
			    		swal({
						  title: "訂單傳送異常",
						  icon: "error",
						}).then( function(confrim) {
						 
						});
			    	}
	    		},
		    error : function(error){
			    	alert("傳送失敗Order");
			    } 
		}).done(function() {

			// =====發送mail=====
    		var mailStatus = "sendOrderMail";
    		$.ajax({
    			url:"Frontframwork/memAjax.php",
				data : {status : mailStatus,order_email : orderEmail,order_name : orderRecipient,order_tel : orderTel ,order_pay : pay,order_pay_install : pay_install,order_address : orderAddress,order_no : orderNo,total_price : totalPrice,cargo_info : cargoInfo,cargo_price : cargoPrice},
				type : "get",
			    cache : false,
			    success : function(mailResult){
			    		console.log(mailResult);
			    		// console.log(totalPrice);		    	
		    		},
			    error : function(error){
				    	alert("傳送失敗Mail");
				    } 
    		}).done(function(){
	    			//刪除庫存和copon
				var quantyStatus = "quantyAndCouponEdit";
				$.ajax({
					url:"Frontframwork/cartAjax.php",
					data : {status : quantyStatus},
					type : "get",
				    cache : false,
				    success : function(result){
				    		// console.log(orderNo);
				    		// console.log(totalPrice);		    	
			    		},
				    error : function(error){
					    	alert("傳送失敗DeleteQuanty");
					    } 
					}).done(function(){
						// ====完成儲存後====
						  // 1.刪除session暫存
						  // 2.刪除cookie清單
						  var status = "clearAll";
						  $.ajax({
							url:"Frontframwork/cartAjax.php",
							data : {status : status},
							type : "get",
						    cache : false,
						    success : function(result){
						    		// console.log(orderNo);
						    		// console.log(totalPrice);		    	
					    		},
						    error : function(error){
							    	alert("傳送失敗DeleteSession");
							    } 
							}).done(function(){
								$("input[name='pay']").val(pay);
								$("input[name='pay_install']").val(pay_install);
								$("input[name='total_price']").val(totalPrice);
								$("input[name='order_no']").val(orderNo);
								$("input[name='order_member_name']").val(orderRecipient);
								$("input[name='order_member_tel']").val(orderTel);
								$("input[name='order_member_email']").val(orderEmail);
								$("input[name='order_member_address']").val(orderAddress);
								$("input[name='order_invoice_print']").val((orderUniNo != null && orderUniNo != "") ? 1 : 0);
								$("input[name='order_invoice_no']").val(orderUniNo);
								$("input[name='order_invoice_title']").val(orderUniTitle);
								
								// $("form#checkoutForm").submit();
								if( pay == 0 || pay == 3 ){ 	//刷卡
									// $("form#checkoutForm").attr("action","payment-ecpay.php");
									$("form#checkoutForm").submit();
								} else if( pay == 2){			//超商
									// $("form#checkoutForm").attr("action","payment-ecpay.php");
									$("form#checkoutForm").submit();
								} else if( pay == 4){			//ATM
									// $("form#checkoutForm").attr("action","payment-ecpay.php");
									$("form#checkoutForm").submit();
								}
							});
					});
	    		});
			
		});
	}

	
});

function firstShop(orderEmail,psw,orderLastName,orderFirstName,orderAddress,orderTel){
	var memCheck = "";

	var statusCheck = "memMailCheck"; 
	// =====檢查是否有重複帳號=====
	// ======如果沒有帳號重複建立新的會員====
	$.ajax({
		url:"Frontframwork/memAjax.php",
		data : {status : statusCheck,mem_email : orderEmail},
		type : "get",
		async : false,
	    cache : false,
	    success : function(result){
	    		if(result){ //true 有重複帳號
	    			memCheck =  1; //帳號重複
	    		}else{ // false 新用戶
	    			memCheck =  2; //新用戶
	    		}
			},
	    error : function(error){
		    	alert("傳送失敗");
		    } 
	});
	if(memCheck == 1){
		return memCheck; 
	}

	var statusCreate = "memCreate";
	$.ajax({
		url:"Frontframwork/memAjax.php",
		data : {status : statusCreate,mem_email : orderEmail,mem_password : psw,mem_lastname : orderLastName, mem_firstname : orderFirstName,mem_address : orderAddress,mem_tel : orderTel},
		type : "get",
		async : false,
	    cache : false,
	    success : function(result){
	    		var member = JSON.parse(result);
	    		memCheck = member["mem_id"];
			},
	    error : function(error){
		    	alert("傳送失敗");
		    } 
	});	

	//發送mail 給管理員
	var statusNewMemCreate = "newMemMail";
	$.ajax({
		url:"Frontframwork/memAjax.php",
		data : {status : statusNewMemCreate,mem_email : orderEmail,mem_lastname : orderLastName, mem_firstname : orderFirstName,mem_tel : orderTel},
		type : "get",
		async : false,
	    cache : false,
	    success : function(result){
			},
	    error : function(error){
		    } 
	});	
	//發送給個別用戶
	var statusNewMemCreateEDM = "newMemMailEDM";
	$.ajax({
		url:"Frontframwork/memAjax.php",
		data : {status : statusNewMemCreateEDM,mem_email : orderEmail,mem_lastname : orderLastName, mem_firstname : orderFirstName,mem_tel : orderTel},
		type : "get",
		async : false,
		cache : false,
		success : function(result){
			},
		error : function(error){
			} 
	});

	// ======儲存cookie 和 session=====
	var tableWrite = "member";
	var statusWrite = "查詢" ;
	var mem_emailWrite = orderEmail;
	var mem_passwordWrite = psw;
	$.ajax({
		url : "Frontframwork/memLogin.php",
		data : {table : tableWrite,status : statusWrite,mem_email : mem_emailWrite,mem_password : mem_passwordWrite},
		type : "POST",
	    cache : false,
	    success : function(result){
	    	if(result){ //登入成功
	    		var success = JSON.parse(result);
	    		if(success == "denied"){
	    			swal({
					  title: "帳號已停權",
					  icon: "error",
					});
	    		}else{   			
		    		// location.reload();
	    		}
	    		
	    	}else{ //false 找不到帳號
	    		swal({
				  title: "帳號/密碼錯誤",
				  icon: "error",
				});
	    	}
	    },
	    error : function(error){
	    	alert("傳送失敗");
	    } 
	});

	// ======回傳儲存的會員ID======
	return memCheck;
}

//訂單ＲＷＤ deliver.php
/*
function rwd(){
	if($(window).width() < 769 ){
		$("tr#cargoTR").find("td:eq(0)").css("display","none");
		$("tr#totalPriceTR").find("td:eq(0)").css("display","none");
	}else{
		$("tr#cargoTR").find("td:eq(0)").css("display","table-cell");	
		$("tr#totalPriceTR").find("td:eq(0)").css("display","table-cell");
		
	}
}

$(window).resize(function(){
	rwd()
});

rwd();
*/


//歷史訂單前往結帳
$("a.rePay").click(function(e){
	e.preventDefault();
	var orderNO = $(this).parent().parent().find(".order_group").text().trim();
	var thisObj = $(this);
	swal({	
	  text:"訂單編號 : '"+orderNO+"' 前往結帳",
	  dangerMode: true,
	  buttons: [ "取消" , "確認"],
	}).then( function(confrim) {
	  if (confrim) {
	  	//儲存session 讓deliver.php 指認由cart.php過去的內容
		var checkStatus ="checkSessionKey-repay";
		$.ajax({
			url : "Frontframwork/cartAjax.php",
			type : "get",
			data : {status : checkStatus },
		    cache : false,
		    success : function(result){
		    	
		    },
		    error : function(error){
		    	// alert("傳送失敗");
		    	alert("key失敗");
		    } 
		});
	  	// console.log("yes");
		  thisObj.siblings("form.rePayForm").submit();		  
	  }else{
	  	// console.log("no");
	  }
	});  
});

//Rechechout 
$("#RecheckoutBtn").click(function(e){
	e.preventDefault();
	var emailReg = /^\w+((-\w+)|(\.\w+))*\@[\w]+((\.|-)[\w]+)*\.[A-Za-z]+$/; 
	var totalPrice = parseInt($("span#TotalPrice").text());
	var orderEmail = $("input#memberEmail").val().trim();
	
	var orderRecipient = $("input#memberName").val().trim();
	var orderAddress = $("input#memberAddress").val().trim();
	var orderTel = $("input#memberPhone").val().trim();
	var orderMemo = $("textarea#purchaseMemo").val().trim() == '' ? null : $("textarea#purchaseMemo").val().trim();
	
	var cargoInfo = $("input[name='cargo']").val(); // 1:有運費 0:沒有運費

	var paywaySelect = $("input[name='payway']"); //付款方式
	var pay = "" ;
	paywaySelect.each(function(){
		if($(this).prop("checked") == true){
			pay = $(this).val();
		}	
	});
	var personInvoiceCheckBtn = $("input#person_invoice"); //發票勾選個人發票
	var companyInvoiceCheckBtn = $("input#company_invoice"); //發票勾選公司發票
	var orderUniNo = $("input#orderUniNo").val().trim() ; //統一編號
	var orderUniTitle = $("input#orderInvoiceTitle").val().trim();//統編抬頭
	var orderInvoiceAddress = $("input#orderInvoiceAddress").val().trim(); //發票寄送位置
	var termCheckBtn = $("input#termCheck"); //服務條款勾選

	$("input[name='total_price']").val(totalPrice);
	$("input[name='order_member_name']").val(orderRecipient);
	$("input[name='order_member_tel']").val(orderTel);
	$("input[name='order_member_email']").val(orderEmail);
	if(orderRecipient == ''  || orderAddress =='' || orderTel == ''){
		swal({
		  title: "請填妥資訊",
		  icon: "error",
		}).then( function(confrim) {
			if(orderRecipient == ''){$("input#memberName").css("backgroundColor","#f00");}
			if(orderAddress == ''){$("input#memberAddress").css("backgroundColor","#f00");}
			if(orderTel == ''){$("input#memberPhone").css("backgroundColor","#f00");}
		});
	}else if(personInvoiceCheckBtn.prop("checked") ===true && ( orderInvoiceAddress == "" && $("input#invoiceCheck").prop("checked") === false ) ) {
		swal({
		  title: "請選擇發票發送地址",
		  icon: "error",
		}).then( function(confrim) {
			$("input#orderInvoiceAddress").focus();
		});
	}else if(termCheckBtn.prop("checked") == false ){
		swal({
		  title: "請勾選同意TGI服務條款",
		  icon: "error",
		}).then( function(confrim) {
		 	termCheckBtn.focus();
		});
	}else{
		//超商收款確認金額不得大於兩萬
		if( totalPrice > 20000 && pay == 2){
			swal({
				title: "超商付款金額不得大於2萬",
				icon: "error",
			  }).then( function(confrim) {
			   
			  });
			  return ;//結束 
		}

		//更新訂單
		var orderNo = $("input[name='order_no']").val().trim();
		if( orderInvoiceAddress == "" ){
			orderInvoiceAddress = "同商品配送地址";
		}
		var status = "orderInfoChange";	
		$.ajax({
			url:"Frontframwork/orderAjax.php",
			data : {status : status,order_no : orderNo ,pay : pay ,order_invoice_address : orderInvoiceAddress},
			type : "get",
		    cache : false,
		    success : function(result){
			    	
	    		},
		    error : function(error){
			    	alert("傳送失敗");
			    } 
		})

		if(pay == 0){
			// console.log('信用卡');
			$("form#checkoutForm").attr("action","payment-test.php");
			$("form#checkoutForm").submit();
		}else if(pay ==2){
			// console.log('超商');
			$("form#checkoutForm").attr("action","payment-paycode.php");
			$("form#checkoutForm").submit();
		}

		// $("form#checkoutForm").submit();
	}
});


//扣款成功mail 
var orderNoCheckOutSend = $("input#orderNoSendMail").val();
var priceCheckOutSend = $("input#priceSendMail").val();
var mailCheckOutSend = $("input#mailSendMail").val();
var recipientCheckOutSend = $("input#recipientSendMail").val();

if( orderNoCheckOutSend != null ){
	var mailStatus = "sendOrderCheckOutMail";

	$.ajax({
		url:"Frontframwork/memAjax.php",
		data : {status : mailStatus,order_mail : mailCheckOutSend,order_name : recipientCheckOutSend,order_no : orderNoCheckOutSend ,order_price : priceCheckOutSend},
		type : "get",
	    cache : false,
	    success : function(mailResult){
	    		console.log(mailResult);
	    		// console.log(totalPrice);		    	
    		},
	    error : function(error){
		    	alert("傳送失敗");
		    } 
	})
}


});


function isValidTaxID(taxId) {
    var invalidList = "00000000,11111111";
    if (/^\d{8}$/.test(taxId) == false || invalidList.indexOf(taxId) != -1) {
        return false;
    }

    var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
        sum = 0,
        calculate = function(product) { // 個位數 + 十位數
            var ones = product % 10,
                tens = (product - ones) / 10;
            return ones + tens;
        };
    for (var i = 0; i < validateOperator.length; i++) {
        sum += calculate(taxId[i] * validateOperator[i]);
    }

    return sum % 10 == 0 || (taxId[6] == "7" && (sum + 1) % 10 == 0);
};