<?php  
// include("config.php");
class DB2 {
	public $db_host ;
	public $database ;
	public $user ;
	public $password ;
	public $pdo ;
	public $error;
	public $lastId;

	public function __construct() {
        $this->db_host = DB_HOSTNAME;
        $this->database = "tgilive";
        $this->user = DB_USERNAME;
        $this->password = DB_PASSWORD;
        $dsn = "mysql:host=".$this->db_host.";port=3306;dbname=".$this->database.";charset=utf8";
        $options = array(
					PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION
					);
        $this->pdo = new PDO($dsn,$this->user,$this->password,$options);
    }

    public function DB_Query($sql,$dic=null){
    	$pdo = $this->pdo;
		$object = $pdo->prepare($sql);
		if($dic){
			foreach ($dic as $key=>$value) {
				$object -> bindValue($key,$value);
			}
		}
		if($object -> execute()){
			return 	$object -> fetchAll(PDO::FETCH_ASSOC);
		}else{
			$this->error = $object -> errorCode();
			return false;
		}

    }

}
?>