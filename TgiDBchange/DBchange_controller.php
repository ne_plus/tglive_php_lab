<?php  
try{

include("../config.php");	
// ====舊DB2========	
include("OldDBquery.php");
// ====新DB========
include("../model/DB.php");

// $_REQUEST['selectItem']="productDBChange";

// <====搜尋舊資料庫會員資訊====>
	$db2 = new DB2();
	$db = new DB();
	date_default_timezone_set("Asia/Taipei");
	switch ($_REQUEST['selectItem']) {
		// =======會員資料=========
		case 'memQueryOld':
			$sql = "select * from tgi_customer order by customer_id asc";
			$result = $db2->DB_Query($sql);
			if(count($result)!=0){
				// echo "<pre>";
				// echo "總共：".count($result)."會員\r\n";
				$jsonStr = json_encode($result);
				echo $jsonStr;
				// echo "</pre>";
			}else{
				echo  "找不到帳號";
			}
			break;
		case 'memDBChange' :

			// ======將舊會員資料寫入新的欄位陣列===========
			$sql = "select customer_id,firstname,lastname,email,telephone,password,ip,status,date_added from tgi_customer";
			$result = $db2->DB_Query($sql);
			if(count($result)!=0){
				foreach ($result as $key => $value) {
					$memDB=array(
							"mem_no"=>$value["customer_id"],
							"mem_mail" =>$value["email"],
							"mem_firstname"=>$value["firstname"],
							"mem_lastname" =>$value["lastname"],
							"mem_tel" =>$value["telephone"],
							"mem_status" =>$value["status"], 
							"mem_createtime" =>strtotime($value["date_added"]), 
							"mem_loginIp" =>$value["ip"]
						);
					// =====更新新資料庫會員資訊========
					$main = array("mem_mail","mem_no");
					$NewDB = $db->DB_Update("member",$memDB,$main);
				}
				echo "更新成功";
								
			}else{
				echo "找不到帳號";
			}
			break;

		case 'memQueryNew':
			$result = $db->DB_Query("select * from member");
			if(count($result)!=0){
				// echo "<pre>";
				// echo "總共：".count($result)."會員\r\n";
				$jsonStr = json_encode($result);
				echo $jsonStr;
				// echo "</pre>";
			}else{
				echo "找不到帳號";
			}
			break;
		// =======會員資料=========
		
		// ========產品資料========
		case 'productOld';	
			$sql = "SELECT * FROM tgi_product a join tgi_product_description b on a.product_id = b.product_id where b.language_id =1 order by a.product_id asc";
			$result = $db2->DB_Query($sql);
			$jsonStr = json_encode($result);
			echo $jsonStr;
			break;

		case 'productDBChange';	
			// ======將舊商品資料寫入新的欄位陣列===========
			$sql = "SELECT * FROM tgi_product a join tgi_product_description b on a.product_id = b.product_id where b.language_id =1 order by a.product_id asc";
			$result = $db2->DB_Query($sql);
			if(count($result)!=0){
				foreach ($result as $key => $value) {
					$proDB=array(
							"product_no"=>$value["product_id"],
							"model_id" =>$value["model"],
							"product_name"=>$value["name"],	
							"product_status" =>"0", 
							"product_content" => html_entity_decode($value["description"]),
							"product_createtime" =>strtotime($value["date_added"]), 
							"product_updatetime" =>$value["date_modified"]
						);
					// =====更新新資料庫商品資訊========
					$main = array("product_no","model_id");
					$NewDB = $db->DB_Update("product",$proDB,$main);
					// echo $NewDB;
					// print_r($proDB);
					$proDB2=array(
							"product_no"=>$value["product_id"],
							"product_spec_price1" =>$value["price"]
						);
					$main2 = array("product_no");
					// $NewDB2 = $db->DB_UpdateOnly("product_spec",$proDB2,$main2);
					$NewDB2 = $db->DB_Update("product_spec",$proDB2,$main2);

					// echo $NewDB2;
				}
				// $jsonStr = json_encode($memDB);
				echo "更新成功";
								
			}else{
				echo "找不到商品";
			}
			// echo html_entity_decode($result[0]["description"]);
			break;	

		case 'productNew';
			$result = $db->DB_Query("select * from product");
			$jsonStr = json_encode($result);
			// echo $jsonStr;
			$productNew =[];
			foreach ($result as $key => $value) {
				$proItem=array(
					"product_no"=>$value["product_no"],
					"model_id" =>$value["model_id"],
					"product_name"=>$value["product_name"],
					"product_content" =>$value["product_content"], 
					"product_createtime" =>$value["product_createtime"], 
					"product_updatetime" =>$value["product_updatetime"]
						);
				$sql = "select product_stock , product_spec_price1 from product_spec where product_no = '".$value["product_no"]."'";
				$spec = $db->DB_Query($sql);
				if($spec){
					$proItem["product_stock"] = $spec[0]["product_stock"];
					$proItem["product_price1"] = $spec[0]["product_spec_price1"];
				}else{
					$proItem["product_stock"] = null;
					$proItem["product_price1"] = null;
				}
				array_push($productNew,$proItem);
			}
			// echo "<pre>";
			// print_r($productNew);
			// echo "</pre>";
			echo json_encode($productNew);

			break;		

		default:
				echo "not found";
			break;
	}
	
}catch(PDOException $e){
	$e->getLine();
	$e->getMessage();
}


?>