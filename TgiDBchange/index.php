<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
	<title>Document</title>
	<style type="text/css">
		form{
			text-align: center;
		}
		table{
			border:1px solid #000;
			margin:20px auto;
			/*display: none;*/
		}
		th{
			border: 1px solid #000;
			padding:5px;
		}
		td{
			border: 1px dotted #ccc;
			padding: 5px;
		}
	</style>
</head>
<body>
<form id="form" action="DBchange_controller.php" method="post">
<h2>會員資料</h2>
	<select name="selectItem" id="sel">
		<option value="0">請選擇</option>
		<option value="memQueryOld">查詢舊資料庫會員</option>
		<option value="memDBChange">資料庫移轉(舊->新)</option>
		<option value="memQueryNew">查詢新資料庫會員</option>
	</select>
<input id="send" type="button" value="send">	
</form>
<table id="show" cellspacing="0">
	<tr><th>會員編號</th><th>會員mail</th><th>會員姓</th><th>會員名</th><th>會員電話</th><th>建立時間</th></tr>
</table>	
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#sel").change(function(){
			console.log($(this).val());
		});

		$("#send").click(function(e){
			e.preventDefault();
			$(".trShow").remove();
			var value = $("#sel").val();
			if(value == 0 || null ){
				alert("請選擇");
			}else{
				// $("#form").submit();
				var xhr = $.ajax({
					url : "DBchange_controller.php",
					data : {selectItem : value},
		            type : "POST",
		            cache : false,
		            success : function(result){
		            	switch(value){
		            		case "memQueryOld":
		            			if(result=="找不到帳號"){
		            				alert("找不到帳號");
		            			}else{
		            				response(result);
		            			}
		            		break;
		            		case "memDBChange":
		            			alert(result);
		            		break;
		            		case "memQueryNew":
		            			if(result=="找不到帳號"){
		            				alert("找不到帳號");
		            			}else{
		            				responseNew(result);
		            			}            			
		            		break;
		            		default:
		            			console.log("error");
		            		break;
		            	}
		            },
		            error : function(error){
		                alert("傳輸失敗");
		            }
				});
				
			}
		});

		function response(res){
			// console.log(res);
			var resObj = JSON.parse(res);	
			for(key in resObj){
				$("#show").append("<tr class='trShow'><td>"+resObj[key]["customer_id"]+"</td><td>"+resObj[key]["email"]+"</td><td>"+resObj[key]["lastname"]+"</td><td>"+resObj[key]["firstname"]+"</td><td>"+resObj[key]["telephone"]+"</td><td>"+resObj[key]["date_added"]+"</td></tr>");
			}		
		};

		function responseNew(res){
			// console.log(res);
			var resObj = JSON.parse(res);
			
			for(key in resObj){				
					// =====timestamp 轉date()=======
				var date = new Date(parseInt(resObj[key]["mem_createtime"])*1000);
				var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);

				$("#show").append("<tr class='trShow'><td>"+resObj[key]["mem_no"]+"</td><td>"+resObj[key]["mem_mail"]+"</td><td>"+resObj[key]["mem_lastname"]+"</td><td>"+resObj[key]["mem_firstname"]+"</td><td>"+resObj[key]["mem_tel"]+"</td><td>"+time+"</td></tr>");
			}	
			
		};
		
		
	});
</script>