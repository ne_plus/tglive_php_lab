<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
	<title>Document</title>
	<style type="text/css">
		form{
			text-align: center;
		}
		table{
			border:1px solid #000;
			margin:20px auto;
			/*display: none;*/
		}
		th{
			border: 1px solid #000;
			padding:5px;
		}
		td{
			border: 1px dotted #ccc;
			padding: 5px;
		}
	</style>
</head>
<body>
<form id="form" action="DBchange_controller.php" method="post">
<h2>產品資料</h2>
	<select name="selectItem" id="sel">
		<option value="0">請選擇</option>
		<option value="productOld">查詢舊資料庫商品</option>
		<option value="productDBChange">資料庫移轉(舊->新)</option>
		<option value="productNew">查詢新資料庫商品</option>
	</select>
<input id="send" type="button" value="send">	
</form>
<span id="count"></span>
<table id="show" cellspacing="0">
	<tr><th>產品編號</th><th>產品編號(user setting)</th><th>產品名稱</th><th>description</th><th>原價</th><th>建立時間</th><th>修改時間</th></tr>
</table>	
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#sel").change(function(){
			console.log($(this).val());
		});

		$("#send").click(function(e){
			e.preventDefault();
			$("#count").text("");
			$(".trShow").remove();
			var value = $("#sel").val();
			if(value == 0 || null ){
				alert("請選擇");
			}else{
				// $("#form").submit();
				var xhr = $.ajax({
					url : "DBchange_controller.php",
					data : {selectItem : value},
		            type : "POST",
		            cache : false,
		            success : function(result){
		            	switch(value){
		            		case "productOld":
		            			response(result);
		            			// console.log(JSON.parse(result));
		            		break;
		            		case "productDBChange":
		            			console.log(result);
		            			// alert("更新成功");
		            		break;
		            		case "productNew":
		            			responseNew(result);          			
		            		break;
		            		default:
		            			console.log("error");
		            		break;
		            	}
		            },
		            error : function(error){
		                alert("傳輸失敗");
		            }
				});
				
			}
		});

		function response(res){
			// console.log(res);
			var resObj = JSON.parse(res);
			$("#count").text("總共"+resObj.length+"筆資料");	
			for(key in resObj){
				$("#show").append("<tr class='trShow'><td>"+resObj[key]["product_id"]+"</td><td>"+resObj[key]["model"]+"</td><td>"+resObj[key]["name"]+"</td><td>"+resObj[key]["description"]+"</td><td>"+resObj[key]["price"]+"</td><td>"+resObj[key]["date_added"]+"</td><td>"+resObj[key]["date_modified"]+"</td></tr>");
			}		
		};

		function responseNew(res){
			// console.log(res);
			var resObj = JSON.parse(res);
			$("#count").text("總共"+resObj.length+"筆資料");
			for(key in resObj){				
					// =====timestamp 轉date()=======
				var date = new Date(parseInt(resObj[key]["product_createtime"])*1000);
				var time = date.getFullYear()+'-'+("0"+(date.getMonth()+1)).slice(-2)+'-'+("0"+date.getDate()).slice(-2)+' '+("0"+date.getHours()).slice(-2)+':'+("0"+date.getMinutes()).slice(-2)+':'+("0"+date.getSeconds()).slice(-2);

				$("#show").append("<tr class='trShow'><td>"+resObj[key]["product_no"]+"</td><td>"+resObj[key]["model_id"]+"</td><td>"+resObj[key]["product_name"]+"</td><td>"+resObj[key]["product_content"]+"</td><td>"+resObj[key]["product_price1"]+"</td><td>"+time+"</td><td>"+resObj[key]["product_updatetime"]+"</td></tr>");
			}	
			
		};
		
		
	});
</script>