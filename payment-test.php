<?php require_once("module/header.php"); ?>
	<section class="purchase">
		<div class="container">
			<div id="payment" class="row">	
				<h4>金流測試</h4>
			</div>	
			<div class="row">
<!-- <form id="purchase" name="purchase" action="payment-success.php"  method="POST" enctype="application/x-www-form-urlencoded"> -->
				<!-- <form id="purchase" name="purchase" action="https://www.esafe.com.tw/Service/Etopm.aspx"  method="POST" enctype="application/x-www-form-urlencoded"> -->
				<!-- <form id="purchase" name="purchase" action="https://test.esafe.com.tw/Service/Etopm.aspx"  method="POST" enctype="application/x-www-form-urlencoded"> -->
				<form id="purchase" name="purchase" action="<?=esafe_web?>"  method="POST" enctype="application/x-www-form-urlencoded">
				<div class="col-12">
				<div class="form-group row">
					    <label for="web" class="col-form-label col-form-label-sm">商店代碼</label>
					    <input id="web" name="web" type="text" class="form-control form-control-sm"  placeholder="請輸入您的Email" value="<?=esafe_acc_credit?>">
				</div>
				<div class="form-group row">
					    <label for="MN" class="col-form-label col-form-label-sm">交易金額</label>
					    <input id="MN" name="MN" type="text" class="form-control form-control-sm"  placeholder="交易金額" value="<?php echo $_REQUEST["total_price"]; ?>">
				</div>
				<div class="form-group row">
					    <label for="OrderInfo" class="col-form-label col-form-label-sm">交易內容</label>
					    <input id="OrderInfo" name="OrderInfo" type="text" class="form-control form-control-sm"  placeholder="交易內容" value="百貨商品">
				</div>
				<div class="form-group row">
					    <label for="Td" class="col-form-label col-form-label-sm">訂單編號</label>
					    <input id="Td" name="Td" type="text" class="form-control form-control-sm"  placeholder="訂單編號" value="<?php echo $_REQUEST["order_no"]; ?>">
				</div>
				<div class="form-group row">
					    <label for="sna" class="col-form-label col-form-label-sm">消費者姓名</label>
					    <input id="sna" name="sna" type="text" class="form-control form-control-sm"  placeholder="消費者姓名" value="<?php echo $_REQUEST["order_member_name"]; ?>">
				</div>
				<div class="form-group row">
					    <label for="sdt" class="col-form-label col-form-label-sm">消費者電話</label>
					    <input id="sdt" name="sdt" type="text" class="form-control form-control-sm"  placeholder="消費者電話" value="<?php echo $_REQUEST["order_member_tel"]; ?>">
				</div>
				<div class="form-group row">
					    <label for="email" class="col-form-label col-form-label-sm">消費者 Email</label>
					    <input id="email" name="email" type="text" class="form-control form-control-sm"  placeholder="消費者 Email" value="<?php echo $_REQUEST["order_member_email"]; ?>">
				</div>
				<div class="form-group row">
					    <label for="note1" class="col-form-label col-form-label-sm">備註1</label>
					    <input id="note1" name="note1" type="text" class="form-control form-control-sm"  placeholder="備註1" value="">
				</div>
				<div class="form-group row">
					    <label for="note2" class="col-form-label col-form-label-sm">備註2</label>
					    <input id="note2" name="note2" type="text" class="form-control form-control-sm"  placeholder="備註2" value="">
				</div>
				<div class="form-group row">
					    <label for="Card_Type" class="col-form-label col-form-label-sm">交易類別</label>
					    <input id="Card_Type" name="Card_Type" type="text" class="form-control form-control-sm"  placeholder="交易類別" value="0">
				</div>
	
				<div class="form-group row">
					    <label for="Country_Type" class="col-form-label col-form-label-sm">語言類別</label>
					    <input id="Country_Type" name="Country_Type" type="text" class="form-control form-control-sm"  placeholder="交易類別" value="">
				</div>
				<div class="form-group row">
					    <label for="Term" class="col-form-label col-form-label-sm">分期期數</label>
					    <input id="Term" name="Term" type="text" class="form-control form-control-sm"  placeholder="分期期數" value="">
				</div>
				<?php
					//正式機金流
					// $ChkValue=sha1('S1702150416'.'tgilive52415016'.$_REQUEST["total_price"]);
					
					//測試機
					// $ChkValue=sha1('S1702239052'.'tgi52415016'.$_REQUEST["total_price"]);

					$ChkValue=sha1(esafe_acc_credit.esafe_psw.$_REQUEST["total_price"]);
					$ChkValue=strtoupper($ChkValue); //轉換為全部大寫
				?>
				<div class="form-group row">
					    <label for="ChkValue" class="col-form-label col-form-label-sm">交易檢查碼</label>
					    <input id="ChkValue" name="ChkValue" type="text" class="form-control form-control-sm"  placeholder="交易檢查碼" value="<?=$ChkValue?>">
				</div>
				</from>
				</div><!-- .col-12 -->
			</div>	<!-- .row -->
			
				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a>

				<script type="text/javascript">
						$('#purchase').submit()
				</script>
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>