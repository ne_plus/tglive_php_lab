<?php 
	require_once("model/DB.php"); 
    require_once("model/mail.php");  
	//UTF-8編碼
	header('Content-Type: text/html; charset=utf-8');

	$db = new DB();
	
    // 付款結果通知
    include('plugIn/ecpay/ECPay.Payment.Integration.php');
    try {
		// 儲存傳回值
		$table = "ecpay_receive_log";
		$arrValue["MerchantID"] = isset($_POST["MerchantID"]) ? $_POST["MerchantID"] : "";
		$arrValue["MerchantTradeNo"] = isset($_POST["MerchantTradeNo"]) ? $_POST["MerchantTradeNo"] : "";
		$arrValue["StoreID"] = isset($_POST["StoreID"]) ? $_POST["StoreID"] : "";
		$arrValue["RtnCode"] = isset($_POST["RtnCode"]) ? $_POST["RtnCode"] : "";
		$arrValue["RtnMsg"] = isset($_POST["RtnMsg"]) ? $_POST["RtnMsg"] : "";
		$arrValue["TradeNo"] = isset($_POST["TradeNo"]) ? $_POST["TradeNo"] : "";
		$arrValue["TradeAmt"] = isset($_POST["TradeAmt"]) ? $_POST["TradeAmt"] : "";
		$arrValue["PaymentDate"] = isset($_POST["PaymentDate"]) ? $_POST["PaymentDate"] : "";
		$arrValue["PaymentType"] = isset($_POST["PaymentType"]) ? $_POST["PaymentType"] : "";
		$arrValue["PaymentTypeChargeFee"] = isset($_POST["PaymentTypeChargeFee"]) ? $_POST["PaymentTypeChargeFee"] : "";
		$arrValue["TradeDate"] = isset($_POST["TradeDate"]) ? $_POST["TradeDate"] : "";
		$arrValue["SimulatePaid"] = isset($_POST["SimulatePaid"]) ? $_POST["SimulatePaid"] : "";
		$arrValue["CustomField1"] = isset($_POST["PaymentNo"]) ? $_POST["PaymentNo"] : "";
		$arrValue["CustomField2"] = isset($_POST["BankCode"]) ? $_POST["BankCode"] : "";
		$arrValue["CustomField3"] = isset($_POST["vAccount"]) ? $_POST["vAccount"] : "";
		$arrValue["CustomField4"] = isset($_POST["ExpireDate"]) ? $_POST["ExpireDate"] : "";
		$arrValue["CheckMacValue"] = isset($_POST["CheckMacValue"]) ? $_POST["CheckMacValue"] : "";
	
		$arrValue["createtime"] =  date('Y/m/d H:i:s');
		$db -> DB_Insert($table, $arrValue);
        
		// 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
        $AL = new ECPay_AllInOne();
        $AL->MerchantID = ecpay_merchantid;
        $AL->HashKey = ecpay_hashkey;
        $AL->HashIV = ecpay_hashiv;
		// $AL->EncryptType = ECPay_EncryptType::ENC_MD5;  // MD5
        $AL->EncryptType = ECPay_EncryptType::ENC_SHA256; // SHA256
        $feedback = $AL->CheckOutFeedback();
		// 更新訂單狀態
		if ($arrValue["MerchantID"] == ecpay_merchantid && $arrValue["RtnCode"] == "1") {
			//付款成功
			$table = "order_item";
			$checkColumn = array("order_group");
			$data = array(
				"order_group" => $arrValue["MerchantTradeNo"],
				"order_pay_status" => 1,
				"order_status" => 1
			);
			$db -> DB_UpdateOnly($table,$data,$checkColumn);
		}
		if ($arrValue["MerchantID"] == ecpay_merchantid && $arrValue["RtnCode"] == "2"  && $arrValue["PaymentType"] = "ATM_CATHAY" ) {
			//ATM支付

			$sql = 'SELECT * FROM order_item WHERE order_group = "'.$arrValue['MerchantTradeNo'].'"';
			$orders = $db->DB_Query($sql);
			$order  = $orders[0];
			$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$order['order_recipient'].'</p><p>感謝您對TGiLive居生活的支持，此信件是通知您我們已收到您的訂購需求，且訂單已經成立，我們將在收到您的付款後，盡快處理您的訂單。</p><p>您選擇的繳費方式為：<b>現金匯款</b> </p><p>下方為您的ATM轉帳資訊 :</p><table border="0" cellspacing="0" style="width:100%; font-size: 12px; text-align: center;border:1px #ccc solid;"><tr><th  style="padding: 10px 0;background-color: #fff;border-bottom:1px #ccc solid;">現金匯款</th></tr><tr><td style="background-color: #fff;font-size:20px;font-weight:800;padding: 30px 0;">'.'匯款銀行代碼:'.$arrValue["CustomField2"].',匯款帳號:'.$arrValue["CustomField3"].'<br/>請於 '.$arrValue["CustomField4"].' 前完成匯款</td></tr></table>';
			$content .='<ul><li>超商代收-代碼的繳費期限為7天，請務必於期限內進行繳款。例：08/01的20:15分購買商品，繳費期限為7天，表示8/08的20:15分前您必須前往繳費。</li><li>若您無法取得超商繳費代碼。請與居生活客服人員聯繫</li><li>超商繳款操作流程：<a href="https://www.tgilive.com/payInfo.php" target="_blank">https://www.tgilive.com/payInfo.php</a></li><li>若有任何訂單上處理的問題，歡迎您利用以下方式與我們聯繫：</li><p>TGiLive居生活粉絲專頁： <span><a href="https://www.facebook.com/TGiLiveTW/" target="_blank" style="color:#00f;">https://www.facebook.com/TGiLiveTW/</a></span></p><p>TGiLive客服信箱： <span><a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a></span></p><p>我們的客服回覆時間： <span>週一至週五10:00am-18:00pm</span></p></ul><p>◎TGiLive居生活防範詐騙聲明：</p><ol><li>我們所有訊息以Email及粉絲專頁私訊為主，不會任意打電話給會員。</li><li>不會傳簡訊或電話通知您誤設分期、簽錯單據、訂單重複等等。</li><li>我們不會要求您操作任何ATM機器。</li><li>接到疑似詐騙電話，切勿聽信前往操作提款機或回撥電話，或直接撥打165反詐騙專線。</li><li>請勿直接回覆信件，若有訂單或帳務問題，請利用上述方式直接與TGiLive居生活聯繫。</li></ol><p>※請注意！TGiLive居生活官網所有付款服務只有「一次付清」單一選項，絕不會誤設成分期付款，若接獲不明電話以此詐騙，請馬上報警或撥打165反詐騙專線。</p><p style="margin-top: 50px;">TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';
			sendgrid('現金匯款資訊', $content, [$order['order_email']]);
		}

		if ($arrValue["MerchantID"] == ecpay_merchantid && $arrValue["RtnCode"] == "10100073" && $arrValue["PaymentType"] = "CVS_CVS" )  {
			//取得超商付款代碼

			$sql = 'SELECT * FROM order_item WHERE order_group = "'.$arrValue['MerchantTradeNo'].'"';
			$orders = $db->DB_Query($sql);
			$order  = $orders[0];
			$content = '<p><img src="https://www.tgilive.com/image/frontend/EC-logo.png" width="100"><span style="width: calc( 100% - 110px );height: 3px; background-color: #FDC5CD;display: inline-block;margin-left: 10px;"></span></p><p>hello,'.$order['order_recipient'].'</p><p>感謝您對TGiLive居生活的支持，此信件是通知您我們已收到您的訂購需求，且訂單已經成立，我們將在收到您的付款後，盡快處理您的訂單。</p><p>您選擇的繳費方式為：<b>超商代碼繳費</b> </p><p>下方為您的超商繳費代碼 :</p><table border="0" cellspacing="0" style="width:100%; font-size: 12px; text-align: center;border:1px #ccc solid;"><tr><th  style="padding: 10px 0;background-color: #fff;border-bottom:1px #ccc solid;">超商代碼</th></tr><tr><td style="background-color: #fff;font-size:20px;font-weight:800;padding: 30px 0;">'.$arrValue["CustomField1"].'</td></tr></table>';
			$content .='<ul><li>超商代收-代碼的繳費期限為7天，請務必於期限內進行繳款。例：08/01的20:15分購買商品，繳費期限為7天，表示8/08的20:15分前您必須前往繳費。</li><li>若您無法取得超商繳費代碼。請與居生活客服人員聯繫</li><li>超商繳款操作流程：<a href="https://www.tgilive.com/payInfo.php" target="_blank">https://www.tgilive.com/payInfo.php</a></li><li>若有任何訂單上處理的問題，歡迎您利用以下方式與我們聯繫：</li><p>TGiLive居生活粉絲專頁： <span><a href="https://www.facebook.com/TGiLiveTW/" target="_blank" style="color:#00f;">https://www.facebook.com/TGiLiveTW/</a></span></p><p>TGiLive客服信箱： <span><a href="mailto:info@tgilive.com" target="_blank" >info@tgilive.com</a></span></p><p>我們的客服回覆時間： <span>週一至週五10:00am-18:00pm</span></p></ul><p>◎TGiLive居生活防範詐騙聲明：</p><ol><li>我們所有訊息以Email及粉絲專頁私訊為主，不會任意打電話給會員。</li><li>不會傳簡訊或電話通知您誤設分期、簽錯單據、訂單重複等等。</li><li>我們不會要求您操作任何ATM機器。</li><li>接到疑似詐騙電話，切勿聽信前往操作提款機或回撥電話，或直接撥打165反詐騙專線。</li><li>請勿直接回覆信件，若有訂單或帳務問題，請利用上述方式直接與TGiLive居生活聯繫。</li></ol><p>※請注意！TGiLive居生活官網所有付款服務只有「一次付清」單一選項，絕不會誤設成分期付款，若接獲不明電話以此詐騙，請馬上報警或撥打165反詐騙專線。</p><p style="margin-top: 50px;">TGiLive居生活團隊 敬上</p><span style="width: calc( 100% );height: 3px; background-color: #FDC5CD;display: inline-block;"></span>';
			sendgrid('超商代碼繳費繳款資訊', $content, [$order['order_email']]);
		}
        // 以付款結果訊息進行相對應的處理
        /** 
        回傳的綠界科技的付款結果訊息如下:
        Array
        (
            [MerchantID] =>
            [MerchantTradeNo] =>
            [StoreID] =>
            [RtnCode] =>
            [RtnMsg] =>
            [TradeNo] =>
            [TradeAmt] =>
            [PaymentDate] =>
            [PaymentType] =>
            [PaymentTypeChargeFee] =>
            [TradeDate] =>
            [SimulatePaid] =>
            [CustomField1] =>
            [CustomField2] =>
            [CustomField3] =>
            [CustomField4] =>
            [CheckMacValue] =>
        )
		
			MerchantID is 2000132
			MerchantTradeNo is TT0000009
			PaymentDate is 2019/06/23 10:34:24
			PaymentType is Credit_CreditCard
			PaymentTypeChargeFee is 1
			RtnCode is 1
			RtnMsg is 交易成功
			SimulatePaid is 0
			TradeAmt is 500
			TradeDate is 2019/06/23 10:33:07
			TradeNo is 1906231033072315
			CheckMacValue is BEC7D0892D666E1130B4012591B0A407
        */

        // 在網頁端回應 1|OK
        echo '1|OK';
    } catch(Exception $e) {
        echo '0|' . $e->getMessage();
		$arrValue["CustomField1"] = $e->getMessage();
		$arrValue["CustomField2"] = "傳遞值:".print_r($_REQUEST,true);
		$arrValue["createtime"] =  date('Y/m/d H:i:s');
		$db -> DB_Insert($table, $arrValue);
    }
?>