<?php require_once("module/header.php"); 

$pid = $_GET["product_no"];
$sql ="select * from product where product_status =1 and product_no = ".$_GET["product_no"];
$result = productQueryAll($sql);

// 檢查商品是否有綁定供應商
$checkVendor = true;
if(!productCateQuery('product',$_GET["product_no"])){
	$checkVendor = false;
}



require_once("class/productClass.php");

$p = new productObj($pid);
$now = date("Y-m"); //年-月



if($result != false && $checkVendor == true){
//商品瀏覽次數(新增修改)
$p -> updateProductViews($pid,$now);
//end商品瀏覽次數

	//有商品
// ----計算商品圖片數量----
$imgCount = 0;
for($j=1 ; $j<6 ; $j++){
	$img = "img".$j;
	if(trim($result[0][$img]) != null || trim($result[0][$img]) != ""){
		$imgCount++;
	};
}
//----抓取商品規格----
$table = "product";
$productNo = $_GET["product_no"] ;
$specResult = productHaveSpecQuery($table,$productNo);



//---相關商品----
$relateProducts = get_relateProducts_by_productId($productNo);

// 加價購
$comboProducts = get_comboProducts_by_productId($productNo);
?>
	<section class="productDetailShow">
		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><a href="brands.php"><?php echo $result[0]["cate_name"][0]; ?></a><form action="brands.php" method="get"><input type="hidden" name="cate_no" value="<?php echo $result[0]["cate_no"][0]; ?>"><input type="hidden" name="brand" value="<?php echo preg_replace('/\s(?=)/', '', $result[0]['cate_name'][0]); ?>"></form></li>
			  <li class="breadcrumb-item active"><?php echo $result[0]['product_name']; ?></li>
		</ol>
		<div class="container mt-65">
			<div class="col-12">
            	<div class="row">
					<div class="col-12 col-md-5">
						<div class="center slide slider-for">
						<?php for($p = 0 ; $p< $imgCount ;$p++) {
							$img = "img".($p+1);	
						?>	
							<div>
								<img src="<?php echo $result[0][$img]; ?>" class="img-fluid">
							</div>
						<?php
						}
						?>
						</div>
						<div class="center slide slider-nav">
						<?php for($p = 0 ; $p< $imgCount ;$p++) {
							$img = "img".($p+1);	
						?>	
							<div>
								<img src="<?php echo $result[0][$img]; ?>" class="img-fluid">
							</div>
						<?php
						}
						?>
						</div>
	                </div>
					<div class="col-12 col-md-7">
						<div class="single_product_desc">
							<div class="form-group row product_title">
								<div class="col-12">
									<h5 class="product_name"><?php echo $result[0]['product_name']; ?></h5>
								</div>
								<div class="col-12">
									<h6 class="product_subtitle"><?php echo $result[0]['product_subtitle']; ?></h6>
								</div>
		                        <div class="col-12">
		                        	 <h6 class="promote"><?php echo $result[0]['product_promote']; ?></h6>
		                        </div>
							</div>
	                       
							<div class="form-group row product_spec">
								<div class="product_spec_wrap">
								<div class="col-12 mb-5">
									<select class="custom-select col-12" id="productSpecSelect">
									<?php for ($s=0 ;$s<count($specResult);$s++ ) { ?>
									  <option value="<?php echo $specResult[$s]["product_spec_no"] ;?>" <?php if($s==0){echo "selected";} ?>><?php echo $specResult[$s]["product_spec_info"]; ?></option>
									<?php } ?> 
									</select>
								</div>
							<?php
								for ($s=0 ;$s<count($specResult);$s++ ) {
							?>
								<div class="col-12 mt-1" <?php if($s>0){echo 'style="display: none;"';} ?>>
									<div class="form" id="<?php echo 'specNo'.$specResult[$s]["product_spec_no"]; ?>">
<!--
										<div class="mb-2 mr-sm-2 mb-sm-0" style="font-size: 14px;">
											<span><?php echo $specResult[$s]['product_spec_info'];?></span>
										</div>
-->
		
										<?php
											$oldPrice = 0 ;
											$newPrice = 0 ;
											$shouldHidn = " ";
											$shouldHidnSecond ="";
		                            		if($specResult[$s]["product_spec_price3"] != 0 || $specResult[$s]["product_spec_price2"] != 0){ //有優惠價
		                            			if($specResult[$s]["product_spec_price3"] != 0){ 
			                            			//有優惠價
			                            			$oldPrice = $specResult[$s]["product_spec_price1"];
			                            			$newPrice = $specResult[$s]["product_spec_price3"];

			                            			if( $specResult[$s]["product_spec_price2"] == 0 ){
			                            				$shouldHidnSecond = " style='display:none' ";
			                            			}
			                            		}else{
				                            		//有售價
				                            		$oldPrice = $specResult[$s]["product_spec_price1"];
			                            			$newPrice = $specResult[$s]["product_spec_price2"];

			                            			if( $specResult[$s]["product_spec_price3"] == 0 ){
			                            				$shouldHidnSecond = " style='display:none' ";
			                            			}
			                            		}
			                            	}else{
				                            		$oldPrice = 0;
			                            			$newPrice = $specResult[$s]["product_spec_price1"];
			                            			$shouldHidn = " style='display:none' ";  	
			                            			$shouldHidnSecond = " style='display:none' ";
			                            	}
		                            	?>
										
										<div class="old-price" <?=$shouldHidn?>><span <?=$shouldHidnSecond?>><?=$lang_product_oldprice?></span><span><?=$lang_product_currency?> </span><?=$oldPrice?></div>
										<div class="old-price" <?=$shouldHidnSecond?>><span><?=$lang_product_currency?> </span><?=$specResult[$s]["product_spec_price2"]?></div>
										<div class="new-price"><span><?=$lang_product_currency?> </span><span class='finalPrice'><?=$newPrice?></span></div>

										
										
										<div class="pay-install-note">
											<div class="note">
												<div class="pay-install">
											<span class="price"><?=$lang_product_install_price?> <?=$lang_product_currency?><?=(int)($newPrice / 3)?> </span>
										</div>
												<?=$lang_product_install_note?></div>
											<div class="title"><?=$lang_product_install_note_title?></div>
											<div class="banks"><?=$lang_product_install_note_banks?></div>
										</div>
										<p>&nbsp;</p>
										
										<div class="input-group">
											<span class="input-group-addon"><?php if($specResult[$s]['product_stock'] == 0){echo "缺貨" ;}else{echo "數量";} ?></span>
											<input type="number" class="form-control form-control-sm qty" min="1" max="<?php echo $specResult[$s]['product_stock']; ?>" value="1" <?php if($specResult[$s]['product_stock'] == 0){echo "disabled" ;} ?>>
										</div>
									</div>
								</div>
							<?php
								}
							?>
								</div><!-- product_spec_wrap -->
							</div><!-- product_spec -->
							<div class="form-group row">
								<div class="purchase_area">
			                        <a id="addToCart" role="button" class="btn btn-sm"><?=$lang_cart_addcart?></a>
			                        <a id="addToCartAndgoPayment" role="button" class="btn btn-sm"><?=$lang_cart_addbuy?></a>
			                        <input type="hidden" name="product_no" value="<?php echo $_GET["product_no"]; ?>">
			                        <input type="hidden" name="product_name" value="<?php echo $result[0]['product_name'];?>">
			                        <input type="hidden" name="cate_name" value="<?php echo $result[0]["cate_name"][0] ; ?>"> <!-- 品牌名稱 -->
			                        <input type="hidden" name="cate_no" value="<?php echo $result[0]["cate_no"][0] ; ?>"> <!-- 品牌分類編號 -->
			                    </div>
							</div>
	                        <div class="form-group row">
	                            <div class="col-12">
	                            	<p class="product_describe"><?php echo $result[0]['product_describe']; ?></p>
	                            </div>
	                        </div>

	                        	<!-- ======社群====== -->
							<div class="form-group row">
	                        	<div class="social col-12">
	                        		<!-- 社群分享 -->
	                        		<div class="d-flex justify-content-start">
		                        		<div>
		                        		  <!-- =======fb 分享====== -->
		                        			<div id="fb-root"></div>
											<script>(function(d, s, id) {
											  var js, fjs = d.getElementsByTagName(s)[0];
												  if (d.getElementById(id)) return;
												  js = d.createElement(s); js.id = id;
												  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.12&appId=432812180472781&autoLogAppEvents=1';
												  fjs.parentNode.insertBefore(js, fjs);
												}(document, 'script', 'facebook-jssdk'));
											</script>
				                        	<div class="fb-share-button" data-href="<?php echo $url; ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a id="fbShare" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><!-- <i class="fab fa-facebook-f fa-2x"></i> --></a></div>
				                        	<!-- =======fb 分享====== -->
				                        	<!-- <script type="text/javascript">
				                        		$("a#fbShare").click(function(e){
 													e.preventDefault();
												  var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + document.URL, 'facebook-popup', 'height=350,width=600');
												  if(facebookWindow.focus) { facebookWindow.focus(); }
												    return false;
				                        		});
				                        	</script> -->
		                        		</div>
	                        		</div>
	                        		<!-- 社群分享 -->
	                        	</div>
	                        	
	                        </div>	
	                        <!-- ======社群====== -->

	                        
							<!-- ======加價購====== -->
							<div class="form-group row">
								<?php 
								if( count($comboProducts)>0){
								?>
								<div class="col-12 comporducts">
									<div class="comporductsTitle bg-secondary text-white text-center border-0 ">
										可加購商品
									</div>
								</div>


								<?php //foreach ($comboProducts as $key => $value) { ?>
									<!-- <div class="social col-3">
										<img src="<?=$value["img1"]?>" style="max-width: 100px;" />
										<div class="purchase_area"><a id="addComboToCart" data-key="<?=$value["id"]?>" role="button" class="btn btn-sm"><?=$lang_cart_addcart?></a></div>
										<?=$value["product_name"]?><br />
										<?=$value["product_spec_info"]?><br />
										$<?=$value["product_spec_price"]?>
									</div> -->
								<?php // } ?>

								<?php foreach ($comboProducts as $key => $value) { ?>
								<div class="comporducts col-lg-6">
									<div class="d-flex align-items-center">
										<div class="comporductsItem-select">
											<!-- <input type="checkbox" > -->
											<input type="radio" name="comboSelect" class="addComboProductBeforeToCart" data-key="<?=$value["id"]?>" data-charge="<?=$value["product_spec_price"]?>" <?=(in_array($value["id"], $orderComboList)) ? 'checked' : ''?> />
										</div>
										<div class="comporductsItem-img">
											<img src="<?=$value["img1"]?>" class="img-fluid">
										</div>
										<div class="comporductsItem-desc">
											<div class="comporductsItem-title">
												<a href="product-detail.php?product_no=<?=$value["product_no"]?>"><?=$value["product_name"]?></a>
											</div>
											<div class="comporductsItem-specInfo text-secondary">規格: <?=$value["product_spec_info"]?></div>
											<div class="text-danger">加購價: $<?=$value["product_spec_price"]?></div>	
										</div>
									</div>
								</div>
								<?php } // end foreach?>
								<?php } // end if?>			

							</div>
							<!-- ======加價購====== -->
							
                    	</div>  <!--  end single_product_desc -->
					</div> <!-- end col-12 col-md-7 -->

                </div>
            </div>

			<div class="col-md-9 offset-md-3 col-sm-12">
				<div class="product_details_box">
					<div class="product_content">
						<?php echo $result[0]["product_content"]; ?>
					</div>

				</div>

			</div><!-- 	col-6 offset-3 -->		
	</div><!-- container -->
	
	<div class="relateProduct my-3">
		<div class="container">
			<h5 class="text-center"><?=$lang_product_recommand?></h5>
			<div class="productRelateBox">
			<?php 
				foreach ($relateProducts as $key => $value) {	
			?>
				 <div class="product-items">
				 	<form action="product-detail.php" method="get">
				  		<div class="content-hover">							  		
					  		<img src="<?php echo $value['img1']; ?>" class="img-fluid">
					  		<div class="block">
				  				<div class="info text-center d-flex align-items-center justify-content-center">
				  					<div class="product-name" style="font-size: 14px;"><?php echo $value['product_name']; ?></div>			
				  				</div>
				  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
				  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
				  				</div>
				  			</div>	
				  			<div class="price text-center">
				  			<?php  
				  				$relateProductsSpecResult = productHaveSpecQuery($table,$value['product_no']);	
				  				// 抓第一筆spec							
								if($relateProductsSpecResult[0]["product_spec_price2"] == 0 && $relateProductsSpecResult[0]["product_spec_price3"] == 0){ //沒有折扣
							?>
								<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$relateProductsSpecResult[0]['product_spec_price1']; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
							<?php		
								}elseif($relateProductsSpecResult[0]["product_spec_price3"] != 0){ //有最低折扣
							?>
								<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$relateProductsSpecResult[0]['product_spec_price3']; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$relateProductsSpecResult[0]['product_spec_price1']; ?></div>
							<?php		
								}else{ //售價
							?>
								<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$relateProductsSpecResult[0]['product_spec_price2']; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$relateProductsSpecResult[0]['product_spec_price1']; ?></div>
							<?php		
								}
				  			?>
			  				</div>
					  	</div>
					  	<input type="hidden" name="product_no" value="<?php echo $value['product_no']; ?>">
					</form>
				  </div>	
			<?php		
				} //----end for loop
			?>
				
			</div>
		</div>
	</div>


	</section>
<?php
}else{ 
?>
<section class="productDetailShow">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
		  <li class="breadcrumb-item active"><a href="brands.php"><?=$lang_menu_findBrand?></a></li>
		  <li class="breadcrumb-item active"><a href="brands.php"><?php echo $result[0]["cate_name"][0]; ?></a><form action="brands.php" method="get"><input type="hidden" name="cate_no" value="<?php echo $result[0]["cate_no"][0]; ?>"></form></li>
	</ol>
	<div class="container">
		<div class="col-12">
			<div class="no-content">
				<?=$lang_product_notforsale?>
			</div>
		</div>
	</div><!-- container -->
</section>
<?php
}
?>

<script>
	// youtube rwd 強制修改尺寸
	function iframResize() {
		if( $(window).width() < 768 ){
			$(".product_details_box").find("iframe").attr("height","200");
			$(".product_details_box").find("iframe").attr("width","100%");
		}
	}
	iframResize()
</script>

<?php require_once("module/footer.php"); ?>