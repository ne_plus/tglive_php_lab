-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: localhost:3306
-- 產生時間： 2017 年 10 月 06 日 04:48
-- 伺服器版本: 5.6.35
-- PHP 版本： 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 資料庫： `tgilive`
--

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `mem_no` int(11) NOT NULL,
  `mem_isAdmin` tinyint(4) NOT NULL,
  `mem_authLevel` tinyint(4) NOT NULL,
  `mem_mail` varchar(100) NOT NULL,
  `mem_password` varchar(100) NOT NULL,
  `mem_firstname` varchar(50) NOT NULL,
  `mem_lastname` varchar(50) NOT NULL,
  `mem_vertifycode` varchar(50) NOT NULL,
  `mem_tel` varchar(100) NOT NULL,
  `mem_gender` tinyint(4) NOT NULL,
  `mem_address` varchar(200) NOT NULL,
  `mem_status` tinyint(4) NOT NULL,
  `mem_createtime` int(11) UNSIGNED NOT NULL,
  `mem_updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `mem_lastLoginTime` int(11) UNSIGNED NOT NULL,
  `mem_loginIp` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `member`
--

INSERT INTO `member` (`mem_no`, `mem_isAdmin`, `mem_authLevel`, `mem_mail`, `mem_password`, `mem_firstname`, `mem_lastname`, `mem_vertifycode`, `mem_tel`, `mem_gender`, `mem_address`, `mem_status`, `mem_createtime`, `mem_updatetime`, `mem_lastLoginTime`, `mem_loginIp`) VALUES
(1, 2, 3, 'jerry123@mail.com', '12345a%b\'cdefg', 'jerry', 'jerry', '6431d1dedb829b371daee541875f4f38', '987654321', 1, '高雄市', 2, 4294967295, '2017-10-05 08:13:05', 4294967295, '16'),
(2, 2, 3, 'jack@mail.com', '12345a%b\'cdefg', 'jack', 'jack', 'dada4fb2e4c2a683b2abcaa8d5e3c43d', '987654321', 1, '臺北', 2, 1507257823, '2017-10-06 02:43:43', 4294967295, '16'),
(4, 2, 3, 'bill@mail.com', '12345a%b\'cdefg', 'bill', 'bill', 'd632febf390e947e8ed1c076853584ff', '987654321', 1, '臺北', 2, 1507260079, '2017-10-06 03:21:19', 4294967295, '16'),
(5, 2, 3, 'kate@mail.com', '12345a%b\'cdefg', 'kate', 'kate', '01a2ad806d1b3ce56c062a7b3e3e5956', '987654321', 1, '臺北', 2, 1507262018, '2017-10-06 03:53:38', 4294967295, '16'),
(6, 2, 3, 'joe@mail.com', '12345a%b\'cdefg', 'joe', 'joe', '1a0d6e8eb380633a0076d3934387a532', '987654321', 1, '臺北', 2, 1507262064, '2017-10-06 03:54:24', 4294967295, '16'),
(7, 2, 3, 'peter@mail.com', '12345a%b\'cdefg', 'peter', 'peter', '69bd28e6a34937c2654e4f0b8c52c64b', '987654321', 1, '臺北', 2, 1507262373, '2017-10-06 03:59:33', 0, '16'),
(8, 2, 3, 'allen@mail.com', '444444', 'S', 'A', '123asd', '22212322', 1, 'Taipei', 0, 0, '2017-10-06 04:25:38', 4294967295, ''),
(9, 2, 3, 'terry@mail.com', '12345a%b\'cdefg', 'terry', 'terry', '127d10d77c9318856b38c086b6d932da', '987654321', 1, '臺北', 2, 1507265075, '2017-10-06 04:44:35', 0, '16'),
(10, 2, 3, 'carry@mail.com', '12345a%b\'cdefg', 'carry', 'carry', 'dd0ed255c4530fe760a2427f480c9ba8', '987654321', 1, '臺北', 2, 1507265215, '2017-10-06 04:46:55', 1507265215, '16');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mem_no`),
  ADD UNIQUE KEY `mem_mail` (`mem_mail`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `member`
--
ALTER TABLE `member`
  MODIFY `mem_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;