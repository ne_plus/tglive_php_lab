<?php require_once("module/header.php"); 
	// ---會員資訊----
	if(isset($_SESSION["memNo"])){
		include("Frontframwork/memInfo.php");	
	}else{
		header( 'Location:index.php');
	}
?>

	<section class="purchase" style="margin-bottom: 0;">
<!--
        <div class="main-menu_memSideBar">
          <ul id="side-main-menu" class="side-menu list-unstyled mt-3">                  
            <li><a href="memcenter.php" class="memberSelect active"><i class="fa fa-user" aria-hidden="true"></i> <span><?=$lang_member_info?><</span></a></li>
            <li><a href="memhistory.php" class="adminSelect"><i class="fa fa-history" aria-hidden="true"></i> <span>購物紀錄</span></a></li>
          </ul>
        </div>
-->
		
		<div class="memcenter">
			<div class="container">
				<div class="row" >
					<div class="col-12">
						<h3><?=$lang_member_info?></h3>	
						<div class="card mb-3">
							<div class="card-body">
								<div class="form-group row">
									<div class="col-12 text-right ">
										<button id="memPswEditBtn" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_member_reset_password?>"><i class="fa fa-key" aria-hidden="true"></i></button>	
										 <button id="memInfoEditBtn" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_member_edit_info?>"><i class="fa fa-cog fa-fw" aria-hidden="true"></i></button>	
									</div>
								</div>
								<from>
								 <div class="form-group row">
								    <label for="memberEmail" class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_account3?></label>
								    <div class="col-lg-6">
								      <input type="email" class="form-control form-control-sm" id="memberEmail" placeholder="<?=$lang_member_account_hint?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_mail']; } ?>" disabled="true">
							      <?php if(isset($_SESSION['memNo'])){ ?>
								      <input type="hidden" id="memberNo" name="mem_no" value="<?php echo $_SESSION['memNo']; ?>">	
							      <?php } ?>
								    </div>
								   
								     
								  </div>

								  <div class="form-group row">
								    <label  class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_fullname?></label>
								    <div class="col-lg-3 col-6">
								      <input type="test" class="form-control form-control-sm" id="memberLastName" placeholder="<?=$lang_member_receiver_firstname?>" value="<?php if(isset($_SESSION['memNo'])){ echo $_SESSION['memLastName']; } ?>" disabled="true">
								    </div>
								     <div class="col-lg-3 col-6">
								      <input type="test" class="form-control form-control-sm" id="memberFirstName" placeholder="<?=$lang_member_receiver_lastname?>" value="<?php if(isset($_SESSION['memNo'])){ echo $_SESSION['memFirstName']; } ?>" disabled="true">
								    </div>
								  </div>
								  <div class="form-group row">
								    <label for="memberPhone" class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_phone2?></label>
								    <div class="col-lg-6">
								      <input type="tel" class="form-control form-control-sm" id="memberPhone" placeholder="<?=$lang_member_phone2?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_tel']; } ?>" disabled="true">
								    </div>
								  </div>
								  <div class="form-group row">
								    <label for="memberAddress" class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_address2?></label>
								    <div class="col-lg-6">
								      <input type="text" class="form-control form-control-sm" id="memberAddress" placeholder="<?=$lang_member_address2?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_address']; } ?>" disabled="true">
								    </div>
								  </div>
									
								<div id="pswArea" style="display: none;">
										<div class="line"></div>
										<div class="form-group row">
										    <label for="memberPSWChange" class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_newpassword?></label>
										    <div class="col-lg-6">
										      <input type="password" class="form-control form-control-sm" id="memberPSWChange" placeholder="<?=$lang_member_newpassword_hint?>" value="">
										 	 </div>
										</div>
										<div class="form-group row">
										    <label for="memberPSWChangeAgain" class="col-lg-2  col-form-label col-form-label-sm"><?=$lang_member_repassword?></label>
										    <div class="col-lg-6">
										      <input type="password" class="form-control form-control-sm" id="memberPSWChangeAgain" placeholder="<?=$lang_member_repassword_hint?>" value="">
										    </div>
										</div>
								</div>	
								 
								 
							 	 </from>
								
								
									<div class="modal-footer col-12">
										<div class="row">
											<div class="text-right">
												<button id="memInfoCancelBtn" type="button" class="btn btn-outline-secondary btn-sm" style="display: none;"><i class="fa fa-ban" aria-hidden="true"></i> <?=$lang_cancel?></button>
												<button id="memInfoConfirmBtn" type="button" class="btn btn-outline-success btn-sm" style="display: none;"><i class="fa  fa-save" aria-hidden="true"></i> <?=$lang_save?></button>

										    	<button id="memPswChangeBtn" type="button" class="btn btn-outline-danger btn-sm" style="display: none;"><i class="fa fa-key" aria-hidden="true"></i> <?=$lang_member_reset_password2?></button>	
											</div>								   		
										</div>			   
									</div>
					 		</div> <!-- .card-body -->
						</div> <!-- .card -->
					</div><!-- .col-lg-12 -->
					
					
					<!-- <a class="btn btn-outline-secondary btn-sm submitBtn" href="payment.php" role="button">填寫付款資訊</a> -->
				</div>	<!-- .info-->

			</div>
		</div><!-- .memcenter -->
	</section>			

<?php require_once("module/footer.php"); ?>