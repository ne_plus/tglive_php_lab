<?php require_once("module/header.php"); ?>

	<section class="pages">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_footer_faq_eliver?>
			
		</ol>
		<div class="container">
		<div class="row">
		<h3><?=$lang_footer_faq_eliver?></h3>
			
		<div class="subtitle">Q1：下單後何時收到貨？</div>
		<p>A：選購商品時，請留意商品頁中購買須知的運送說明
		 <li>商品下單後依訂單編號開始寄送（2~5個工作天內送達），依物流公司作業時間為主。注意：收件地址請勿為郵政信箱。</li>
		 <li>商品頁標示為「預購」商品，將以實際出貨或製作日標示為主。（不適用2~5個工作天出貨。）</li>
		 <li>送貨方式由物流宅配送達。</li>
		 <li>若遇週休假日及例假日（國定假日），因金融機構系統於假日無法立即入帳，將以確實核對到帳款日來計算配送時間。<br/>
		<span class="highlight">※如遇不可抗力之因素，例如：颱風、地震及水災...等，將視當時狀況處理。</span></li>
		 <li>週休假日及例假日，暫停出貨和客服服務，造成不便，敬請見諒。</li>
		 <li>請確認您留的收貨地址有人員到貨簽收。</li></p>
		
		<div class="subtitle">Q2：顯示已出貨卻未收到商品？</div>
		<p>A：當您收到『TGI居生活』發出商品出貨的通知信時，表示商品已由廠商出貨，商品出貨後經由物流配送，過程約三至四個工作天，請耐心等候。</p>
		
		<div class="subtitle">Q3：訂多樣商品會一次收到嗎？</div>
		<p>A：若選購的商品為不同品牌時，將由不同的廠商分別寄出，商品將會陸續到貨，感謝您的耐心等候。</p>
		
		<div class="subtitle">Q4：如何線上查詢出貨單號狀態？</div>
		<p>A：請先登入，並如下方截圖方式點選您的名稱，並點選訂單記錄即可查詢您的訂單狀態，或是來信至<a href="mailto:info@tgilive.com" target="_blank">客服中心（Email：info@tgilive.com）</a>，我們將協助查詢出貨進度。</p>
	
				
			</div>	<!-- .row -->
			
<!-- 				<a class="btn btn-outline-secondary btn-sm submitBtn" href="javascript:$('#purchase').submit();" role="button">確定購買</a> -->
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>