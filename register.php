<?php require_once("module/header.php"); 
	if(isset($_SESSION['memNo']) === true){
			header("location:index.php");
	}
?>

<section class="registerShow marginTop100">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
		  <li class="breadcrumb-item active"><?=$lang_member_register2?></li>
	</ol>

	<div class="container registerBox">
		<div class="row">
			<div class="col-12">
				<form>
				  <div class="form-group">
				    <label for="userAcountCreate"><span class="form_focus">*</span><?=$lang_member_account2?></label>
				    <input type="text" class="form-control form-control-sm" id="userAcountCreate" placeholder="Example@mail.com" autocomplete="off">
					</div>
					<div class="form-group">
				    <label for="userPswCreate"><span class="form_focus">*</span><?=$lang_member_password2?></label>
				    <input type="password" class="form-control form-control-sm" id="userPswCreate" placeholder="<?=$lang_member_password_hint?>">
				  </div>
				   <div class="form-group">
				    <label for="userPswConfrimCreate"><span class="form_focus">*</span><?=$lang_member_repassword?></label>
				    <input type="password" class="form-control form-control-sm" id="userPswConfrimCreate" placeholder="<?=$lang_member_repassword_hint?>">
				  </div>
				  <div class="row">
				  	<div class="col-12 col-md">
				  		<div class="form-group">
						    <label for="userLastNameCreate"><span class="form_focus">*</span><?=$lang_member_lastname?></label>
						    <input type="text" class="form-control form-control-sm" id="userLastNameCreate" placeholder="<?=$lang_member_lastname?>">
						</div>
				  	</div>
				  	<div class="col-12 col-md">
				  		<div class="form-group">
						    <label for="userFirstNameCreate"><span class="form_focus">*</span><?=$lang_member_firstname?></label>
						    <input type="text" class="form-control form-control-sm" id="userFirstNameCreate" placeholder="<?=$lang_member_firstname?>">
						</div>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label for="userPhoneCreate"><span class="form_focus">*</span><?=$lang_member_phone?></label>
				    <input type="tel" class="form-control form-control-sm" id="userPhoneCreate" placeholder="<?=$lang_member_phone_hint?>">
				  </div>
				   <div class="form-group">
				    <label for="userAddressCreate"><?=$lang_member_address?></label>
				    <input type="text" class="form-control form-control-sm" id="userAddressCreate" placeholder="<?=$lang_member_address_hint?>">
				  </div>
				  <button id="registerConfirmButton"  class="btn btn-primary mb-2" style="display: block; width:100%;"><?=$lang_member_register?></button>
				</form>
			</div>
		</div>
	</div>


</section>
	
  
<?php require_once("module/footer.php"); ?>