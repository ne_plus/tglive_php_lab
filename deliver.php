<?php require_once("module/header.php"); 
	
	// -----購物車資訊----
	$orderTempList = [];
	if(isset($_SESSION["orders"]) && isset($_SESSION["checkSessionKey"])){ //有存在購物清單
		//刪除checkSessionKey
		unset($_SESSION["checkSessionKey"]);

		foreach ($_SESSION["orders"] as $orderskey => $ordersvalue) {
			foreach ($ordersvalue as $key => $value) {
				array_push($orderTempList,$value);
			}		
		}
	}else{ //沒有購物清單
		header("location:cart.php");
	}
	// ---會員資訊----
	if(isset($_SESSION["memNo"])){
		include("Frontframwork/memInfo.php");	
	}

	// 免運設定
	$cartSql ="SELECT * from cart_set where cs_no = 1";
	$cartPriceResult = $db -> DB_Query($cartSql);
	$cartPrice = $cartPriceResult[0]['cs_price']; //免運門檻
	$cartCargo = $cartPriceResult[0]['cs_cargo']; //運費金額
?>

	<section class="purchase">

		<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			  <li class="breadcrumb-item active"><?=$lang_cart_cart?>
			
		</ol>
		<div class="container">
			<div class="row cartMenu">
				<div class="col-4 step">
					<li><span>1.</span> <?=$lang_cart_cart_list?></li>
				</div>
				<div class="col-4 step active">
					<li><span>2.</span> <?=$lang_cart_fill_form?></li>
				</div>
				<div class="col-4 step">
					<li><span>3.</span> <?=$lang_cart_order_complete?></li>
				</div>
				
			</div><!-- .cartMenu.row -->

			<!-- ====商品訂購明細===== -->
			<h3><?=$lang_cart_order_info2?></h3>
			<div id="cart" class="row">
				<div class="col-12 table-responsive">
					<table id="orderInfoTable" class="table table-striped">
						<thead id="thead-rwd-hide">
							<tr>
							  <th scope="col"><?=$lang_cart_item?></th>
							  <th scope="col"><?=$lang_cart_product?></th>
							  <th scope="col"><?=$lang_cart_spec2?></th>
							  <th scope="col"><?=$lang_cart_order_supply?></th>
							  <th scope="col"><?=$lang_cart_brand?></th>
							  <th scope="col"><?=$lang_cart_qty?></th>
							  <th scope="col"><?=$lang_cart_order_fee?></th>
							  <th scope="col"><?=$lang_cart_coupon?></th>
							  <th scope="col"><?=$lang_cart_sum?></th>						  
							</tr>
						</thead>
						<tbody>
						<?php
						  if(count($orderTempList)==0){ //沒有購物清單
						  ?>
						  <tr class="text-center"><td colspan="8" style="height:100px;line-height: 100px;"><?=$lang_cart_empty2?></td></tr>
						  <?php	
						  }else{
						  	  $totalPrice = 0;
								$rowcounter = 1;
								$productCountArr ; //計算combo 數量不重複(同產品不同規格的商品只會出現一次加購選項)
							  foreach ($orderTempList as $key => $value) {
  	
								$price = $value['price'];
						      	
						      	//商品費用(數量*單價)
						      	$productCharge = ((int)$price*(int)$value["quanty"]);
						      	// -----確認coupon是否可以使用----
						      	$couponDiscount = '';
						      	if($value["coupon_no"] != ''){
						      		$disPrice = ((int)$value["order_price_discount"]*(int)$value["quanty"]);
				      				$couponDiscount ="<span>-" . $lang_product_currency . " <span>".$disPrice."<span><span class='ml-1'>(".$value["coupon_name"].")</span><span>";
						      	}else{
						      		$disPrice = 0;
						      	}

						      	//計算小計(商品費用-折扣碼)
						      	$productFinalCharge = $productCharge - $disPrice;

						      	//計算總金額
						      	$totalPrice += $productFinalCharge;
						?>	 
						<tr>
					      <th scope="row" style="vertical-align: middle;"><?php echo $key+1; ?></th>
					      <td style="vertical-align: middle;">
					      	<a href="product-detail.php?product_no=<?php echo $value['product_no'];?>"><?php echo $value["product_name"]; ?></a>
					      	<!-- ===rwd==== -->
					      	<div class="order-rwd my-2"><span><?=$lang_cart_spec?> : </span><?php echo $specOrderShow[0]["product_spec_info"]; ?></div>
					      	<div class="order-rwd my-2"><span><?=$lang_cart_qty?> : </span><?php echo $value["quanty"]; ?></div>
					      	<?php if( $disPrice != 0){ ?>	
					      	<div class="order-rwd my-2"><span><?=$lang_cart_coupon?> : </span><?php echo $couponDiscount; ?></div>
					      	<?php } ?>
					      	<div class="order-rwd"><span><?=$lang_cart_sum?> : </span><?=$lang_product_currency?> <span ><?php echo $productFinalCharge; ?></div>
					      </td>
					      <td class="orderSpec"  style="vertical-align: middle;"><?php echo $value["product_spec_info"]; ?></td>
					      <td class="orderVendor" style="vertical-align: middle;"><?php echo  $value["vendor_name"]; ?><input type="hidden" name="vendor_no[]" value="<?php echo $value["vendor_no"]; ?>"></td>
					      <td class="orderBrand" style="vertical-align: middle;"><?php echo  $value["cate_name"]; ?></td>
					      <td class="orderQty Qty-rwd" style="vertical-align: middle;"><?php echo $value["quanty"]; ?></td>
					      <td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span><?php echo $productCharge; ?></span></td>
					      <td class="orderCoupon" style="vertical-align: middle;"><span><?php  echo $couponDiscount;?></td>
					      <td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span class="charge"><?php echo $productFinalCharge; ?></span></td>
					    </tr>  
						<?php $rowcounter++; ?>
						
						
						<?php
							// 加價購
							$comboProducts = get_comboProducts_by_productId($value['product_no']);
							if ($comboProducts && count($comboProducts) >= 1) {
								foreach ($comboProducts as $key2 => $value2) {
									if (in_array($value2["id"], $orderComboList) && !isset($productCountArr[$value['product_no']]['combo'])) {
										$productCountArr[$value['product_no']]['combo'] = 1; // 
										//計算總金額
										$comboQty = $comboorders[$value2["id"]]['quanty'];
										$totalPrice += (int)$value2["product_spec_price"]*(int)$comboQty ;
									?>
										<tr>
											<th scope="row" style="vertical-align: middle;"><?php echo $key+1, '<span class="badge badge-pill badge-info ml-1">加購商品</span>' ?></th>
											<td style="vertical-align: middle;">
											<a href="product-detail.php?product_no=<?php echo $value2['product_no'];?>"><?php echo $value2["product_name"]; ?></a>
											<!-- ===rwd==== -->
											<div class="order-rwd my-2"><span><?=$lang_cart_spec?> : </span><?=$value2["product_spec_info"]?></div>
											<div class="order-rwd my-2"><span><?=$lang_cart_qty?> : </span><?=$comboQty?></div>
											<?php if( $disPrice != 0){ ?>	
											<!-- <div class="order-rwd my-2"><span><?=$lang_cart_coupon?> : </span><?php echo $couponDiscount; ?></div> -->
											<?php } ?>
											<div class="order-rwd"><span><?=$lang_cart_sum?> : </span><?=$lang_product_currency?> <span ><?=(int)$value2["product_spec_price"]*(int)$comboQty?></div>
											</td>
											<td class="orderSpec"  style="vertical-align: middle;"><?=$value2["product_spec_info"]?></td>
											<td class="orderVendor" style="vertical-align: middle;"><?=$value2["parent_name"]?><input type="hidden" name="vendor_no[]" value="<?php echo $value2["vendor_no"]; ?>"></td>
											<td class="orderBrand" style="vertical-align: middle;"><?=$value2["cate_name"]?></td>
											<td class="orderQty Qty-rwd" style="vertical-align: middle;"><?=$comboQty?></td>
											<td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span><?=(int)$value2["product_spec_price"]*(int)$comboQty?></span></td>
											<td class="orderCoupon" style="vertical-align: middle;"><span></td>
											<td class="orderCharge" style="vertical-align: middle;"><?=$lang_product_currency?> <span class="charge"><?=(int)$value2["product_spec_price"]*(int)$comboQty?></span></td>
										</tr>
									<?php
									}
									$rowcounter++;
								}
							}
						?>
						
						<?php
						} //end foreach loop 
						}//--- end if 判斷是否有購物清單
						// =====全館免運======
						if($totalPrice >= (int)$cartPrice ){ //免運 //全館免運$totalPrice >= 0
							$cargoPrice = $lang_cart_notransfee;
							$cargoInfo = 0;
							$cargoPriceText = $lang_cart_notransfee;
						}else{ //有運費
							$cargoInfo = 1;
							$cargoPrice = $cartCargo;
							$totalPrice += $cartCargo ;
							$cargoPriceText = $lang_product_currency.$cargoPrice;
						}
						$totalPriceText = $lang_product_currency.$totalPrice;
					  ?>
						<tr><td colspan="9"></td></tr>
						
						</tbody>
					</table> 
					<div class="row">
						<div class="col-12 col-md-8 leftBlock">
						<img src="image/frontend/320x100.jpg"   width="100%" style="max-width: 320px;" />
					</div>
						<div class="col-12 col-md-4 rightBlock">
							<?=$lang_cart_transfee?> :<span><?=$cargoPriceText?> </span>
							<input type="hidden" name="cargo" value="<?=$cargoInfo?>"></span><br/>
							<?=$lang_cart_total?> :<span><?=$totalPriceText?></span>
							<span id="cargoPrice" style="display: none"><?=$cargoPrice?></span>
							<span id="TotalPrice" style="display: none"><?=$totalPrice?></span>
						</div>
					</div> 		 <!-- row -->			
				</div>
			</div>

			<!-- ====end 商品訂購明細===== -->

			<?php if(isset($_SESSION['memNo']) === false){ ?>

			<div class="checkMember col-12">
				<div class="row">
					<div class="col-12 text-center p-2" style="background-color: rgba(255,200,207,1);">
						<img src="image/frontend/EC-logo-white.png" style="width:80px">
					</div>
					<div class="col-sm-6 col-12 firstShop">
						<div id="firstShopTitle" class="text-center">
							<?=$lang_cart_first_buy?>
						</div>
						<div class="firstShopInfo text-center">
							<?=$lang_cart_first_join?>
						</div>
						<div class="text-center">	
							<button id="firstShopBtn" type="button" class="btn btn-outline-success btn-sm"><?=$lang_cart_first_go?></button>
						</div>
					</div>
					
					<div class="col-sm-6 col-12 loginPanel">
						<div id="loginTitle" class="text-center mb-2"><?=$lang_member_login2?></div>
						 <div class="form-group row">
						    <label for="LoginEmail" class="col-lg-3 col-form-label col-form-label-sm"><?=$lang_member_login2?></label>
						    <div class="col-lg-9">
						      <input type="email" class="form-control form-control-sm" id="LoginEmail" placeholder="<?=$lang_member_account_hint?>">
						    </div>
						  </div> 
						  <div class="form-group row">
						    <label for="LoginPassword" class="col-lg-3 col-form-label col-form-label-sm"><?=$lang_member_password2?></label>
						    <div class="col-lg-9">
						      <input type="password" class="form-control form-control-sm" id="LoginPassword" placeholder="<?=$lang_member_password_hint2?>">
						        <a href="#" class="forgetPass" id="forgot-pass-shop"><?=$lang_member_forgot2?></a>
						    </div>
						  
						  </div>
						  <div class="text-center">
						  	<button id="shopLoginBtn" type="button" class="btn btn-outline-success btn-sm"><?=$lang_member_login2?></button>
						  	<!-- <div class="my-2 col-12" style="color:#ccc;">or</div> 
						  	<button id="shopFBLoginBtn" type="button" class="btn btn-outline-success btn-sm">FB帳號登入</button> -->
						  </div>

					</div><!-- loginPanel -->
				</div>
				
			</div>
			<?php }?>
				
			<div class="infoBox" style="<?php if(isset($_SESSION['memNo']) === false){ echo "display:none;"; } ?>">
	
			<h3><?=$lang_cart_order_info2?></h3>
			<div id="info" class="row" >	
				
				<div class="col-lg-7 col">
						<from>
						 <div class="form-group row">
						    <label for="memberEmail" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_cart_account?></label>
						    <div class="col-sm-9">
						      <input type="email" class="form-control form-control-sm" id="memberEmail" placeholder="<?=$lang_member_account_hint?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_mail']; } ?>">
					      <?php if(isset($_SESSION['memNo'])){ ?>
						      <input type="hidden" id="memberNo" name="mem_no" value="<?php echo $_SESSION['memNo']; ?>">	
					      <?php } ?>
						    </div>
						  </div>
						  <div class="form-group row" style="<?php if(isset($_SESSION['memNo'])){ echo "display:none;"; } ?>">
						    <label for="memberPassword" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_password2?></label>
						    <div class="col-sm-9">
						      <input type="password" class="form-control form-control-sm" id="memberPassword" placeholder="<?=$lang_member_newpassword_hint?>">
						    </div>
						  </div>
						  <div class="form-group row" style="<?php if(isset($_SESSION['memNo'])){ echo "display:none;"; } ?>">
						    <label for="memberPasswordAgain" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_repassword?></label>
						    <div class="col-sm-9">
						      <input type="password" class="form-control form-control-sm" id="memberPasswordAgain" placeholder="<?=$lang_member_repassword_hint?>">
						      <small class="form_note"><?=$lang_cart_first_join_hint?></small>
						    </div>
						    
						    
						  </div>
						  <!-- <div class="form-group row">
						    <label for="memberName" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_receiver_fullname?></label>
						    <div class="col-sm-9">
						      <input type="test" class="form-control form-control-sm" id="memberName" placeholder="<?=$lang_member_receiver_fullname?>" value="<?php //if(isset($_SESSION['memNo'])){ echo $_SESSION['memLastName'].$_SESSION['memFirstName']; } ?>">
						    </div>
						  </div> -->
						  <div class="form-group row">
						    <label  class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_receiver_fullname?></label>
						    <div class="col-sm-4">
						      <input type="text" class="form-control form-control-sm" id="memberLastName" placeholder="<?=$lang_member_receiver_lastname?>" value="<?php if(isset($_SESSION['memNo'])){ echo $_SESSION['memLastName']; } ?>">
						    </div>
						     <div class="col-sm-5">
						      <input type="text" class="form-control form-control-sm" id="memberFirstName" placeholder="<?=$lang_member_receiver_fullname?>" value="<?php if(isset($_SESSION['memNo'])){ echo $_SESSION['memFirstName']; } ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="memberPhone" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_phone2?></label>
						    <div class="col-sm-9">
						      <input type="tel" class="form-control form-control-sm" id="memberPhone" placeholder="<?=$lang_member_phone2?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_tel']; } ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="memberAddress" class="col-sm-3 col-form-label col-form-label-sm"><?=$lang_member_address2?></label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control form-control-sm" id="memberAddress" placeholder="<?=$lang_member_address2?>" value="<?php if(isset($_SESSION['memNo'])){ echo $memResult[0]['mem_address']; } ?>">
						    </div>
						  </div>
						  <div class="form-group row">
							  <div class="col-12">
							  <label for="purchaseMemo" class="col-form-label col-form-label-sm"><?=$lang_cart_memo?></label>
							  <textarea class="form-control form-control-sm" id="purchaseMemo" rows="3" placeholder="<?=$lang_cart_memo_placholder?>"></textarea>
							  </div>
							   
						  </div>
					 	 </from>
				
					
				</div><!-- .col-sm-7 -->
				
				
				<!-- <a class="btn btn-outline-secondary btn-sm submitBtn" href="payment.php" role="button">填寫付款資訊</a> -->
			</div>	<!-- .info-->

				
			<h3><?=$lang_cart_paymentinfo2?></h3>
			
			<div id="payment" class="row">
				<div class="col-12">
					<?=$lang_cart_choose_payby?>：
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway1" value="0" checked> <?=$lang_cart_choose_payby_cc?>
					  </label>
					</div>
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway3" value="3"> <?=$lang_cart_choose_payby_cc_installment?>
					  </label>
					</div>
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway4" value="4"> <?=$lang_cart_choose_payby_atm?>
					  </label>
					</div>
					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="payway" id="payway2" value="2"> <?=$lang_cart_choose_payby_store?>
					  </label>
					</div>
				</div><!-- .col-12 -->
			
				<div class="col-lg-7 col installInfo" style="display: none;">
					<div class="form-group row">
						<label for="orderUniNo" class="col-lg-2 col-form-label col-form-label-sm"><?=$lang_cart_choose_payby_cc_installment_choose?></label>
						<div class="col-lg-10">
							<select class="form-control form-control-sm" name="payinstallment" id="orderIstallment">
								<option value="3"><?=$lang_cart_choose_payby_cc_installment_3?> $<?php echo (int)($totalPrice / 3); ?></option>
								<?php if ($totalPrice >= 3000) { ?><option value="6" selected><?=$lang_cart_choose_payby_cc_installment_6?> $<?php echo (int)($totalPrice / 6); ?></option><?php } ?>
								<?php if ($totalPrice >= 10000) { ?><option value="12" selected><?=$lang_cart_choose_payby_cc_installment_12?> $<?php echo (int)($totalPrice / 12); ?></option><?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-2 col-form-label col-form-label-sm"><?=$lang_cart_choose_payby_cc_installment_title?></label>
						<div class="col-lg-10">
							<div class="bankslist"><?=$lang_cart_choose_payby_cc_installment_banks?></div>
							<span><?=$lang_product_install_note?></span>
						</div>
					</div>
				</div>
			</div>	<!-- .row -->
			
			
			<div class="row">
				<div class="col-12 ">
					<?=$lang_cart_choose_invoice?>：

					<div class="form-check form-check-inline">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="invoiceSelect" id="person_invoice" value="0" checked> <?=$lang_cart_invoice_einvoice?>
					  </label>
					</div>
					<div class="form-check form-check-inline" style="/*display: none;*/">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="invoiceSelect" id="company_invoice" value="1"> <?=$lang_cart_invoice_cominvoice?>
					  </label>
					</div>
					
				</div><!-- .col-12 -->
					<div class="col-lg-7 col invoiceInfo">
						<div class="form-group row companyInvoice" style="display: none;">
							<label for="orderUniNo" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_cominvoice_no?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderUniNo" placeholder="" value="" maxlength="8" />
						    </div>
						</div>
						<div class="form-group row companyInvoice" style="display: none;">
							<label for="orderInvoiceTitle" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_cominvoice_title?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderInvoiceTitle" placeholder="" value="" maxlength="60" />
						    </div>
						</div>
						<div class="form-group row invoice">
							<label for="orderInvoiceAddress" class="col-12 col-form-label col-form-label-sm"><?=$lang_cart_invoice_address?>:</label>
						    <div class="col-12">
						      <input type="text" class="form-control form-control-sm" id="orderInvoiceAddress" placeholder="" value="" maxlength="200" />
						    </div>
						    <div class="col-12" style="padding-left: 35px;padding-top: 5px;">
						    	<input class="form-check-input" type="checkbox" id="invoiceCheck">
					       		<label class="form-check-label" for="invoiceCheck"><?=$lang_cart_invoice_address_same?></label>
						    </div>	
						</div>
					</div>


				<div class="form-check col-12">
			        <input class="form-check-input" type="checkbox" id="termCheck">
			        <label class="form-check-label" for="termCheck"><?=$lang_cart_agree?><a target="_blank" href="terms.php"><?=$lang_footer_terms_2?></a><?=$lang_and?><a target="_blank" href="privacy.php"><?=$lang_footer_privacy?></a>
			        </label>
			    </div>
				<a id="checkoutBtn" class="btn btn-outline-secondary btn-sm submitBtn" href="deliver.php" role="button"><?=$lang_cart_confirm_buy?></a>
				<form id="checkoutForm" action="payment-ecpay.php" method="post" enctype="application/x-www-form-urlencoded">
					<input type="hidden" name="pay" value="">
					<input type="hidden" name="pay_install" value="">
					<input type="hidden" name="total_price" value="">
					<input type="hidden" name="order_no" value="">
					<input type="hidden" name="order_member_name" value="">
					<input type="hidden" name="order_member_tel" value="">
					<input type="hidden" name="order_member_email" value="">
					<input type="hidden" name="order_member_address" value="">
					<input type="hidden" name="order_invoice_print" value="">
					<input type="hidden" name="order_invoice_no" value="">
					<input type="hidden" name="order_invoice_title" value="">
				</form>
			
			</div><!-- .row-->
			
			</div><!-- 	.infoBox -->
			
		</div><!-- .container -->
	</section>			

<?php require_once("module/footer.php"); ?>