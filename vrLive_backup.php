<?php require_once("module/header.php"); 
include("class/categoryClass.php");
include("class/productClass.php");


$cateID =  49; //指抓取 VR專區
$cate = new category($cateID);
$result = $cate->getProductsByCateId($cateID,$without=null ,$productStatus=1 , $page=null , $isPagenation=null , $recPerPage=null);
$productCount = count($result[0]["products"]) ;

?>


	<section class="vrlive marginTop100">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
			<li class="breadcrumb-item active"><?=$lang_menu_vrLive?></li>	 
        </ol>
        
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="vrlive-area">
						<iframe width="100%" height="100%" src="https://livetour.istaging.com/e8aa56c2-1c95-43e7-9fdc-c3ffacd3952a/?ui=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>	
			
		<div class="container mt-3">
            <?php  
            foreach ($result as $key => $value) {
            ?> 

			<div class="row">
            <?php  
                for ($i=0; $i < $productCount; $i++) { 
                    $priceTop = "NT";
                    $priceLow = "NT";	
                    if( trim($value["products"][$i]["lowPrice"]) == null ){
                        $priceLow .= $value["products"][$i]["highPrice"];
                        $priceTop = "";
                    }else{
                        $priceLow .= $value["products"][$i]["lowPrice"];
                        $priceTop .= $value["products"][$i]["highPrice"];
                    }		
            ?>	

				<div class="col-6 col-md-4 col-lg-2">
					<div class="row">
						<div class="product-items">
                              <form action="product-detail.php" method="get">
						  		<div class="content-hover">
							  		<img src="<?php echo $value['products'][$i]['product_img'][0]; ?>" class="img-fluid">
								  	<div class="block">
						  				<div class="info text-center d-flex align-items-center justify-content-center">
						  					<div class="product-name" style="font-size: 14px;"><?php echo $value["products"][$i]["product_name"]; ?></div>
						  				</div>
						  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
						  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
						  				</div>
						  			</div>
						  			<div class="price text-center">
										<!-- PRICE只抓第一筆資料 -->
										<div class="price-new mr-1" style="font-size: 16px; color: red;"><?php echo $priceLow; ?></div><div class="price-old" style="text-decoration: line-through; color: rgb(0, 0, 0);"><?php echo $priceTop; ?></div>		
					  				</div>
				  				</div>
					  			<input type="hidden" name="product_no" value="<?php echo $value["products"][$i]["product_no"]; ?>">
							  </form>
						  </div>	
					</div>	
				</div>

                <?php 
                    } //--end for
                } // --end foreach
                ?>


			</div>
		</div>
	</section>
	
  
<?php require_once("module/footer.php"); ?>