<?php  
include("../model/DB.php");
date_default_timezone_set("Asia/Taipei");
$db = new DB();
$result = $db->DB_Query("select * from member order by mem_no asc");
$arr = [] ;
foreach ($result as $key => $value) {
	$memDB=array(
		"mem_no"=>$value["mem_no"],
		"mem_mail" =>$value["mem_mail"],
		"mem_name"=>$value["mem_lastname"].$value["mem_firstname"],
		"mem_tel" =>$value["mem_tel"],
		"mem_status" =>$value["mem_status"], 
		"mem_createtime" =>date("Y-m-d H:i:s",$value["mem_createtime"]), 
		"mem_lastLoginTime" =>date("Y-m-d H:i:s",$value["mem_lastLoginTime"])
	);
	array_push($arr,$memDB);
}
// $array = array("data"=>$result);
$array = array("data"=>$arr);
// echo "<pre>";
// print_r($array);
// echo "</pre>";
$jsonStr = json_encode($array);
echo $jsonStr;
// print_r($array);
?>