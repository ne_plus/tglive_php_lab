<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/tag.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/tag.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/tag.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/tag.php");
}else{ //前端
	include("model/tag.php");
}




function query($sql,$data){
	$tag = new TAG($sql,$data);
	$result = $tag->tagQuery();
	if(!$result){
		return false ; 
	}else{
		return $result;
	}
}

function addTagInCate($table,$data){ //加入標籤到分類裡
	$db = NEW DB();
	return $db -> DB_Insert($table,$data);
}

function removeTagInCate($table,$cateTagNo){ //移除分類裡的標籤
	$db = NEW DB();
	$sql="delete from ".$table." where cate_tag_no= '".$cateTagNo."'" ;
	return $db->exec($sql);
}

function cateTagRelateQuery($cateNo){ //標籤分類搜尋現有的分類沒有包含的標籤
	
	$db = NEW DB();

	//已經存在的標籤
	$sql = "SELECT * FROM tag a  left join category_tag_relate b on a.tag_no=b.tag_no where b.cate_no = :cate_no";
	$dic = array(":cate_no" => $cateNo);
	$cateUseTag = $db->DB_query($sql,$dic);
	if(!$cateUseTag){ 
		$tagFilter = false;
	}else{
		$tagUse = [];
		foreach ($cateUseTag as $key => $value) {
			array_push($tagUse,"tag_no != ".$value["tag_no"]);
		}
		$tagFilter =  implode(" and ",$tagUse);
		// return $tagUse;
	}

	//搜尋所有標籤
	if( $tagFilter == false){ //
		$sqlfinal ="select * from tag" ;
	}else{
		$sqlfinal =sprintf("select * from tag where %s",$tagFilter);;
	}
	$result = $db->DB_query($sqlfinal,$dic);
	return $result;
	
}

function cateTagRelateHaveQuery($cateNo){ //標籤分類搜尋現有分類中所包含的標籤
	$db = NEW DB();
	$sql = "SELECT * FROM tag a  left join category_tag_relate b on a.tag_no=b.tag_no where b.cate_no = :cate_no";
	$dic = array(":cate_no" => $cateNo);
	$cateUseTag = $db->DB_query($sql,$dic);
	// if(!$cateUseTag){ 
	// 	return false;
	// }else{
	// 	return $cateUseTag;
	// }
	return $cateUseTag;
}

function addTag($table,$tagName,$item){
	$sql = "select * from ".$table." where tag_name=:tag_name";
	$data = array(":tag_name"=>$tagName);
	$tag = new TAG($sql,$data);
	$checkColumn = array("tag_name");
	if(!$tag->tagCreate($table,$item,$checkColumn)){
		return false ;//"標籤已經存在" ; 		
	}else{
		return true ; //"更新成功";
	}

}

function addTagProduct($table,$tagName,$item){
	$sql = "select * from ".$table." where tag_name=:tag_name";
	$data = array(":tag_name"=>$tagName);
	$tag = new TAG($sql,$data);
	$checkColumn = array("tag_name");
	if(!$tag->tagCreate($table,$item,$checkColumn)){
		return false ;//"標籤已經存在" ; 		
	}else{
		return $tag->lastId ; //"更新成功回傳tag_no";
	}

}

function editTag($table,$tagNo,$item,$tagName){
	$sql = "select * from ".$table." where tag_no=:tag_no";
	$data = array(":tag_no"=>$tagNo);
	$tag = new TAG($sql,$data);
	$checkColumn = array("tag_no");
	// --判斷名字是否重複-----
	$sql2 = "select * from ".$table." where tag_name=:tag_name";
	$db = new DB();
	$dic = array(
		":tag_name" => $tagName
		);
	if( $db->DB_Query($sql2,$dic)){ //有
		return "有相同標籤";
	}else{//無
		if(!$tag->tagUpdate($table,$item,$checkColumn)){
			return false ;//"找不到標籤" ; 		
		}else{
			return true ; //"更新成功";
		}
	}
	
	
}

function deleteTag($table,$tagNo){
	$sql = "select * from ".$table." where tag_no=:tag_no";
	$data = array(":tag_no"=>$tagNo);
	$tag = new TAG($sql,$data);

	$sql="delete from ".$table." where tag_no= '".$tagNo."'" ;
	if(!$tag->delete($sql)){
		return false ; //找不到要刪除的標籤
	}else{
		return true ;
	}
}


// -------tag找尋所有商品-----
function getProductByTag($tagNo){ //$tagNO 陣列
		$tagBind = [];
		foreach ($tagNo as $tagkey => $tagvalue) {
			array_push($tagBind, "tag_no = ".$tagvalue);
		}
		$str = implode(" or ",$tagBind);
		$sql = sprintf("select DISTINCT a.product_no from tag_products_relate a join product b on a.product_no = b.product_no where b.product_status=1 and (%s) ORDER BY a.product_no DESC",$str);
		$db = new DB();
		$tagRelate = $db ->DB_Query($sql);
		return $tagRelate;
	// };
	// return $sql;
}
// $tagNo = array("1","3");

// echo "<pre>";
// var_dump(getProductByTag($tagNo));
// echo "</pre>";
?>