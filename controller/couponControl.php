<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/coupon.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/coupon.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/coupon.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/coupon.php");
}else{ //前端
	include("model/coupon.php");
}


date_default_timezone_set("Asia/Taipei");


function query($sql,$data){
	$coupon = new Coupon($sql,$data);
	$result = $coupon->couponQuery();
	if(!$result){
		return false ; 
	}else{
		return $result;
	}
}

function addCoupon($table,$couponName,$item){
	$sql = "select * from ".$table." where coupon_name=:coupon_name";
	$data = array(":coupon_name"=>$couponName);
	$coupon = new Coupon($sql,$data);
	$checkColumn = array("coupon_name");
	if(!$coupon->couponCreate($table,$item,$checkColumn)){
		return false ;//"標籤已經存在" ; 		
	}else{
		return true ; //"更新成功";
	}

}

function editCoupon($table,$couponNo,$item,$couponName){
	$sql = "select * from ".$table." where coupon_no=:coupon_no";
	$data = array(":coupon_no"=>$couponNo);
	$coupon = new Coupon($sql,$data);
	$checkColumn = array("coupon_no");
	if($coupon->couponQuery()["coupon_name"] == $couponName){//僅需要修改優惠其他資訊
		$result = $coupon->couponUpdate($table,$item,$checkColumn);
		if($result){
			return true ; //更新成功
		}else{
			return false ;//"找不到優惠劵" ; 
		}
	}else{
		// --判斷名字是否重複-----
		$sql2 = "select * from ".$table." where coupon_name=:coupon_name";
		$db = new DB();
		$dic = array(
			":coupon_name" => $couponName
			);
		if( $db->DB_Query($sql2,$dic)){ //有
			return "有相同優惠劵";
		}else{//無
			if(!$coupon->couponUpdate($table,$item,$checkColumn)){
				return false ;//"找不到優惠劵" ; 		
			}else{
				return true ; //"更新成功";
			}
		}
	}
		
}

function cateDelete($table,$cateNo,$item){
	$db = New DB();
	$checkColumn = array("coupon_used");
	$dateBind =[];
	foreach ($item as $key => $value) {
    	array_push($dateBind, $key." = :".$key);	    	
		}
	$sql = sprintf("update %s set %s where coupon_used = %s ",$table,implode(',',$dateBind),$cateNo);	
	$object = $db->pdo->prepare($sql);
	foreach ($item as $key => $value) {
    		$object->bindValue(":".$key,$value);
    	}
	// return $db->DB_UpdateOnly($table,$item,$checkColumn);
	$result = $object->execute();
	return $result;
}


function deleteCoupon($table,$couponNo){
	$sql = "select * from ".$table." where coupon_no=:coupon_no";
	$data = array(":coupon_no"=>$couponNo);
	$coupon = new Coupon($sql,$data);

	$sql="delete from ".$table." where coupon_no= '".$couponNo."'" ;
	if(!$coupon->delete($sql)){
		return false ; //找不到要刪除的優惠劵
	}else{
		return true ;
	}
}







// // ===查詢---
// $sql = "select * from coupon";
// $data = null;

// var_dump(query($sql,$data));


// // ===新增---
// $table = "coupon";
// $couponName = "我是新的優惠卷";
// $item = array(
// 	"coupon_name"=> $couponName,
// 	"coupon_code"=> "abcde12345",
// 	"coupon_discount"=>"20",
// 	"coupon_start"=> strtotime("2017-11-17T22:01"),
// 	"coupon_end"=>strtotime("2017-11-20T17:00"),
// 	"coupon_used"=>70,
// 	"coupon_target"=>0,
// 	"coupon_status"=>0,
// 	"coupon_times"=>100
// 	);

// // $str = strtotime("2017-11-18T22:01");
// // echo $str,"<br>";
// // echo date("Y-m-dTH:i:s", $str);
// var_dump(addCoupon($table,$couponName,$item));


?>