<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/user.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/user.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/user.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/user.php");
}else{ //前端
	include("model/user.php");
}


function get_comboProducts_by_productId($pid){
	$db = new DB();
	$sql = sprintf("SELECT c.*, p.model_id, p.product_name, p.img1, s.product_spec_info FROM product_combo c INNER JOIN product p ON c.product_no = p.product_no INNER JOIN product_spec s ON c.product_spec_no = s.product_spec_no WHERE c.rel_product_no= %d AND NOW() BETWEEN c.starttime AND c.endtime AND c.product_combo_status = 1 AND c.isdeleted = 0 AND p.product_status = 1 AND s.product_stock >= 1 AND s.product_spec_status = 1", $pid);
	$result = $db->DB_Query($sql);
	foreach ($result as $key => $value) {
		// 子類別
		$sql = sprintf("SELECT c1.cate_name, c2.cate_name parent_name FROM category c1 LEFT OUTER JOIN category c2 ON c1.cate_parents = c2.cate_no where c1.cate_no in (SELECT cate_no FROM category_products_relate where product_no = %d) AND c1.cate_level = 2", $pid);
		$result2 = $db->DB_Query($sql);
		if ($result2 && count($result2) >= 1) {
			$result[$key]["cate_name"] = $result2[0]["cate_name"];
			$result[$key]["parent_name"] = $result2[0]["parent_name"];
		}
	}
	return $result;
}

function query($table,$email,$psw){  //查詢   傳入$table 帳號 密碼
	if($table == "admin" ){		
			$data = array(
				":adm_email" => $email,
				":adm_password" => $psw
				);	
			$sql = "select * from ".$table." where adm_email=:adm_email and adm_password=:adm_password";

			$user = new USER($sql,$data,$table);
			$result = $user->isMember();
			if($result){
				return $result;
			}else{
				$result =false;
				return $result; //"查無此帳號"
			}			
		}elseif($table == "member"){
			$data = array(
				":mem_mail" => $email,
				// ":mem_vertifycode" => $mem_vertifycode
				":mem_password"	=> $psw			
				);	
			$sql = "select * from ".$table." where mem_mail=:mem_mail and mem_password=:mem_password ";

			$user = new USER($sql,$data,$table);
			$result = $user->isMember();
			if($result){
				return $result;
			}else{
				return false; //"查無此帳號"
			}	
		}
}
function checkMemMail($table,$email){
	$table = "member";
	$data = array(":mem_mail" => $email);
	$sql = "select * from ".$table." where mem_mail=:mem_mail";
	$user = new USER($sql,$data,$table);
	$result = $user->isMember();
	if($result){
		return $result; //有重複帳號
	}else{
		return false; //"查無此帳號"
	}	
}

function create($table,$mail,$item){ //新增   $item為傳入的新增欄位
	if($table == "admin"){ //<======新管理者
			$data = array(":adm_email" => $mail);	
			$sql = "select * from ".$table." where adm_email=:adm_email";
			$user = new USER($sql,$data,$table); //帳號重複性檢查
			// $column = array(
			// 	"adm_authLevel"=> $item["adm_authLevel"],
			// 	"adm_email"=> $item["adm_email"],
			// 	"adm_password"=> $item["adm_password"],
			// 	"adm_name"=> $item["adm_name"],
			// 	"adm_status"=> $item["adm_status"],
			// 	"adm_createtime"=> time(),
			// 	"adm_updatetime"=> null,
			// 	"adm_lastLoginTime"=> time(),
			// 	"adm_loginIP"=> $item["adm_loginIP"]
			// 	);
			$checkColumn = array("adm_email");
			if(!$user->adduser($table,$item,$checkColumn)){
				return false ;//echo "帳號已經存在";
			}else{
				return true ; //echo "帳號已新增";
			}; 
		}elseif($table == "member"){ //<======新會員
			$data = array(":mem_mail" => $mail);	
			$sql = "select * from ".$table." where mem_mail=:mem_mail";
			$user = new USER($sql,$data,$table); //帳號重複性檢查
			// $column = array(
			// 	"mem_mail"=> $item["mem_mail"],
			// 	"mem_password"=> $item["mem_password"],
			// 	"mem_firstname"=> $item["mem_firstname"],
			// 	"mem_lastname"=> $item["mem_lastname"],
			// 	"mem_tel"=> $item["mem_tel"],
			// 	"mem_address"=> $item["mem_address"],
			// 	"mem_status"=> $item["mem_status"],
			// 	"mem_createtime"=> time(),
			// 	"mem_updatetime"=> null,
			// 	"mem_lastLoginTime"=> time(),
			// 	"mem_loginIP"=> $item["mem_loginIP"]
			// 	);
			$checkColumn = array("mem_mail");
			if(!$user->adduser($table,$item,$checkColumn)){
				echo "帳號已經存在";
			}else{

				//驗證碼-----------------------------------------
				// if($user->getMemberVertityCode($mail)){  
				// //得到vertifycode後回傳vertifyCode
				// 	$vertifyCode = $user->getMemberVertityCode($mail);
				// 	$column = array(
				// 		"mem_mail" => $mail,
				// 		"mem_vertifycode" => $vertifyCode
				// 		);
				// 	$checkColumn=array("mem_mail");
				// 	if($user->edituser($table,$column,$checkColumn)){
				// 		//vertifycode 寫入資料庫  
						
				// 		//寄送mail
				// 		// include("../model/mail.php");
				// 		if(strpos(strtolower($_SERVER["PHP_SELF"]),"/admin/framwork")){ //admin/framwork 引用
				// 			include("../../../model/mail.php");
				// 		}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"/controller/")){ //controller 引用
				// 			include("../model/mail.php");
				// 		}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"/view/admin/")){ //admin 直接引用
				// 			include("../../model/mail.php");
				// 		}

				// 		$mailler = new MAIL();
				// 		$title = "新會員通知";
				// 		$content = "<a href='http://google.com'>{$vertifyCode}</a>";
				// 		if($mailler->sendMail($title,$content,$item["mem_mail"],$item["mem_lastname"])){
				// 			echo "信件寄送完成";
				// 		}else{
				// 			echo "信件寄送失敗";
				// 		}
				// 	}else{
				// 		echo "失敗" ;
				// 	}
				

				// }else{ // vertifyCode 取得失敗
				// 	echo "ng";
				// } 

				//驗證碼-----------------------------------------end
				$result = $user->getNewMemberLastInsertId();
				$feedback = [];
				$feedback["mem_id"] = $result;
				// //寄送mail
				// if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
				// 	include("../../model/mail.php");
				// }elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
				// 	include("../model/mail.php");
				// }elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
				// 	include("../model/mail.php");
				// }elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
				// 	include("../model/mail.php");
				// }else{ //前端
				// 	include("model/mail.php");
				// }

				// $mailler = new MAIL();
				// $title = "新會員通知";
				// $content = "恭喜成為TGI Live會員";
				// if($mailler->sendMail($title,$content,$item["mem_mail"],$item["mem_lastname"])){
				// 	$feedback["mail_status"] = "成功";
				// 	// echo "信件寄送完成";
				// }else{
				// 	$feedback["mail_status"] = "失敗";
				// 	// echo "信件寄送失敗";
				// }

				
				return $feedback;

			}; 
		}
}

function edit($table,$mail,$item){ //修改
	if($table == "admin"){ 
			$data = array(":adm_email" => $mail);	
			$sql = "select * from ".$table." where adm_email=:adm_email";
			$user = new USER($sql,$data,$table); //帳號重複性檢查
			// $column = array(
			// 	"adm_authLevel"=> $item["adm_authLevel"],
			// 	"adm_email"=> $item["adm_email"],
			// 	"adm_password"=> $item["adm_password"],
			// 	"adm_name"=> $item["adm_name"],
			// 	"adm_status"=> $item["adm_status"],
			// 	"adm_createtime"=> time(),
			// 	"adm_updatetime"=> null,
			// 	"adm_lastLoginTime"=> time(),
			// 	"adm_loginIP"=> $item["adm_loginIP"]
			// 	);
			$checkColumn = array("adm_email");
			if(!$user->edituser($table,$item,$checkColumn)){
				return false ; //echo "更新失敗";
			}else{
				return true ; //echo "已更新資訊";
			}; 
		}elseif($table == "member"){
			$data = array(":mem_mail" => $mail);	
			$sql = "select * from ".$table." where mem_mail=:mem_mail";
			$user = new USER($sql,$data,$table); //帳號重複性檢查
			// $column = array(
			// 	"mem_mail"=> $item["mem_mail"],
			// 	"mem_password"=> $item["mem_password"],
			// 	"mem_firstname"=> $item["mem_firstname"],
			// 	"mem_lastname"=> $item["mem_lastname"],
			// 	"mem_tel"=> $item["mem_tel"],
			// 	"mem_address"=> $item["mem_address"],
			// 	"mem_status"=> $item["mem_status"],
			// 	"mem_createtime"=> time(),
			// 	"mem_updatetime"=> null,
			// 	"mem_lastLoginTime"=> time(),
			// 	"mem_loginIP"=> $item["mem_loginIP"]
			// 	);
			$checkColumn = array("mem_mail");
			if(!$user->edituser($table,$item,$checkColumn)){
				return false ;//echo "更新失敗";
			}else{
				return true ;//echo "已更新資訊";
			}; 
		}
}

function sendMail($subject,$content,$mail,$userName){
	$feedback = [];
	//寄送mail
	if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
		include("../../model/mail.php");
	}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
		include("../model/mail.php");
	}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
		include("../model/mail.php");
	}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
		include("../model/mail.php");
	}else{ //前端
		include("model/mail.php");
	}

    // =====php mailer======
	// $mailler = new MAIL();
	// // $title = $title;// "新會員通知"
	// // $content = $content;//"恭喜成為TGI Live會員"
	// if($mailler->sendMail($title,$content,$mail,$userName)){
	// 	$feedback["mail_status"] = "成功";
	// 	// echo "信件寄送完成";
	// }else{
	// 	$feedback["mail_status"] = "失敗";
	// 	// echo "信件寄送失敗";
	// }


	// =====sendgrid=====

	return sendgrid($subject, $content, $mail);



	// return $feedback;
}


?>