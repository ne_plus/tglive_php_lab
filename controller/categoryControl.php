<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/category.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/category.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/category.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/category.php");
}else{ //前端
	include("model/category.php");
}


// ------收尋所有的供應商------
function cateSearchFatherAll(){ //收尋所有的父層
	$sql ="select * from category where cate_parents = '62' and cate_level='1' order by cate_sort asc";
	$Cate = new Category($sql);
	$result = $Cate->cateSelectAll($sql);
	// echo "<pre>";
	$father = [];
	foreach ($result as $key => $value) {
		array_push($father,$value)  ;
	}
	return $father ;
	// echo "</pre>";
} 

function cateInfo($cateNo){
	$sql ="select * from category where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic);
	$result = $Cate->cateInfo();
	if($result){
		return $result ;
	}else{
		return false ; //沒有分類
	}
}

function cateSearch($cateNo){ //收尋子層(輸入父親名找到子層 or 輸入子層名找到自己和兄弟)
	$sql ="select * from category where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic); //
	$result = $Cate->cateChildQuery();
	// echo "<pre>";
	// print_r($result);
	// echo "</pre>";
	if($result){
		return $result;
	}else{
		return false ; //沒有子層
	}
	
}

function cateFatherSearch($cateNo){ // 子層找父親(輸入子層名字照到父層的資訊 or 父層找到自己的資訊)
	$sql ="select * from category where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic); //
	$result = $Cate->cateFatherQuery();
	echo "<pre>";
	print_r($result);
	echo "</pre>";
	// if($result){
	// 	foreach ($result as $key => $value) {
	// 		 $value["cate_name"];
	// 	}
	// };
}

// -------categoryAjax "新增"執行方式一(會先偵測cate_name 是否重複)----------
function cateCreate($cateName,$table,$item){
	$sql = "select * from category where cate_name=:cate_name";
	$dic = array(":cate_name"=>$cateName);
	$Cate = new Category($sql,$dic);
		// =====找子層排序======
	if($item["cate_parents"]!=0){
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	}else{ //父層
		$item["cate_sort"]=0;
	}
	
		// =====新增分類=====
	$result = $Cate->cateCreate($table,$item);
	if($result){
		return $Cate->lastId;//true ; // echo "新增標籤";
	}else{
		return false ; //echo "有存在帳號";
	}
}

// -------categoryAjax "新增"執行方式二----------
function cateCreate2($cateName,$table,$item){
	$sql = "select * from category";
	$Cate = new Category($sql,null);
	if($item["cate_parents"]==63 || $item["cate_parents"]==61){ //行銷活動使用以及標籤分類
		// $item["cate_sort"]=0;
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
		// =====找子層排序======//供應商以及品牌
	}elseif($item["cate_parents"]!=62){
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	}else{ //父層
		// $item["cate_sort"]=0; //父親不做cate_sort排序
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	}
	
		// =====新增分類=====
	$result = $Cate->cateCreate2($table,$item);
	if($result){
		return $Cate->lastId;//true ; // echo "新增標籤";
	}else{
		return false ; //echo "有存在帳號";
	}
}



function updateCate($cateNo,$table,$item,$cateName){
	$sql ="select * from ".$table." where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic); 
	$checkColumn = array("cate_no");
	if($item["cate_parents"]==63){ //行銷活動使用
		// $item["cate_sort"]=0;
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	// =====找子層排序======
	}elseif($item["cate_parents"]!=62){
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	}else{ //父層
		// $item["cate_sort"]=0;
		$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
		$sort = $Cate->cateSelectAll($sql);
		if(count($sort)!= 0){
			$sort = implode($sort[0])+1;
			$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
		}else{
			$item["cate_sort"]=1;
		}
	}
   // -------先行判斷cate_name是否有重複再執行-----
	// if( $Cate->cateInfo()["cate_name"] == $cateName){ //僅需要修改分類其他資訊
	// 	$result = $Cate->cateUpdate($table,$item,$checkColumn);
	// 	if($result){
	// 		return true ;//echo "分類更新成功";
	// 	}else{
	// 		return false ; // echo "找不到分類"
	// 	}
	// }else{ //修改分類加命名
	// 	// --判斷名字是否重複-----
	// 	$sql2 = "select * from ".$table." where cate_name=:cate_name";
	// 	$db = new DB();
	// 	$dic = array(":cate_name" => $cateName);
	// 	if( $db->DB_Query($sql2,$dic)){ //有
	// 		return "相同分類" ;
	// 	}else{
	// 		$result = $Cate->cateUpdate($table,$item,$checkColumn);
	// 		if($result){
	// 			return true ;//echo "分類更新成功";
	// 		}else{
	// 			return false ; // echo "找不到分類"
	// 		}
	// 	}
	// }	

	// -------直接執行更新----------------------

	$result = $Cate->cateUpdate($table,$item,$checkColumn);
	if($result){
		return true ;//echo "分類更新成功";
	}else{
		return false ; // echo "找不到分類"
	}
}

function childToFatherCateUpdate($cateNo,$table,$item){
	$sql ="select * from ".$table." where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic); 
	$checkColumn = array("cate_no");
	$sql = "select cate_sort from category where cate_parents = '".$item["cate_parents"]."' order by cate_sort desc";
	$sort = $Cate->cateSelectAll($sql);
	if(count($sort)!= 0){
		$sort = implode($sort[0])+1;
		$item["cate_sort"]=$sort; //插入sort 的key value到陣列裡
	}else{
		$item["cate_sort"]=1;
	}
	$result = $Cate->cateUpdate($table,$item,$checkColumn);
	if($result){
		return true ;//echo "分類更新成功";
	}else{
		return false ; // echo "找不到分類"
	}
	
	
}


function deleteCate($cateNo,$table,$item){
	$sql = "select * from ".$table." where cate_no=:cate_no";
	$data = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$data);

	$sql="delete from ".$table." where cate_no= '".$cateNo."'" ;
	if(!$Cate->delete($sql)){
		return false ; //找不到要刪除的標籤
	}else{
		return true ;
	}
}

// -------ID找尋所有分類-----
function getCateNoFindAllLevelCate($cateNo,$table){ //$cateParent,
	$sql ="select * from ".$table." where cate_no = ".$cateNo." order by cate_no asc";
	$Cate = new Category($sql);
	$result = $Cate->cateSelectAll($sql);

	if(count($result) == 0){
		return false; //查無分類
	}else{
		$allCateLevel = [];
		foreach ($result as $key => $value){

			switch ($value["cate_level"]){
				case "0": // 第一層 顯示所有子孫
					array_push($allCateLevel,$value);
					$parentNO = $value["cate_no"];
					$cateChild =  $Cate->cateAllChildQuery($parentNO);
					if($cateChild==false){ //沒有子層
						return $allCateLevel;
					}else{
						foreach ($cateChild as $childKey => $childValue) {
							$allCateLevel[0]["childs"][$childKey]=$childValue;
							$parent2NO = $childValue["cate_no"];
							$cate2Child =  $Cate->cateAllChildQuery($parent2NO);
							if($cate2Child !=false ){//有子層
								foreach ($cate2Child as $child2Key => $child2Value){
									$allCateLevel[0]["childs"][$childKey]["childs"][$child2Key] = $child2Value;
								}
							}
						}
						return $allCateLevel;
					}

					break;
				case "1": // 第二層 找尋父親和所有孩子
					// ---找爸爸---
					$cateNo = $value["cate_parents"];
					$cateFather = $Cate -> cateAllFatherQuery($cateNo);
					array_push($allCateLevel,$cateFather[0]); //父親只會有一個

					// ---找尋自己的兄弟-----
					$fatherNo = $cateFather[0]["cate_no"];
					$cateSibling = $Cate -> cateAllChildQuery($fatherNo);
					foreach ($cateSibling as $SiblingKey => $SiblingValue){
						$allCateLevel[0]["childs"][$SiblingKey] = $SiblingValue;

					// ----找孩子----
						$parentNO = $SiblingValue["cate_no"];	
						$cateChild = $Cate->cateAllChildQuery($parentNO);
						if($cateChild !=false ){//有子層
							foreach ($cateChild as $childKey => $childValue){
								$allCateLevel[0]["childs"][$SiblingKey]["childs"][$childKey] = $childValue;
							}
						}	
					}
					return $allCateLevel;

					break;
				case "2": // 第三層 找尋父親和所有父親和爺爺
				// ----找尋爸爸抓爺爺ID---
					$fatherNo = $value["cate_parents"];
					$cateFather = $Cate -> cateAllFatherQuery($fatherNo);
					$grandFatherNo = $cateFather[0]["cate_parents"];

				// ----找尋爺爺---
					$cateGrandFather = $Cate -> cateAllFatherQuery($grandFatherNo);
					array_push($allCateLevel,$cateGrandFather[0]);
				//----找尋所有爸爸----	
					$cateFathers = $Cate->cateAllChildQuery($grandFatherNo);
					foreach ($cateFathers as $FathersKey => $FathersValue){
						$allCateLevel[0]["childs"][$FathersKey] = $FathersValue;
				// ----找尋自己所有兄弟----
						$parentNO = $FathersValue["cate_no"];
						$cateChilds = $Cate->cateAllChildQuery($parentNO);
						if( $cateChilds != false ){
							foreach ($cateChilds as $childsKey => $childsValue){
								$allCateLevel[0]["childs"][$FathersKey]["childs"][$childsKey] = $childsValue;
							}
						}
					}
					
					return 	$allCateLevel;
					break;
				default: 
	              	return "跑錯邊";
	              	break;
			}
		}
	}
}


// -------分類找商品-----
function getProductByCateNo($cateNo){ 
	$sql ="select a.product_no , b.product_name , b.model_id , b.img1 from category_products_relate a join product b on a.product_no = b.product_no where cate_no = ".$cateNo." and b.product_status = 1 order by b.product_no desc";
	$db = new DB();
	$cateRelate = $db ->DB_Query($sql);
	$products = [];
	foreach ($cateRelate as $key => $value) {
		$products[$key]["product_no"]=$value["product_no"];
		$products[$key]["product_name"]=$value["product_name"];
		$products[$key]["model_id"]=$value["model_id"];
		$products[$key]["img1"]=$value["img1"];
		$specSql = "select * from product_spec where product_spec_status = 1 and product_no='".$value["product_no"]."'";
		$specResult = $db->DB_Query($specSql);
		foreach ($specResult as $speckey => $specvalue) { //因為在一開使篩選就確定要事上架狀態的產品(至少有一種規格)
			// $products[$key]["product_stock"][$speckey] = $specvalue["product_stock"];
			$products[$key]["product_spec_price_old"][$speckey] = $specvalue["product_spec_price1"];
			if($specvalue["product_spec_price2"] == 0 && $specvalue["product_spec_price3"] == 0){ //沒有折扣
				$products[$key]["product_spec_price_discount"][$speckey]= null ;
			}else{ //有折扣
				if( $specvalue["product_spec_price3"] != 0 ){
					$priceDiscount = $specvalue["product_spec_price3"];
					$products[$key]["product_spec_price_discount"][$speckey] = $priceDiscount;
				}else{
					$products[$key]["product_spec_price_discount"][$speckey] =$specvalue["product_spec_price2"];
				}
			}
		}
	}
	return $products;
}

function cate360primaryUpdate($cateNo,$table,$item) {
	$sql ="select * from ".$table." where cate_no=:cate_no";
	$dic = array(":cate_no"=>$cateNo);
	$Cate = new Category($sql,$dic); 
	$checkColumn = array("cate_no");
	$result = $Cate->cateUpdate($table,$item,$checkColumn);
	if($result){
		return true ;//echo "分類更新成功";
	}else{
		return false ; // echo "找不到分類"
	}
}



// ---用id 找尋所有分類層級---
// $cateNo = "58";
// $table = "category" ;
// echo "<pre>";
// print_r(getCateNoFindAllLevelCate($cateNo,$table));

// echo "</pre>";

// $cateName = "test";
// $cateNo = "27";
// $table ="category";
// $item = array(
// 	"cate_no" => $cateNo,
// 	"cate_name"=> $cateName
// 	);

// echo "<pre>";
// var_dump(updateCate($cateNo,$table,$item));
// echo "</pre>";

// -------分類找商品-----
// $cateNo = "9";
// echo "<pre>";
// print_r(getProductByCateNo($cateNo));

// echo "</pre>";



// $cateName = "test321";
// $cateParents = "16";
// if($cateParents != 0){
// 	$cateLevel = "1";
// }else{
// 	$cateLevel = "0";
// }
// $table ="category";
// $item = array(
// 	"cate_parents"=> $cateParents,
// 	"cate_name"=> $cateName,
// 	"cate_level"=> $cateLevel
// 	);

// var_dump(cateCreate($cateName,$table,$item));

// cateSearch("28");

// cateSearchFatherAll();

// cateFatherSearch("18");
// print_r($Cate->cateQuery());



	
?>