<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/article.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/article.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/article.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/article.php");
}else{ //前端
	include("model/article.php");
}


date_default_timezone_set("Asia/Taipei");


function query($table,$articleNo){
	if( $articleNo != null ) {
		$sql = "select * from ".$table." where article_no = :article_no";
		$data = array(":article_no"=>$articleNo);
	} else {
		$sql = "select * from ".$table;
		$data = null;
	}
	
	$article = new Article($sql,$data);
	$result = $article->articleQuery();
	if(!$result){
		return false ; 
	}else{
		return $result;
	}
}


//直接新增文章
function addArticle($table,$item){
	$sql = "select * from ".$table;
	$data = null;
	$article = new Article($sql,$data);
	return $article -> articleCreate($table,$item) ; //成功回傳 id / 失敗 false
}
//編輯文章
function editArticle($table,$articleNo,$item){
	$sql = "select * from ".$table." where article_no=:article_no";
	$data = array(":article_no"=>$articleNo);
	$article = new Article($sql,$data);
	$checkColumn = array("article_no");
	$result = $article->articleUpdate($table,$item,$checkColumn);
	return 	$result; //true 更新成功  //false無需要更新的相同ID
}
//建立文章分類
function articleRelateCreate($table,$item){
	$db = new DB();
	return $db -> DB_Insert($table,$item);
}
//刪除文章分類
function articleRelateDelete($table,$item){
	$db = new DB();
	$itemBind = array();
	foreach ($item as $key => $value) {
		array_push($itemBind ,$key ." = ". $value );
	}
	
	$sql=sprintf("delete from %s where %s" , $table , implode(' and ',$itemBind) ) ;

	return $db->exec($sql) ;

}

function deleteArticle($table,$articleNo){
	$sql = "select * from ".$table." where article_no=:article_no";
	$data = array(":article_no"=>$articleNo);
	$article = new Article($sql,$data);

	$sql="delete from ".$table." where article_no= '".$articleNo."'" ;
	$result = $article->delete($sql) ;
	return $result;//true 刪除成功  //false找不到要刪除的相同文章ID
}

//尋找分類標籤 
function articleTagQuery($cateNo,$articleNo){ 

	$allTag = [];
	//搜尋 所有文章分類的標籤
	$db = New DB();
	$ArticleTagAllSql = "select * from category_tag_relate a join tag b on a.tag_no = b.tag_no  where a.cate_no = ".$cateNo;
	$ArticleTagAllResult = $db -> DB_query($ArticleTagAllSql);
	foreach ($ArticleTagAllResult as $key => $value) {
		array_push( $allTag , $value);
	}

	$useTag = [];
	if( $articleNo == null || $articleNo == ""){
		$articleNo ="null";
	}
	//搜尋文章ID 有包含的分類標籤
	$articleRelatesql = "select * from tag_article_relate a join tag b on a.tag_no = b.tag_no  where a.article_no = ".$articleNo;
	$articleRelateResult = $db -> DB_query($articleRelatesql);
	foreach ($articleRelateResult as $key => $value) {
		$useTag["tag_no"][$key] = $value["tag_no"];
		$useTag["tag_article_no"][$key] = $value["tag_article_no"];
	}

	$return = array("all"=> $allTag,"use"=> $useTag);
	return $return;
}


// echo "<pre>";
// print_r(articleTagQuery("50",null));
// echo "</pre>";



// ===查詢---
// $table = "article";
// $articleNo = null;

// echo "<pre>";
// var_dump(query($table,$articleNo));
// echo date("Y-m-d H:i:s",query($table,$articleNo)["article_createtime"]);;
// echo "</pre>";

// //===新增---
// $table = "article";

// $item = array(
// 	"article_title"=> "444紀念",
// 	"article_content"=> "新文章內容123",
// 	"article_describe"=> "文章描述",
// 	"article_img"=> "",
// 	"article_owner"=> "G編",
// 	"article_status"=> 1,
// 	"article_createtime"=> time(),
// 	);

// echo "<pre>";
// var_dump(addArticle($table,$item));
// echo "</pre>";

// // //編輯
// $table = "article";
// $articleNo = 5;
// $item = array(
// 	"article_no"=>5,
// 	"article_title"=> "修改標題",
// 	"article_content"=> "修改內容",
// 	"article_describe"=> "修改簡述123",
// 	"article_img"=> "",
// 	"article_owner"=> "C編",
// 	"article_status"=> 1
// 	);

// echo "<pre>";
// var_dump(editArticle($table,$articleNo,$item));
// echo "</pre>";

// // 刪除文章
// $table = "article";
// $articleNo = 4;

// echo "<pre>";
// var_dump(deleteArticle($table,$articleNo));
// echo "</pre>";

?>