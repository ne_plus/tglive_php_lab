<?php  


// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/product.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/product.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/product.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/product.php");
}else{ //前端使用
	include("model/product.php");
}


function productQueryAll($sql){
	// $sql = "select * from product ";
	$db = new DB();
	$result = $db->DB_Query($sql);
	if($result){
		$products = [];
		foreach ($result as $key => $value) {
			$products[$key]["product_no"] = $value["product_no"];
			$products[$key]["product_name"] = $value["product_name"];
			$products[$key]["product_subtitle"] = $value["product_subtitle"];
			$products[$key]["product_promote"] = $value["product_promote"];
			$products[$key]["product_describe"] = $value["product_describe"];
			$products[$key]["img1"] = $value["img1"];
			$products[$key]["img2"] = $value["img2"];
			$products[$key]["img3"] = $value["img3"];
			$products[$key]["img4"] = $value["img4"];
			$products[$key]["img5"] = $value["img5"];
			$products[$key]["product_content"] = $value["product_content"];
			$products[$key]["product_status"] = $value["product_status"];
			$sql = "select * from product where product_no=:product_no" ;
			$dic=array(":product_no"=>$value["product_no"]);
			$product = new Product($sql,$dic); //product DB initial
			// -------------標籤搜尋
			$tagRelate = $product->productRelateTag();
			if($tagRelate){ //有標籤存在
				foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
					$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
					$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
				}
				 
			}else{ //沒有標籤
				$products[$key]["tag_name"] = null ;
				$products[$key]["tag_no"] =null ;
			}
			// -------------分類搜尋
			
			$resultRelate = $product->productRelateCate();//搜尋商品的供應商和品牌
			if($resultRelate){ //有商品分類
				foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
					if($valuecateRelate["cate_parents"] == 63){ //行銷分類
						$products[$key]["activity"][$keycateRelate]=$valuecateRelate["cate_name"];
						$products[$key]["activity_no"][$keycateRelate]=$valuecateRelate["cate_no"];
					}else{ //供應商品牌
						$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
						$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
						$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
						$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
						if($valuecateRelate["cate_parents"] != 62 ){//有父層
								$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
								$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
								$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
						}else{ //沒有父層
							$products[$key]["cate_father_name"] = null ;
							$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
						}
					}
					
				}

				// ---------產品規格-----
				$resultSpec = $product->productSpecUseInfo();
				if($resultSpec){ //使用中的規格
					foreach ($resultSpec as $keySpec => $valueSpec){
						$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
						$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
						if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
							// $products[$key]["product_spec_price2"][$keySpec] = null ;
							// $products[$key]["product_spec_price3"][$keySpec] = null ;
							$products[$key]["product_spec_price_discount"][$keySpec]= null ;
						}else{ //有折扣
							$priceDiscount = $valueSpec["product_spec_price2"];
							if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
								$priceDiscount = $valueSpec["product_spec_price3"];
								$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
							}elseif($valueSpec["product_spec_price3"] == 0){
								$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
							}else{
								$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
							}
							// $products[$key]["product_spec_price2"][$keySpec] = $valueSpec["product_spec_price2"] ;
							// $products[$key]["product_spec_price3"][$keySpec] = $valueSpec["product_spec_price3"] ;
						}
						
					}
					
				}else{ //沒有規格
					$products[$key]["product_stock"][$keySpec] = null;
				}


			}else{ // 沒有商品分類
				$products[$key]["cate"] = null;
			}

			// -------------分類搜尋行銷活動
			$resultActivity = $product->productRelateCateActivity();
			if($resultActivity){ //有存在本月推薦分類
				foreach ($resultActivity as $activitykey => $activityvalue) {
					$products[$key]["activity"][$activitykey] = $activityvalue["cate_name"];
					$products[$key]["activityCateNo"][$activitykey] = $activityvalue["cate_no"];
					$products[$key]["activityRelateNo"][$activitykey] = $activityvalue["cate_product_no"];
				}
			}else{ //沒有

			}
		}
		return  $products;
	}else{
		return false;
	}
	
}

// ====品牌ID找商品(首頁)=====
function get_brandId_queryProducts($cateNo){
	// ----找品牌商品---
	$db = new DB;
	$brandSql =  "select * from category_products_relate a join product b on a.product_no= b.product_no where cate_no= $cateNo and product_status=1 limit 0,8";
	$brandResult = $db -> DB_Query($brandSql);
	if($brandResult){
		$products = [];
		foreach ($brandResult as $key => $value) {
			$products[$key]["product_no"] = $value["product_no"];
			$products[$key]["product_name"] = $value["product_name"];
			$products[$key]["product_subtitle"] = $value["product_subtitle"];
			$products[$key]["img1"] = $value["img1"];
			$products[$key]["product_status"] = $value["product_status"];
			$sql = "select * from product where product_no=:product_no" ;
			$dic=array(":product_no"=>$value["product_no"]);
			$product = new Product($sql,$dic); //product DB initial
			// -------------標籤搜尋
			$tagRelate = $product->productRelateTag();
			if($tagRelate){ //有標籤存在
				foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
					$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
					$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
				}
				 
			}else{ //沒有標籤
				$products[$key]["tag_name"] = null ;
				$products[$key]["tag_no"] =null ;
			}

			// -------------分類搜尋
			
			$resultRelate = $product->productRelateCate();
			if($resultRelate){ //有商品分類
				foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
					$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
					$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
					$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
					$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
					if($valuecateRelate["cate_parents"] != 0 ){//有父層
							$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
							$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
							$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
					}else{ //沒有父層
						$products[$key]["cate_father_name"] = null ;
						$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
					}
				}

			}else{ // 沒有商品分類
				$products[$key]["cate"] = null;
			}

			// -------------分類搜尋本月推薦
			$resultMonth = $product->productRelateCateMonth();
			if($resultMonth){ //有存在本月推薦分類 (單筆商品只會綁定一次本月推薦)
				$products[$key]["month"] = "1";//$resultMonth[0]["cate_name"];
				$products[$key]["monthRelateNo"] =$resultMonth[0]["cate_product_no"];
			}else{ //沒有

			}

			// -------------分類搜尋行銷活動
			$resultActivity = $product->productRelateCateActivity();
			if($resultActivity){ //有存在本月推薦分類
				foreach ($resultActivity as $activitykey => $activityvalue) {
					$products[$key]["activity"][$activitykey] = $activityvalue["cate_name"];
					$products[$key]["activityRelateNo"][$activitykey] = $activityvalue["cate_product_no"];
				}
			}else{ //沒有

			}

			// ---------產品規格-----
			$resultSpec = $product->productSpecUseInfo();
			if($resultSpec){ //使用中的規格
				foreach ($resultSpec as $keySpec => $valueSpec){
					$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
					$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
					if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
						$products[$key]["product_spec_price_discount"][$keySpec]= null ;
					}else{ //有折扣
						$priceDiscount = $valueSpec["product_spec_price2"];
						if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
							$priceDiscount = $valueSpec["product_spec_price3"];
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}elseif($valueSpec["product_spec_price3"] == 0){
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}else{
							$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
						}
					}
					
				}
				
			}else{ //沒有規格
				$products[$key]["product_stock"][$keySpec] = null;
			}

		}
		$array["data"] = $products;
	}else{ //沒有商品
		$products = [];
		$array["data"] = $products;
	}
	return $array["data"];
}

// ====行銷活動(首頁)=====
function get_activityId_queryProducts($acitvityNo,$limit){
	$limitSetting ="";
	if( $limit != null) {
		$limitSetting = "limit 0,".$limit;
	}else{
		$limitSetting = "";
	}
	$db = new DB();
	//本月推薦 
	$monthSql = "select * from category_products_relate a join product b on a.product_no = b.product_no where cate_no = $acitvityNo and b.product_status =1 order by a.cate_product_updatetime desc ".$limitSetting;
	$monthResult = $db -> DB_Query($monthSql);
	if($monthResult){
		$products = [];
		foreach ($monthResult as $key => $value) {
			$products[$key]["product_no"] = $value["product_no"];
			$products[$key]["product_name"] = $value["product_name"];
			$products[$key]["product_subtitle"] = $value["product_subtitle"];
			$products[$key]["img1"] = $value["img1"];
			$products[$key]["product_status"] = $value["product_status"];
			$sql = "select * from product where product_no=:product_no" ;
			$dic=array(":product_no"=>$value["product_no"]);
			$product = new Product($sql,$dic); //product DB initial
			// -------------標籤搜尋
			$tagRelate = $product->productRelateTag();
			if($tagRelate){ //有標籤存在
				foreach ($tagRelate as $keytagRelate => $valuetagRelate) {
					$products[$key]["tag_name"][$keytagRelate] = $valuetagRelate["tag_name"];
					$products[$key]["tag_no"][$keytagRelate] = $valuetagRelate["tag_no"];
				}
				 
			}else{ //沒有標籤
				$products[$key]["tag_name"] = null ;
				$products[$key]["tag_no"] =null ;
			}
			// -------------分類搜尋(供應商/品牌)
			
			$resultRelate = $product->productRelateCate();
			if($resultRelate){ //有商品分類
				foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
					$products[$key]["cate_name"][$keycateRelate] = $valuecateRelate["cate_name"];
					$products[$key]["cate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
					$products[$key]["cate_parents"][$keycateRelate] = $valuecateRelate["cate_parents"];
					$products[$key]["cate_level"][$keycateRelate] = $valuecateRelate["cate_level"];
					if($valuecateRelate["cate_parents"] != 0 ){//有父層
							$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
							$products[$key]["cate_father_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
							$products[$key]["cate"][$keycateRelate] = $resultCateParents[0]["cate_name"].">".$valuecateRelate["cate_name"];
					}else{ //沒有父層
						$products[$key]["cate_father_name"] = null ;
						$products[$key]["cate"][$keycateRelate] = $valuecateRelate["cate_name"];
					}
				}

			}else{ // 沒有商品分類
				$products[$key]["cate"] = null;
			}

			// -------------分類搜尋本月推薦
			$resultMonth = $product->productRelateCateMonth();
			if($resultMonth){ //有存在本月推薦分類 (單筆商品只會綁定一次本月推薦)
				$products[$key]["month"] = "1";//$resultMonth[0]["cate_name"];
				$products[$key]["monthRelateNo"] =$resultMonth[0]["cate_product_no"];
			}else{ //沒有

			}

			// -------------分類搜尋行銷活動
			$resultActivity = $product->productRelateCateActivity();
			if($resultActivity){ //有存在本月推薦分類
				foreach ($resultActivity as $activitykey => $activityvalue) {
					$products[$key]["activity"][$activitykey] = $activityvalue["cate_name"];
					$products[$key]["activityRelateNo"][$activitykey] = $activityvalue["cate_product_no"];
				}
			}else{ //沒有

			}

			// ---------產品規格-----
			$resultSpec = $product->productSpecUseInfo();
			if($resultSpec){ //使用中的規格
				foreach ($resultSpec as $keySpec => $valueSpec){
					$products[$key]["product_stock"][$keySpec] = $valueSpec["product_stock"] ;
					$products[$key]["product_spec_price_old"][$keySpec] = $valueSpec["product_spec_price1"] ;
					if($valueSpec["product_spec_price2"] == 0 && $valueSpec["product_spec_price3"] == 0){ //沒有折扣
						$products[$key]["product_spec_price_discount"][$keySpec]= null ;
					}else{ //有折扣
						$priceDiscount = $valueSpec["product_spec_price2"];
						if( $priceDiscount >  $valueSpec["product_spec_price3"] && $valueSpec["product_spec_price3"] != 0 ){
							$priceDiscount = $valueSpec["product_spec_price3"];
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}elseif($valueSpec["product_spec_price3"] == 0){
							$products[$key]["product_spec_price_discount"][$keySpec] = $priceDiscount;
						}else{
							$products[$key]["product_spec_price_discount"][$keySpec] =$valueSpec["product_spec_price3"];
						}
					}
					
				}
				
			}else{ //沒有規格
				$products[$key]["product_stock"][$keySpec] = null;
			}

		}
		$array["month"] = $products;
	}else{ //沒有商品
		$products = [];
		$array["month"] = $products;
		// echo json_encode($array);
	}

	return $array["month"];
}




function productQuery($table,$productNo) {
	$sql = "select * from ".$table." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$result = $product -> productInfo();
	if($result){
		return $result;
	}else{
		return false ;
	}
}	

function productSpecQuery($table,$productNo) {
	$sql = "select * from ".$table." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$result = $product -> productSpecInfo();
	if($result){
		return $result;
	}else{
		return false ;
	}
}

function productHaveSpecQuery($table,$productNo) {
	$sql = "select * from ".$table." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$result = $product -> productSpecUseInfo();
	if($result){
		return $result;
	}else{
		return false ;
	}
}	

function productCateQuery($table,$productNo){
	$sql = "select * from ".$table." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$result = $product -> productRelateCate();
	if($result){ //有商品分類
		return $result;
	}else{ //沒有商品分類
		return false ;
	}
}

function productComboQuery($productNo){
	$db = new DB();
	$sql = "SELECT c.*, p.model_id, p.product_name, p.product_status, s.product_spec_info, s.product_stock, s.product_spec_price1, s.product_spec_price2, s.product_spec_price3, s.product_spec_status FROM product_combo c INNER JOIN product p ON c.product_no = p.product_no INNER JOIN product_spec s ON c.product_spec_no = s.product_spec_no WHERE rel_product_no='" . $productNo . "' AND isdeleted = 0" ;
	$result = $db->DB_Query($sql);
	if($result){ //有加購價
		return $result;
	}else{ //沒有加購價
		return false ;
	}
}
function proCreate($table,$data){	
	$product = new DB();
	$result = $product->DB_Insert($table,$data);
	// return $result;
	if($result){ 
		return $product->lastId ;//echo "產品新增成功";
	}else{
		return false ; // echo "新增失敗"
	}
}


function proUpdate($table,$item,$productNo){
	$sql = "select * from ".$table." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("product_no");
	$result = $product->editProduct($table,$item,$checkColumn);
	if($result){
		return true ;//echo "產品更新成功";
	}else{
		return false ; // echo "找不到產品"
	}
}


function proUpdateTag($table1,$table2,$item,$productNo,$tagNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("tag_product_no");
	$result = $product->productUpdateTag($table2,$item,$checkColumn,$tagNo);
	if($result){
		// return true ;//echo "產品加入標籤";		
		return $result; //回傳該產品對應的標籤關係
	}else{
		return false ; // echo "找不到產品"
	}
}

function proUpdateCate($table1,$table2,$item,$productNo,$cateNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("cate_product_no");
	$result = $product->productUpdateCate($table2,$item,$checkColumn,$cateNo);
	if($result){
		// return true ;//echo "產品加入分類";
		return $result ; //回傳產品對應的分類關係
	}else{
		return false ; // echo "找不到產品"
	}
}

function proCateFatherQuery($sql){
	$db = new DB();
	$result = $db->DB_Query($sql);
	return $result;
}


function deleteCate($table1,$table2,$productNo,$CateProductNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);

	$sql="delete from ".$table2." where cate_product_no= '".$CateProductNo."'" ;
	if(!$product->delete($sql)){
		return false ; //找不到要刪除的分類
	}else{
		return true ;
	}
}

function deleteTag($table1,$table2,$productNo,$tagProductNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);

	$sql="delete from ".$table2." where tag_product_no= '".$tagProductNo."'" ;
	if(!$product->delete($sql)){
		return false ; //找不到要刪除的標籤
	}else{
		return true ;
	}
}


function proCreateSpec($table1,$table2,$item,$productNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("product_spec_no");
	$result = $product->productUpdateSpec($table2,$item,$checkColumn);
	if($result){ //ture 新增成功 回傳更新的欄位
		return $product->productUpdateSpecQuery(); 
	}else{
		return false ; // echo "找不到產品"
	}
}

function proEditSpec($table1,$table2,$item,$productNo,$productSpecNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("product_spec_no");
	$result = $product->productUpdateSpec($table2,$item,$checkColumn);
	if($result){ //ture 更新成功 回傳更新的欄位
		return $product->productUpdateSpecQuery(); 
	}else{
		return false ; // echo "找不到產品"
	}
}

function proEditSpecOnly($table1,$table2,$item,$productNo,$productSpecNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);
	$checkColumn = array("product_spec_no");
	$result = $product->productUpdateSpec($table2,$item,$checkColumn);
	if($result){ //ture 更新成功 
		return true; 
	}else{
		return false ; // echo "找不到產品"
	}
}

function get_relateProducts_by_productId($pid){
	// 1.收尋產品類別內標籤的相關產品
	// ====收尋產品類別的標籤分類 的標籤編號====
	$db = new DB();
	$sql = "SELECT * FROM category a join category_tag_relate b on a.cate_no=b.cate_no join tag_products_relate c on b.tag_no = c.tag_no WHERE cate_parents= 61 and cate_level =1 and cate_name='產品類別' and product_no= $pid";
	$result = $db->DB_Query($sql);
	$product = [];
	$productID = []; //紀錄已存在的商品ID向下收尋時省略
	array_push($productID,"$pid");
	if($result){ //有產品類別的標籤
		foreach ($result as $key => $value) {
			// ===過濾已經有出現過的商品====
			$productIdIgnore = [] ;
			foreach ($productID as $productIDkey => $productIDvalue) {
				array_push($productIdIgnore , "a.product_no != ".$productIDvalue);
			}
			$ignore =  implode(" and ",$productIdIgnore);

			$productTagRelateSql = sprintf("SELECT a.product_no , a.model_id , a.product_name , a.product_describe , a.img1 FROM product a join tag_products_relate b on a.product_no = b.product_no WHERE tag_no = ".$value['tag_no']." and a.product_status = 1 and %s",$ignore);
			$productTagRelateResult = $db->DB_Query($productTagRelateSql);
			foreach ($productTagRelateResult as $productTagRelateResultkey => $productTagRelateResultvalue) {
				array_push($product,$productTagRelateResultvalue);
				array_push($productID,$productTagRelateResultvalue["product_no"]);
			}
		}
		
	}else{
		 //沒有產品類別的標籤
	}

	//2.收尋產品品牌的其他相關產品
	if(count($product)<10){ //如果標籤的相關產品數量小於10項下繼續收尋
		// ---收尋產品的品牌ID---
		$brandRelateProductSql ="SELECT b.cate_no FROM category_products_relate a JOIN category b on a.cate_no = b.cate_no where a.product_no ='".$pid."' and b.cate_parents <> 63 and b.cate_parents <> 61";
		$productBrandResult = $db->DB_Query($brandRelateProductSql);
		foreach ($productBrandResult as $key => $value) {
			// ===過濾已經有出現過的商品====
			$productIdIgnore = [] ;
			foreach ($productID as $productIDkey => $productIDvalue) {
				array_push($productIdIgnore , "a.product_no != ".$productIDvalue);
			}
			$ignore =  implode(" and ",$productIdIgnore);

			$brandRelateProductsSql = sprintf("SELECT a.product_no , a.model_id , a.product_name , a.product_describe , a.img1 FROM product a JOIN category_products_relate b on a.product_no = b.product_no where b.cate_no = '".$value["cate_no"] ."' and a.product_status = 1 and %s",$ignore);
			$brandRelateProductsResult = $db->DB_Query($brandRelateProductsSql);
			foreach ($brandRelateProductsResult as $brandRelateProductsResultkey => $brandRelateProductsResultvalue) {
				array_push($product,$brandRelateProductsResultvalue);
				array_push($productID,$brandRelateProductsResultvalue["product_no"]);
			}
		}
	}else{   //超過10個商品數量直接feedback
		return $product; 
	}

	// 3.收尋產品標籤的其他相關產品
	if(count($product)<10){ //數量小於10項下繼續收尋
		// ---收尋產品的心情標籤ID---(改為依類別篩選)
		$tagRelate2ProductSql ="SELECT b.tag_no FROM category a join category_tag_relate b on a.cate_no=b.cate_no join tag_products_relate c on b.tag_no = c.tag_no WHERE cate_parents= 61 and cate_level =1 and cate_name='依類別篩選' and product_no= $pid";
		$tagRelate2ProductResult = $db->DB_Query($tagRelate2ProductSql); 
		foreach ($tagRelate2ProductResult as $key => $value) {
			// ===過濾已經有出現過的商品====
			$productIdIgnore = [] ;
			foreach ($productID as $productIDkey => $productIDvalue) {
				array_push($productIdIgnore , "a.product_no != ".$productIDvalue);
			}
			$ignore =  implode(" and ",$productIdIgnore);

			$tagRelate2ProductsSql = sprintf("SELECT a.product_no , a.model_id , a.product_name , a.product_describe , a.img1 FROM product a join tag_products_relate b on a.product_no = b.product_no WHERE tag_no = ".$value['tag_no']." and a.product_status = 1 and %s",$ignore);
			$tagRelate2ProductsResult = $db->DB_Query($tagRelate2ProductsSql);
			foreach ($tagRelate2ProductsResult as $tagRelate2ProductsResultkey => $tagRelate2ProductsResultvalue) {
				array_push($product,$tagRelate2ProductsResultvalue);
				array_push($productID,$tagRelate2ProductsResultvalue["product_no"]);
			}
		}

	}else{   //超過10個商品數量直接feedback
		return $product; 
	}	

	// 4.收尋優惠卷標籤 
	if(count($product)<10){ //數量小於10項下繼續收尋
		// ---收尋產品的心情標籤ID---
		$tagRelate2ProductSql ="SELECT b.tag_no FROM category a join category_tag_relate b on a.cate_no=b.cate_no join tag_products_relate c on b.tag_no = c.tag_no WHERE cate_parents= 61 and cate_level =1 and cate_name='優惠劵' and product_no= $pid";
		$tagRelate2ProductResult = $db->DB_Query($tagRelate2ProductSql); 
		foreach ($tagRelate2ProductResult as $key => $value) {
			// ===過濾已經有出現過的商品====
			$productIdIgnore = [] ;
			foreach ($productID as $productIDkey => $productIDvalue) {
				array_push($productIdIgnore , "a.product_no != ".$productIDvalue);
			}
			$ignore =  implode(" and ",$productIdIgnore);

			$tagRelate2ProductsSql = sprintf("SELECT a.product_no , a.model_id , a.product_name , a.product_describe , a.img1 FROM product a join tag_products_relate b on a.product_no = b.product_no WHERE tag_no = ".$value['tag_no']." and a.product_status = 1 and %s",$ignore);
			$tagRelate2ProductsResult = $db->DB_Query($tagRelate2ProductsSql);
			foreach ($tagRelate2ProductsResult as $tagRelate2ProductsResultkey => $tagRelate2ProductsResultvalue) {
				array_push($product,$tagRelate2ProductsResultvalue);
				array_push($productID,$tagRelate2ProductsResultvalue["product_no"]);
			}
		}

	}else{   //超過10個商品數量直接feedback
		return $product; 
	}	

	return $product; 

}

function get_comboProducts_by_productId($pid){
	$db = new DB();
	$sql = sprintf("SELECT c.*, p.model_id, p.product_name, p.img1, s.product_spec_info, s.product_stock FROM product_combo c INNER JOIN product p ON c.product_no = p.product_no INNER JOIN product_spec s ON c.product_spec_no = s.product_spec_no WHERE c.rel_product_no= %d AND NOW() BETWEEN c.starttime AND c.endtime AND c.product_combo_status = 1 AND c.isdeleted = 0 AND p.product_status = 1 AND s.product_stock >= 1 AND s.product_spec_status = 1", $pid);
	$result = $db->DB_Query($sql);
	foreach ($result as $key => $value) {
		// 子類別
		$sql = sprintf("SELECT c1.cate_name, c1.cate_no, c2.cate_name parent_name, c2.cate_no vendor_no FROM category c1 LEFT OUTER JOIN category c2 ON c1.cate_parents = c2.cate_no where c1.cate_no in (SELECT cate_no FROM category_products_relate where product_no = %d) AND c1.cate_level = 2", $value['product_no']);
		$result2 = $db->DB_Query($sql);
		if ($result2 && count($result2) >= 1) {
			$result[$key]["cate_no"] = $result2[0]["cate_no"];
			$result[$key]["cate_name"] = $result2[0]["cate_name"];
			$result[$key]["vendor_no"] = $result2[0]["vendor_no"];
			$result[$key]["parent_name"] = $result2[0]["parent_name"];
		}
	}
	return $result;
}

// =====購物車=====
function findBrandsInfo($productNo){ //收尋商品品牌跟廠商
	$sql = "select * from product where product_no=:product_no" ;
	$dic=array(":product_no"=>$productNo);
	$product = new Product($sql,$dic); //product DB initial

	$resultRelate = $product->productRelateCate();
	$brandInfo =[];
	if($resultRelate){ //有商品分類品牌與廠商
		foreach ($resultRelate as $keycateRelate => $valuecateRelate) {
			$brandInfo["brand_name"][$keycateRelate] = $valuecateRelate["cate_name"];
			$brandInfo["bcate_no"][$keycateRelate] = $valuecateRelate["cate_no"];
			if($valuecateRelate["cate_parents"] != 0 ){//有父層
					$resultCateParents = $product->cateParents($valuecateRelate["cate_parents"]);
					$brandInfo["vendor_name"][$keycateRelate] = $resultCateParents[0]["cate_name"];
					$brandInfo["vcate_no"][$keycateRelate] = $resultCateParents[0]["cate_no"];

			}else{ //沒有父層
				$brandInfo["vendor_name"][$keycateRelate] = null;
					$brandInfo["vcate_no"][$keycateRelate] = null;
			}
		}

	}else{ // 沒有商品分類
		$brandInfo["cate"] = null;
	}
	return $brandInfo;
}


function findSpecInfo($productSpecNo){
	$sql ="select * from product_spec where product_spec_no = $productSpecNo";
	$db = new DB();
	$result = $db->DB_Query($sql);
	return $result;
}

function findCounponUseTag($productNo){
	$sql = "select * from product where product_no=:product_no" ;
	$dic=array(":product_no"=> $productNo );
	$product = new Product($sql,$dic); //product DB initial
	// -------------標籤搜尋
	$tagRelate = $product->productRelateTag();

	$couponResult = [] ;
	$db = NEW DB();
	foreach ($tagRelate as $key => $value) {
		$couponSql = "SELECT * from coupon a join tag b on a.coupon_used = b.tag_no where a.coupon_status = 1 and b.tag_no = ".$value['tag_no'];
		$coupontag = $db->DB_Query($couponSql);
		if(count($coupontag) != 0 ){
			foreach ($coupontag as $key => $value) {
				array_push($couponResult , $value);
			}
			
		}	
	}
	return $couponResult;
}
//=====購物車=====


function deleteSpec($table1,$table2,$productNo,$productSpecNo){
	$sql = "select * from ".$table1." where product_no=:product_no" ;
	$dic = array(":product_no"=>$productNo);
	$product = new Product($sql,$dic);

	$sql="delete from ".$table2." where product_spec_no= '".$productSpecNo."'" ;
	if(!$product->delete($sql)){
		return false ; //找不到要刪除的標籤
	}else{
		return true ;
	}
}


?>