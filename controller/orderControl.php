<?php  
// include("../model/user.php");
if(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/framwork")){ //admin/framwork 引用
	include("../../model/order.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"controller/")){ //controller 引用
	include("../model/order.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"admin/")){ //admin 直接引用
	include("../model/order.php");
}elseif(strpos(strtolower($_SERVER["PHP_SELF"]),"frontframwork/")){ //前端framwork
	include("../model/order.php");
}else{//前端
	include("model/order.php");
}


date_default_timezone_set("Asia/Taipei");


function query($sql,$data){
	$order = new Order($sql,$data);
	$result = $order->orderQuery();
	if(!$result){
		return false ; //找不到訂單
	}else{
		return $result;
	}
}

function detailQuery($sql,$data,$orderNo){
	$order = new Order($sql,$data);
	$result = $order->orderDetailQuery($orderNo);
	if(!$result){
		return false ; //找不到訂單
	}else{
		return $result;
	}
}


function createOrder($item,$cateValue){
	$sql = "select * from order_item";
	$order = new Order($sql,null);
	$result = $order->orderCreate($item,$cateValue);
	return $result;
}

function editOrderItem($table,$item,$orderNo){
	$sql = "select * from order_item where order_no = :order_no";
	$data = array(":order_no"=>$orderNo);
	$order = new Order($sql,$data);
	$checkColumn = array("order_no");
	$result = $order->orderUpdate($table,$item,$checkColumn);
	return $result;
}






// // ===查詢---
// $sql = "select * from order_item";
// $data = null;

// var_dump(query($sql,$data));


// // ===新增---
// $orderEmail = "jack@mail.com";
// $orderCreatetime = time();
// $orderGroup = $orderEmail.$orderCreatetime;
// $item = array(
// 	"mem_no" => 355,
// 	"cate_no" => 55,
// 	"order_group" => $orderGroup,
// 	"order_email" => $orderEmail,
// 	"order_recipient" => "熊天",
// 	"order_address" => "台北市內湖", 
// 	"order_tel" => "0937930193",
// 	"order_price" => "20000",
// 	"order_cargo" => "90",
// 	"coupon_code" => null,
// 	"order_discount" => null,
// 	"order_pay" => 0,
// 	"order_status" => 1,
// 	"order_delivery" => 0,
// 	"order_createtime" => $orderCreatetime,
// 	);


// // echo $str,"<br>";
// echo "<pre>";
// print_r($item);
// echo "</pre>";
// var_dump(addCoupon($table,$couponName,$item));


?>