
  <div class="gotop">
  	<i class="fa fa-angle-up fa-3x" aria-hidden="true"></i>
  </div>

  <footer >
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-12 footer_logo">
            	<img src="image/frontend/EC-logo-gray.png" class="logo">
            	
            	 <div class="social-bottins">
	            	
				<a href="https://www.facebook.com/TGiLiveTW/" target="_blank" ><img src="image/frontend/fb-gray.png" ></a><!--

				<a href=""><img src="image/frontend/line-gray.png" ></a>
-->
				<a href="https://www.instagram.com/tgi_live/" target="_blank" ><img src="image/frontend/instagram-gray.png" ></a>
				<a href="mailto:info@tgilive.com" target="_blank" ><img src="image/frontend/mail-gray.png" ></a>
				
		       </div>
		        <div><?=$lang_footer_copyright_1?></div>
		        <div><?=$lang_footer_copyright_2?></div>
            </div>
			  <div class="col-md-3 col-6 footer_content text-right">
				 <ul>
				 
	             	<li><a href="newbie.php"><?=$lang_footer_newbie?></a></li>
	             	<li><a href="faq-member.php"><?=$lang_footer_faq_member?></a></li>
	             	<li><a href="faq-deliver.php"><?=$lang_footer_faq_eliver?></a></li>
	             	<li><a href="faq-returns.php"><?=$lang_footer_faq_returns?></a></li>
								 <li><a href="payInfo.php"><?=$lang_footer_payinfo?></a></li>
	             </ul>
	            
            </div>
            <div class="col-md-3 col-6 footer_content text-right">
	             <ul>
	             	<li><a href="terms.php"><?=$lang_footer_terms?></a></li>
	             	<li><a href="privacy.php"><?=$lang_footer_privacy?></a></li>
					 <li><a href="contacts.php"><?=$lang_footer_contacts?></a></li>
					 <li><a href="https://marketing.tgilive.com" target="_blank"><?=$lang_footer_marketing?></a></li>
	             </ul>
            </div>
          
             
          </div><!--  row -->
          
        </div><!--  container -->
<!--
		<div class="footer_bottom">
		           	
		             <div>Copyright TGILive &copy; 2016-2017</div>
         </div>
-->

  </footer>
  </body>
</html>

<!-- header 切換 GA -->
<script type="text/javascript">
	<?php 
	if(strpos(strtolower($_SERVER["REQUEST_URI"]),"product-detail.php?")){ //產品內頁 
		$productDimension =  $result[0]["cate_name"][0].' | '.$result[0]['product_name'];
	?>
		$(document).attr("title","<?php echo $productDimension; ?>");
	<?php } else if( strpos(strtolower($_SERVER["REQUEST_URI"]),"brands.php?") ) { 
		$brandDimension = $cateSelectResult[0]["cate_name"];
	?>
		$(document).attr("title","<?php echo $brandDimension; ?>");
	<?php } ?>
</script>
<!-- header 切換 GA -->



