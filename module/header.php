<?php 
ob_start();
session_start();

// ===檢查客戶端是否有關閉cookie=====
// 先儲存cookie確認碼
setcookie("CookieCheck","OK",time()+3600,"/");

require_once("controller/productControl.php");  		
date_default_timezone_set("Asia/Taipei");
$db = new DB();

//memToken check
if(isset($_SESSION["memNo"]) === false){
	if (isset($_COOKIE["mem_token"])) {
		$_SESSION["where"] = $_SERVER["PHP_SELF"];

		header('Location: Frontframwork/memTokenCheck.php');
	}	
}

// 找品牌清單
$cateBrandUse =[];
$sqlSort ="SELECT * from category_products_relate a join category b where a.cate_no = b.cate_no and cate_level = 2 group by b.cate_name asc";
$cateUseResult = $db -> DB_Query($sqlSort);
if($cateUseResult){
	foreach ($cateUseResult as $key => $value) {
		array_push($cateBrandUse,$value);
	}
}

// 關鍵字清單
//抓出有綁定產品的標籤
$cid = 189;
$tagRecommendSql = "SELECT * from category_tag_relate a join tag b on a.tag_no = b.tag_no where a.cate_no = ".$cid." group by a.tag_no";
$tagRecommendResult = $db->DB_query($tagRecommendSql);


//購物車清單
include("Frontframwork/cookieCart.php");

// 語系設定檔
include_once("lang.php");
?>

<!doctype html>
<html lang="en">
  <head>
    <title>TGILive居生活</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- facebook_og ===================================-->
	<?php include("Frontframwork/facebook_og.php"); ?>
	<!-- facebook_og end ===============================-->

    <!-- jquery -->
	<script src="js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="js/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

	<!-- sweetalert.js -->
	<script src="js/sweetalert.min.js"></script>

    <!-- font english -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	
	<!-- product  -->
	<link rel="stylesheet" type="text/css" href="css/cart.css">
	<link rel="stylesheet" type="text/css" href="css/product.css">
	<link rel="stylesheet" type="text/css" href="css/article.css">
	<script src="js/product.js"></script>
	<script src="js/brands.js"></script>
	<!-- user -->
	<link rel="stylesheet" type="text/css" href="css/tgilive.css?v1.2">
	<link rel="stylesheet" type="text/css" href="css/tgilive_rwd.css?V1.2">
	<script src="js/index.js"></script>
	<script src="js/cart.js"></script>
	<script src="js/memcenter.js"></script>
	<script src="plugIn/jquery-mousewheel/jquery.mousewheel.min.js"></script>
	<script src="js/fbLogin.js"></script>

		<!-- article -->
	
		<!-- datatable -->
	<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">	

	<script src="js/style.js"></script>
		<!-- slick -->
	<link rel="stylesheet" type="text/css" href="plugIn/slick/css/slick.css">
  	<link rel="stylesheet" type="text/css" href="plugIn/slick/css/slick-theme.css">
	<script src="plugIn/slick/js/slick.js"></script>
	<link href="css/all.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/fa-svg-with-js.css">
	<script defer src="js/fontawesome/fontawesome-all.js"></script>

	<link rel="Shortcut Icon" type="image/x-icon" href="image/frontend/logo-shortcut.ico">



	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K3ZT6L9');</script>
	<!-- End Google Tag Manager -->
			

  </head>
  <body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3ZT6L9"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
  <header>
  	<div class="container-fluid">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" href="index.php">
				<img src="image/frontend/LogoC.png" id="logo">
		  	</a>

		  <button class="navbar-toggler" type="button" id="nav-button-rwd">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <!-- ======桌機===== -->
		  <div class="navbar" id="mainNav">
			    
		    <ul class="navbar-nav">
			<li class="nav-item">
		        <a class="nav-link" href="vrLive.php"><?=$lang_menu_vrLive?></a>
		      </li>	
		      <li class="nav-item ">
		        <a class="nav-link" id="findProducts" href="products.php"><?=$lang_menu_findProducts?></a>
		      </li>
		      <li class="nav-item brandHoverAct">
		        <a class="nav-link" href="#"><?=$lang_menu_findBrand?> <i class="fa fa-chevron-down" aria-hidden="true"></i></a>

		        <div class="brandHover">
					<div class="hoverContent d-flex align-items-center">
						<div class="container">
							<div class="row">
								<div class="col-md-9 col-sm-12">	
									<div class="row">
										<?php foreach ($cateBrandUse as $key => $value){
											$brandImgNo = "brand".$key;
					                	?>
										<div class="col-3">
											<div class="brandDetail text-center" id="<?php echo $brandImgNo; ?>" ><a href="brands.php?cate_no=<?php echo $value["cate_no"] ;?>&brand=<?php echo preg_replace('/\s(?=)/', '', $value['cate_name']); ?>"><?php echo $value["cate_name"]; ?></a></div>	
										</div>
					                	<?php
					                	}
					                	?>
										
									</div>
										
								</div>
								<div class="col-3 navBrandImgCol" >
									<?php  
										foreach ($cateBrandUse as $key => $value) {
											$brandImgName = "imgbrand".$key;
											if($key == 0){
									?>
									<img class="<?php echo $brandImgName; ?> imgbrand img-fluid" src="<?php echo $value['cate_logo_img']; ?>" >
									<?php				
											}else{
									?>
									<img class="<?php echo $brandImgName; ?> imgbrand img-fluid" src="<?php echo $value['cate_logo_img']; ?>" style="display: none; opacity: 0;">	
									<?php		
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
		      </li>

		      <!-- <li class="nav-item">
		        <a class="nav-link" href="#">優惠活動</a>
		      </li> -->
		      <li class="nav-item">
		        <a class="nav-link" href="styles.php"><?=$lang_menu_style?></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="activity.php"><?=$lang_menu_activity?></a>
		      </li>	

					<!-- <li class="nav-item">
						<a class="nav-link" href="360index.php"><?=$lang_menu_vrLive?></a>
		      </li>	 -->
		    </ul>
		    <form class="form-inline" id="searchForm" action="result.php" method="get">
		      <input class="form-control form-control-sm  searchAll" type="text" placeholder="<?=$lang_menu_searchProduct?>" aria-label="Search" name="productSearch" autocomplete="off">

		      <!-- 關鍵字 -->
		      <?php if( count($tagRecommendResult) != 0 ){ ?>
		      <div class="recommendTag-area">
		      	<?php foreach ($tagRecommendResult as $key => $value) { ?>
		      	<span class="recommendTag-content"><a href="result.php?productSearch=<?php echo $value['tag_name']; ?>"><?php echo $value['tag_name']; ?></a></span>
		      	<?php } ?>
		      </div>
		      <?php } ?>
		      <!-- 關鍵字 -->

		      <button id="searchAllButton"> <i class="fas fa-search searchIcon"></i></button>
		    </form>
		  
		    <ul class="navbar-nav navbar-right">
		    	<li class="nav-item" id="cartArea">
		    		<div id="cartListCount"><?php echo count($orderList); ?></div>
					<a class="nav-link" href="cart.php"><i class="fas fa-shopping-bag"></i> <?=$lang_cart_cart?></a>
					<div class="cartCheck">
						<div class="cartCheckBox">
						<?php  
							if(count($orderList) == 0){
						?>
							<div class="cartInfo text-center my-2"><?=$lang_cart_empty?></div>	
						<?php
							}else{
								foreach ($orderList as $key => $value) {
								$table = "product";	
								$productOrderShow = productQuery($table,$value["product_no"]);
								$specOrderShow = findSpecInfo($value["product_spec_no"]);
						?>
							<div class="orderListBox my-2">
								<div class="cartItem-img">
									<img src="<?php echo $productOrderShow['img1']; ?>" class="img-fluid">
								</div>
								<div class="cartItem-desc">
									<div class="cartItem-title">
										<a href="product-detail.php?product_no=<?=$productOrderShow["product_no"]?>"><?=$productOrderShow["product_name"]?></a>
										<?php 
											
										//print_r($productOrderShow);
										?>
	<!--
										<?php if(mb_strlen($productOrderShow["product_name"]) > 10){
											echo mb_substr($productOrderShow["product_name"],0,10,"UTF-8")."...";									
										}else{
											echo $productOrderShow["product_name"];
										}
										?>
	-->
									</div>
									<div class="cartItem-subtitle">
										<?=$productOrderShow["product_subtitle"]?>
	<!--
										<?php
										if(mb_strlen($specOrderShow[0]["product_spec_info"]) > 15){
											echo mb_substr($specOrderShow[0]["product_spec_info"],0,10,"UTF-8")."...";
										}else{
											echo $specOrderShow[0]["product_spec_info"];
										}
										?>
	-->
									</div>
									<div class="cartItem-specInfo">
										<?=$specOrderShow[0]["product_spec_info"];?>
									</div>
								</div>
							</div><!-- .orderListBox -->
								
						<?php				
								}// end foreach loop	
							} //---end if 判斷是否有購物車清單
						?>
							
						<button id="checkoutButton" type="button" class="btn btn-outline-success mt-1" style="width:100%;font-size: 12px; <?php if(count($orderList) == 0){echo 'display:none;' ;} ?>"><?=$lang_cart_checkout?></button>
						</div>
						
					</div>
				</li>
				<li class="nav-item" id="login" <?php if(isset($_SESSION["memNo"]) === true){echo 'style="display:none;"';} ?> >
					<a class="nav-link" ><i class="fas fa-user-circle"></i> <?=$lang_member_login?></a>
				</li>
				<li class="nav-item" id="memCenter" <?php if(isset($_SESSION["memNo"]) === false){echo 'style="display:none;"';} ?>>
					<a class="nav-link" ><i class="fas fa-user-circle mr-1"></i><span class="last-name mr-1"><?php if(isset($_SESSION["memNo"]) === true){echo $_SESSION["memLastName"];} ?></span><span class="first-name"><?php if(isset($_SESSION["memNo"]) === true){echo $_SESSION["memFirstName"] ;} ?></span></a>
					<div id="logoutPannelBox">
						<ul id="logoutPannel">
							<li><a href="memcenter.php"><?=$lang_member_center?></a></li>
							<li><a href="memhistory.php"><?=$lang_member_history?></a></li>
							<li <?php if(isset($_SESSION["memNo"]) === false){echo 'style="display:none;"';} ?>>
								<a id="logout"  href=""><?=$lang_member_logout?></a>
							</li>
						</ul>
					</div>
					
				</li>
				<!-- <li class="nav-item" id="logout" <?php //if(isset($_COOKIE["memNo"]) === false){echo 'style="display:none;"';} ?>>
					<a class="nav-link" ><?=$lang_member_logout?></a>
				</li>	 -->
		    </ul>
		  </div> <!-- end #mainNav -->
		<!-- ======桌機===== -->

		<!-- ======rwd===== -->
		<div class="nav-rwd">
			<ul>
				<li>
					<button id="cancelButton-rwd"><i class="fa fa-times" aria-hidden="true"></i></button>
				</li>
				<li>
					<form class="form-inline" id="searchForm-rwd" action="result.php" method="get">
						<input class="form-control form-control-sm  searchAll-rwd" type="text" placeholder="<?=$lang_menu_searchProduct?>" aria-label="Search" name="productSearch"><button id="searchAllButton-rwd"><i class="fas fa-search searchIcon"></i></button>

						<!-- 關鍵字 -->
						<?php if( count($tagRecommendResult) != 0 ){ ?>	
						<div class="recommendTag-area">
							<?php foreach ($tagRecommendResult as $key => $value) { ?>
					      	<span class="recommendTag-content"><a href="result.php?productSearch=<?php echo $value['tag_name']; ?>"><?php echo $value['tag_name']; ?></a></span>
					      	<?php } ?>
					    </div>
						<?php } ?>
					</form>
				</li>
				<li>
		        	<a href="vrLive.php"><?=$lang_menu_vrLive?></a>
					</li> 
				<li><a href="products.php"><?=$lang_menu_findProducts?></a></li>
				<li class="brandHoverTrigger-rwd">
					<a href=""><span class="mr-1"><?=$lang_menu_findBrand?></span><i class="fas fa-angle-left" aria-hidden="true"></i></a>	
				</li>
				<li class="brandHover-rwd">
					<div class="col-12">
						<div class="row">
						<?php foreach ($cateBrandUse as $key => $value){
							$brandImgNo = "brand".$key;
	                	?>
							<div class="col-6 col-sm-4 text-center p-2"><a href="brands.php?cate_no=<?php echo $value["cate_no"] ;?>&brand=<?php echo preg_replace('/\s(?=)/', '', $value['cate_name']); ?>"><?php echo $value["cate_name"]; ?></a></div>
						<?php
	                	}
	                	?>	
						</div>
					</div>
						
				</li>
				<li>
			        <a href="styles.php"><?=$lang_menu_style?></a>
			    </li>
			    <li>
			        <a href="activity.php"><?=$lang_menu_activity?></a>
			    </li>
					 
				<!-- <li>
		        	<a href="360index.php"><?=$lang_menu_vrLive?></a>
		      	</li> -->
				<?php if(isset($_SESSION["memNo"]) === false){ ?>
				<li><a id="login-rwd" href=""><?=$lang_member_login?></a></li>
				<?php }else{ ?>
				<li><a href="memcenter.php"><?=$lang_member_center?></a></li>
				<li><a href="memhistory.php"><?=$lang_member_history?></a></li>
				<li><a id="logout-rwd" href=""><?=$lang_member_logout?></a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="cart-rwd">
			<a id="cartListCheck-rwd" href="cart.php"><i class="fas fa-shopping-bag fa-2x"></i><div class="cart-count-rwd"><?php echo count($orderList); ?></div></a>
		</div>
		<div class="cartList-rwd">
			<button id="cancelCartButton-rwd"><i class="fa fa-times" aria-hidden="true"></i></button>
			<div class="p-2">
				<?php  
					if(count($orderList) == 0){
				?>
				<div class="cartInfo text-center my-2"><?=$lang_cart_empty?></div>	
				<?php
					}else{
						foreach ($orderList as $key => $value) {
						$table = "product";	
						$productOrderShow = productQuery($table,$value["product_no"]);
						$specOrderShow = findSpecInfo($value["product_spec_no"]);
				?>
					<div class="orderListBox-rwd my-1">
						<div class="cartItem-img-rwd">
							<img src="<?php echo $productOrderShow['img1']; ?>" class="img-fluid" height="20">
						</div>
						<div class="cartItem-desc-rwd">
							<div class="cartItem-title">
								<a href="product-detail.php?product_no=<?=$productOrderShow["product_no"]?>"><?=$productOrderShow["product_name"]?></a>
							</div>	
							<div class="cartItem-specInfo">
								<?=$specOrderShow[0]["product_spec_info"];?>
							</div>
						</div>	

					</div>	
				<?php		
					}
				}
				?>		
				<a role="button" href="cart.php" class="btn btn-outline-success mt-1" style="color:#fff;border-color:#f67f90;background-color:#f67f90;width:100%;font-size: 12px; <?php if(count($orderList) == 0){echo 'display:none;' ;} ?>"><?=$lang_cart_checkout?></a>
				</div>
			</div>
		</div>
		<!-- ======rwd===== -->


		</nav>
		</div><!-- 	container-fluid -->
  </header>

  <!-- Modal (登入)-->
	<div class="modal fade" id="loginBox" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	         <div class="loginTitle"><img src="image/frontend/EC-logo-white.png" class="img-fluid" style="width:100px;"></div>
	        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button> -->
	      </div>
	      <div class="modal-body text-center">
            	<form id="login-form" method="post">
	              <div class="form-group">
	                <label for="login-username" class="label-custom"><?=$lang_member_account?></label>
	                <input id="login-username" type="text" name="loginUsername" required="">
	                <div class="login-username-describe" style="display: none;"><?=$lang_member_account_hint?></div>
	              </div>
	              <div class="form-group">
	                <label for="login-password" class="label-custom"><?=$lang_member_password?></label>
	                <input id="login-password" type="password" name="loginPassword" required="">
	                <!-- <div class="login-password-describe" style="display: none;">請輸入英數碼六位數以上</div> -->
	              </div>
	              
	              <!-- This should be submit button but I replaced it with <a> for demo purposes-->
	            </form>
	            <button id="loginButton" href="#" class="btn btn-primary mb-2" style="display: block; width:100%;"><?=$lang_member_login?></button>
	            <!-- <div class="col-12 mb-2" style="color:#ccc;">or</div> -->
	            <button id="registerButton" href="#" class="btn btn-primary mb-4" style="display: block; width:100%;" ><?=$lang_member_register?></button>
	            <a href="#" id="forgot-pass" ><?=$lang_member_forgot?></a>
	            <!-- <small>Do not have an account? </small>
	            <a href="register.html" class="signup">Signup</a> -->
	      </div>
	     <!--  <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div> -->
	    </div>
	  </div>
	</div>


