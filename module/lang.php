<?php 
	$lang_menu_findProducts = "找商品";
	$lang_menu_findBrand = "找品牌";
	$lang_menu_style = "居風格";
	$lang_menu_activity = "特別活動";
	$lang_menu_vrLive = "居空間(VR商城)";
	$lang_menu_searchProduct = "搜尋商品";
	$lang_menu_home = "首頁";
	
	$lang_style_latest = "最新文章 - latest";
	$lang_style_index = "分類 - index";
	
	$lang_brands_other = "其他品牌";
	$lang_brands_brand = "品牌";
	$lang_brands_loved = "品牌精選<span>Brand We Loved</span>";
	
	$lang_cancel = "取消";
	$lang_save = "儲存";
	$lang_delete = "刪除";
	$lang_confirm = "確認";
	$lang_and = "和";
	
	$lang_product_hot = "人氣商品";
	$lang_product_popular = "熱門商品";
	$lang_product_popular2 = "What's popular";
	$lang_product_recommand = "推薦商品";
	$lang_product_recommand_new = "推薦新品<span>New Arrival</span>";
	$lang_product_filter_price = "依價格篩選";
	$lang_product_filter_price_1 = "500元以下";
	$lang_product_filter_price_2 = "501~1000元";
	$lang_product_filter_price_3 = "1001~2000元";
	$lang_product_filter_price_4 = "2001~3000元";
	$lang_product_filter_price_5 = "3001元以上";
	$lang_product_notforsale = "<h2>Oh No!</h2> <h4>產品已經下架</h4>";
	$lang_product_notfound = "<strong>OH NO!</strong> 這個篩選沒有商品！再試試其他選擇吧";
	$lang_product_sort_time = "最新商品";
	$lang_product_sort_pricetop = "價格由高到低";
	$lang_product_sort_pricelow = "價格由低到高";
	$lang_product_oldprice = "原價";
	$lang_product_currency = "NT.";
	$lang_product_install_price = "3 期 0 利率";
	$lang_product_install_currency = "NTD";
	$lang_product_install_note = "結帳總金額 3,000 元以上可分 6 期 0 利率，10,000 元以上可分 12 期 0 利率。";
	$lang_product_install_note_title = "合作銀行";
	$lang_product_install_note_banks = "中國信託 / 台新 / 玉山 / 富邦 / 遠東 / 永豐 / 國泰世華 / 華南 / 日盛 / 樂天 / 安泰 / 聯邦 / 兆豐 / 台中商銀 / 上海銀行 / 凱基 / 匯豐 / 星展 / 新光 / 合庫 / 彰銀 / 一銀 / 元大 / 陽信 / 台灣企銀 / 華泰 / 三信商銀";

	$lang_search_title = "搜尋";
	$lang_search_match = "符合搜尋條件的商品";
	$lang_search_notfound = "找不到商品!!!";
	
	$lang_cart_empty = "無購物清單";
	$lang_cart_empty2 = "沒有購物清單";
	$lang_cart_cart = "購物袋";
	$lang_cart_cart_list = "購物清單";
	$lang_cart_addcart = "加入購物車";
	$lang_cart_addbuy = "直接購買";
	$lang_cart_mycart = "我的購物袋";
	$lang_cart_checkout = "前往結帳";
	$lang_cart_payway = "付款方式";
	$lang_cart_paymentinfo = "結帳資訊";
	$lang_cart_paymentinfo2 = "付款資料";
	$lang_cart_order_info = "訂單資訊";
	$lang_cart_order_info2 = "訂購資訊";
	$lang_cart_memo = "備註";
	$lang_cart_memo_placholder = "居生活為響應環保政策，全面使用電子發票，中獎發票將於中獎公布後寄出。\n若另需紙本發票請於此填寫＂紙本發票＂。居生活將於您收到產品後七日內寄出。";
	$lang_cart_account = "Email 信箱";
	$lang_cart_search_hint = "請輸入訂單編號";
	$lang_cart_item = "品項";
	$lang_cart_product = "商品名稱";
	$lang_cart_brand = "品牌";
	$lang_cart_spec = "規格";
	$lang_cart_spec2 = "商品規格";
	$lang_cart_qty = "數量";
	$lang_cart_transfee = "運費";
	$lang_cart_notransfee = "免運";
	$lang_cart_coupon = "優惠劵";
	$lang_cart_couponcode = "優惠劵代碼";
	$lang_cart_sum = "小計";
	$lang_cart_calc = "金額計算";
	$lang_cart_total = "總金額";
	$lang_cart_gopay = "前往結帳";
	$lang_cart_address = "配送地址";
	$lang_cart_invoice_einvoice = "電子發票";
	$lang_cart_invoice_cominvoice = "公司發票";
	$lang_cart_invoice_cominvoice_no = "統一編號";
	$lang_cart_invoice_cominvoice_title = "發票抬頭";
	$lang_cart_invoice_address = "紙本發票寄送地址";
	$lang_cart_invoice_address_same = "發票寄送地址同商品配送地址";
	$lang_cart_invoice_infoemail = "通知信箱";
	$lang_cart_order_time = "下單時間";
	$lang_cart_order_supply = "供應商";
	$lang_cart_order_unitprice = "單價";
	$lang_cart_order_fee = "費用";
	$lang_cart_order_price = "訂單金額";
	$lang_cart_first_buy = "首次購物";
	$lang_cart_first_join = "首次購物後即可加入TGI Live會員";
	$lang_cart_first_join_hint = "設定密碼可日後交易完成後，查詢訂單記錄喔";
	$lang_cart_choose_payby = "請選擇付款方式";
	$lang_cart_choose_payby_cc = "信用卡全額付清";
	$lang_cart_choose_payby_cc_installment = "信用卡分期(可選擇期數)";
	$lang_cart_choose_payby_cc_installment_choose = "選擇期數";
	$lang_cart_choose_payby_cc_installment_3 = "3 期 0 利率，每期";
	$lang_cart_choose_payby_cc_installment_6 = "6 期 0 利率，每期";
	$lang_cart_choose_payby_cc_installment_12 = "12 期 0 利率，每期";
	$lang_cart_choose_payby_cc_installment_title = "合作銀行";
	$lang_cart_choose_payby_cc_installment_banks = "中國信託 / 台新 / 玉山 / 富邦 / 遠東 / 永豐 / 國泰世華 / 華南 / 日盛 / 樂天 / 安泰 / 聯邦 / 兆豐 / 台中商銀 / 上海銀行 / 凱基 / 匯豐 / 星展 / 新光 / 合庫 / 彰銀 / 一銀 / 元大 / 陽信 / 台灣企銀 / 華泰 / 三信商銀";
	$lang_cart_choose_payby_atm = "現金匯款";
	$lang_cart_choose_payby_store = "超商付款(7-11、全家、OK、萊爾富)";
	$lang_cart_choose_payby_store_711 = "7-11";
	$lang_cart_choose_payby_store_family = "全家";
	$lang_cart_choose_payby_store_hilife = "萊爾富";
	$lang_cart_choose_payby_store_OK = "OK";
	$lang_cart_choose_invoice = "請選擇發票種類";
	$lang_cart_first_go = "首次Go";
	$lang_cart_order_status = "訂單狀態";
	$lang_cart_order_detail = "訂單內容";
	$lang_cart_payment_done = "完成付款";
	$lang_cart_fill_form = "填寫訂購資料";
	$lang_cart_confirm_buy = "確定購買";
	$lang_paymentinfo_cvc = "超商代碼";
	$lang_paymentinfo_bank = "匯款銀行代碼";
	$lang_paymentinfo_atm = "匯款帳號";
	$lang_cart_order_create = "訂單成立";
	$lang_cart_order_create_thank_1 = "感謝您對TGiLive居生活的支持，您的訂單";
	$lang_cart_order_create_thank_2 = "已經成立，消費金額總計 NT.";
	$lang_cart_order_create_thank_3 = "請於3天內前往超商繳款，我們將會盡快處理您的訂單。";
	$lang_cart_order_create_paycode_get = "取得繳款代碼";
	$lang_cart_order_create_paycode_hint = "您的繳款代碼";
	$lang_cart_order_create_paycode_work = "超商代碼機台操作";
	$lang_cart_order_create_paycode_work_step_711_1 = "請選擇代碼輸入";
	$lang_cart_order_create_paycode_work_step_711_2 = "輸入LAC開頭之繳款代碼共14碼(含英文)";
	$lang_cart_order_create_paycode_work_step_711_3 = "確認繳費項目並列印";
	$lang_cart_order_create_paycode_work_step_711_4 = "請在三小時內繳費完畢";
	$lang_cart_order_create_paycode_work_step_family_1 = "選擇右上角：繳費";
	$lang_cart_order_create_paycode_work_step_family_2 = "選擇：代碼繳費後同意服務條款";
	$lang_cart_order_create_paycode_work_step_family_3 = "輸入LAC開頭之繳款代碼共14碼(含英文)";
	$lang_cart_order_create_paycode_work_step_family_4 = "確認繳費項目並列印";
	$lang_cart_order_create_paycode_work_step_family_5 = "請在3小時內繳費完畢";
	$lang_cart_order_create_paycode_work_step_hilife_1 = "選則左二：代收繳款";
	$lang_cart_order_create_paycode_work_step_hilife_2 = "選擇左四：網路交易";
	$lang_cart_order_create_paycode_work_step_hilife_3 = "選擇：右下紅陽科技後同意服務條款";
	$lang_cart_order_create_paycode_work_step_hilife_4 = "輸入LAC開頭之繳款代碼共14碼(含英文)";
	$lang_cart_order_create_paycode_work_step_hilife_5 = "確認繳費項目並列印";
	$lang_cart_order_create_paycode_work_step_hilife_6 = "請在3小時內繳費完畢";
	$lang_cart_order_create_paycode_work_step_OK_1 = "選則左上角：繳費";
	$lang_cart_order_create_paycode_work_step_OK_2 = "選擇左三：網路交易";
	$lang_cart_order_create_paycode_work_step_OK_3 = "選擇：代碼繳費後同意服務條款";
	$lang_cart_order_create_paycode_work_step_OK_4 = "輸入LAC開頭之繳款代碼共14碼(含英文)";
	$lang_cart_order_create_paycode_work_step_OK_5 = "確認繳費項目並列印";
	$lang_cart_order_create_paycode_work_step_OK_6 = "請在3小時內繳費完畢";

	$lang_cart_order_complete = "完成訂購";
	$lang_cart_order_complete_1 = "感謝您對TGiLive居生活的支持，您的訂單";
	$lang_cart_order_complete_2 = "已經付款成功，消費金額總計 NT.";
	$lang_cart_order_complete_3 = "我們將會盡快處理您的訂單。";
	$lang_cart_order_success = "交易成功";
	$lang_cart_order_fail = "交易失敗";
	$lang_cart_check_order = "查看訂單";
	$lang_cart_agree = "我同意接受TGILive";

	$lang_member_totalrecord_1 = "總共";
	$lang_member_totalrecord_2 = "筆訂單紀錄";
	$lang_member_account = "帳號  <small>Account</small>";
	$lang_member_account2 = "帳號(E-mail)";
	$lang_member_account3 = "帳號";
	$lang_member_account_hint = "請輸入E-mail";
	$lang_member_password = "密碼  <small>Password</small>";
	$lang_member_password2 = "密碼";
	$lang_member_newpassword = "新密碼";
	$lang_member_newpassword_hint = "請輸入您的新密碼(英數六位碼以上)";
	$lang_member_repassword = "密碼確認";
	$lang_member_reset_password = "重設密碼";
	$lang_member_reset_password2 = "修改密碼";
	$lang_member_edit_info = "修改資料";
	$lang_member_password_hint = "請輸入英數六碼";
	$lang_member_password_hint2 = "請輸入您的密碼";
	$lang_member_repassword_hint = "請再次輸入您的密碼";
	$lang_member_center = "會員中心";
	$lang_member_info = "會員資料";
	$lang_member_history = "訂單紀錄";
	$lang_member_history2 = "歷史訂單";
	$lang_member_nohistory = "找不到訂單紀錄";
	$lang_member_nohistory2 = "沒有訂單紀錄";
	$lang_member_login = "登入";
	$lang_member_login2 = "會員登入";
	$lang_member_register = "註冊";
	$lang_member_register2 = "會員註冊";
	$lang_member_logout = "登出";
	$lang_member_forgot = "Forgot Password?";
	$lang_member_forgot2 = "Forgot Password?";
	$lang_member_fullname = "會員姓名";
	$lang_member_firstname = "名字";
	$lang_member_lastname = "姓氏";
	$lang_member_receiver = "收件人";
	$lang_member_receiver_firstname = "收件人名字";
	$lang_member_receiver_lastname = "收件人姓氏";
	$lang_member_receiver_fullname = "收件人姓名";
	$lang_member_phone = "電話";
	$lang_member_phone2 = "聯絡電話";
	$lang_member_phone_hint = "09xxxxxxxx";
	$lang_member_address = "地址";
	$lang_member_address2 = "收件地址";
	$lang_member_address_hint = "請輸入地址";
	$lang_member_goshop = "前往購物";
	$lang_member_goshop2 = "繼續購物";
	$lang_member_order_status = "訂單編號";
	$lang_member_order_status_0 = "尚未付款";
	$lang_member_order_status_1 = "收款確認";
	$lang_member_order_status_2 = "出貨";
	$lang_member_order_status_3 = "退貨";
	$lang_member_order_status_4 = "取消";
	$lang_member_order_status_5 = "完成";

	$lang_footer_copyright_1 = "THANK GOD IT'S LIVE";
	$lang_footer_copyright_2 = "Copyright TGILive &copy; 2016, All Rights Reserved";
	$lang_footer_marketing = "居行銷";
	$lang_footer_newbie = "首次使用";
	$lang_footer_faq_member = "會員問題";
	$lang_footer_faq_eliver = "運送問題";
	$lang_footer_faq_returns = "退貨問題";
	$lang_footer_payinfo = "付款方式";
	$lang_footer_terms = "TGI服務條款";
	$lang_footer_terms_2 = "服務條款";
	$lang_footer_privacy = "隱私權政策";
	$lang_footer_contacts = "聯絡我們";

	$lang_360_main = "360主頁";
	$lang_360_partner_brands = "實境商城<span>Reality Mall</span>";
	$lang_360_main_products = "主打商品";
	$lang_360_main_products2 = "Primary products";
	$lang_360_relate_brands = "相關店鋪";
	$lang_360_link = "360 店舖"
?>