<?php
require_once("model/DB.php"); 

//UTF-8編碼
header('Content-Type: text/html; charset=utf-8');

//-------------------------------------------
//  商家端交易授權傳送程式demo（超商代收（代碼）交易）
//
//  運作原理：Form POST Method(HTTPS)
//  付款流程：消費者付款完成->紅陽系統->paid.php（本程式）
//  參數值說明請參見技術文件
//-------------------------------------------

//設定（※請修改）
$merchantID = 'S1702150465'; //商家代號（超商代收（代碼））（可登入商家專區至「服務設定」中查詢paycode服務的代碼）
$transPassword = 'tgilive52415016'; //交易密碼（可登入商家專區至「密碼修改」處設定，此密碼非後台登入密碼）

//回傳參數值
$buysafeno = getPostData('buysafeno'); //紅陽交易編號
$web = getPostData('web'); //商家代號
if ($merchantID != $web) {
    exit('商家代號錯誤');
}
$UserNo = getPostData('UserNo'); //用戶編號
$Td = getPostData('Td'); //商家訂單編號
$MN = getPostData('MN'); //交易金額
$note1 = urldecode(getPostData('note1')); //備註1
$note2 = urldecode(getPostData('note2')); //備註2
$PayDate = getPostData('PayDate'); //繳款日期(YYYYMMDD)
$PayType = getPostData('PayType'); //繳款方式，4：全家超商繳款、5：統一超商繳款、6：OK 超商繳款、7：萊爾富超商繳款
$PayAgency = getPostData('PayAgency'); //超商門市代碼
$PayAgencyName = getPostData('PayAgency'); //超商門市名稱
$PayAgencyTel = getPostData('PayAgencyTel'); //超商門市代碼
$PayAgencyAddress = getPostData('PayAgencyAddress'); //超商門市地址
$errcode = getPostData('errcode'); //交易回覆代碼
$errmsg = urldecode(getPostData('errmsg')); //錯誤訊息（交易成功為空字串）
$InvoiceNo = getPostData('InvoiceNo'); //發票號碼
$CargoNo = getPostData('CargoNo'); //交貨便代碼
$StoreID = getPostData('StoreID'); //取貨門市店號
$StoreName = urldecode(getPostData('StoreName')); //取貨門市店名
$ChkValue = getPostData('ChkValue'); //交易檢查碼（SHA1雜湊值）

//自行設計更新資料庫訂單狀態
if (!empty($errcode) && $ChkValue == getChkValue($web . $transPassword . $buysafeno . $MN . $errcode . $CargoNo)) {
    if ($errcode == '00') {
        //付款成功
        //寫入訂單為金額已付款
        $db = new DB();
        $table = "order_item";
        
        // $adminMemo = urldecode($_REQUEST["admin_memo"]);

        $checkColumn = array("order_group");
        $data = array(
            "order_group" => $Td ,
            "order_pay_status" => 1,
            "order_status" => 1
            );
        // print_r($item);
        $db -> DB_UpdateOnly($table,$data,$checkColumn);
    } else {
        //付款失敗
    }
    exit('0000');
} else {
    exit('交易檢查碼錯誤');
}

function getPostData($key)
{
    return array_key_exists($key, $_POST) ? $_POST[$key] : '';
}

/**
 * 檢查交易檢查碼是否正確（SHA1雜湊值）
 */
function getChkValue($string)
{
    return strtoupper(sha1($string));
}




?>