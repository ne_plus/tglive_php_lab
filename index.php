<?php require_once("module/header.php"); ?>
<?php //require_once("controller/productControl.php"); 


// ---供應商品牌---
$brandSelect ="SELECT * from brand_select";
$cateNoResult =  $db->DB_Query($brandSelect);
// $cateNo = 166 ;	
$cateNo = $cateNoResult[0]['cate_no'] ;
$brandIdProducts = get_brandId_queryProducts($cateNo);
// -----找品牌資訊----
$brandInfoSql = "SELECT * from category where cate_no = $cateNo";
$brandInfoResult = $db -> DB_Query($brandInfoSql);

// -----本月推薦----
$acitvityNo = 90; //cateNo
$activityProducts = get_activityId_queryProducts($acitvityNo,12);

// ----特色商品---
$popularCateNo = 94; //cateNo
$popularProducts = get_activityId_queryProducts($popularCateNo,null);

//banner 更換
$now =  time();//date("Y-m-d H:i:s");
$bannerSql = "SELECT * from banner where banner_status = 1 order by banner_no asc";
$banner = $db -> DB_Query($bannerSql);

?>
  <section>
  	<div class="container-fluid marginBottom60 marginTop100 marginTop80-rwd">
  		<div class="row">
  			<div id="myCarousel" class="carousel slide" data-ride="carousel" >
		        <ol class="carousel-indicators">
				<?php
					foreach ($banner as $key => $value) {
				?>
					<li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="<?php if( $key == 0 ){echo 'active';} ?>"></li>
				<?php		
					}
				?>
				<!-- old -->
		          <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		          <li data-target="#myCarousel" data-slide-to="1"></li>
		          <li data-target="#myCarousel" data-slide-to="2"></li>
		          <li data-target="#myCarousel" data-slide-to="3"></li>
		          <li data-target="#myCarousel" data-slide-to="4"></li>
		          <li data-target="#myCarousel" data-slide-to="5"></li> -->
		        </ol>
		        <div class="carousel-inner">
				<?php
					foreach ($banner as $key => $value) {
				?>
				 <div class="carousel-item <?php if( $key == 0 ){echo 'active';} ?>">
		            <a href="<?php echo $value["link"]; ?>" <?php if($value["blank"]){echo "target=\"_blank\""; }?>><img src="<?php echo $value["img"]; ?>" ></a>
		          </div>
				<?php
					}
				?>
				<!-- old	 -->
		          <!-- <div class="carousel-item active">
		            <a href="https://www.tgilive.com/activity.php?cate_no=182" ><img class="first-slide" src="image/frontend/banner/indexBanner-01.jpg" alt="First slide"></a>
		          </div>
		          <div class="carousel-item">
		           <a href="https://www.tgilive.com/activity.php?cate_no=178"><img class="second-slide" src="image/frontend/banner/indexBanner-02.jpg" alt="Second slide"></a>
		          </div>
		          <div class="carousel-item">
		           <a href="https://www.tgilive.com/brands.php?cate_no=108&brand=OTOTO"><img class="third-slide" src="image/frontend/banner/indexBanner-03.jpg" alt="Third slide"></a>
		          </div>
		           <div class="carousel-item">
		          <a href="https://www.tgilive.com/brands.php?cate_no=156&brand=Le'Bloom"><img class="fourth-slided" src="image/frontend/banner/indexBanner-04.jpg" alt="Fourth slide"></a>
		          </div>
		           <div class="carousel-item">
		           <a href="https://www.tgilive.com/brands.php?cate_no=166&brand=木木研室"><img class="fifth-slide" src="image/frontend/banner/indexBanner-05.jpg" alt="Fifth slide"></a>
		          </div>
		          <div class="carousel-item">
		            <a href="https://www.tgilive.com/brands.php?cate_no=130&brand=FUcoi藻安美肌"><img class="sixth-slide" src="image/frontend/banner/indexBanner-06.jpg" alt="Fifth slide"></a>
		          </div> -->
		        </div>
		        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
		          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		          <span class="sr-only">Previous</span>
		        </a>
		        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
		          <span class="carousel-control-next-icon" aria-hidden="true"></span>
		          <span class="sr-only">Next</span>
		        </a>
		      </div>
  		</div>	
  	</div>
  		
  </section>

  <section>
	<div class="container">
				<div class="row" >
					<div class="v-signs">
						<?=$lang_product_recommand_new?>
					</div>
						<?php  
						
							foreach ($activityProducts as $key => $value){
						?>
							<div class="recommend product-items">
							  	<form action="product-detail.php" method="get">
							  		<div class="content-hover">							  		
								  		<img src="<?php echo $value['img1']; ?>" class="img-fluid">
								  		<div class="block">
							  				<div class="info text-center d-flex align-items-center justify-content-center">
							  					<div class="product-name" style="font-size: 14px;"><?php echo $value["product_name"]; ?></div>			
							  				</div>
							  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
							  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
							  				</div>
							  			</div>	
							  			<div class="price text-center">
						  					<!-- PRICE只抓第一筆資料 -->
												<?php  
													if($value['product_spec_price_discount'][0] == null ){
														// echo "沒有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
												<?php		
													}else{
														// echo "有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div>
												<?php		
													}
												?>	
						  				</div>
								  	</div>
								  	<input type="hidden" name="product_no" value="<?php echo $value["product_no"]; ?>">
							  	</form>	
							  </div>
						<?php
							} 
						?>

				</div><!-- row -->
			</div><!-- container -->
	

  </section>
  
  <section>
  	<div class="container">
  		<div class="row">
	  			<div class="v-signs">
						<?=$lang_brands_loved?>
				</div>
 
					<div class="brand-block col-12 col-sm-4">
							<div class="content">
								<div class="brand-logo">
									<?php
										$img_url = trim(preg_replace('/\s\s+/', ' ', $brandInfoResult[0]['cate_logo_img']));
										
									?>
									<div class="brand-logo-content" style="background: #ffffff url('<?=$img_url?>')  no-repeat center center; background-size: 80% 80% ">
									</div>
								</div>
								<div class="brand-describe text-left">
									<p class='font14 p-3'><?php echo $brandInfoResult[0]["cate_describe"] ;?></p>
								</div>
							</div>
					</div>
					
					<div class="col-12 col-sm-8 brand-products">
						<div class="row">		
					<?php
							
							for($b=0;$b<8;$b++){
								if ($brandIdProducts[$b]){
								?>
								<div class="product-items">
							  	<form action="product-detail.php" action="get">
									<div class="content-hover">
										<img src="<?php echo $brandIdProducts[$b]['img1']; ?>" class="img-fluid">
										<div class="block">
							  				<div class="info text-center d-flex align-items-center justify-content-center">
							  					<div class="product-name" style="font-size: 14px;"><?php echo $brandIdProducts[$b]["product_name"]; ?></div>			
							  				</div>
							  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
							  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
							  				</div>
							  			</div>	
							  			<div class="price text-center">
						  					<!-- PRICE只抓第一筆資料 -->
												<?php  
													if($brandIdProducts[$b]['product_spec_price_discount'][0] == null ){
														// echo "沒有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$brandIdProducts[$b]['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
												<?php		
													}else{
														// echo "有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$brandIdProducts[$b]['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$brandIdProducts[$b]['product_spec_price_old'][0]; ?></div>
												<?php		
													}
												?>	
						  				</div>	
									</div>
									<input type="hidden" name="product_no" value="<?php echo $brandIdProducts[$b]['product_no'] ;?>">
								</form>
						  	  </div>	
								<?php
								}
							}
					?>

  		</div><!-- row -->
  	</div><!-- container -->
  </section>	

  <section>
  	<div class="container feature">
  		<div class="row">
			<div class="col-12 header">
				<h4><?=$lang_product_popular?></h4>
				<small><?=$lang_product_popular2?></small>
			</div>
				<?php  foreach ($popularProducts as $key => $value) { ?>
							<div class="product-items">
							  	<form action="product-detail.php" method="get">
							  		<div class="content-hover">							  		
								  		<img src="<?php echo $value['img1']; ?>" class="img-fluid">
								  		<div class="block">
							  				<div class="info text-center d-flex align-items-center justify-content-center">
							  					<div class="product-name" style="font-size: 14px;"><?php echo $value["product_name"]; ?></div>			
							  				</div>
							  				<div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="<?=$lang_cart_addcart?>">
							  					<img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
							  				</div>
							  			</div>	
							  			<div class="price text-center">
						  					<!-- PRICE只抓第一筆資料 -->
												<?php  
													if($value['product_spec_price_discount'][0] == null ){
														// echo "沒有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"></div>
												<?php		
													}else{
														// echo "有折扣";
												?>
												<div class="price-new mr-1" style="font-size: 16px;color:red;"><?php echo "NT ".$value['product_spec_price_discount'][0]; ?></div><div class="price-old" style="text-decoration: line-through; color:#000;"><?php echo "NT ".$value['product_spec_price_old'][0]; ?></div>
												<?php		
													}
												?>	
						  				</div>
								  	</div>
								  	<input type="hidden" name="product_no" value="<?php echo $value["product_no"]; ?>">
							  	</form>	
							  </div>
						<?php
								} 
							// }
						?>
		
			
	</div><!-- row -->
  	</div><!-- container -->
  </section>

<?php require_once("module/footer.php"); ?>