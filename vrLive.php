<?php
require_once("module/header.php"); 
include("class/categoryClass.php");
include("class/productClass.php");

// 搜尋360品牌
$query360 = $db->DB_Query("SELECT * from category where cate_360_status = 1 and cate_level = 2 order by cate_updatetime");
$query360primary = $db->DB_Query("SELECT * from category where cate_360_status = 1 and cate_360_main = 1 and cate_level = 2");

// 主打品牌ID
$primary360ID = 0;
if (count($query360primary) == 0) {
    $primary360ID = 0 ;// 沒有設定
} else {
    $primary360ID = $query360primary[0]['cate_no'] ;
}

// 主打商品連結
$query360primaryLink = '';
if (count($query360primary) != 0 ){
    $query360primaryLink = $query360primary[0]['cate_360_link'];
}

// 主打品牌商品
$cate = new category($primary360ID);
$result = $cate->get360ProductsByCateId($primary360ID,null ,$productStatus=1 ,$product360Status=1, $page=null , $isPagenation=null , $recPerPage=null);
$productCount = isset($result[0]['products'])? count($result[0]["products"]) : 0 ;
// echo "<pre>";
// print_r($cate->get360ProductsByCateId($primary360ID,null ,$productStatus=1 ,$product360Status=1, $page=null , $isPagenation=null , $recPerPage=null));
// echo "</pre>";
?>

<section class="vrlive marginTop100">
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php"><?=$lang_menu_home?></a></li>
    <li class="breadcrumb-item active"><?=$lang_menu_vrLive?></li>	 
</ol>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="vrlive-area">
                <iframe width="100%" height="100%" src="<?=$query360primaryLink?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>	
    
<div class="container mt-5">
    <div class="row">
        <div class="v-signs"><?=$lang_360_partner_brands?></div>

        <?php
            foreach($query360 as $key => $value) {
               if ($key <= 11){
        ?>
        <div class="recommend product-items">
            <form action="360brand.php" method="get" target="_blank">
                <div class="content-hover">
                    <img src="<?=$value['cate_logo_img']?>" class="img-fluid">
                    <div class="block" style="background-color: rgba(255,200,207,.7);">
                        <div class="info text-center d-flex align-items-center justify-content-center">
                            <div class="product-name" style="font-size: 14px;"><?=$value['cate_name']?></div>
                        </div>
                        <div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
                            <img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
                        </div>
                    </div>
                    <!-- <div class="price text-center">
                        <div class="price-new mr-1" style="font-size: 16px; color: red;">NT80</div><div class="price-old" style="text-decoration: line-through; color: rgb(0, 0, 0);">NT100</div>		
                    </div> -->
                </div>
                <input type="hidden" name="cate_no" value="<?=$value["cate_no"]?>">
                <input type="hidden" name="brand" value="<?=$value['cate_name']?>">
            </form>	
        </div>

        <?php
               } // end if
        } // end foreach
        ?>
        
    </div>
</div>

<div class="container mt-5">
    <div class="row feature">
        <div class="col-12 header">
            <h4><?=$lang_360_main_products?></h4>
            <small><?=$lang_360_main_products2?></small>
        </div>
    </div>
    
    <div class="row">

    <?php  
    if ($productCount != 0 ) {
        foreach ($result as $key => $value) {
    ?>
    <?php  
        for ($i=0; $i < $productCount; $i++) { 
            $priceTop = "NT";
            $priceLow = "NT";	
            if( trim($value["products"][$i]["lowPrice"]) == null ){
                $priceLow .= $value["products"][$i]["highPrice"];
                $priceTop = "";
            }else{
                $priceLow .= $value["products"][$i]["lowPrice"];
                $priceTop .= $value["products"][$i]["highPrice"];
            }		
    ?>	

        <div class="col-6 col-md-4 col-lg-2">
            <div class="row">
                <div class="product-items">
                    <form action="product-detail.php" method="get">
                    <div class="content-hover">
                        <img src="<?php echo $value['products'][$i]['product_img'][0]; ?>" class="img-fluid">
                        <div class="block">
                            <div class="info text-center d-flex align-items-center justify-content-center">
                                <div class="product-name" style="font-size: 14px;"><?php echo $value["products"][$i]["product_name"]; ?></div>
                            </div>
                            <div class="add-cart text-center" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$lang_cart_addcart?>">
                                <img height="20" src="image/frontend/shopping-white.png" title="<?=$lang_cart_addcart?>">
                            </div>
                        </div>
                        <div class="price text-center">
                            
                            <div class="price-new mr-1" style="font-size: 16px; color: red;"><?php echo $priceLow; ?></div><div class="price-old" style="text-decoration: line-through; color: rgb(0, 0, 0);"><?php echo $priceTop; ?></div>		
                        </div>
                    </div>
                    <input type="hidden" name="product_no" value="<?php echo $value["products"][$i]["product_no"]; ?>">
                    </form>
                </div>	
            </div>	
        </div>

    <?php 
            } //--end for
        } // --end foreach
    } // --end if   $productCount 
    ?>


    </div>
</div>
</section>

<?php require_once("module/footer.php"); ?>